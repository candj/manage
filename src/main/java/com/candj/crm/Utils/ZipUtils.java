package com.candj.crm.Utils;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

import java.io.*;

/**
 *ファイルをZIPに圧縮する為のツールクラス
 *
 * @version 1.0.0
 */
public class ZipUtils {

	private static final int BUFFER = 2 * 1024;

	public static void compress(File file, OutputStream out) throws IOException {

		ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(out));
		try {
			zipOut.setEncoding("MS932");
			if (file.isDirectory()) {
				compressDirectory(file, zipOut, "");
			} else {
				compressFile(file, zipOut, "");
			}

		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		} finally {
			if (null != zipOut) {
				zipOut.close();
			}

		}
	}

	/** 压缩一个目录 */
	private static void compressDirectory(File dir, ZipOutputStream zipOut, String baseDir) throws IOException {
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			compressFile(files[i], zipOut, baseDir + dir.getName() + "/");
		}
	}

	/** 压缩一个文件 */
	private static void compressFile(File file, ZipOutputStream zipOut, String baseDir) throws IOException {
		if (!file.exists()) {
			return;
		}

		BufferedInputStream bis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(file));
			ZipEntry entry = new ZipEntry(baseDir + file.getName());
			zipOut.putNextEntry(entry);
			int count;
			byte data[] = new byte[BUFFER];
			while ((count = bis.read(data, 0, BUFFER)) != -1) {
				zipOut.write(data, 0, count);
			}

		} finally {
			if (null != bis) {
				bis.close();
			}
		}
	}

}
