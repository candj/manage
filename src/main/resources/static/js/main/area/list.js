

			var colNames = [
				"地区ID",
				"地区名",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "areaId",
					index: "areaId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "areaName",
					index: "areaName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	