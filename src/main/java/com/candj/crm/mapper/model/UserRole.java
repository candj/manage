package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザーロール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserRole {
    /** ID */
    private Integer userRoleId;

    /** ユーザー */
    private AppUser appUser;

    /** ユーザーID */
    private Integer userId;

    /** ロール */
    private Role role;

    /** ロールID */
    private Integer roleId;

    /** ステータス */
    private Byte userRoleStatus;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
