package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.service.OrderingService;
import com.candj.crm.service.WorkService;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class WorkOrderingSelectRestController {

    @Autowired
    WorkService workService;

    @Autowired
    OrderingService orderingService;

    /**
     * 注文一覧画面を表示する。
     * @param userId 注文
     * @return 結果
     */
    @RequestMapping(value = "work/insert/selectOrderByUser", method = RequestMethod.POST)
    public Object selectOrderByUserInsert(Integer userId, YearMonth yearMonth) throws Exception{
        MonthDto monthDto = new MonthDto();
        //条件で注文を検索する。（連携情報含む）
        monthDto.setUserId(userId);
        DateTime startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);;
        monthDto.setStartOfMonth(startOfMonth);
        monthDto.setEndOfMonth(startOfMonth.plusMonths(1).minusSeconds(1));
        List<OrderingDto> orderingDtoList = orderingService.selectByUserId(monthDto).stream().collect(Collectors.toList());
        return orderingDtoList;
    }

    /**
     * 注文一覧画面を表示する。
     * @param userId 注文
     * @return 結果
     */
    @RequestMapping(value = "work/edit/selectOrderByUser", method = RequestMethod.POST)
    public Object selectOrderByUserEdit(Integer userId, YearMonth yearMonth) throws Exception{
        MonthDto monthDto = new MonthDto();
        //条件で注文を検索する。（連携情報含む）
        monthDto.setUserId(userId);
        DateTime startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);;
        monthDto.setStartOfMonth(startOfMonth);
        monthDto.setEndOfMonth(startOfMonth.plusMonths(1).minusSeconds(1));
        List<OrderingDto> orderingDtoList = orderingService.selectByUserId(monthDto).stream().collect(Collectors.toList());
        return orderingDtoList;
    }
}
