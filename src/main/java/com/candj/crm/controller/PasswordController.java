package com.candj.crm.controller;

import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.ChangePassword;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * パスワードの変更。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/password")
@Controller
public class PasswordController {

    @Autowired
    AppUserService passwordService;

    /**
     * ユーザー一覧画面を表示する。
     *
     * @param changePassword ユーザー
     * @param model          モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("changePassword") ChangePassword changePassword, ModelMap model) {
        return new ModelAndView("password/list", model);
    }


}
