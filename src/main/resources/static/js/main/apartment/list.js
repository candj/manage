

			var colNames = [
				"マンションID",
				"建物ID",
				"总楼层数",
				"地下楼层数",
				"总户数",
				"停车场",
				"管理人",
				"管理形態",
				"管理组",
				"管理公司",
				"ペット可",
				"中古",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "apartmentId",
					index: "apartmentId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "apartmentBulidingId",
					index: "apartmentBulidingId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "allFloorNumber",
					index: "allFloorNumber",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "underFloorNumber",
					index: "underFloorNumber",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "unitsNumber",
					index: "unitsNumber",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "parkingLot",
					index: "parkingLot",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "managePeople",
					index: "managePeople",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "manageMethod",
					index: "manageMethod",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "manageGroup",
					index: "manageGroup",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "manageCompany",
					index: "manageCompany",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "pet",
					index: "pet",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "apartmentUsed",
					index: "apartmentUsed",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	