package com.candj.crm.dao.mapper;

import com.candj.crm.dto.MonthDto;
import com.candj.crm.mapper.model.Order;
import com.candj.webpower.web.core.model.WhereCondition;

import java.time.Month;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 注文情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface OrderDao {
    /**
     * プライマリーキーで注文を検索する。
     * @param orderId 注文ID
     * @return 結果
     */
    Order selectByPrimaryKey(Integer orderId);

    /**
     * プライマリーキーで注文検索する。（連携情報含む）
     * @param orderId 注文ID
     * @return 結果
     */
    List<Order> selectAllByPrimaryKey(Integer orderId);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Order> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Order> selectByExample(@Param("example") WhereCondition example);

    /**
     * 注文を新規追加する。
     * @param record 注文
     * @return 結果
     */
    int insertSelective(Order record);

    /**
     * 注文を新規追加する。
     * @param record 注文
     * @return 結果
     */
    int insert(Order record);

    /**
     * プライマリーキーで注文を更新する。
     * @param record 注文
     * @return 結果
     */
    int updateByPrimaryKey(Order record);

    /**
     * プライマリーキーで注文を更新する。
     * @param record 注文
     * @return 結果
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     * 注文を更新する。
     * @param record 注文
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Order record, @Param("example") WhereCondition example);

    /**
     * 注文を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Order example);

    /**
     * 注文を削除する。
     * @param orderId 注文ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer orderId);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param monthDto 検索条件
     * @return 結果
     */
    List<Order> selectByUserId(MonthDto monthDto);
}
