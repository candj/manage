package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.SocialInsuranceTemp;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface SocialInsuranceTempDao {

    /**
     * プライマリーキーで費用を検索する。
     * @param socialInsuranceId ID
     * @return 結果
     */
    SocialInsuranceTemp selectByPrimaryKey(Integer socialInsuranceId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param socialInsuranceId ID
     * @return 結果
     */
    List<SocialInsuranceTemp> selectAllByPrimaryKey(Integer socialInsuranceId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<SocialInsuranceTemp> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<SocialInsuranceTemp> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(SocialInsuranceTemp record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(SocialInsuranceTemp record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(SocialInsuranceTemp record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(SocialInsuranceTemp record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(SocialInsuranceTemp record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") SocialInsuranceTemp example);

    /**
     * 費用を削除する。
     * @param socialInsuranceId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer socialInsuranceId);
}
