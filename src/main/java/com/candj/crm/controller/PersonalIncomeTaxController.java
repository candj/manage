package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.PersonalIncomeTaxDto;
import com.candj.crm.service.PersonalIncomeTaxService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 社会保険料率情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/personalIncomeTax")
@Controller
public class PersonalIncomeTaxController {

    @Autowired
    PersonalIncomeTaxService personalIncomeTaxService;

    /**
     * 税率、保険料率を新規追加する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(PersonalIncomeTaxDto personalIncomeTaxDto, ModelMap model) {
        return new ModelAndView("personalIncomeTax/insert", model);
    }

    /**
     * 税率、保険料率を編集する。
     * @param personalIncomeTaxId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{personalIncomeTaxId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer personalIncomeTaxId, ModelMap model) {
        //プライマリーキーで税率、保険料率を検索する。
        PersonalIncomeTaxDto personalIncomeTaxDto = personalIncomeTaxService.selectByPrimaryKey(personalIncomeTaxId);
        personalIncomeTaxDto.setYearMonth(DateTimeUtil.dateTimeToString(personalIncomeTaxDto.getPersonalIncomeTaxStartMonth()));
        model.addAttribute("personalIncomeTaxDto", personalIncomeTaxDto);
        return new ModelAndView("personalIncomeTax/edit", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("personalIncomeTaxDto") PersonalIncomeTaxDto personalIncomeTaxDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("personalIncomeTax/list", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/fileList", method = RequestMethod.GET)
    public ModelAndView fileList(@ModelAttribute("personalIncomeTaxDto") PersonalIncomeTaxDto personalIncomeTaxDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("personalIncomeTax/fileList", model);
    }

}
