package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.SocialInsuranceDto;
import com.candj.crm.service.SocialInsuranceService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 社会保険料率情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/socialInsurance")
@Controller
public class SocialInsuranceController {


    @Autowired
    SocialInsuranceService socialInsuranceService;

    /**
     * 税率、保険料率を新規追加する。
     * @param socialInsuranceDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(SocialInsuranceDto socialInsuranceDto, ModelMap model) {
        return new ModelAndView("socialInsurance/insert", model);
    }

    /**
     * 税率、保険料率を編集する。
     * @param socialInsuranceId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{socialInsuranceId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer socialInsuranceId, ModelMap model) {
        //プライマリーキーで税率、保険料率を検索する。
        SocialInsuranceDto socialInsuranceDto = socialInsuranceService.selectByPrimaryKey(socialInsuranceId);
        socialInsuranceDto.setYearMonth(DateTimeUtil.dateTimeToString(socialInsuranceDto.getSocialInsuranceStartMonth()));
        model.addAttribute("socialInsuranceDto", socialInsuranceDto);
        return new ModelAndView("socialInsurance/edit", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param socialInsuranceDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("socialInsuranceDto") SocialInsuranceDto socialInsuranceDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("socialInsurance/list", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param socialInsuranceDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/fileList", method = RequestMethod.GET)
    public ModelAndView fileList(@ModelAttribute("socialInsuranceDto") SocialInsuranceDto socialInsuranceDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("socialInsurance/fileList", model);
    }

}
