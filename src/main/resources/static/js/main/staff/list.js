

			var colNames = [
				"ID",
				"スタッフ名前",
				"類別",
				"会社",
				"在職ステータス",
				"性別",
				"部署",
				"基本給料（円/月）",
				"イーメールアドレス",
				"電話番号",
				"入社時間",
				"ステータス",
			];
			var colModel = [
				{
					name: "userId",
					index: "userId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "userName",
					index: "userName",
					width: 100,
					search : false,
					align: "left",
				},
                {
					name: "userType",
					index: "userType",
					width: 100,
					search : false,
                    formatter: function (cellvalue, options, rowObject) {
                                if (cellvalue == '1') {
                                    return '本社';
                                } else if (cellvalue == '3') {
                                    return '協力会社';
                                } else if (cellvalue == '4') {
                                    return '無会社';
                                 }
                    },
					align: "left",
				},
                {
                	name: "company.companyName",
                	index: "company.companyName",
                	width: 100,
                	search : false,
                	align: "left",
                },
                {
					name: "employmentStatus",
					index: "employmentStatus",
					width: 100,
					search : false,
					formatter: function (cellvalue, options, rowObject) {
                                if (cellvalue == '00') {
                                    return '在職';
                                } else if (cellvalue == '01') {
                                    return '離職';
                                }
                    },
					align: "left",
				},
				{
                	name: "userSex",
                	index: "userSex",
                	width: 100,
                	search : false,
                	align: "left",
                },
				{
					name: "userDepartment",
					index: "userDepartment",
					width: 100,
					search : false,
					align: "left",
				},
				{
                	name: "userBaseSalary",
                	index: "userBaseSalary",
                	width: 100,
                	search : false,
                	formatter : "integer",
                      formatoptions : {
                        prefix : '$',
                        suffix : '円',
                        thousandsSeparator : ','
                      },
                    hidden : true,
                	align: "left",
                },
				{
					name: "userEmail",
					index: "userEmail",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "userTelephoneNumber",
					index: "userTelephoneNumber",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "employmentDate",
					index: "employmentDate",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                            if (cellvalue != null) {
                                var date = new Date(cellvalue);
                                return date.Format('yyyy-MM-dd');
                            } else {
                                return '';
                            }
                    },
					align: "left",
				},
				{
					name: "status",
					index: "status",
					width: 100,
					search : false,
					hidden : true,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

