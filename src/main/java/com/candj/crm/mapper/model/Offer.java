package com.candj.crm.mapper.model;

import com.candj.crm.dto.OrderingDto;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 見積
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Offer {
    /** 見積詳細  */
    private java.util.List<OfferDetail> offerDetail;

    /** 注文  */
    private java.util.List<OrderingColumnOnly> ordering;

    /** 注文  */
    private OrderingColumnOnly orderingDto;

    /** 見積 ID */
    private Integer offerId;

    /** 注文 ID */
    private Integer orderId;

    /** 見積番号 */
    private String offerNumber;

    /** 注文番号 */
    private String orderNumber;

    /** 種類 */
    private String offerType;

    /** 元会社 */
    private SendCompany sendCompany;

    /** 元会社ID */
    private Integer sendCompanyId;

    /** 先会社 */
    private ReceiveCompany receiveCompany;

    /** 先会社ID */
    private Integer receiveCompanyId;

    /** プロジェクト */
    private Project project;

    /** プロジェクトID */
    private Integer projectId;

    /** プロジェクト摘要 */
    private String projectDescription;

    /** 日付 */
    private org.joda.time.DateTime date;

    /** 業務名 */
    private String workName;

    /** 作業時間基準（下限） */
    private Integer workTimeLowerLimit;

    /** 作業時間基準（上限） */
    private Integer workTimeUpperLimit;

    /** 作業時間精算標準 */
    @Length(max=2)
    private String workTimeCalculateStandard;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 納入場所 */
    private String workPlace;

    /** 有効期限 */
    private Integer offerPeriodOfValidity;

    /** 納期 */
    private org.joda.time.DateTime offerDeliveryDate;

    /** 支払いサイト */
    private PaySite paySite;

    /** 支払いサイトID */
    private Integer paySiteId;

    /** 税抜き合計金額 */
    private Integer offerAmountWithoutTax;

    /** 消費税率 */
    private BigDecimal offerConsumptionTaxRate;

    /** 消費税 */
    private Integer offerConsumptionTax;

    /** 税込み合計金額 */
    private Integer offerTotalAmount;

    /** 精算方法 */
    private String afterPaymentPattern;

    /** 精算方法 */
    private String afterPaymentPatternName;

    /** 見積ステータス */
    private String offerStatus;

    /** 注文作成FLAG */
    private boolean haveOrderFlag;

    /** 削除FLAG */
    private boolean offerDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
