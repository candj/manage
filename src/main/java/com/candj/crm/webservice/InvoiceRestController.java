package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 請求情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/invoiceRest")
@RestController
public class InvoiceRestController {

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    InvoiceDetailService invoiceDetailService;

    @Autowired
    InvoiceDetailTemporaryService invoiceDetailTemporaryService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    OrderingService orderingService;

    @Autowired
    PaySiteService paySiteService;

    @Autowired
    CompanyService companyService;

    @Autowired
    Calculate calculate;

    /**
     * 請求を新規追加する。
     *
     * @param invoiceId 請求ID
     * @return 結果
     */
    @RequestMapping(value = "{invoiceId}", method = RequestMethod.GET)
    public InvoiceDto get(@PathVariable Integer invoiceId) {
        //プライマリーキーで請求を検索する。
        InvoiceDto ret = invoiceService.selectByPrimaryKey(invoiceId);
        return ret;
    }

    /**
     * 請求を新規追加する。
     *
     * @param invoiceDto 請求
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody InvoiceDto invoiceDto, Errors rrrors) {

        invoiceDto.setInvoiceDeleteFlag(true);

        List<PaySiteDto> paySiteDtoList = paySiteService.selectByMyExample(invoiceDto.getOrderId());

        InvoiceDetailDto invoiceDetailData = new InvoiceDetailDto();
        List<InvoiceDetailDto> invoiceDetailDatas = invoiceDetailTemporaryService.selectByExample(invoiceDetailData);
        for(InvoiceDetailDto i : invoiceDetailDatas){
            invoiceDetailTemporaryService.deleteByPrimaryKey(i.getInvoiceDetailId());
        }

        if (!paySiteDtoList.isEmpty()){

            PaySiteDto paySiteDto = paySiteDtoList.get(0);
            String payName = paySiteDto.getPaySiteName();
            org.joda.time.DateTime invoiceDate = invoiceDto.getInvoiceDate();
            DateTime invoiceDeliveryDate;
            if ("翌月末日お支払い".contains(payName)){
                invoiceDeliveryDate = invoiceDate.plusMonths(1).dayOfMonth().withMaximumValue();
            }else {
                invoiceDeliveryDate = invoiceDate.plusMonths(2).dayOfMonth().withMaximumValue();
            }
            invoiceDto.setInvoiceDeliveryDate(invoiceDeliveryDate);

            //請求番号を生成する
            OrderingDto orderingDto = orderingService.selectAllByPrimaryKey(invoiceDto.getOrderId()).get(0);
            invoiceDto.setInvoiceNumber(orderingDto.getOffer().getReceiveCompany().getReceiveCompanyNameAbbr()
                    + " "
                    + DateTimeUtil.dateTimeToStringNumber(invoiceDto.getInvoiceDate()));
            //請求を新規追加する。
            int ret = invoiceService.insertSelective(invoiceDto);

            List<InvoiceDto> invoiceDtos = invoiceService.selectAllByExample(invoiceDto, null);

            //種類が「派遣」の場合、請求詳細を作成する
            if (invoiceDtos.get(0).getOrdering().getOffer().getOfferType().equals("00")) {
                InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
                Integer totalAmountWithoutTax = 0;
                Integer totalCommutingCost = 0;
                for(InvoiceDetailDto i : invoiceDetailDatas){
                    i.setInvoiceId(null);
                    i.setInvoiceId(invoiceDtos.get(0).getInvoiceId());
                    invoiceDetailService.insertSelective(i);
                    totalAmountWithoutTax = i.getAmountMonth() + i.getAmountExtra() + i.getCommutingCostAmount() + totalAmountWithoutTax;
                    totalCommutingCost = i.getCommutingCostAmount() + totalCommutingCost;
                }
                invoiceDtos.get(0).setInvoiceAmountWithoutTax(totalAmountWithoutTax);
                invoiceDtos.get(0).setInvoiceCommutingCost(totalCommutingCost);
                //消費税
                TaxDto dto = new TaxDto();
                dto.setTaxLowerLimit(totalAmountWithoutTax);
                dto.setTaxName("01");
                Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
                invoiceDtos.get(0).setInvoiceConsumptionTax(tax);
                //税込み合計金額
                invoiceDtos.get(0).setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
                invoiceService.updateByPrimaryKeySelective(invoiceDtos.get(0));
            }
            return ret;
        }

        return 0;
    }

    /**
     * 請求を変更する。
     *
     * @param invoiceDto 請求
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody InvoiceDto invoiceDto, Errors rrrors) {

        invoiceDto.setInvoiceDeleteFlag(true);

        List<PaySiteDto> paySiteDtoList = paySiteService.selectByMyExample(invoiceDto.getOrderId());

        if (!paySiteDtoList.isEmpty()){

            String payName = paySiteDtoList.get(0).getPaySiteName();
            org.joda.time.DateTime invoiceDate = invoiceDto.getInvoiceDate();
            DateTime invoiceDeliveryDate;
            if ("翌月末日お支払い".contains(payName)){
                invoiceDeliveryDate = invoiceDate.plusMonths(1).dayOfMonth().withMaximumValue();
            }else {
                invoiceDeliveryDate = invoiceDate.plusMonths(2).dayOfMonth().withMaximumValue();
            }
            invoiceDto.setInvoiceDeliveryDate(invoiceDeliveryDate);

            //請求番号を生成する
            OrderingDto orderingDto = orderingService.selectAllByPrimaryKey(invoiceDto.getOrderId()).get(0);
            invoiceDto.setInvoiceNumber(orderingDto.getOffer().getReceiveCompany().getReceiveCompanyNameAbbr()
                    + " "
                    + DateTimeUtil.dateTimeToStringNumber(invoiceDto.getInvoiceDate()));
            //プライマリーキーで請求を更新する。

            return invoiceService.updateByPrimaryKeySelective(invoiceDto);
        }
        return 0;
    }

    /**
     * 請求を新規追加する。
     *
     * @param invoiceId 請求ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{invoiceId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer invoiceId) {

        InvoiceDto invoiceDto = invoiceService.selectByPrimaryKey(invoiceId);

        invoiceDto.setInvoiceDeleteFlag(false);

        //請求を削除する。
        int ret = invoiceService.updateByPrimaryKey(invoiceDto);
        return ret;
    }

    /**
     * 請求一覧画面を表示する。
     *
     * @param invoiceDto 請求
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<InvoiceDto> list(InvoiceDto invoiceDto,
                                     @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                     @RequestParam(name = "projectName", required = false) String projectName,
                                     JqgridPageReq pageReq) {
        //条件で請求を検索する。（連携情報含む）
        DateTime startOfMonth = new DateTime();
        if (yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);

        if (invoiceDto.getSendCompanyName() != null && !invoiceDto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(invoiceDto.getSendCompanyName());
            if (!companyDtoList.isEmpty()){
                invoiceDto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<InvoiceDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        if (invoiceDto.getReceiveCompanyName() != null && !invoiceDto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(invoiceDto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                invoiceDto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<InvoiceDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0) {
            Page<InvoiceDto> page = PageUtil.startPage(pageReq);
            invoiceDto.setWorkStartDate(startOfMonth);
            invoiceDto.setWorkEndDate(endOfMonth);
            if (projectName.equals("")) {
                projectName = null;
            }
            List<InvoiceDto> ret = invoiceService.selectAllByExample(invoiceDto, projectName);
            return PageUtil.resp(page);
        } else {
            Page<InvoiceDto> page = PageUtil.startPage(pageReq);
            invoiceDto.setWorkStartDate(startOfMonth);
            invoiceDto.setWorkEndDate(endOfMonth);
            List<InvoiceDto> ret = invoiceService.selectAllByMyExample(invoiceDto, projectName,appUser.getUserId() );
            return PageUtil.resp(page);
        }
    }

    /**
     * 請求一覧画面を表示する。
     *
     * @param invoiceDto 請求
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<InvoiceDto> listAll(InvoiceDto invoiceDto, JqgridPageReq pageReq) {
        Page<InvoiceDto> page = PageUtil.startPage(pageReq);
        //条件で請求を検索する。（連携情報含む）
        List<InvoiceDto> ret = invoiceService.selectByExample(invoiceDto);
        return PageUtil.resp(page);
    }
}
