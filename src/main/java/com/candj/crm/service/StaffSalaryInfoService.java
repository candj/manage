package com.candj.crm.service;

import com.candj.crm.dao.mapper.StaffSalaryInfoDao;
import com.candj.crm.dto.StaffSalaryInfoDto;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.mapper.model.StaffSalaryInfo;
import com.candj.crm.mapper.model.Tax;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.Comparator;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * スタッフ基本給料情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class StaffSalaryInfoService {
    @Autowired
    StaffSalaryInfoDao staffSalaryInfoDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでスタッフ基本給料を検索する。
     *
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    public StaffSalaryInfoDto selectByPrimaryKey(Integer staffSalaryInfoId) {
        //プライマリーキーでスタッフ基本給料を検索する。
        StaffSalaryInfo ret = staffSalaryInfoDao.selectByPrimaryKey(staffSalaryInfoId);
        return DozerHelper.map(mapper, ret, StaffSalaryInfoDto.class);
    }

    /**
     * プライマリーキーでスタッフ基本給料を検索する。（連携情報含む）
     *
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    public List<StaffSalaryInfoDto> selectAllByPrimaryKey(Integer staffSalaryInfoId) {
        //プライマリーキーでスタッフ基本給料検索する。（連携情報含む）
        List<StaffSalaryInfo> ret = staffSalaryInfoDao.selectAllByPrimaryKey(staffSalaryInfoId);
        return DozerHelper.mapList(mapper, ret, StaffSalaryInfoDto.class);
    }

    /**
     * 条件でスタッフ基本給料を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffSalaryInfoDto> selectByExample(StaffSalaryInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("staff_salary_info.staff_salary_info_id", dto.getStaffSalaryInfoId())
                    .andEqualTo("staff_salary_info.user_id", dto.getUserId())
                    .andEqualTo("staff_salary_info.salary_pay", dto.getSalaryPay())
                    .andLessThanOrEqualTo("staff_salary_info.salary_start_date", dto.getSalaryStartDate())
                    .andGreaterThan("staff_salary_info.salary_end_date", dto.getSalaryStartDate())
                    .andEqualTo("staff_salary_info.create_user", dto.getCreateUser())
                    .andEqualTo("staff_salary_info.update_user", dto.getUpdateUser())
                    .andEqualTo("staff_salary_info.create_time", dto.getCreateTime())
                    .andEqualTo("staff_salary_info.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("staff_salary_info.staff_salary_info_id", dto.getStaffSalaryInfoId())
                    .andEqualTo("staff_salary_info.user_id", dto.getUserId())
                    .andEqualTo("staff_salary_info.salary_pay", dto.getSalaryPay())
                    .andLessThanOrEqualTo("staff_salary_info.salary_start_date", dto.getSalaryStartDate())
                    .andIsNull("staff_salary_info.salary_end_date")
                    .andEqualTo("staff_salary_info.create_user", dto.getCreateUser())
                    .andEqualTo("staff_salary_info.update_user", dto.getUpdateUser())
                    .andEqualTo("staff_salary_info.create_time", dto.getCreateTime())
                    .andEqualTo("staff_salary_info.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ基本給料を検索する。（連携情報含む）)
        List<StaffSalaryInfo> ret = staffSalaryInfoDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffSalaryInfoDto.class);
    }

    /**
     * 条件でスタッフ基本給料を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffSalaryInfoDto> selectAllByExample(StaffSalaryInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andLike("app_user.user_name", dto.getAppUser().getUserName())
                    .andEqualTo("staff_salary_info.staff_salary_info_id", dto.getStaffSalaryInfoId())
                    .andEqualTo("staff_salary_info.user_id", dto.getUserId())
                    .andEqualTo("staff_salary_info.salary_pay", dto.getSalaryPay())
                    .andLessThanOrEqualTo("staff_salary_info.salary_start_date", dto.getSalaryStartDate())
                    .andGreaterThanOrEqualTo("staff_salary_info.salary_end_date", dto.getSalaryEndDate())
                    .andEqualTo("staff_salary_info.create_user", dto.getCreateUser())
                    .andEqualTo("staff_salary_info.update_user", dto.getUpdateUser())
                    .andEqualTo("staff_salary_info.create_time", dto.getCreateTime())
                    .andEqualTo("staff_salary_info.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ基本給料を検索する。（連携情報含む）)
        List<StaffSalaryInfo> ret = staffSalaryInfoDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffSalaryInfoDto.class);
    }

    public List<StaffSalaryInfoDto> selectAllByMyExample(StaffSalaryInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("staff_salary_info.staff_salary_info_id", dto.getStaffSalaryInfoId())
                    .andEqualTo("staff_salary_info.user_id", dto.getUserId())
                    .andEqualTo("staff_salary_info.salary_pay", dto.getSalaryPay())
                    .andLessThanOrEqualTo("staff_salary_info.salary_start_date", dto.getSalaryStartDate())
                    .andGreaterThanOrEqualTo("staff_salary_info.salary_end_date", dto.getSalaryEndDate())
                    .andEqualTo("staff_salary_info.create_user", dto.getCreateUser())
                    .andEqualTo("staff_salary_info.update_user", dto.getUpdateUser())
                    .andEqualTo("staff_salary_info.create_time", dto.getCreateTime())
                    .andEqualTo("staff_salary_info.update_time", dto.getUpdateTime());

            whereCondition.or()
                    .andEqualTo("staff_salary_info.staff_salary_info_id", dto.getStaffSalaryInfoId())
                    .andEqualTo("staff_salary_info.user_id", dto.getUserId())
                    .andEqualTo("staff_salary_info.salary_pay", dto.getSalaryPay())
                    .andLessThanOrEqualTo("staff_salary_info.salary_start_date", dto.getSalaryStartDate())
                    .andIsNull("staff_salary_info.salary_end_date")
                    .andEqualTo("staff_salary_info.create_user", dto.getCreateUser())
                    .andEqualTo("staff_salary_info.update_user", dto.getUpdateUser())
                    .andEqualTo("staff_salary_info.create_time", dto.getCreateTime())
                    .andEqualTo("staff_salary_info.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ基本給料を検索する。（連携情報含む）)
        List<StaffSalaryInfo> ret = staffSalaryInfoDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffSalaryInfoDto.class);
    }

    /**
     * スタッフ基本給料を新規追加する。
     *
     * @param dto スタッフ基本給料
     * @return 結果
     */
    public int insertSelective(StaffSalaryInfoDto dto) {
        StaffSalaryInfo staffSalaryInfo = mapper.map(dto, StaffSalaryInfo.class);
        //スタッフ基本給料を新規追加する。
        int ret = staffSalaryInfoDao.insertSelective(staffSalaryInfo);
        return ret;
    }

    /**
     * スタッフ基本給料を新規追加する。
     *
     * @param dto スタッフ基本給料
     * @return 結果
     */
    public int insert(StaffSalaryInfoDto dto) {
        StaffSalaryInfo staffSalaryInfo = mapper.map(dto, StaffSalaryInfo.class);
        //スタッフ基本給料を新規追加する。
        int ret = staffSalaryInfoDao.insert(staffSalaryInfo);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ基本給料を更新する。
     *
     * @param dto スタッフ基本給料
     * @return 結果
     */
    public int updateByPrimaryKey(StaffSalaryInfoDto dto) {
        StaffSalaryInfo staffSalaryInfo = mapper.map(dto, StaffSalaryInfo.class);
        //プライマリーキーでスタッフ基本給料を更新する。
        int ret = staffSalaryInfoDao.updateByPrimaryKey(staffSalaryInfo);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ基本給料を更新する。
     *
     * @param dto スタッフ基本給料
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffSalaryInfoDto dto) {
        StaffSalaryInfo staffSalaryInfo = mapper.map(dto, StaffSalaryInfo.class);
        //プライマリーキーでスタッフ基本給料を更新する。
        int ret = staffSalaryInfoDao.updateByPrimaryKeySelective(staffSalaryInfo);
        return ret;
    }

    /**
     * スタッフ基本給料を削除する。
     *
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer staffSalaryInfoId) {
        //スタッフ基本給料を削除する。
        int ret = staffSalaryInfoDao.deleteByPrimaryKey(staffSalaryInfoId);
        return ret;
    }

    Comparator<StaffSalaryInfo> descWorkStartTimeComp = new Comparator<StaffSalaryInfo>() {
        @Override
        public int compare(StaffSalaryInfo t1, StaffSalaryInfo t2) {
            return t2.getSalaryStartDate().compareTo(t1.getSalaryStartDate());
        }
    };

    public void setTaxEndMonth(Integer userId){
        List<StaffSalaryInfo> staffSalaryInfoList = staffSalaryInfoDao.selectAllByPrimaryKeyFromUserId(userId);
        staffSalaryInfoList.sort(descWorkStartTimeComp);
        DateTime lastEndMonth = null;
        for(StaffSalaryInfo t : staffSalaryInfoList){
            t.setSalaryEndDate(lastEndMonth == null? null : lastEndMonth);
            lastEndMonth = t.getSalaryStartDate().minusSeconds(1);
            staffSalaryInfoDao.updateByPrimaryKeySelective(t);
        }
    }
}
