package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

/**
 * 請求詳細
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class InvoiceDetail {
    /** ID */
    private Integer invoiceDetailId;

    /** 請求 */
    private Invoice invoice;

    /** 請求ID */
    private Integer invoiceId;

    /** 開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** ユーザー */
    private AppUser appUser;

    /** 担当者ID */
    private Integer userId;

    /** 人月 */
    private java.math.BigDecimal personMonth;

    /** 人月単価 */
    private Integer salaryPersonMonth;

    /** 月額金額 */
    private Integer amountMonth;

    /** 残業 */
    private String overTimeWork;

    /** 時間 */
    private java.math.BigDecimal overTime;

    /** 超過単価 */
    private Integer salaryExtra;

    /** 控除単価 */
    private Integer salaryDeduction;

    /** 超過金額 */
    private Integer amountExtra;

    /** 総金額 */
    private Integer totalAmount;

    /** 摘要 */
    private String remark;

    /** 作業時間 */
    private java.math.BigDecimal workHour;

    /** 交通費 */
    private Integer commutingCostAmount;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
