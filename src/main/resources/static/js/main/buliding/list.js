

			var colNames = [
				"建物ID",
				"土地ID",
				"建物面積",
				"建筑物结构",
				"房屋外部图片文件夹路径",
				"房屋内部图片文件夹路径",
				"设备图片文件夹路径",
				"抗震，隔震，抗震结构",
				"防犯カメラ",
				"设计公司",
				"施工公司",
				"投资区分",
				"投资状况",
				"回利率",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "bulidingId",
					index: "bulidingId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "bulidingLandId",
					index: "bulidingLandId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "buildingArea",
					index: "buildingArea",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "buildingSystemConfigId",
					index: "buildingSystemConfigId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "pictureOutUrl",
					index: "pictureOutUrl",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "pictureInUrl",
					index: "pictureInUrl",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "equipmentPictureUrl",
					index: "equipmentPictureUrl",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "shockproof",
					index: "shockproof",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "cameras",
					index: "cameras",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "designCompany",
					index: "designCompany",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "constructionCompany",
					index: "constructionCompany",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "investSpecies",
					index: "investSpecies",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "investStatus",
					index: "investStatus",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "investRatio",
					index: "investRatio",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	