package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;

import javax.validation.constraints.Digits;

/**
 * 税率
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class TaxDto {

    /** 税率ID */
    @Digits(integer=10, fraction=0)
    private Integer taxId;

    /** 税名 */
    @Length(max=128)
    private String taxName;

    /** 税率(%) */
    @Digits(integer=3, fraction=2)
    private java.math.BigDecimal taxRate;

    /** 所得区間上限 */
    @Digits(integer=32, fraction=0)
    private Integer taxUpperLimit;

    /** 所得区間下限 */
    @Digits(integer=32, fraction=0)
    private Integer taxLowerLimit;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** 適用年月開始時間 */
    private org.joda.time.DateTime taxStartMonth;

    /** 適用年月終了時間 */
    private org.joda.time.DateTime taxEndMonth;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
