package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectTaxDto {

    private String taxName;

    /** 年月 */
    private org.joda.time.DateTime month;

}
