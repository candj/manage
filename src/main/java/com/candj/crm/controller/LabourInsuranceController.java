package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.LabourInsuranceDto;
import com.candj.crm.service.LabourInsuranceService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/labourInsurance")
@Controller
public class LabourInsuranceController {

    @Autowired
    LabourInsuranceService labourInsuranceService;

    /**
     * 税率、保険料率を新規追加する。
     * @param labourInsuranceDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(LabourInsuranceDto labourInsuranceDto, ModelMap model) {
        return new ModelAndView("labourInsurance/insert", model);
    }

    /**
     * 税率、保険料率を編集する。
     * @param labourInsuranceId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{labourInsuranceId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer labourInsuranceId, ModelMap model) {
        //プライマリーキーで税率、保険料率を検索する。
        LabourInsuranceDto labourInsuranceDto = labourInsuranceService.selectByPrimaryKey(labourInsuranceId);
        labourInsuranceDto.setYearMonth(DateTimeUtil.dateTimeToString(labourInsuranceDto.getLabourInsuranceStartMonth()));
        model.addAttribute("labourInsuranceDto", labourInsuranceDto);
        return new ModelAndView("labourInsurance/edit", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param labourInsuranceDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("labourInsuranceDto") LabourInsuranceDto labourInsuranceDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("labourInsurance/list", model);
    }
}
