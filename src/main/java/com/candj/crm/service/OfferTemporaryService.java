package com.candj.crm.service;

import com.candj.crm.dao.mapper.OfferTemporaryDao;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.mapper.model.OrderingColumnOnly;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 見積情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class OfferTemporaryService {
    @Autowired
    OfferTemporaryDao offerTemporaryDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで見積を検索する。
     * @param offerId 見積 ID
     * @return 結果
     */
    public OfferDto selectByPrimaryKey(Integer offerId) {
        //プライマリーキーで見積を検索する。
        Offer ret = offerTemporaryDao.selectByPrimaryKey(offerId);
        return DozerHelper.map(mapper, ret, OfferDto.class);
    }

    /**
     * プライマリーキーで見積を検索する。（連携情報含む）
     * @param offerId 見積 ID
     * @return 結果
     */
    public List<OfferDto> selectAllByPrimaryKey(Integer offerId) {
        //プライマリーキーで見積検索する。（連携情報含む）
        List<Offer> ret = offerTemporaryDao.selectAllByPrimaryKey(offerId);
        return DozerHelper.mapList(mapper, ret, OfferDto.class);
    }

    /**
     * 条件で見積を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OfferDto> selectByExample(OfferDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("offer.offer_id", dto.getOfferId())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("send_company_id", dto.getSendCompanyId())
                    .andEqualTo("receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("offer.date", dto.getDate())
                    .andEqualTo("offer.offer_number", dto.getOfferNumber())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("offer.work_name", dto.getWorkName())
                    .andEqualTo("offer.work_time_lower_limit", dto.getWorkTimeLowerLimit())
                    .andEqualTo("offer.work_time_upper_limit", dto.getWorkTimeUpperLimit())
                    .andEqualTo("offer.work_start_date", dto.getWorkStartDate())
                    .andEqualTo("offer.work_end_date", dto.getWorkEndDate())
                    .andEqualTo("offer.work_place", dto.getWorkPlace())
                    .andEqualTo("offer.offer_period_of_validity", dto.getOfferPeriodOfValidity())
                    .andEqualTo("offer.offer_delivery_date", dto.getOfferDeliveryDate())
                    .andEqualTo("offer.pay_site_id", dto.getPaySiteId())
                    .andEqualTo("offer.offer_amount_without_tax", dto.getOfferAmountWithoutTax())
                    .andEqualTo("offer.offer_consumption_tax", dto.getOfferConsumptionTax())
                    .andEqualTo("offer.offer_total_amount", dto.getOfferTotalAmount())
                    .andEqualTo("offer.after_payment_pattern", dto.getAfterPaymentPattern())
                    .andEqualTo("offer.offer_status", dto.getOfferStatus())
                    .andEqualTo("offer.create_user", dto.getCreateUser())
                    .andEqualTo("offer.update_user", dto.getUpdateUser())
                    .andEqualTo("offer.create_time", dto.getCreateTime())
                    .andEqualTo("offer.update_time", dto.getUpdateTime());
        }
        //条件で見積を検索する。（連携情報含む）)
        List<Offer> ret = offerTemporaryDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OfferDto.class);
    }

    /**
     * 条件で見積を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OfferDto> selectByMyExample(OfferDto dto, Integer userId) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("offer.offer_id", dto.getOfferId())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("send_company_id", dto.getSendCompanyId())
                    .andEqualTo("receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("offer.date", dto.getDate())
                    .andEqualTo("offer.offer_number", dto.getOfferNumber())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("offer.work_name", dto.getWorkName())
                    .andEqualTo("offer.work_time_lower_limit", dto.getWorkTimeLowerLimit())
                    .andEqualTo("offer.work_time_upper_limit", dto.getWorkTimeUpperLimit())
                    .andLessThanOrEqualTo("offer.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("offer.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("offer.work_place", dto.getWorkPlace())
                    .andEqualTo("offer.offer_period_of_validity", dto.getOfferPeriodOfValidity())
                    .andEqualTo("offer.offer_delivery_date", dto.getOfferDeliveryDate())
                    .andEqualTo("offer.pay_site_id", dto.getPaySiteId())
                    .andEqualTo("offer.offer_amount_without_tax", dto.getOfferAmountWithoutTax())
                    .andEqualTo("offer.offer_consumption_tax", dto.getOfferConsumptionTax())
                    .andEqualTo("offer.offer_total_amount", dto.getOfferTotalAmount())
                    .andEqualTo("offer.after_payment_pattern", dto.getAfterPaymentPattern())
                    .andEqualTo("offer.offer_status", dto.getOfferStatus())
                    .andEqualTo("offer.create_user", dto.getCreateUser())
                    .andEqualTo("offer.update_user", dto.getUpdateUser())
                    .andEqualTo("offer.create_time", dto.getCreateTime())
                    .andEqualTo("offer.update_time", dto.getUpdateTime())
                    .andEqualTo("offer.offer_delete_flag", 1)
                    .andEqualTo("offer_detail.user_id", userId);
        }
        //条件で見積を検索する。（連携情報含む）)
        List<Offer> ret = offerTemporaryDao.selectByMyExample(whereCondition);
        ret.forEach(o -> {
            if(o.getOrdering().get(0).getOrderId() != null){
                for(OrderingColumnOnly ordering : o.getOrdering()) {
                    if(ordering.isOrderDeleteFlag() != false) {
                        o.setOrderingDto(ordering);
                        break;
                    }
                }

            }
        });
        return DozerHelper.mapList(mapper, ret, OfferDto.class);
    }

    /**
     * 条件で見積を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OfferDto> selectAllByExample(OfferDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("offer.offer_id", dto.getOfferId())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("send_company_id", dto.getSendCompanyId())
                    .andEqualTo("receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("offer.date", dto.getDate())
                    .andLike("offer.offer_number", dto.getOfferNumber())
                    .andEqualTo("offer.offer_type", dto.getOfferType())
                    .andEqualTo("offer.work_name", dto.getWorkName())
                    .andEqualTo("offer.work_time_lower_limit", dto.getWorkTimeLowerLimit())
                    .andEqualTo("offer.work_time_upper_limit", dto.getWorkTimeUpperLimit())
                    .andLessThanOrEqualTo("offer.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("offer.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("offer.work_place", dto.getWorkPlace())
                    .andEqualTo("offer.offer_period_of_validity", dto.getOfferPeriodOfValidity())
                    .andEqualTo("offer.offer_delivery_date", dto.getOfferDeliveryDate())
                    .andEqualTo("offer.pay_site_id", dto.getPaySiteId())
                    .andEqualTo("offer.offer_amount_without_tax", dto.getOfferAmountWithoutTax())
                    .andEqualTo("offer.offer_consumption_tax", dto.getOfferConsumptionTax())
                    .andEqualTo("offer.offer_total_amount", dto.getOfferTotalAmount())
                    .andEqualTo("offer.after_payment_pattern", dto.getAfterPaymentPattern())
                    .andEqualTo("offer.offer_status", dto.getOfferStatus())
                    .andEqualTo("offer.create_user", dto.getCreateUser())
                    .andEqualTo("offer.update_user", dto.getUpdateUser())
                    .andEqualTo("offer.create_time", dto.getCreateTime())
                    .andEqualTo("offer.update_time", dto.getUpdateTime())
                    .andEqualTo("offer.offer_delete_flag", 1);
        }
        //条件で見積を検索する。（連携情報含む）)
        List<Offer> ret = offerTemporaryDao.selectAllByExample(whereCondition);
        ret.forEach(o -> {
            if(o.getOrdering().get(0).getOrderId() != null){
                for(OrderingColumnOnly ordering : o.getOrdering()) {
                    if(ordering.isOrderDeleteFlag() != false) {
                        o.setOrderingDto(ordering);
                        break;
                    }
                }

            }
        });
        return DozerHelper.mapList(mapper, ret, OfferDto.class);
    }

    /**
     * 見積を新規追加する。
     * @param dto 見積
     * @return 結果
     */
    public int insertSelective(OfferDto dto) {
        Offer offer  =  mapper.map(dto, Offer.class);
        //見積を新規追加する。
        int ret = offerTemporaryDao.insertSelective(offer);
        return ret;
    }

    /**
     * 見積を新規追加する。
     * @param dto 見積
     * @return 結果
     */
    public int insert(OfferDto dto) {
        Offer offer  =  mapper.map(dto, Offer.class);
        //見積を新規追加する。
        int ret = offerTemporaryDao.insert(offer);
        return ret;
    }

    /**
     * プライマリーキーで見積を更新する。
     * @param dto 見積
     * @return 結果
     */
    public int updateByPrimaryKey(OfferDto dto) {
        Offer offer  =  mapper.map(dto, Offer.class);
        //プライマリーキーで見積を更新する。
        int ret = offerTemporaryDao.updateByPrimaryKey(offer);
        return ret;
    }

    /**
     * プライマリーキーで見積を更新する。
     * @param dto 見積
     * @return 結果
     */
    public int updateByPrimaryKeySelective(OfferDto dto) {
        Offer offer  =  mapper.map(dto, Offer.class);
        //プライマリーキーで見積を更新する。
        int ret = offerTemporaryDao.updateByPrimaryKeySelective(offer);
        return ret;
    }

    /**
     * 見積を削除する。
     * @param offerId 見積 ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer offerId) {
        //見積を削除する。
        int ret = offerTemporaryDao.deleteByPrimaryKey(offerId);
        return ret;
    }

    /**
     * プライマリーキーで見積を検索する。（連携情報含む）
     * @param offerId 見積ID
     * @return 結果
     */
    public List<Offer> selectOfferByPrimaryKey(Integer offerId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Offer> ret = offerTemporaryDao.selectAllByPrimaryKey(offerId);
        return ret;
    }

}
