package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.Tax;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface TaxDao {
    /**
     * プライマリーキーで費用を検索する。
     * @param taxId ID
     * @return 結果
     */
    Tax selectByPrimaryKey(Integer taxId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param taxId ID
     * @return 結果
     */
    List<Tax> selectAllByPrimaryKey(Integer taxId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Tax> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Tax> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(Tax record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(Tax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(Tax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(Tax record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Tax record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Tax example);

    /**
     * 費用を削除する。
     * @param taxId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer taxId);

    /**
     * 税名で費用検索する。（連携情報含む）
     * @param taxName ID
     * @return 結果
     */
    List<Tax> selectByTaxName(String taxName);
}
