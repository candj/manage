package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class User {
    /** ユーザーID */
    private Integer userId;

    /** 会社ID */
    private Integer companyId;

    /** タイプ */
    private Integer userType;

    /** 名前 */
    private String userName;

    /** 性別 */
    private String userSex;

    /** 生年月日 */
    private org.joda.time.DateTime birthDate;

    /** 部門 */
    private String userDepartment;

    /** イーメールアドレス */
    private String userEmail;

    /** 電話番号 */
    private String userTelephoneNumber;

    /** パスワード */
    private String password;

    /** ステータス */
    private Byte userStatus;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
