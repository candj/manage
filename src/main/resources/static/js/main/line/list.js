

			var colNames = [
				"线路ID",
				"线路名称",
				"线路种类ID(1:JR，2:地下鉄，3:私鉄・その他)",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "lineId",
					index: "lineId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "lineName",
					index: "lineName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "lineKind",
					index: "lineKind",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	