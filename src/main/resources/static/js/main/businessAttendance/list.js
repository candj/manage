

			var colNames = [
				"ID",
				"業務名",
				"業務名「中国語」",
				"業務コード",
				"暗証化コード",
				"FALSE:今日　TRUE:明日（出勤だけ）",
				"業務区分ID",
			];
			var colModel = [
				{
					name: "businessId",
					index: "businessId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "businessName",
					index: "businessName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessNameCn",
					index: "businessNameCn",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessCode",
					index: "businessCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessEncryptCode",
					index: "businessEncryptCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "limitEndTimeDay",
					index: "limitEndTimeDay",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "businessTypeId",
					index: "businessTypeId",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

