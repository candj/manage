package com.candj.crm.service;

import com.candj.crm.dao.mapper.PersonalIncomeTaxTempDao;
import com.candj.crm.dto.PersonalIncomeTaxTempDto;
import com.candj.crm.mapper.model.PersonalIncomeTempTax;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class PersonalIncomeTaxTempService {

    @Autowired
    PersonalIncomeTaxTempDao personalIncomeTaxTempDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで税率、保険料率を検索する。
     *
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public PersonalIncomeTaxTempDto selectByPrimaryKey(Integer personalIncomeTaxId) {
        //プライマリーキーで税率、保険料率を検索する。
        PersonalIncomeTempTax ret = personalIncomeTaxTempDao.selectByPrimaryKey(personalIncomeTaxId);
        return DozerHelper.map(mapper, ret, PersonalIncomeTaxTempDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     *
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public List<PersonalIncomeTaxTempDto> selectAllByPrimaryKey(Integer personalIncomeTaxId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<PersonalIncomeTempTax> ret = personalIncomeTaxTempDao.selectAllByPrimaryKey(personalIncomeTaxId);
        return DozerHelper.mapList(mapper, ret, PersonalIncomeTaxTempDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<PersonalIncomeTaxTempDto> selectByExample(PersonalIncomeTaxTempDto dto) {
        WhereCondition whereCondition = new WhereCondition();

        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<PersonalIncomeTempTax> ret = personalIncomeTaxTempDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, PersonalIncomeTaxTempDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<PersonalIncomeTaxTempDto> selectAllByExample(PersonalIncomeTaxTempDto dto) {
        WhereCondition whereCondition = new WhereCondition();

        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<PersonalIncomeTempTax> personalIncomeTempTaxes = personalIncomeTaxTempDao.selectAllByExample(whereCondition);

        return DozerHelper.mapList(mapper, personalIncomeTempTaxes, PersonalIncomeTaxTempDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(PersonalIncomeTaxTempDto dto) {
        PersonalIncomeTempTax personalIncomeTempTax = mapper.map(dto, PersonalIncomeTempTax.class);
        //税率、保険料率を新規追加する。
        int ret = personalIncomeTaxTempDao.insertSelective(personalIncomeTempTax);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(PersonalIncomeTaxTempDto dto) {
        PersonalIncomeTempTax personalIncomeTempTax = mapper.map(dto, PersonalIncomeTempTax.class);
        //税率、保険料率を新規追加する。
        int ret = personalIncomeTaxTempDao.insert(personalIncomeTempTax);
        return ret;
    }


    /**
     * 税率、保険料率を削除する。
     *
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer personalIncomeTaxId) {
        //税率、保険料率を削除する。
        int ret = personalIncomeTaxTempDao.deleteByPrimaryKey(personalIncomeTaxId);
        return ret;
    }
}
