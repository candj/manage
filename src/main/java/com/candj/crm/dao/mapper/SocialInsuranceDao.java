package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.SocialInsurance;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface SocialInsuranceDao {
    /**
     * プライマリーキーで費用を検索する。
     * @param socialInsuranceId ID
     * @return 結果
     */
    SocialInsurance selectByPrimaryKey(Integer socialInsuranceId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param socialInsuranceId ID
     * @return 結果
     */
    List<SocialInsurance> selectAllByPrimaryKey(Integer socialInsuranceId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<SocialInsurance> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<SocialInsurance> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(SocialInsurance record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(SocialInsurance record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(SocialInsurance record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(SocialInsurance record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(SocialInsurance record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") SocialInsurance example);

    /**
     * 費用を削除する。
     * @param socialInsuranceId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer socialInsuranceId);

}
