package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 交通費
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Commute {
    /** 交通費ID */
    private Integer commuteId;

    /** ユーザー */
    private AppUser appUser;

    /** スタッフID */
    private Integer userId;

    /** 申請月 */
    private org.joda.time.DateTime commuteApplyMonth;

    /** 乗車日 */
    private org.joda.time.DateTime commuteDate;

    /** 乗車駅 */
    private String commuteGetOnStation;

    /** 降車駅 */
    private String commuteGetOffStation;

    /** 金額 */
    private Integer commuteAmount;

    /** コメント */
    private String commuteRemark;

    /** 添付ファイルパス */
    private String commuteAttachmentPath;

    /** 添付ファイル名 */
    private String commuteAttachmentName;

    /** 処理状態 */
    private String commuteAdmitStatus;

    /** ステータス */
    private Byte commuteStatus;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
