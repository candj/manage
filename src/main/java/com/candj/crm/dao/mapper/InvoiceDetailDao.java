package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.InvoiceDetail;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 請求詳細情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface InvoiceDetailDao {
    /**
     * プライマリーキーで請求詳細を検索する。
     * @param invoiceDetailId ID
     * @return 結果
     */
    InvoiceDetail selectByPrimaryKey(Integer invoiceDetailId);

    /**
     * プライマリーキーで請求詳細検索する。（連携情報含む）
     * @param invoiceDetailId ID
     * @return 結果
     */
    List<InvoiceDetail> selectAllByPrimaryKey(Integer invoiceDetailId);

    /**
     * 条件で請求詳細を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<InvoiceDetail> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で請求詳細を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<InvoiceDetail> selectByExample(@Param("example") WhereCondition example);

    /**
     * 条件で請求詳細を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<InvoiceDetail> selectByInvoiceId(Integer InvoiceId);

    /**
     * 請求詳細を新規追加する。
     * @param record 請求詳細
     * @return 結果
     */
    int insertSelective(InvoiceDetail record);

    /**
     * 請求詳細を新規追加する。
     * @param record 請求詳細
     * @return 結果
     */
    int insert(InvoiceDetail record);

    /**
     * プライマリーキーで請求詳細を更新する。
     * @param record 請求詳細
     * @return 結果
     */
    int updateByPrimaryKey(InvoiceDetail record);

    /**
     * プライマリーキーで請求詳細を更新する。
     * @param record 請求詳細
     * @return 結果
     */
    int updateByPrimaryKeySelective(InvoiceDetail record);

    /**
     * 請求詳細を更新する。
     * @param record 請求詳細
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(InvoiceDetail record, @Param("example") WhereCondition example);

    /**
     * 請求詳細を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") InvoiceDetail example);

    /**
     * 請求詳細を削除する。
     * @param invoiceDetailId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer invoiceDetailId);
}
