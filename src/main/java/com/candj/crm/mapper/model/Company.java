package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Company {
    /** ユーザー */
    private java.util.List<AppUser> appUser;

    /** 会社ID */
    private Integer companyId;

    /** 会社区分 */
    private Byte companyType;

    /** 会社名 */
    private String companyName;

    /** 会社名（略称） */
    private String companyNameAbbr;

    /** 銀行 */
    private String companyBank;

    /** 銀行支店名 */
    private String companyBankName;

    /** 預金種類 */
    private String companyBankAccountType;

    /** 口座番号 */
    private Integer companyBankAccountNumber;

    /** 口座名義 */
    private String companyBankAccountName;

    private Byte salaryPayTime;

    /** 住所 */
    private String companyAdress;

    /** 郵便番号 */
    private String companyPostCode;

    /** 電話番号 */
    private String companyTelephoneNumber;

    /** FAX番号 */
    private String companyFaxNumber;

    /** ステータス */
    private boolean companyDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
