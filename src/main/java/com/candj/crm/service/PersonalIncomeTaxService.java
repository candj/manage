package com.candj.crm.service;

import com.candj.crm.dao.mapper.PersonalIncomeTaxDao;
import com.candj.crm.dto.PersonalIncomeTaxDto;
import com.candj.crm.mapper.model.PersonalIncomeTax;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class PersonalIncomeTaxService {

    @Autowired
    PersonalIncomeTaxDao personalIncomeTaxDao;

    @Autowired
    Mapper mapper;

    Comparator<PersonalIncomeTax> descWorkStartTimeComp = new Comparator<PersonalIncomeTax>() {
        @Override
        public int compare(PersonalIncomeTax t1, PersonalIncomeTax t2) {
            return t2.getPersonalIncomeTaxStartMonth().compareTo(t1.getPersonalIncomeTaxStartMonth());
        }
    };

    static List<String> personalIncomeTaxName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * プライマリーキーで税率、保険料率を検索する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public PersonalIncomeTaxDto selectByPrimaryKey(Integer personalIncomeTaxId) {
        //プライマリーキーで税率、保険料率を検索する。
        PersonalIncomeTax ret = personalIncomeTaxDao.selectByPrimaryKey(personalIncomeTaxId);
        return DozerHelper.map(mapper, ret, PersonalIncomeTaxDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public List<PersonalIncomeTaxDto> selectAllByPrimaryKey(Integer personalIncomeTaxId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<PersonalIncomeTax> ret = personalIncomeTaxDao.selectAllByPrimaryKey(personalIncomeTaxId);
        return DozerHelper.mapList(mapper, ret, PersonalIncomeTaxDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<PersonalIncomeTaxDto> selectByExample(PersonalIncomeTaxDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("personal_income_tax.personal_income_tax_id", dto.getPersonalIncomeTaxId())
                    .andEqualTo("personal_income_tax.personal_income_tax_excel_path", dto.getPersonalIncomeTaxExcelPath())
                    .andEqualTo("personal_income_tax.create_user", dto.getCreateUser())
                    .andEqualTo("personal_income_tax.update_user", dto.getUpdateUser())
                    .andEqualTo("personal_income_tax.create_time", dto.getCreateTime())
                    .andEqualTo("personal_income_tax.update_time", dto.getUpdateTime());
        }
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<PersonalIncomeTax> ret = personalIncomeTaxDao.selectByExample(whereCondition);
        ret.forEach(r -> {
            File file = new File(r.getPersonalIncomeTaxExcelPath());
            r.setPersonalIncomeTaxExcelName(file.getName());
        });
        return DozerHelper.mapList(mapper, ret, PersonalIncomeTaxDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<PersonalIncomeTaxDto> selectAllByExample(PersonalIncomeTaxDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("personal_income_tax.personal_income_tax_id", dto.getPersonalIncomeTaxId())
                    .andLessThanOrEqualTo("personal_income_tax.personal_income_tax_start_month", dto.getPersonalIncomeTaxStartMonth())
                    .andGreaterThanOrEqualTo("personal_income_tax.personal_income_tax_end_month", dto.getPersonalIncomeTaxEndMonth())
                    .andEqualTo("personal_income_tax.create_user", dto.getCreateUser())
                    .andEqualTo("personal_income_tax.update_user", dto.getUpdateUser())
                    .andEqualTo("personal_income_tax.create_time", dto.getCreateTime())
                    .andEqualTo("personal_income_tax.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("personal_income_tax.personal_income_tax_id", dto.getPersonalIncomeTaxId())
                    .andLessThanOrEqualTo("personal_income_tax.personal_income_tax_start_month", dto.getPersonalIncomeTaxStartMonth())
                    .andIsNull("personal_income_tax.personal_income_tax_end_month")
                    .andEqualTo("personal_income_tax.create_user", dto.getCreateUser())
                    .andEqualTo("personal_income_tax.update_user", dto.getUpdateUser())
                    .andEqualTo("personal_income_tax.create_time", dto.getCreateTime())
                    .andEqualTo("personal_income_tax.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("personal_income_tax.personal_income_tax_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<PersonalIncomeTax> ret = personalIncomeTaxDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, PersonalIncomeTaxDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(PersonalIncomeTaxDto dto) {
        PersonalIncomeTax personalIncomeTax  =  mapper.map(dto, PersonalIncomeTax.class);
        //税率、保険料率を新規追加する。
        int ret = personalIncomeTaxDao.insertSelective(personalIncomeTax);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(PersonalIncomeTaxDto dto) {
        PersonalIncomeTax personalIncomeTax  =  mapper.map(dto, PersonalIncomeTax.class);
        //税率、保険料率を新規追加する。
        int ret = personalIncomeTaxDao.insert(personalIncomeTax);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKey(PersonalIncomeTaxDto dto) {
        PersonalIncomeTax personalIncomeTax  =  mapper.map(dto, PersonalIncomeTax.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = personalIncomeTaxDao.updateByPrimaryKey(personalIncomeTax);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKeySelective(PersonalIncomeTaxDto dto) {
        PersonalIncomeTax personalIncomeTax  =  mapper.map(dto, PersonalIncomeTax.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = personalIncomeTaxDao.updateByPrimaryKeySelective(personalIncomeTax);
        return ret;
    }

    /**
     * 税率、保険料率を削除する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer personalIncomeTaxId) {
        //税率、保険料率を削除する。
        int ret = personalIncomeTaxDao.deleteByPrimaryKey(personalIncomeTaxId);
        return ret;
    }

    public void setPersonalIncomeTaxEndMonth(PersonalIncomeTaxDto personalIncomeTaxDto){
        WhereCondition whereCondition= new WhereCondition() ;
        List<PersonalIncomeTax> personalIncomeTaxList = personalIncomeTaxDao.selectByExample(whereCondition);
        personalIncomeTaxList.sort(descWorkStartTimeComp);
        DateTime lastEndMonth = null;
        for(PersonalIncomeTax s : personalIncomeTaxList){
            s.setPersonalIncomeTaxEndMonth(lastEndMonth == null? null : lastEndMonth);
            lastEndMonth = s.getPersonalIncomeTaxStartMonth().minusSeconds(1);
            personalIncomeTaxDao.updateByPrimaryKeySelective(s);
        }
    }

}
