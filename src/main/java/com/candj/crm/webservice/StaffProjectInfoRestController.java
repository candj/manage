package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.StaffProjectInfoDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.AppUserService;
import com.candj.crm.service.StaffProjectInfoService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * スタッフプロジェクト情報情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/staffProjectInfoRest")
@RestController
public class StaffProjectInfoRestController {

    @Autowired
    StaffProjectInfoService staffProjectInfoService;

    @Autowired
    AppUserService appUserService;

    /**
     * スタッフプロジェクト情報を新規追加する。
     *
     * @param staffProjectInfoId ID
     * @return 結果
     */
    @RequestMapping(value = "{staffProjectInfoId}", method = RequestMethod.GET)
    public StaffProjectInfoDto get(@PathVariable Integer staffProjectInfoId) {
        //プライマリーキーでスタッフプロジェクト情報を検索する。
        StaffProjectInfoDto ret = staffProjectInfoService.selectByPrimaryKey(staffProjectInfoId);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を新規追加する。
     *
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffProjectInfoDto staffProjectInfoDto, Errors rrrors) {
        //スタッフプロジェクト情報を新規追加する。
        int ret = staffProjectInfoService.insertSelective(staffProjectInfoDto);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を変更する。
     *
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffProjectInfoDto staffProjectInfoDto, Errors rrrors) {
        //プライマリーキーでスタッフプロジェクト情報を更新する。
        int ret = staffProjectInfoService.updateByPrimaryKeySelective(staffProjectInfoDto);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を新規追加する。
     *
     * @param staffProjectInfoId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{staffProjectInfoId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer staffProjectInfoId) {
        //スタッフプロジェクト情報を削除する。
        int ret = staffProjectInfoService.deleteByPrimaryKey(staffProjectInfoId);
        return ret;
    }

    /**
     * スタッフプロジェクト情報一覧画面を表示する。
     *
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param pageReq             改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffProjectInfoDto> list(StaffProjectInfoDto staffProjectInfoDto,
                                              @RequestParam(name = "userName", required = false) String userName,
                                              @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                              JqgridPageReq pageReq) {
        Page<StaffProjectInfoDto> page = PageUtil.startPage(pageReq);

        AppUser appUser = new AppUser();
        if(userName != null && !userName.equals("")) {
            appUser.setUserName(userName);
        }
        if(staffProjectInfoDto.getStaffInProject() != null) {
            if (staffProjectInfoDto.getStaffInProject().equals("")) {
                staffProjectInfoDto.setStaffInProject(null);
            }
        }
        staffProjectInfoDto.setAppUser(appUser);

        List<StaffProjectInfoDto> ret;

        if (staffProjectInfoDto.getStaffInProjectDateISNear() != null && !staffProjectInfoDto.getStaffInProjectDateISNear().isEmpty()) {
            if (staffProjectInfoDto.getStaffInProjectDateISNear().equals("00")) {
                DateTime time = new DateTime().plusMonths(1);
                staffProjectInfoDto.setProjectStartDate(time);
                ret = staffProjectInfoService.selectByMyExample1(staffProjectInfoDto);
            } else {
                DateTime time = new DateTime().plusMonths(1);
                staffProjectInfoDto.setProjectStartDate(time);
                ret = staffProjectInfoService.selectByMyExample2(staffProjectInfoDto);
            }
        }
//        else if (staffProjectInfoDto.getStaffInProject() != null && !staffProjectInfoDto.getStaffInProjectDateISNear().isEmpty()){
//            ret = staffProjectInfoService.selectAllByMyExample(staffProjectInfoDto);
//        }
        else {
            ret = staffProjectInfoService.selectAllByExample(staffProjectInfoDto);
        }

        return PageUtil.resp(page);
    }

    /**
     * スタッフプロジェクト情報一覧画面を表示する。
     *
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param pageReq             改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffProjectInfoDto> listAll(StaffProjectInfoDto staffProjectInfoDto,
                                                 JqgridPageReq pageReq) {
        Page<StaffProjectInfoDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフプロジェクト情報を検索する。（連携情報含む）
        List<StaffProjectInfoDto> ret = staffProjectInfoService.selectAllByExample(staffProjectInfoDto);
        return PageUtil.resp(page);
    }
}
