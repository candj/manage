package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.Invoice;
import com.candj.crm.mapper.model.Ordering;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 請求情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface InvoiceDao {
    /**
     * プライマリーキーで請求を検索する。
     * @param invoiceId 請求ID
     * @return 結果
     */
    Invoice selectByPrimaryKey(Integer invoiceId);

    /**
     * プライマリーキーで請求検索する。（連携情報含む）
     * @param invoiceId 請求ID
     * @return 結果
     */
    List<Invoice> selectAllByPrimaryKey(Integer invoiceId);

    /**
     * 条件で請求を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Invoice> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で請求を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Invoice> selectAllByMyExample(@Param("example") WhereCondition example);

    /**
     * 条件で請求を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Invoice> selectByExample(@Param("example") WhereCondition example);

    /**
     * 請求を新規追加する。
     * @param record 請求
     * @return 結果
     */
    int insertSelective(Invoice record);

    /**
     * 請求を新規追加する。
     * @param record 請求
     * @return 結果
     */
    int insert(Invoice record);

    /**
     * プライマリーキーで請求を更新する。
     * @param record 請求
     * @return 結果
     */
    int updateByPrimaryKey(Invoice record);

    /**
     * プライマリーキーで請求を更新する。
     * @param record 請求
     * @return 結果
     */
    int updateByPrimaryKeySelective(Invoice record);

    /**
     * 請求を更新する。
     * @param record 請求
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Invoice record, @Param("example") WhereCondition example);

    /**
     * 請求を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Invoice example);

    /**
     * 請求を削除する。
     * @param invoiceId 請求ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer invoiceId);

    /**
     * 見積IDで注文検索する。（連携情報含む）
     * @param orderId 見積 ID
     * @return 結果
     */
    List<Invoice> selectByOrderId(Integer orderId);
}
