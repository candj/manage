package com.candj.crm.core.config;

import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.candj.webpower.web.core.convert.DateTimeStringConverter;
import com.candj.webpower.web.core.convert.StringDateTimeConverter;
import com.candj.webpower.web.core.json.JodaSerializers;
import com.candj.webpower.web.core.security.PrivacyConfig;
import com.candj.webpower.web.core.util.LogUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * MVCコンフィグ.
 */
@Configuration
@Slf4j
@SuppressWarnings({ "unused" })
public class MvcConfig extends WebMvcConfigurationSupport {

	/** basename . */
	@Value("${spring.messages.basename}")
	private String[] basename;
	/** encoding . */
	@Value("${spring.messages.encoding}")
	private String encoding;

	/**
	 * Httpメッセージコンバータを設定します.
	 * @param converters コンバータ
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(converter());
        converters.add(new StringHttpMessageConverter(StandardCharsets.UTF_8));
		addDefaultHttpMessageConverters(converters);
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new StringDateTimeConverter());
		registry.addConverter(new DateTimeStringConverter());

	}

	/**
	 * @return HttpMessageConverter
	 */
	//@Bean
	public HttpMessageConverter<?> converter() {
		ObjectMapper objectMapper = Jackson2ObjectMapperBuilder
				.json()
				//.propertyNamingStrategy(new PropertyNamingStrategy.SnakeCaseStrategy())
				.serializerByType(DateTime.class, new JodaSerializers.JodaDateTimeSerializer())
				.deserializerByType(DateTime.class, new JodaSerializers.JodaDateTimeDeserializer())
				.serializerByType(LocalDateTime.class, new JodaSerializers.JodaLocalDateTimeSerializer())
				.deserializerByType(LocalDateTime.class, new JodaSerializers.JodaLocalDateTimeDeserializer())
				.serializerByType(LocalDate.class, new JodaSerializers.JodaLocalDateSerializer())
				.deserializerByType(LocalDate.class, new JodaSerializers.JodaLocalDateDeserializer())
				.build();
		return new MappingJackson2HttpMessageConverter(objectMapper);
	}

	/** @return フィルタ */
	@Bean
	public FilterRegistrationBean<?> corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin(CorsConfiguration.ALL);
		config.addAllowedHeader(CorsConfiguration.ALL);
		// ブラウザからのX-HTTP-REQUESTを受け付ける場合はpre-flightのためにOPTIONSメソッドを許可しなくてはいけない
		config.addAllowedMethod(HttpMethod.GET);
		config.addAllowedMethod(HttpMethod.HEAD);
		config.addAllowedMethod(HttpMethod.POST);
		source.registerCorsConfiguration("/**", config);
		FilterRegistrationBean<?> bean = new FilterRegistrationBean<>(new CorsFilter(source));

		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

	/**
	 * ValidationメッセージをUTF-8で設定します.
	 */
	@Override
	public Validator getValidator() {
		return validator();
	}

	/**
	 * @return {@link LocalValidatorFactoryBean}.
	 */
	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		validator.setValidationMessageSource(this.messageSource());
		return validator;
	}

	/**
	 * @return {@link MessageSource}.
	 */
	private MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(this.basename);
		messageSource.setDefaultEncoding(this.encoding);
		return messageSource;
	}

	/**
	 * 運用ログ情報を設定します.
	 */
	@PostConstruct
	public void setLogConfig() {
		LogUtil.setLogConfiguration(this.logConfig());
	}

	/**
	 * @return {@link LogConfig}.
	 */
	@Bean
	public LogConfig logConfig() {
		return new LogConfig();
	}

	/**
	 * @return Privacy config
	 */
	@Bean
	public PrivacyConfig privacyConfig() {
		return new PrivacyConfig();
	}

	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
			"classpath:/META-INF/resources/", "classpath:/resources/",
			"classpath:/static/", "classpath:/public/" };

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**")
				.addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}





}
