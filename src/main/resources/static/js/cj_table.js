;(function (factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory)
  } else {
    factory(jQuery)
  }
})(function ($) {
  $.extend($.fn, {
    cjtable: function (options) {
      if (!this.length) {
        if (options && options.debug && window.console) {
          console.warn("Nothing selected, can't xctable, returning nothing.")
        }
        return
      }

      return $.cjtable(options, this)
    },
  })

  $.cjtable = function (options, table) {
    swal = top.swal
    toast = top.toastr
    layer = top.layer

    var defaults = {
        layer: {},
        add: '.btn-add',
        delete: '.btn-delete',
        update: '.btn-update',
        pull: '.btn-pull',
        postingIcon: 'fa fa-refresh fa-spin fa-fw margin-bottom',
        deleteConfirm: 'データを削除してもよろしいですか?',
      },
      init = function () {
        var pagerId = `#${table[0].id}_pager`
        // tableにadd,delete,updateボタンを追加する
        // ツイルボタンを作成します
    	if (defaults.opts.detailUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '詳細',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: ondetail,
              position: 'first',
            })
        }
        if (defaults.opts.addOfferUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '見積',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onaddoffer,
              position: 'first',
            })
        }
        if (defaults.opts.addOrderUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '注文',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onaddorder,
              position: 'first',
            })
        }
        if (defaults.opts.addInvoiceUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '請求',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onaddinvoice,
              position: 'first',
            })
        }
        if (defaults.opts.addWorkUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '作業実際登録',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onaddwork,
              position: 'first',
            })
        }
        if (defaults.opts.copyUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: 'コピー',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onCopy,
              position: 'first',
            })
        }
        if (defaults.opts.deleteUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '削除',
            buttonicon: 'glyphicon-trash ui-icon-delete',
            onClickButton: ondelete,
            position: 'first',
          })
        }
        if (defaults.opts.editUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '編集',
            buttonicon: 'glyphicon-edit ui-icon-edit',
            onClickButton: onupdate,
            position: 'first',
          })
        }
        if (defaults.opts.addSalaryUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '追加',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: onaddsalary,
              position: 'first',
            })
        }
        if (defaults.opts.addUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '追加',
            buttonicon: 'glyphicon-plus ui-icon-add',
            onClickButton: onadd,
            position: 'first',
          })
        }
        return false
      },
      getKeyValue = function () {
        var rowid = table.jqGrid('getGridParam', 'selrow')
        var key = table.data('key')
        if (!key) {
          layer.msg('主キーを設置をしてください。')
          return null
        }
        if (!rowid) {
          layer.msg('データを選択してください。')
          return null
        }

        var rowData = table.jqGrid('getRowData', rowid)
        var vals = {}
        var keys = key.split(',')
        for (var i = 0; i < keys.length; i++) {
          vals[keys[i]] = rowData[keys[i]]
        }
        if(rowData['salaryMonth'] != null){
          vals['yearMonth'] = rowData['salaryMonth']
        }
        return vals
      },

      getValue = function () {
              var rowid = table.jqGrid('getGridParam', 'selrow')
              var key = table.data('key')
              if (!key) {
                layer.msg('主キーを設置をしてください。')
                return null
              }
              if (!rowid) {
                layer.msg('データを選択してください。')
                return null
              }

              var rowData = table.jqGrid('getRowData', rowid)
              var vals = {}
              var keys = key.split(',')
              for (var i = 0; i < keys.length; i++) {
                vals[keys[i]] = rowData[keys[i]]
              }
              if(rowData['salaryMonth'] != null){
                vals['yearMonth'] = rowData['salaryMonth']
              }
              return vals
      },
      getMonth = function () {
              var rowid = table.jqGrid('getGridParam', 'selrow')
              var key = table.data('key')

              var rowData = table.jqGrid('getRowData', rowid)
              if(rowData.length > 1){
                var data = rowData[0]
              } else{
                var data = rowData
              }
              var vals = {}
              if(data['salaryMonth'] != null){
                vals['yearMonth'] = data['salaryMonth']
              }
              return vals
      },
      openLayer = function (url) {
        var width = defaults.layer.width || '900px'
        var height = defaults.layer.height || '90%'
        layer.open({
          type: 2,
          title: ' ',
          shadeClose: false,
          shade: 0.6,
          area: [width, height],
          content: [url],
        })
      },
      resetSubmit = function (btn) {
        btn.removeAttr('disabled')
        return false
      },
      disabledSubmit = function (btn) {
        btn.attr('disabled', 'disabled')
        return false
      },
      del = function (options) {
        var ld,
          layer = top.layer
        $.ajax({
          url: options.url,
          type: 'DELETE',
          data: options.data,
          beforeSend: function () {
            ld = layer.load(3)
            disabledSubmit(options.target)
          },
          success: function (response) {
            if (top.toastr)
              top.toastr.success(
                null,
                '操作完了しました。'
              )
              
              swal("削除しました", "", "success");

            $('.glyphicon-refresh').click()
          },
          error: function (e) {
            var message = null
            var res = e.responseJSON
            if (res) {
              message = res.message || '処理を失敗しました。'
              if (top.swal) top.swal(message, '', 'warning')
            } else {
              message = 'システムエラーが発生しました。'
              if (top.swal) top.swal(message, '', 'error')
            }
          },
          complete: function () {
            layer.close(ld)
            resetSubmit(options.target)
          },
        })
        return false
      },
      onadd = function () {
        openLayer(defaults.basepath + defaults.opts.addUrl)
        return false
      },
      onaddsalary = function () {
              var url = defaults.basepath + defaults.opts.addSalaryUrl
              var data = getMonth()
              if (!data) {
                return false
              }

              var params = []
              $.each(data, function (n, v) {
                params.push(v)
              })

              url = url + '/' + params.join('/')
              openLayer(url)
              return false
      },
      onupdate = function () {
        var url = defaults.basepath + defaults.opts.editUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },

      onCopy = function () {
        var url = defaults.basepath + defaults.opts.copyUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },

      ondetail = function () {
           var url = defaults.basepath + defaults.opts.detailUrl
                  var data = getKeyValue()
                  if (!data) {
                    return false
                  }

                  var params = []
                  $.each(data, function (n, v) {
                    params.push(v)
                  })

                  url = url + '/' + params.join('/')
                  openLayer(url)
                  return false
      },
      onaddoffer = function () {
        var url = defaults.basepath + defaults.opts.addOfferUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },
      onaddorder = function () {
        var url = defaults.basepath + defaults.opts.addOrderUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },

      onaddinvoice = function () {
        var url = defaults.basepath + defaults.opts.addInvoiceUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },
      onaddwork = function () {
        var url = defaults.basepath + defaults.opts.addWorkUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },
      ondelete = function () {
        var url = defaults.basepath + defaults.opts.deleteUrl
        var confirm = defaults.deleteConfirm
        var txt = '削除してもよろしいですか。'

        var data = getKeyValue()
        if (!data) {
          return false
        }
        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })
        url = url + '/' + params.join('/')
        swal(
          {
            title: confirm,
            text: txt,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル',
            closeOnConfirm: false,
          },
          function (isConfirm) {
            if (isConfirm) {
              del({
                target: top.$('.showSweetAlert').find('.confirm'),
                url: url
              })
            }
          }
        )

        return false
      }

    $.extend(defaults, options)

    init();
  }
})


$(document).ready(function() {

	$('#searchForGrid').click(function(){		
		var submitData = $(this).closest("form").serializeJSON();
		var gridId = $(this).data("grid-id");
		$("#"+ gridId).setGridParam({
			postData:JSON.parse(submitData)
		}).trigger("reloadGrid");  
		
		return false;
	});
	
})