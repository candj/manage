

			var colNames = [
				"支払いサイトID",
				"名称",
			];
			var colModel = [
				{
					name: "paySiteId",
					index: "paySiteId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "paySiteName",
					index: "paySiteName",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	