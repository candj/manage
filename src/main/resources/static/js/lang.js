var cjMessage = {
		staffCodeExists:"スタッフコードがすでに存在しています。",
		staffEncryptCodeExists:"スタッフバーコードすでに存在しています。",
		staffCodeNoExists:"スタッフ情報を見つかりませんでした、正確なコードを入力してください。",
		businessCodeNoExists:"業務情報を見つかりませんでした、正確なコードを入力してください。",
		businessCodeExists:"業務コードがすでに存在しています。",
		businessEncryptCodeExists:"業務バーコードすでに存在しています。",
		startWorkExists:"本日にすでに出勤しましたが、正確なバーコードをスキャンしてください。",
		leavingWorkExists:"本日にすでに退勤しましたが、正確なバーコードをスキャンしてください。",
		startWorkTimeSameAs:"開始時間は重ねてしましたが、正確な時間を入力してください。"
}