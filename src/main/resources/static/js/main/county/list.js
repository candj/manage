

			var colNames = [
				"都道府县ID",
				"都道府县名称",
				"地区",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "countyId",
					index: "countyId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "countyName",
					index: "countyName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "countyAreaId",
					index: "countyAreaId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	