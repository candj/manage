package com.candj.crm.service;

import com.candj.crm.dao.mapper.OfferDetailDao;
import com.candj.crm.dao.mapper.OfferDetailTemporaryDao;
import com.candj.crm.dto.OfferDetailDto;
import com.candj.crm.dto.OfferDetailForWorkDto;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 見積詳細 情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class OfferDetailTemporaryService {

    @Autowired
    OfferDetailTemporaryDao offerDetailTemporaryDao;

    @Autowired
    OfferDetailDao offerDetailDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで見積詳細 を検索する。
     * @param offerDetailId
     * @return 結果
     */
    public OfferDetailDto selectByPrimaryKey(Integer offerDetailId) {
        //プライマリーキーで見積詳細 を検索する。
        OfferDetail ret = offerDetailTemporaryDao.selectByPrimaryKey(offerDetailId);
        return DozerHelper.map(mapper, ret, OfferDetailDto.class);
    }

    /**
     * プライマリーキーで見積詳細 を検索する。（連携情報含む）
     * @param offerDetailId
     * @return 結果
     */
    public List<OfferDetailDto> selectAllByPrimaryKey(Integer offerDetailId) {
        //プライマリーキーで見積詳細 検索する。（連携情報含む）
        List<OfferDetail> ret = offerDetailTemporaryDao.selectAllByPrimaryKey(offerDetailId);
        return DozerHelper.mapList(mapper, ret, OfferDetailDto.class);
    }

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OfferDetailDto> selectByExample(OfferDetailDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("offer_detail.offer_detail_id", dto.getOfferDetailId())
            .andEqualTo("offer_detail.offer_id", dto.getOfferId())
            .andGreaterThanOrEqualTo("offer_detail.work_start_date", dto.getWorkStartDate())//andGreaterThanOrEqualTo
            .andLessThanOrEqualTo("offer_detail.work_end_date", dto.getWorkEndDate())
            .andEqualTo("offer_detail.user_id", dto.getUserId())
            .andEqualTo("offer_detail.work_name", dto.getWorkName())
            .andEqualTo("offer_detail.amount", dto.getAmount())
            .andEqualTo("offer_detail.man_hour", dto.getManHour())
            .andEqualTo("offer_detail.total_amount", dto.getTotalAmount())
            .andEqualTo("offer_detail.create_user", dto.getCreateUser())
            .andEqualTo("offer_detail.update_user", dto.getUpdateUser())
            .andEqualTo("offer_detail.create_time", dto.getCreateTime())
            .andEqualTo("offer_detail.update_time", dto.getUpdateTime());
        }
        //条件で見積詳細 を検索する。（連携情報含む）)
        List<OfferDetail> ret = offerDetailTemporaryDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OfferDetailDto.class);
    }

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）
     * @param offerId 検索条件
     * @return 結果
     */
    public List<OfferDetailDto> selectByOfferId(Integer offerId) {
        //条件で見積詳細 を検索する。（連携情報含む）)
        List<OfferDetail> ret = offerDetailTemporaryDao.selectByOfferId(offerId);
        return DozerHelper.mapList(mapper, ret, OfferDetailDto.class);
    }

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OfferDetailDto> selectAllByExample(OfferDetailDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("offer_detail.offer_detail_id", dto.getOfferDetailId())
            .andEqualTo("offer_detail.offer_id", dto.getOfferId())
            .andEqualTo("offer_detail.work_start_date", dto.getWorkStartDate())
            .andEqualTo("offer_detail.work_end_date", dto.getWorkEndDate())
            .andEqualTo("offer_detail.user_id", dto.getUserId())
            .andEqualTo("offer_detail.work_name", dto.getWorkName())
            .andEqualTo("offer_detail.amount", dto.getAmount())
            .andEqualTo("offer_detail.man_hour", dto.getManHour())
            .andEqualTo("offer_detail.total_amount", dto.getTotalAmount())
            .andEqualTo("offer_detail.create_user", dto.getCreateUser())
            .andEqualTo("offer_detail.update_user", dto.getUpdateUser())
            .andEqualTo("offer_detail.create_time", dto.getCreateTime())
            .andEqualTo("offer_detail.update_time", dto.getUpdateTime());
        }
        //条件で見積詳細 を検索する。（連携情報含む）)
        List<OfferDetail> ret = offerDetailTemporaryDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OfferDetailDto.class);
    }

    /**
     * 見積詳細 を新規追加する。
     * @param dto 見積詳細 
     * @return 結果
     */
    public int insertSelective(OfferDetailDto dto) {
        OfferDetail offerDetail  =  mapper.map(dto, OfferDetail.class);
        //見積詳細 を新規追加する。
        int ret = offerDetailTemporaryDao.insertSelective(offerDetail);
        return ret;
    }

    /**
     * 見積詳細 を新規追加する。
     * @param dto 見積詳細 
     * @return 結果
     */
    public int insert(OfferDetailDto dto) {
        OfferDetail offerDetail  =  mapper.map(dto, OfferDetail.class);
        //見積詳細 を新規追加する。
        int ret = offerDetailTemporaryDao.insert(offerDetail);
        return ret;
    }

    /**
     * プライマリーキーで見積詳細 を更新する。
     * @param dto 見積詳細 
     * @return 結果
     */
    public int updateByPrimaryKey(OfferDetailDto dto) {
        OfferDetail offerDetail  =  mapper.map(dto, OfferDetail.class);
        //プライマリーキーで見積詳細 を更新する。
        int ret = offerDetailTemporaryDao.updateByPrimaryKey(offerDetail);
        return ret;
    }

    /**
     * プライマリーキーで見積詳細 を更新する。
     * @param dto 見積詳細 
     * @return 結果
     */
    public int updateByPrimaryKeySelective(OfferDetailDto dto) {
        OfferDetail offerDetail  =  mapper.map(dto, OfferDetail.class);
        //プライマリーキーで見積詳細 を更新する。
        int ret = offerDetailTemporaryDao.updateByPrimaryKeySelective(offerDetail);
        return ret;
    }

    /**
     * 見積詳細 を削除する。
     * @param offerDetailId
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer offerDetailId) {
        //見積詳細 を削除する。
        int ret = offerDetailTemporaryDao.deleteByPrimaryKey(offerDetailId);
        return ret;
    }

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）
     * @param offerId 検索条件
     * @return 結果
     */
    public List<OfferDetail> selectByStaffAndMonth(OfferDetailForWorkDto offerDetailForWorkDto) {
        //条件で見積詳細 を検索する。（連携情報含む）)
        List<OfferDetail> ret = offerDetailTemporaryDao.selectByStaffAndMonth(offerDetailForWorkDto);
        return ret;
    }
}
