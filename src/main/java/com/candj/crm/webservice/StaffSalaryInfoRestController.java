package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.StaffSalaryInfoDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.StaffSalaryInfoService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * スタッフ基本給料情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/staffSalaryInfoRest")
@RestController
public class StaffSalaryInfoRestController {
    @Autowired
    StaffSalaryInfoService staffSalaryInfoService;

    /**
     * スタッフ基本給料を新規追加する。
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    @RequestMapping(value = "{staffSalaryInfoId}", method = RequestMethod.GET)
    public StaffSalaryInfoDto get(@PathVariable Integer staffSalaryInfoId) {
        //プライマリーキーでスタッフ基本給料を検索する。
        StaffSalaryInfoDto ret = staffSalaryInfoService.selectByPrimaryKey(staffSalaryInfoId);
        return ret;
    }

    /**
     * スタッフ基本給料を新規追加する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffSalaryInfoDto staffSalaryInfoDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(staffSalaryInfoDto.getYearMonth());
        staffSalaryInfoDto.setSalaryStartDate(month);
        //スタッフ基本給料を新規追加する。
        int ret = staffSalaryInfoService.insertSelective(staffSalaryInfoDto);
        staffSalaryInfoService.setTaxEndMonth(staffSalaryInfoDto.getUserId());
        return ret;
    }

    /**
     * スタッフ基本給料を変更する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffSalaryInfoDto staffSalaryInfoDto, Errors rrrors) {
        //プライマリーキーでスタッフ基本給料を更新する。
        int ret = staffSalaryInfoService.updateByPrimaryKeySelective(staffSalaryInfoDto);
        return ret;
    }

    /**
     * スタッフ基本給料を新規追加する。
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{staffSalaryInfoId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer staffSalaryInfoId) {
        //スタッフ基本給料を削除する。
        int ret = staffSalaryInfoService.deleteByPrimaryKey(staffSalaryInfoId);
        return ret;
    }

    /**
     * スタッフ基本給料一覧画面を表示する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffSalaryInfoDto> list(StaffSalaryInfoDto staffSalaryInfoDto,
                                             @RequestParam(name = "userName", required = false) String userName,
                                             JqgridPageReq pageReq) {
        Page<StaffSalaryInfoDto> page = PageUtil.startPage(pageReq);

        AppUser appUser = new AppUser();
        appUser.setUserName(userName);

        staffSalaryInfoDto.setAppUser(appUser);
        //条件でスタッフ基本給料を検索する。（連携情報含む）
        List<StaffSalaryInfoDto> ret = staffSalaryInfoService.selectAllByExample(staffSalaryInfoDto);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ基本給料一覧画面を表示する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffSalaryInfoDto> listAll(StaffSalaryInfoDto staffSalaryInfoDto, JqgridPageReq pageReq) {
        Page<StaffSalaryInfoDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ基本給料を検索する。（連携情報含む）
        List<StaffSalaryInfoDto> ret = staffSalaryInfoService.selectAllByExample(staffSalaryInfoDto);
        return PageUtil.resp(page);
    }
}
