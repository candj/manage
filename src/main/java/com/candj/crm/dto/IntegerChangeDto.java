package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntegerChangeDto {

    private Integer a;

    private Integer b;

}
