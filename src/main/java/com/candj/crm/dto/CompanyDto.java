package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class CompanyDto {
    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** ユーザー */
    private java.util.List<AppUserDto> appUser;

    /** 会社区分 */
    @Digits(integer=3, fraction=0)
    private Byte companyType;

    /** 会社名 */
    @Length(max=128)
    private String companyName;

    /** 会社名（略称） */
    @Length(max=128)
    private String companyNameAbbr;


    @Digits(integer=3, fraction=0)
    private Byte salaryPayTime;

    /** 銀行 */
    @Length(max=64)
    private String companyBank;

    /** 銀行支店名 */
    @Length(max=64)
    private String companyBankName;

    /** 預金種類 */
    @Length(max=64)
    private String companyBankAccountType;

    /** 口座番号 */
    @Digits(integer=10, fraction=0)
    private Integer companyBankAccountNumber;

    /** 口座名義 */
    @Length(max=64)
    private String companyBankAccountName;

    /** 住所 */
    @Length(max=255)
    private String companyAdress;

    /** 郵便番号 */
    @Length(max=64)
    private String companyPostCode;

    /** 電話番号 */
    @Length(max=64)
    private String companyTelephoneNumber;

    /** FAX番号 */
    @Length(max=64)
    private String companyFaxNumber;

    /** ステータス */
    private boolean companyDeleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
