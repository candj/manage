package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUser {

    /** 請求詳細 */
    private java.util.List<InvoiceDetail> invoiceDetail;

    /** 年月 */
    private org.joda.time.DateTime month;

    /** 作業実際 */
    private java.util.List<Work> work;

    /** 給料 */
    private java.util.List<Salary> salaryList;

    /** 給料 */
    private Salary salary;

    /** ユーザーID */
    private Integer userId;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer companyId;

    /** タイプ */
    private Integer userType;

    /** 名前 */
    private String userName;

    /** 性別 */
    private String userSex;

    /** 生年月日 */
    private org.joda.time.DateTime birthDate;

    /** 部門 */
    private String userDepartment;

    /** イーメールアドレス */
    private String userEmail;

    /** 電話番号 */
    private String userTelephoneNumber;

    /** 入社日時 */
    private org.joda.time.DateTime employmentDate;

    /** 退社日付 */
    private org.joda.time.DateTime leavingDate;

    /** 在職状態 */
    private String employmentStatus;

    /** 基本給料 */
    private Integer userBaseSalary;

    /** パスワード */
    private String password;

    /** ゲスト種類 */
    private String guestType;

    /** 作業登録マック */
    private boolean register;

    /** ステータス */
    private boolean userDeleteFlag;

    private boolean playPayDateISNear;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
