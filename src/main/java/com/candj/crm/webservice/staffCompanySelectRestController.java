package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.StaffSalaryInfo;
import com.candj.crm.service.*;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class staffCompanySelectRestController {

    @Autowired
    CompanyService companyService;

    @Autowired
    AppUserService appUserService;

    @Autowired
    StaffSalaryInfoService staffSalaryInfoService;

    @Autowired
    OfferService offerService;

    @Autowired
    OfferTemporaryService offerTemporaryService;

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "staff/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserType(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "staff/edit/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserTypeForEdit(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "offerDetail/insert/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserTypeForOfferDetail(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param companyId 会社ID
     * @return 結果
     */
    @RequestMapping(value = "offerDetail/insert/selectStaff", method = RequestMethod.POST)
    public Object selectStaffByCompanyForOfferDetail(Integer companyId) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto dto = new AppUserDto();
        List<AppUserDto> appUserDtoList = new ArrayList<AppUserDto>();
        if(companyId == -1) {
            appUserDtoList = appUserService.selectNoCompanyStaff();
        }
        else{
            dto.setCompanyId(companyId);
            appUserDtoList = appUserService.selectStaffByExample(dto);
        }

        return appUserDtoList;
    }

    /**
     *
     * @param userId スタッフID
     * @return 結果
     */
    @RequestMapping(value = "offerDetail/insert/staffSalary", method = RequestMethod.POST)
    public Object staffSalary(Integer userId, Integer offerId, DateTime date) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto appUserDto = appUserService.selectByPrimaryKey(userId);
        OfferDto offerDto = offerService.selectAllByPrimaryKey(offerId).get(0);
        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        if(offerDto.getReceiveCompany().getReceiveCompanyType() == 0) {
            if (appUserDto.getUserType() == 3 || appUserDto.getUserType() == 4) {
                StaffSalaryInfoDto dto = new StaffSalaryInfoDto();
                dto.setUserId(userId);
                dto.setSalaryStartDate(date);
                staffSalaryInfoDto = staffSalaryInfoService.selectByExample(dto).get(0);
            }
        }

        return staffSalaryInfoDto;
    }

    /**
     *
     * @param userId スタッフID
     * @return 結果
     */
    @RequestMapping(value = "offerDetail/insert/staffSalaryForInsert", method = RequestMethod.POST)
    public Object staffSalaryForInsert(Integer userId, DateTime date) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto appUserDto = appUserService.selectByPrimaryKey(userId);
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        CompanyDto companyDto = companyService.selectByPrimaryKey(offerDto.getReceiveCompanyId());
        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        if(companyDto.getCompanyType() == 0) {
            if (appUserDto.getUserType() == 3 || appUserDto.getUserType() == 4) {
                StaffSalaryInfoDto dto = new StaffSalaryInfoDto();
                dto.setUserId(userId);
                dto.setSalaryStartDate(date);
                staffSalaryInfoDto = staffSalaryInfoService.selectByExample(dto).get(0);
            }
        }

        return staffSalaryInfoDto;
    }

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "staffSalaryInfo/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserTypeForStaffInfo(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param companyId 会社ID
     * @return 結果
     */
    @RequestMapping(value = "staffSalaryInfo/selectStaff", method = RequestMethod.POST)
    public Object selectStaffByCompanyForStaffInfo(Integer companyId) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto dto = new AppUserDto();
        List<AppUserDto> appUserDtoList = new ArrayList<AppUserDto>();
        if(companyId == -1) {
            appUserDtoList = appUserService.selectNoCompanyStaff();
        }
        else{
            dto.setCompanyId(companyId);
            appUserDtoList = appUserService.selectStaffByExample(dto);
        }

        return appUserDtoList;
    }

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "work/insert/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserTypeForWork(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param companyId 会社ID
     * @return 結果
     */
    @RequestMapping(value = "work/insert/selectStaff", method = RequestMethod.POST)
    public Object selectStaffByCompanyForWork(Integer companyId) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto dto = new AppUserDto();
        List<AppUserDto> appUserDtoList = new ArrayList<AppUserDto>();
        if(companyId == -1) {
            appUserDtoList = appUserService.selectNoCompanyStaff();
        }
        else{
            dto.setCompanyId(companyId);
            appUserDtoList = appUserService.selectStaffByExample(dto);
        }

        return appUserDtoList;
    }

    /**
     *
     * @param userType スタッフ類別
     * @return 結果
     */
    @RequestMapping(value = "commute/insert/selectCompany", method = RequestMethod.POST)
    public Object selectCompanyByUserTypeForCommute(Integer userType) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        CompanyDto dto = new CompanyDto();
        List<CompanyDto> companyDtoList;
        if(userType == 1) {
            companyDtoList = companyService.selectThisCompany(dto);
        }
        else if(userType == 3){
            companyDtoList = companyService.selectOtherCompany(dto);
        }
        else{
            companyDtoList = null;
        }
        return companyDtoList;
    }

    /**
     *
     * @param companyId 会社ID
     * @return 結果
     */
    @RequestMapping(value = "commute/insert/selectStaff", method = RequestMethod.POST)
    public Object selectStaffByCompanyForCommute(Integer companyId) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        AppUserDto dto = new AppUserDto();
        List<AppUserDto> appUserDtoList = new ArrayList<AppUserDto>();
        if(companyId == -1) {
            appUserDtoList = appUserService.selectNoCompanyStaff();
        }
        else{
            dto.setCompanyId(companyId);
            appUserDtoList = appUserService.selectStaffByExample(dto);
        }

        return appUserDtoList;
    }

}
