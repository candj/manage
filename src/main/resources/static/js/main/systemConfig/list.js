

			var colNames = [
				"系统设置ID",
				"系统设置名称",
				"系统设置种别",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "systemConfigId",
					index: "systemConfigId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "systemConfigName",
					index: "systemConfigName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "systemConfigKind",
					index: "systemConfigKind",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	