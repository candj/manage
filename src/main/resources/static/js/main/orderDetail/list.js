

			var colNames = [
				"${column.colName}",
				"見積ID",
				"作業員",
				"業務名",
				"単価(円)",
				"工数(人月)",
				"合計金額(円)",
			];
			var colModel = [
				{
					name: "offerDetailId",
					index: "offerDetailId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "offerId",
					index: "offerId",
					width: 100,
					search : false,
					hidden: true,
					align: "right",
				},
				{
					name: "appUser.userName",
					index: "appUser.userName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "workName",
					index: "workName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "amount",
					index: "amount",
					width: 100,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "manHour",
					index: "manHour",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "totalAmount",
					index: "totalAmount",
					width: 100,
					search : false,
					formatter : "integer",

					align: "right",
				},
			];

var postData = {
	offerId : $('#offerId').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});