

			var colNames = [
				"税率ID",
				"適用年月開始時間",
				"税率(個人分)(%)",
				"税率(会社分)(%)",
			];
			var colModel = [
				{
					name: "labourInsuranceId",
					index: "labourInsuranceId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
                {
                    name: "labourInsuranceStartMonth",
                    index: "labourInsuranceStartMonth",
                    width: 100,
                    search : false,
                    formatter : function(cellvalue, options, rowObject) {
                        if (cellvalue != null) {
                            var date = new Date(cellvalue);
                            return date.Format('yyyy-MM');
                        } else {
                            return '';
                        }
                    },
                    align: "left",
                },
				{
                	name: "labourInsuranceRatePersonal",
                	index: "labourInsuranceRatePersonal",
                	width: 100,
                	search : false,
                	align: "left",
                },
                {
                	name: "labourInsuranceRateCompany",
                	index: "labourInsuranceRateCompany",
                	width: 100,
                	search : false,
                	align: "left",
                },
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	