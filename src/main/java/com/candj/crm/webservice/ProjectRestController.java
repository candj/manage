package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.DateUtil;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * プロジェクト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/projectRest")
@RestController
public class ProjectRestController {

    @Autowired
    ProjectService projectService;

    @Autowired
    OfferService offerService;

    @Autowired
    OrderingService orderingService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    CompanyService companyService;

    /**
     * プロジェクトを新規追加する。
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    @RequestMapping(value = "{projectId}", method = RequestMethod.GET)
    public ProjectDto get(@PathVariable Integer projectId) {
        //プライマリーキーでプロジェクトを検索する。
        ProjectDto ret = projectService.selectByPrimaryKey(projectId);
        return ret;
    }

    /**
     * プロジェクトを新規追加する。
     *
     * @param projectDto プロジェクト
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody ProjectDto projectDto, Errors rrrors) {
        projectDto.setProjectDeleteFlag(true);
        //プロジェクトを新規追加する。
        int ret = projectService.insertSelective(projectDto);
        return ret;
    }

    /**
     * プロジェクトを変更する。
     *
     * @param projectDto プロジェクト
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody ProjectDto projectDto, Errors rrrors) {
        projectDto.setProjectDeleteFlag(true);
        //プライマリーキーでプロジェクトを更新する。
        int ret = projectService.updateByPrimaryKeySelective(projectDto);
        return ret;
    }

    /**
     * プロジェクトを新規追加する。
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{projectId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer projectId) {
        ProjectDto projectDto = projectService.selectByPrimaryKey(projectId);

        //連携削除

        projectDto.setProjectDeleteFlag(false);
        //プロジェクトを削除する。
        int ret = projectService.updateByPrimaryKeySelective(projectDto);
        return ret;
    }

    /**
     * プロジェクト一覧画面を表示する。
     *
     * @param projectDto プロジェクト
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<ProjectDto> list(ProjectDto projectDto,
                                     @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                     JqgridPageReq pageReq) {

        DateTime startOfMonth = new DateTime();
        if (yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);

        if (projectDto.getSendCompanyName() != null && !projectDto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(projectDto.getSendCompanyName());
            if (!companyDtoList.isEmpty()) {
                projectDto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<ProjectDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        if (projectDto.getReceiveCompanyName() != null && !projectDto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(projectDto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                projectDto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<ProjectDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0) {
            Page<ProjectDto> page = PageUtil.startPage(pageReq);
            projectDto.setProjectStartDate(startOfMonth);
            projectDto.setProjectEndDate(endOfMonth);
            //条件でプロジェクトを検索する。（連携情報含む）
            List<ProjectDto> ret = projectService.selectAllByExample(projectDto);
            return PageUtil.resp(page);
        } else {
            Page<ProjectDto> page = PageUtil.startPage(pageReq);
            projectDto.setProjectStartDate(startOfMonth);
            projectDto.setProjectEndDate(endOfMonth);
            List<ProjectDto> ret = projectService.selectByMyExample(projectDto, appUser.getUserId());
            return PageUtil.resp(page);
        }

    }

    /**
     * プロジェクト一覧画面を表示する。
     *
     * @param projectDto プロジェクト
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<ProjectDto> listAll(ProjectDto projectDto, JqgridPageReq pageReq) {
        Page<ProjectDto> page = PageUtil.startPage(pageReq);
        //条件でプロジェクトを検索する。（連携情報含む）
        List<ProjectDto> ret = projectService.selectAllByExample(projectDto);
        return PageUtil.resp(page);
    }
}
