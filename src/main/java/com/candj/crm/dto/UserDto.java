package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserDto {
    /** ユーザーID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** タイプ */
    @Digits(integer=10, fraction=0)
    private Integer userType;

    /** 名前 */
    @Length(max=64)
    private String userName;

    /** 性別 */
    @Length(max=5)
    private String userSex;

    /** 生年月日 */
    private org.joda.time.DateTime birthDate;

    /** 部門 */
    @Length(max=128)
    private String userDepartment;

    /** イーメールアドレス */
    @Length(max=255)
    private String userEmail;

    /** 電話番号 */
    @Length(max=64)
    private String userTelephoneNumber;

    /** パスワード */
    @Length(max=64)
    private String password;

    /** ステータス */
    @Digits(integer=3, fraction=0)
    private Byte userStatus;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
