package com.candj.crm.service;

import com.candj.crm.dao.mapper.CostDao;
import com.candj.crm.dto.CostDto;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.mapper.model.Cost;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 費用情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class CostService {
    @Autowired
    CostDao costDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで費用を検索する。
     * @param costId ID
     * @return 結果
     */
    public CostDto selectByPrimaryKey(Integer costId) {
        //プライマリーキーで費用を検索する。
        Cost ret = costDao.selectByPrimaryKey(costId);
        return DozerHelper.map(mapper, ret, CostDto.class);
    }

    /**
     * プライマリーキーで費用を検索する。（連携情報含む）
     * @param costId ID
     * @return 結果
     */
    public List<CostDto> selectAllByPrimaryKey(Integer costId) {
        //プライマリーキーで費用検索する。（連携情報含む）
        List<Cost> ret = costDao.selectAllByPrimaryKey(costId);
        return DozerHelper.mapList(mapper, ret, CostDto.class);
    }

    /**
     * 条件で費用を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CostDto> selectByExample(CostDto dto, MonthDto monthDto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("cost.cost_id", dto.getCostId())
            .andGreaterThanOrEqualTo("cost.cost_date", monthDto.getStartOfMonth())
            .andLessThanOrEqualTo("cost.cost_date", monthDto.getEndOfMonth())
            .andEqualTo("cost.cost_summary", dto.getCostSummary())
            .andEqualTo("cost.income_amount", dto.getIncomeAmount())
            .andEqualTo("cost.expend_amount", dto.getExpendAmount())
            .andEqualTo("cost.business_income_amount", dto.getBusinessIncomeAmount())
            .andEqualTo("cost.business_expend_amount", dto.getBusinessExpendAmount())
            .andEqualTo("cost.cost_total_amount", dto.getCostTotalAmount())
            .andEqualTo("cost.remark", dto.getRemark())
            .andEqualTo("cost.create_user", dto.getCreateUser())
            .andEqualTo("cost.update_user", dto.getUpdateUser())
            .andEqualTo("cost.create_time", dto.getCreateTime())
            .andEqualTo("cost.update_time", dto.getUpdateTime());
        }
        //条件で費用を検索する。（連携情報含む）)
        List<Cost> ret = costDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CostDto.class);
    }

    /**
     * 条件で費用を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CostDto> selectAllByExample(CostDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("cost.cost_id", dto.getCostId())
            .andEqualTo("cost.cost_date", dto.getCostDate())
            .andEqualTo("cost.cost_summary", dto.getCostSummary())
            .andEqualTo("cost.income_amount", dto.getIncomeAmount())
            .andEqualTo("cost.expend_amount", dto.getExpendAmount())
            .andEqualTo("cost.business_income_amount", dto.getBusinessIncomeAmount())
            .andEqualTo("cost.business_expend_amount", dto.getBusinessExpendAmount())
            .andEqualTo("cost.cost_total_amount", dto.getCostTotalAmount())
            .andEqualTo("cost.remark", dto.getRemark())
            .andEqualTo("cost.create_user", dto.getCreateUser())
            .andEqualTo("cost.update_user", dto.getUpdateUser())
            .andEqualTo("cost.create_time", dto.getCreateTime())
            .andEqualTo("cost.update_time", dto.getUpdateTime());
        }
        //条件で費用を検索する。（連携情報含む）)
        List<Cost> ret = costDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CostDto.class);
    }

    /**
     * 費用を新規追加する。
     * @param dto 費用
     * @return 結果
     */
    public int insertSelective(CostDto dto) {
        Cost cost  =  mapper.map(dto, Cost.class);
        //費用を新規追加する。
        int ret = costDao.insertSelective(cost);
        return ret;
    }

    /**
     * 費用を新規追加する。
     * @param dto 費用
     * @return 結果
     */
    public int insert(CostDto dto) {
        Cost cost  =  mapper.map(dto, Cost.class);
        //費用を新規追加する。
        int ret = costDao.insert(cost);
        return ret;
    }

    /**
     * プライマリーキーで費用を更新する。
     * @param dto 費用
     * @return 結果
     */
    public int updateByPrimaryKey(CostDto dto) {
        Cost cost  =  mapper.map(dto, Cost.class);
        //プライマリーキーで費用を更新する。
        int ret = costDao.updateByPrimaryKey(cost);
        return ret;
    }

    /**
     * プライマリーキーで費用を更新する。
     * @param dto 費用
     * @return 結果
     */
    public int updateByPrimaryKeySelective(CostDto dto) {
        Cost cost  =  mapper.map(dto, Cost.class);
        //プライマリーキーで費用を更新する。
        int ret = costDao.updateByPrimaryKeySelective(cost);
        return ret;
    }

    /**
     * 費用を削除する。
     * @param costId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer costId) {
        //費用を削除する。
        int ret = costDao.deleteByPrimaryKey(costId);
        return ret;
    }
}
