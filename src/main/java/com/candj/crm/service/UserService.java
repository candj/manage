package com.candj.crm.service;

import com.candj.crm.dao.mapper.UserDao;
import com.candj.crm.dto.UserDto;
import com.candj.crm.mapper.model.User;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class UserService {
    @Autowired
    UserDao userDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでユーザーを検索する。
     * @param userId ユーザーID
     * @return 結果
     */
    public UserDto selectByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザーを検索する。
        User ret = userDao.selectByPrimaryKey(userId);
        return DozerHelper.map(mapper, ret, UserDto.class);
    }

    /**
     * プライマリーキーでユーザーを検索する。（連携情報含む）
     * @param userId ユーザーID
     * @return 結果
     */
    public List<UserDto> selectAllByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザー検索する。（連携情報含む）
        List<User> ret = userDao.selectAllByPrimaryKey(userId);
        return DozerHelper.mapList(mapper, ret, UserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserDto> selectByExample(UserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user.user_id", dto.getUserId())
            .andEqualTo("user.company_id", dto.getCompanyId())
            .andEqualTo("user.user_type", dto.getUserType())
            .andLike("user.user_name", dto.getUserName())
            .andEqualTo("user.user_sex", dto.getUserSex())
            .andEqualTo("user.birth_date", dto.getBirthDate())
            .andEqualTo("user.user_department", dto.getUserDepartment())
            .andEqualTo("user.user_email", dto.getUserEmail())
            .andEqualTo("user.user_telephone_number", dto.getUserTelephoneNumber())
            .andEqualTo("user.password", dto.getPassword())
            .andEqualTo("user.user_status", dto.getUserStatus())
            .andEqualTo("user.create_user", dto.getCreateUser())
            .andEqualTo("user.update_user", dto.getUpdateUser())
            .andEqualTo("user.create_time", dto.getCreateTime())
            .andEqualTo("user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<User> ret = userDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserDto> selectAllByExample(UserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user.user_id", dto.getUserId())
            .andEqualTo("user.company_id", dto.getCompanyId())
            .andEqualTo("user.user_type", dto.getUserType())
            .andLike("user.user_name", dto.getUserName())
            .andEqualTo("user.user_sex", dto.getUserSex())
            .andEqualTo("user.birth_date", dto.getBirthDate())
            .andEqualTo("user.user_department", dto.getUserDepartment())
            .andEqualTo("user.user_email", dto.getUserEmail())
            .andEqualTo("user.user_telephone_number", dto.getUserTelephoneNumber())
            .andEqualTo("user.password", dto.getPassword())
            .andEqualTo("user.user_status", dto.getUserStatus())
            .andEqualTo("user.create_user", dto.getCreateUser())
            .andEqualTo("user.update_user", dto.getUpdateUser())
            .andEqualTo("user.create_time", dto.getCreateTime())
            .andEqualTo("user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<User> ret = userDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserDto.class);
    }

    /**
     * ユーザーを新規追加する。
     * @param dto ユーザー
     * @return 結果
     */
    public int insertSelective(UserDto dto) {
        User user  =  mapper.map(dto, User.class);
        //ユーザーを新規追加する。
        int ret = userDao.insertSelective(user);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param dto ユーザー
     * @return 結果
     */
    public int insert(UserDto dto) {
        User user  =  mapper.map(dto, User.class);
        //ユーザーを新規追加する。
        int ret = userDao.insert(user);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKey(UserDto dto) {
        User user  =  mapper.map(dto, User.class);
        //プライマリーキーでユーザーを更新する。
        int ret = userDao.updateByPrimaryKey(user);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKeySelective(UserDto dto) {
        User user  =  mapper.map(dto, User.class);
        //プライマリーキーでユーザーを更新する。
        int ret = userDao.updateByPrimaryKeySelective(user);
        return ret;
    }

    /**
     * ユーザーを削除する。
     * @param userId ユーザーID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer userId) {
        //ユーザーを削除する。
        int ret = userDao.deleteByPrimaryKey(userId);
        return ret;
    }
}
