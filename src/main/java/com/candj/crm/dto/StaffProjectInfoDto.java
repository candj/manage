package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * スタッフプロジェクト情報
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffProjectInfoDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer staffProjectInfoId;

    /** 担当者ID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** offerID */
    @Digits(integer=10, fraction=0)
    private Integer offerId;

    /** スタッフプロジェクト情報 */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** プロジェクト名 */
    @Length(max=128)
    private String projectName;

    /** プロジェクトにあり */
    @Length(max=2)
    private String staffInProject;

    private String staffInProjectDateISNear;

    /** 開始期日 */
    private org.joda.time.DateTime projectStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime projectEndDate;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
