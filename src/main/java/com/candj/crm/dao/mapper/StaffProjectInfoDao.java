package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.StaffProjectInfo;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフプロジェクト情報情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface StaffProjectInfoDao {
    /**
     * プライマリーキーでスタッフプロジェクト情報を検索する。
     * @param staffProjectInfoId ID
     * @return 結果
     */
    StaffProjectInfo selectByPrimaryKey(Integer staffProjectInfoId);

    /**
     * プライマリーキーでスタッフプロジェクト情報検索する。（連携情報含む）
     * @param staffProjectInfoId ID
     * @return 結果
     */
    List<StaffProjectInfo> selectAllByPrimaryKey(Integer staffProjectInfoId);

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffProjectInfo> selectAllByExample1(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffProjectInfo> selectAllByExample2(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffProjectInfo> selectAllByMyExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffProjectInfo> selectByExample(@Param("example") WhereCondition example);

    /**
     * スタッフプロジェクト情報を新規追加する。
     * @param record スタッフプロジェクト情報
     * @return 結果
     */
    int insertSelective(StaffProjectInfo record);

    /**
     * スタッフプロジェクト情報を新規追加する。
     * @param record スタッフプロジェクト情報
     * @return 結果
     */
    int insert(StaffProjectInfo record);

    /**
     * プライマリーキーでスタッフプロジェクト情報を更新する。
     * @param record スタッフプロジェクト情報
     * @return 結果
     */
    int updateByPrimaryKey(StaffProjectInfo record);

    /**
     * プライマリーキーでスタッフプロジェクト情報を更新する。
     * @param record スタッフプロジェクト情報
     * @return 結果
     */
    int updateByPrimaryKeySelective(StaffProjectInfo record);

    /**
     * スタッフプロジェクト情報を更新する。
     * @param record スタッフプロジェクト情報
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(StaffProjectInfo record, @Param("example") WhereCondition example);

    /**
     * スタッフプロジェクト情報を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") StaffProjectInfo example);

    /**
     * スタッフプロジェクト情報を削除する。
     * @param staffProjectInfoId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer staffProjectInfoId);

    /**
     * スタッフプロジェクト情報を削除する。
     * @param offerId ID
     * @return 結果
     */
    int deleteByOfferId(Integer offerId);
}
