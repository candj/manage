package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.Ordering;
import com.candj.crm.service.InvoiceDetailService;
import com.candj.crm.service.InvoiceDetailTemporaryService;
import com.candj.crm.service.InvoiceService;
import com.candj.crm.service.OrderingService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 請求詳細情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/invoiceDetailRest")
@RestController
public class InvoiceDetailRestController {
    @Autowired
    InvoiceDetailService invoiceDetailService;

    @Autowired
    InvoiceDetailTemporaryService invoiceDetailTemporaryService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    OrderingService orderingService;

    @Autowired
    Calculate calculate;

    /**
     * 請求詳細を新規追加する。
     * @param invoiceDetailId ID
     * @return 結果
     */
    @RequestMapping(value = "{invoiceDetailId}", method = RequestMethod.GET)
    public InvoiceDetailDto get(@PathVariable Integer invoiceDetailId) {
        //プライマリーキーで請求詳細を検索する。
        InvoiceDetailDto ret = invoiceDetailService.selectByPrimaryKey(invoiceDetailId);
        return ret;
    }

    /**
     * 請求詳細を新規追加する。
     * @param invoiceDetailDto 請求詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody InvoiceDetailDto invoiceDetailDto, Errors rrrors) {
        //請求詳細　該記録スタッフの月額金額
        invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(invoiceDetailDto.getSalaryPersonMonth(),
                                            invoiceDetailDto.getPersonMonth()));
        //請求詳細　該記録スタッフの超過金額
        invoiceDetailDto.setAmountExtra(calculate.salaryExtraCalculate(invoiceDetailDto.getSalaryExtra(),
                                            invoiceDetailDto.getSalaryDeduction(), invoiceDetailDto.getOverTime()));
        //請求詳細を新規追加する。
        int ret = invoiceDetailService.insertSelective(invoiceDetailDto);

        //見積総金額の計算と更新
        List<InvoiceDetailDto>invoiceDetailDtos = invoiceDetailService.selectByInvoiceId(invoiceDetailDto.getInvoiceId());
        InvoiceDto invoiceDto = invoiceService.selectAllByPrimaryKey(invoiceDetailDto.getInvoiceId()).get(0);
        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        for(InvoiceDetailDto invoiceDetail: invoiceDetailDtos){
            totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetail.getAmountMonth() + invoiceDetail.getAmountExtra();
        }
        invoiceDto.setInvoiceAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
        invoiceDto.setInvoiceConsumptionTax(tax);
        //税込み合計金額
        invoiceDto.setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
        invoiceService.updateByPrimaryKeySelective(invoiceDto);
        return ret;
    }

    /**
     * 請求詳細を変更する。
     * @param invoiceDetailDto 請求詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody InvoiceDetailDto invoiceDetailDto, Errors rrrors) {
        //請求詳細　該記録スタッフの月額金額
        invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(invoiceDetailDto.getSalaryPersonMonth(),
                invoiceDetailDto.getPersonMonth()));
        //請求詳細　該記録スタッフの超過金額
        invoiceDetailDto.setAmountExtra(calculate.salaryExtraCalculate(invoiceDetailDto.getSalaryExtra(),
                invoiceDetailDto.getSalaryDeduction(), invoiceDetailDto.getOverTime()));
        //プライマリーキーで請求詳細を更新する。
        int ret = invoiceDetailService.updateByPrimaryKeySelective(invoiceDetailDto);

        //見積総金額の計算と更新
        List<InvoiceDetailDto>invoiceDetailDtos = invoiceDetailService.selectByInvoiceId(invoiceDetailDto.getInvoiceId());
        InvoiceDto invoiceDto = invoiceService.selectAllByPrimaryKey(invoiceDetailDto.getInvoiceId()).get(0);
        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        for(InvoiceDetailDto invoiceDetail: invoiceDetailDtos){
            totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetail.getAmountMonth() + invoiceDetail.getAmountExtra();
        }
        invoiceDto.setInvoiceAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
        invoiceDto.setInvoiceConsumptionTax(tax);
        //税込み合計金額
        invoiceDto.setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
        invoiceService.updateByPrimaryKeySelective(invoiceDto);
        return ret;
    }

    /**
     * 請求詳細を変更する。
     * @param invoiceDetailDto 請求詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/editForInsert", method = RequestMethod.PUT)
    public int updateForInsert(@Valid @RequestBody InvoiceDetailDto invoiceDetailDto, Errors rrrors) {
        //請求詳細　該記録スタッフの月額金額
        invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(invoiceDetailDto.getSalaryPersonMonth(),
                invoiceDetailDto.getPersonMonth()));
        //請求詳細　該記録スタッフの超過金額
        invoiceDetailDto.setAmountExtra(calculate.salaryExtraCalculate(invoiceDetailDto.getSalaryExtra(),
                invoiceDetailDto.getSalaryDeduction(), invoiceDetailDto.getOverTime()));
        //プライマリーキーで請求詳細を更新する。
        int ret = invoiceDetailTemporaryService.updateByPrimaryKeySelective(invoiceDetailDto);

        /*//見積総金額の計算と更新
        List<InvoiceDetailDto>invoiceDetailDtos = invoiceDetailTemporaryService.selectByInvoiceId(invoiceDetailDto.getInvoiceId());
        InvoiceDto invoiceDto = invoiceService.selectAllByPrimaryKey(invoiceDetailDto.getInvoiceId()).get(0);
        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        for(InvoiceDetailDto invoiceDetail: invoiceDetailDtos){
            totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetail.getAmountMonth() + invoiceDetail.getAmountExtra();
        }
        invoiceDto.setInvoiceAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
        invoiceDto.setInvoiceConsumptionTax(tax);
        //税込み合計金額
        invoiceDto.setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
        invoiceService.updateByPrimaryKeySelective(invoiceDto);*/
        return ret;
    }

    /**
     * 請求詳細を新規追加する。
     * @param invoiceDetailId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{invoiceDetailId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer invoiceDetailId) {
        //請求詳細を削除する。
        int ret = invoiceDetailService.deleteByPrimaryKey(invoiceDetailId);
        return ret;
    }

    /**
     * 請求詳細を新規追加する。
     * @param invoiceDetailId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/deleteForInsert/{invoiceDetailId}", method = RequestMethod.DELETE)
    public int deleteForInsert(@PathVariable Integer invoiceDetailId) {
        //請求詳細を削除する。
        int ret = invoiceDetailTemporaryService.deleteByPrimaryKey(invoiceDetailId);
        return ret;
    }

    /**
     * 請求詳細一覧画面を表示する。
     * @param invoiceDetailDto 請求詳細
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<InvoiceDetailDto> list(InvoiceDetailDto invoiceDetailDto, JqgridPageReq pageReq) {
        Page<InvoiceDetailDto> page = PageUtil.startPage(pageReq);
        //条件で請求詳細を検索する。（連携情報含む）
        List<InvoiceDetailDto> ret = invoiceDetailService.selectByInvoiceId(invoiceDetailDto.getInvoiceId());
        return PageUtil.resp(page);
    }

    /**
     * 請求詳細一覧画面を表示する。
     * @param invoiceDetailDto 請求詳細
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listForInsert/{orderId}", method = RequestMethod.POST)
    public PageInfo<InvoiceDetailDto> listForInsert(InvoiceDetailDto invoiceDetailDto,
                                                    @PathVariable Integer orderId,
                                                    JqgridPageReq pageReq) {
        Page<InvoiceDetailDto> page = PageUtil.startPage(pageReq);
        //条件で請求詳細を検索する。（連携情報含む）
        //List<Ordering> orderings = orderingService.selectOrderByPrimaryKey(orderId);
        //InvoiceDto invoiceDto = new InvoiceDto();
        //List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailService.makeInvoiceDetailList(orderings.get(0), invoiceDto);
        /*invoiceDetailDtoList.forEach(i -> {
            page.add(i);
        });*/
        List<InvoiceDetailDto> ret = invoiceDetailTemporaryService.selectAllByExample(invoiceDetailDto, orderId);
        return PageUtil.resp(page);
    }

    /**
     * 請求詳細一覧画面を表示する。
     * @param invoiceDetailDto 請求詳細
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<InvoiceDetailDto> listAll(InvoiceDetailDto invoiceDetailDto, JqgridPageReq pageReq) {
        Page<InvoiceDetailDto> page = PageUtil.startPage(pageReq);
        //条件で請求詳細を検索する。（連携情報含む）
        List<InvoiceDetailDto> ret = invoiceDetailService.selectAllByExample(invoiceDetailDto);
        return PageUtil.resp(page);
    }

    /**
     * 勤務の承認状態を変更する。
     *
     * @param invoiceDetailDto 勤務
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/editRows", method = RequestMethod.POST)
    public int editRows(InvoiceDetailDto invoiceDetailDto, Errors rrrors) {
        //承認状態を更新する
        int ret = 1;
        return ret;
    }
}
