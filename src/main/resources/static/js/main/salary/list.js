var context_path = $('meta[name=basepath]').attr('content');


var colNames = [
    "ID",
    "yearMonth",
    "スタッフ",
    "基本給(円)",
    "残業代/営業費用</br>/食費(円)",
    "交通費(円)",
    "社会保険料</br>(会社分)(円)",
    "社会保険料</br>(個人分)(円)",
    "住宅補助金、</br>(その他)(円)",
    "労働保険料</br>(会社分)(円)",
    "労働保険料</br>(個人分)(円)",
    "ボーナス(円)",
    "個人所得税(円)",
    "実給(円)",
    "扶養人数",
    "ステータス",
    "備考",
    "給料支払日付"
];
var colModel = [
    {
        name: "salaryId",
        index: "salaryId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "salaryMonth",
        index: "salaryMonth",
        width: 100,
        search: false,
        hidden: true,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM');
            } else {
                return '';
            }
        },
        align: "right",
    },
    {
        name: "appUser.userName",
        index: "appUser.userName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "basePay",
        index: "basePay",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "overtimePay",
        index: "overtimePay",
        width: 110,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "commutingCost",
        index: "commutingCost",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "socialInsuranceCompany",
        index: "socialInsuranceCompany",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "socialInsurancePersonal",
        index: "socialInsurancePersonal",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "housingSubsidies",
        index: "housingSubsidies",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "labourInsuranceCompany",
        index: "labourInsuranceCompany",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "labourInsurancePersonal",
        index: "labourInsurancePersonal",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "bonus",
        index: "bonus",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "personalIncomeTax",
        index: "personalIncomeTax",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "salaryAmount",
        index: "salaryAmount",
        width: 100,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "bringUpNumber",
        index: "bringUpNumber",
        width: 100,
        search: false,
        align: "right",
    },
    {
        name: "status",
        index: "status",
        width: 100,
        search: false,
        hidden: true,
        align: "right",
    },
    {
        name: "remark",
        index: "remark",
        width: 300,
        search: false,
        align: "left",
    },
    {
        name: "playPayDateISNear",
        index: "playPayDateISNear",
        width: 300,
        search: false,
        hidden: true,
        align: "left",
    }
];

var postData = {
    yearMonth: $('#yearMonth').val(),
    userName: $('#userName').val(),
};

var shrinkToFit = false;

var gridComplete = function () {
    $("#grid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "scroll"});

    $('.emptyCell').each(
        function (index, element) {
            $(this).closest('td').addClass('btn-update emptyTdCell').attr(
                'data-url', $(this).attr('data-url'));
        })
    $('.btn-update').click(function () {
        var width = '900px'
        var height = '90%'
        layer.open({
            type: 2,
            title: ' ',
            shadeClose: false,
            shade: 0.6,
            area: [width, height],
            content: [$(this).attr("data-url")],
        })
        return false;

    });

    var ids = $("#grid").jqGrid("getDataIDs");
    var rowDatas = $("#grid").jqGrid("getRowData");
    for (var ii = 0; ii < rowDatas.length; ii++) {
        var rowData = rowDatas[ii];
        var playPayDateISNear = rowData.playPayDateISNear;
        if (playPayDateISNear === "true") {
            $("#" + ids[ii] + " td").css("background-color", "red");
        }
    }

}


$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete);
});	
	