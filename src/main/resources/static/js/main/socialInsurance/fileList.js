

			var colNames = [
			    "社会保険料率ID",
				"適用年月(開始時間)",
				"Excelファイル",
			];
			var colModel = [
				{
					name: "socialInsuranceId",
					index: "socialInsuranceId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
                {
                    name: "socialInsuranceStartMonth",
                    index: "socialInsuranceStartMonth",
                    width: 50,
                    search : false,
                    formatter : function(cellvalue, options, rowObject) {
                       if (cellvalue != null) {
                           var date = new Date(cellvalue);
                           return date.Format('yyyy-MM');
                       } else {
                           return '';
                       }
                    },
                    align: "left",
                },
                {
                        name: "socialInsuranceExcelName",
                        index: "socialInsuranceExcelName",
                        width: 100,
                        search: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue != null && cellvalue != undefined) {
                                var selectHtml = '<a href="/api/socialInsuranceRest/download/'
                                    + rowObject.socialInsuranceId
                                    + '">'
                                    + cellvalue
                                    + '</a>';
                            } else {
                                var selectHtml = "";
                            }
                            return selectHtml;
                        },
                        align: "left",
                },
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	