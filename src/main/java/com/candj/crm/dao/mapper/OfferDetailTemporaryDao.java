package com.candj.crm.dao.mapper;

import com.candj.crm.dto.OfferDetailForWorkDto;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 見積詳細 情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface OfferDetailTemporaryDao {
    /**
     * プライマリーキーで見積詳細 を検索する。
     * @param offerDetailId 
     * @return 結果
     */
    OfferDetail selectByPrimaryKey(Integer offerDetailId);

    /**
     * プライマリーキーで見積詳細 検索する。（連携情報含む）
     * @param offerDetailId 
     * @return 結果
     */
    List<OfferDetail> selectAllByPrimaryKey(Integer offerDetailId);

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<OfferDetail> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<OfferDetail> selectByExample(@Param("example") WhereCondition example);

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）)
     * @return 結果
     */
    List<OfferDetail> selectByOfferId(Integer offerId);

    /**
     * 見積詳細 を新規追加する。
     * @param record 見積詳細 
     * @return 結果
     */
    int insertSelective(OfferDetail record);

    /**
     * 見積詳細 を新規追加する。
     * @param record 見積詳細 
     * @return 結果
     */
    int insert(OfferDetail record);

    /**
     * プライマリーキーで見積詳細 を更新する。
     * @param record 見積詳細 
     * @return 結果
     */
    int updateByPrimaryKey(OfferDetail record);

    /**
     * プライマリーキーで見積詳細 を更新する。
     * @param record 見積詳細 
     * @return 結果
     */
    int updateByPrimaryKeySelective(OfferDetail record);

    /**
     * 見積詳細 を更新する。
     * @param record 見積詳細 
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(OfferDetail record, @Param("example") WhereCondition example);

    /**
     * 見積詳細 を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") OfferDetail example);

    /**
     * 見積詳細 を削除する。
     * @param offerDetailId 
     * @return 結果
     */
    int deleteByPrimaryKey(Integer offerDetailId);

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<OfferDetail> selectByStaffAndMonth(OfferDetailForWorkDto record);
}
