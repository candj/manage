package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.Project;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * プロジェクト情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface ProjectDao {
    /**
     * プライマリーキーでプロジェクトを検索する。
     * @param projectId プロジェクトID
     * @return 結果
     */
    Project selectByPrimaryKey(Integer projectId);

    /**
     * プライマリーキーでプロジェクト検索する。（連携情報含む）
     * @param projectId プロジェクトID
     * @return 結果
     */
    List<Project> selectAllByPrimaryKey(Integer projectId);

    /**
     * プライマリーキーでプロジェクト検索する。（連携情報含む）
     * @param example 検索条件
     * @return 結果
     */
    List<Project> selectByMyExample(@Param("example") WhereCondition example);


    /**
     * 条件でプロジェクトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Project> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でプロジェクトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Project> selectByExample(@Param("example") WhereCondition example);

    /**
     * プロジェクトを新規追加する。
     * @param record プロジェクト
     * @return 結果
     */
    int insertSelective(Project record);

    /**
     * プロジェクトを新規追加する。
     * @param record プロジェクト
     * @return 結果
     */
    int insert(Project record);

    /**
     * プライマリーキーでプロジェクトを更新する。
     * @param record プロジェクト
     * @return 結果
     */
    int updateByPrimaryKey(Project record);

    /**
     * プライマリーキーでプロジェクトを更新する。
     * @param record プロジェクト
     * @return 結果
     */
    int updateByPrimaryKeySelective(Project record);

    /**
     * プロジェクトを更新する。
     * @param record プロジェクト
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Project record, @Param("example") WhereCondition example);

    /**
     * プロジェクトを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Project example);

    /**
     * プロジェクトを削除する。
     * @param projectId プロジェクトID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer projectId);
}
