package com.candj.crm.service;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.MailTemplateDto;
import com.candj.crm.dto.SalaryDto;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.io.StringWriter;

@Component
public class SendMailService {

    @Autowired
    private MailgunService mailgunService;

    @Autowired
    private SalaryService salaryService;

    Template tp = new Template();
    RuntimeServices rs = RuntimeSingleton.getRuntimeServices();
    VelocityContext vc = new VelocityContext();
    SimpleNode sn;
    public void sendMail(String mailTo,Integer salaryId,IMailBodyCallback callback) throws ParseException {
        MailTemplateDto mailTemplateDto = new MailTemplateDto();
        mailTemplateDto.setCode(0);
        SalaryDto salaryDto = salaryService.selectByPrimaryKey(salaryId);
        String time = DateTimeUtil.dateTimeToStringForMonth(salaryDto.getSalaryMonth());
        mailTemplateDto.setTitle(time + "給料明細");
        mailTemplateDto.setContent("基本給：" + salaryDto.getBasePay().toString() + "\n"
                                    + "残業代/営業費用/食費：" + salaryDto.getOvertimePay().toString() + "\n"
                                    + "交通費：" + salaryDto.getCommutingCost().toString() + "\n"
                                    + "社会保険料：" + salaryDto.getSocialInsurancePersonal().toString() + "\n"
                                    + "労働保険料：" + salaryDto.getLabourInsurancePersonal().toString() + "\n"
                                    + "住宅補助金、（その他）：" + salaryDto.getHousingSubsidies().toString() + "\n"
                                    + "ボーナス：" + salaryDto.getBonus().toString() + "\n"
                                    + "労働保険料：" + salaryDto.getLabourInsurancePersonal().toString() + "\n"
                                    + "扶養人数：" + salaryDto.getBringUpNumber().toString() + "\n"
                                    + "個人所得税：" + salaryDto.getPersonalIncomeTax().toString() + "\n"
                                    + "実給：" + salaryDto.getSalaryAmount().toString() + "\n");
        mailTemplateDto.setLink("http://192.168.2.8:8050/");
        StringReader sr = new StringReader(mailTemplateDto.getContent());
        sn = rs.parse(sr, tp);
        tp.setRuntimeServices(rs);
        tp.setData(sn);
        tp.initDocument();
        callback.putVaule(vc);
        StringWriter sw = new StringWriter();
        tp.merge(vc, sw);
        String body = sw.toString();
        mailgunService.mailTo(mailTo, mailTemplateDto.getTitle(), body+mailTemplateDto.getLink());
    }
}
