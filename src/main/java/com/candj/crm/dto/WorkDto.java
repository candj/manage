package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 作業
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkDto {
    /** 作業ID */
    @Digits(integer=10, fraction=0)
    private Integer workId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** スタッフ */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 種類 */
    @Length(max=2)
    private String workType;

    /** 注文ID */
    @Digits(integer=10, fraction=0)
    private Integer orderId;

    /** 注文 */
    private com.candj.crm.mapper.model.Ordering ordering;

    /** 年月 */
    private org.joda.time.DateTime workMonth;

    /** 年月 */
    private String yearMonth;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 単価 */
    @Digits(integer=10, fraction=0)
    private Integer unitPrice;

    /** 工数 */
    @Digits(integer=8, fraction=2)
    private java.math.BigDecimal workManHour;

    /** 作業時間 */
    @Digits(integer=9, fraction=1)
    private java.math.BigDecimal workHour;

    /** 勤務表パース */
    @Length(max=255)
    private String workAttachmentPath;

    /** 勤務表パース */
    @Length(max=255)
    private String workAttachmentName;

    /** 処理状態 */
    @Length(max=2)
    private String workAdmitStatus;

    /** ステータス */
    @Digits(integer=3, fraction=0)
    private Byte workStatus;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** タイプ */
    @Digits(integer=10, fraction=0)
    private Integer userType;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
