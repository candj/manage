package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Project;
import com.candj.crm.service.*;

import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 見積情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/offer")
@Controller
public class OfferController {
    @Autowired
    OfferService offerService;

    @Autowired
    OfferTemporaryService offerTemporaryService;

    @Autowired
    OfferDetailTemporaryService offerDetailTemporaryService;

    @Autowired
    CompanyService companyService;

    @Autowired
    ProjectService projectService;

    @Autowired
    TaxService taxService;

    /**
     * 見積を新規追加する。
     * @param offerDto 見積
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(OfferDto offerDto, ModelMap model) {
        OfferDetailDto offerDetailDto = new OfferDetailDto();
        List<OfferDto> offerDtoList = offerTemporaryService.selectByExample(offerDto);
        offerDto.setDate(new DateTime());
        offerDto.setOfferPeriodOfValidity(3);
        offerDto.setWorkTimeLowerLimit(140);
        offerDto.setWorkTimeUpperLimit(200);
        offerDto.setWorkName("システム開発支援");
        for(OfferDto o : offerDtoList){
            offerTemporaryService.deleteByPrimaryKey(o.getOfferId());
        }
        CompanyDto companyDto = new CompanyDto();
        Byte i = 0;
        companyDto.setCompanyType(i);
        Integer companyId = companyService.selectByExample(companyDto).get(0).getCompanyId();
        offerDto.setReceiveCompanyId(companyId);
        offerTemporaryService.insertSelective(offerDto);
        List<OfferDetailDto> offerDetailDtoList = offerDetailTemporaryService.selectByExample(offerDetailDto);
        for(OfferDetailDto od : offerDetailDtoList){
            offerDetailTemporaryService.deleteByPrimaryKey(od.getOfferDetailId());
        }
        return new ModelAndView("offer/insert", model);
    }

    /**
     * 見積を新規追加(確認)する。
     * @param offerDto 見積
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid OfferDto offerDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offer/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("offer/insertConfirm", model);
    }

    /**
     * 見積を新規追加(終了)する。
     * @param offerDto 見積
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid OfferDto offerDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offer/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //見積を新規追加する。
        int ret = offerService.insertSelective(offerDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/offer/insertFinish", model);
    }

    /**
     * 見積を編集する。
     * @param offerId 見積 ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{offerId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer offerId, ModelMap model) {
        //プライマリーキーで見積を検索する。
        List<OfferDto> offerDtos = offerService.selectAllByPrimaryKey(offerId);
        model.addAttribute("offerDto", offerDtos.get(0));
        return new ModelAndView("offer/edit", model);
    }


    /**
     * 見積を編集する。
     * @param offerId 見積 ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/copy/{offerId}", method = RequestMethod.GET)
    public ModelAndView copy(@PathVariable Integer offerId, ModelMap model) {
        //プライマリーキーで見積を検索する。
        List<OfferDto> offerDtos = offerService.selectAllByPrimaryKey(offerId);
        model.addAttribute("offerDto", offerDtos.get(0));
        return new ModelAndView("offer/copy", model);
    }

    /**
     * 見積を編集(確認)する。
     * @param offerDto 見積
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid OfferDto offerDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offer/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("offer/editConfirm", model);
    }

    /**
     * 見積を編集(終了)する。
     * @param offerDto 見積
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid OfferDto offerDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offer/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで見積を更新する。
        int ret = offerService.updateByPrimaryKeySelective(offerDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("offer/editFinish", model);
    }

    /**
     * 見積一覧画面を表示する。
     * @param offerDto 見積
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("offerDto") OfferDto offerDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        String projectName = "";
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("projectName", projectName);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0){
            return new ModelAndView("offer/list", model);
        }else {
            return new ModelAndView("offerForStaff/list", model);
        }

    }
}
