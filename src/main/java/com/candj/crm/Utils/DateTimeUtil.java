package com.candj.crm.Utils;

import com.candj.crm.dto.MonthDto;
import org.apache.http.client.utils.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {

    public static DateTime yearMonthToDateTime(YearMonth yearMonth) {
        DateTime dateTime = new DateTime();
        if(yearMonth != null) {
            dateTime = new DateTime(yearMonth.getYear(), yearMonth.getMonthOfYear(), 1, 0, 0, 0, 0);
        }
        return dateTime;
    }

    public static String dateTimeToString(DateTime dateTime) {
        DateFormat format = new SimpleDateFormat("yyyy-MM");
        Date date = dateTime.toDate();
        String string = format.format(date);
        return string;
    }

    public static String dateTimeToStringForMonth(DateTime dateTime) {
        DateFormat format = new SimpleDateFormat("yyyy年MM月");
        Date date = dateTime.toDate();
        String string = format.format(date);
        return string;
    }

    public static String dateTimeToStringDate(DateTime dateTime) {
        DateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
        Date date = dateTime.toDate();
        String string = format.format(date);
        return string;
    }

    public static String dateTimeToStringNumber(DateTime dateTime) {
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = dateTime.toDate();
        String string = format.format(date);
        return string;
    }

    public static DateTime StringToDateTime(String string) {
        DateFormat format = new SimpleDateFormat("yyyy-MM");
        Date date = new Date();
        try {
            date = format.parse(string);
        }catch(ParseException e){
            e.printStackTrace();
        }
        DateTime datetime = new DateTime(date);
        return datetime;
    }

    public static DateTime StringToDateTimeDate(String string) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = format.parse(string);
        }catch(ParseException e){
            e.printStackTrace();
        }
        DateTime datetime = new DateTime(date);
        return datetime;
    }

    public static MonthDto getMonthStartAndEnd(DateTime startOfMonth){
        MonthDto monthDto = new MonthDto();
        monthDto.setStartOfMonth(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        monthDto.setEndOfMonth(endOfMonth);
        return monthDto;
    }
}
