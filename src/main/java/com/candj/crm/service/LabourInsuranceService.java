package com.candj.crm.service;

import com.candj.crm.dao.mapper.LabourInsuranceDao;
import com.candj.crm.dto.LabourInsuranceDto;
import com.candj.crm.mapper.model.LabourInsurance;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class LabourInsuranceService {

    @Autowired
    LabourInsuranceDao labourInsuranceDao;

    @Autowired
    Mapper mapper;

    Comparator<LabourInsurance> descWorkStartTimeComp = new Comparator<LabourInsurance>() {
        @Override
        public int compare(LabourInsurance t1, LabourInsurance t2) {
            return t2.getLabourInsuranceStartMonth().compareTo(t1.getLabourInsuranceStartMonth());
        }
    };

    /**
     * プライマリーキーで税率、保険料率を検索する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    public LabourInsuranceDto selectByPrimaryKey(Integer labourInsuranceId) {
        //プライマリーキーで税率、保険料率を検索する。
        LabourInsurance ret = labourInsuranceDao.selectByPrimaryKey(labourInsuranceId);
        return DozerHelper.map(mapper, ret, LabourInsuranceDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     * @param labourInsuranceId ID
     * @return 結果
     */
    public List<LabourInsuranceDto> selectAllByPrimaryKey(Integer labourInsuranceId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<LabourInsurance> ret = labourInsuranceDao.selectAllByPrimaryKey(labourInsuranceId);
        return DozerHelper.mapList(mapper, ret, LabourInsuranceDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<LabourInsuranceDto> selectByExample(LabourInsuranceDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("labour_insurance.labour_insurance_id", dto.getLabourInsuranceId())
                    .andEqualTo("labour_insurance.labour_insurance_rate", dto.getLabourInsuranceRatePersonal())
                    .andEqualTo("labour_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("labour_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("labour_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("labour_insurance.update_time", dto.getUpdateTime());
        }
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<LabourInsurance> ret = labourInsuranceDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, LabourInsuranceDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<LabourInsuranceDto> selectAllByExample(LabourInsuranceDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("labour_insurance.labour_insurance_id", dto.getLabourInsuranceId())
                    .andEqualTo("labour_insurance.labour_insurance_rate_company", dto.getLabourInsuranceRateCompany())
                    .andEqualTo("labour_insurance.labour_insurance_rate_personal", dto.getLabourInsuranceRatePersonal())
                    .andLessThanOrEqualTo("labour_insurance.labour_insurance_start_month", dto.getLabourInsuranceStartMonth())
                    .andGreaterThanOrEqualTo("labour_insurance.labour_insurance_end_month", dto.getLabourInsuranceEndMonth())
                    .andEqualTo("labour_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("labour_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("labour_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("labour_insurance.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("labour_insurance.labour_insurance_id", dto.getLabourInsuranceId())
                    .andEqualTo("labour_insurance.labour_insurance_rate_company", dto.getLabourInsuranceRateCompany())
                    .andEqualTo("labour_insurance.labour_insurance_rate_personal", dto.getLabourInsuranceRatePersonal())
                    .andLessThanOrEqualTo("labour_insurance.labour_insurance_start_month", dto.getLabourInsuranceStartMonth())
                    .andIsNull("labour_insurance.labour_insurance_end_month")
                    .andEqualTo("labour_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("labour_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("labour_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("labour_insurance.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("labour_insurance.labour_insurance_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<LabourInsurance> ret = labourInsuranceDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, LabourInsuranceDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(LabourInsuranceDto dto) {
        LabourInsurance labourInsurance  =  mapper.map(dto, LabourInsurance.class);
        //税率、保険料率を新規追加する。
        int ret = labourInsuranceDao.insertSelective(labourInsurance);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(LabourInsuranceDto dto) {
        LabourInsurance labourInsurance  =  mapper.map(dto, LabourInsurance.class);
        //税率、保険料率を新規追加する。
        int ret = labourInsuranceDao.insert(labourInsurance);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKey(LabourInsuranceDto dto) {
        LabourInsurance labourInsurance  =  mapper.map(dto, LabourInsurance.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = labourInsuranceDao.updateByPrimaryKey(labourInsurance);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKeySelective(LabourInsuranceDto dto) {
        LabourInsurance labourInsurance  =  mapper.map(dto, LabourInsurance.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = labourInsuranceDao.updateByPrimaryKeySelective(labourInsurance);
        return ret;
    }

    /**
     * 税率、保険料率を削除する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer labourInsuranceId) {
        //税率、保険料率を削除する。
        int ret = labourInsuranceDao.deleteByPrimaryKey(labourInsuranceId);
        return ret;
    }

    public void setLabourInsuranceEndMonth(LabourInsuranceDto labourInsuranceDto){
        WhereCondition whereCondition= new WhereCondition() ;
        List<LabourInsurance> labourInsuranceList = labourInsuranceDao.selectByExample(whereCondition);
        labourInsuranceList.sort(descWorkStartTimeComp);
        DateTime lastEndMonth = null;
        for(LabourInsurance t : labourInsuranceList){
            t.setLabourInsuranceEndMonth(lastEndMonth == null? null : lastEndMonth);
            lastEndMonth = t.getLabourInsuranceStartMonth().minusSeconds(1);
            labourInsuranceDao.updateByPrimaryKeySelective(t);
        }
    }

}
