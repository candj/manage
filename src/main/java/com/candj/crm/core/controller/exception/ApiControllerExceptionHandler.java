package com.candj.crm.core.controller.exception;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.candj.webpower.web.core.response.error.ErrorResponse;
import com.candj.webpower.web.core.util.ResponseUtil;

import lombok.Getter;
import lombok.Setter;

/**
 * ApiControllerExceptionHandler.
 */
@RestControllerAdvice
@Component
public class ApiControllerExceptionHandler extends AbstractApiControllerExceptionHandler {

    /** エラーコード: データ不整合(存在しているはずのデータない等). */
    private static final String INCONSISTENT_ERROR_CODE = "C000-0015";


    /** メッセージソース. */
    @Autowired
    @Setter
    @Getter
    private MessageSource messageSource;



    /**
     * データ不整合エラーレスポンスを生成します.
     * @return ErrorResponse list
     */
    protected ErrorResponse dataErrorResponse() {
        return errorResponse(INCONSISTENT_ERROR_CODE, null);
    }

    /**
     * @param code エラーコード
     * @param fieldName フィールド名
     * @param args パラメータ
     * @return ErrorResponse
     */
    protected ErrorResponse errorResponse(@Nonnull String code, @Nullable String fieldName, Object... args) {
        String message = getMessage(code, args);
        return new ErrorResponse(
                code,
                message,
                fieldName != null ? ResponseUtil.camelCaseToSnakeCase(fieldName) : "");
    }
}
