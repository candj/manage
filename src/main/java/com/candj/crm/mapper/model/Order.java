package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 注文
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Order {
    /** 注文ID */
    private Integer orderId;

    /** 見積 ID */
    private Integer offerId;

    /** 注文番号 */
    private String orderNumber;

    /** 日付 */
    private org.joda.time.DateTime orderDate;

    /** ステータス */
    private String orderStatus;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
