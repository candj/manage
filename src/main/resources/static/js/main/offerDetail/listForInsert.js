

			var colNames = [
				"${column.colName}",
				"作業員",
				"業務名",
				"単価(円)",
				"工数（人月）",
				"合計金額(円)",
			];
			var colModel = [
				{
					name: "offerDetailId",
					index: "offerDetailId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "appUser.userName",
					index: "appUser.userName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "workName",
					index: "workName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "amount",
					index: "amount",
					width: 100,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "manHour",
					index: "manHour",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "totalAmount",
					index: "totalAmount",
					width: 100,
					search : false,
					formatter : "integer",

					align: "right",
				},
			];

var postData = {
	offerId : $('#offerId').val(),
};

var caption = '<button class="btn btn-success btn-sm btn-add btn-tool" data-url="/offerDetail/insert?offerId='+$('#offerId').val()+'"><i class="glyphicon glyphicon-plus"></i>&nbsp;新規</button>';

var gridComplete = function() {
		$('#table_list').cjtable();
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});