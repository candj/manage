package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Ordering;
import com.candj.crm.service.OrderingService;
import com.candj.crm.service.WorkService;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import com.candj.webpower.web.core.util.LogUtil;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 作業情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/work")
@Controller
public class WorkController {
    @Autowired
    WorkService workService;

    @Autowired
    OrderingService orderingService;

    /**
     * 作業を新規追加する。
     *
     * @param workDto 作業
     * @param model   モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{yearMonth}", method = RequestMethod.GET)
    public ModelAndView insert(WorkDto workDto, @PathVariable YearMonth yearMonth, ModelMap model) {
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        MonthDto monthDto = DateTimeUtil.getMonthStartAndEnd(month);
        List<Ordering> orderingList = orderingService.selectByMonth(monthDto);
        //デフォルト値を設置する
        workDto.setWorkType("00");
        workDto.setWorkStartDate(month);
        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);
        workDto.setWorkEndDate(endOfMonth);
        model.addAttribute("yearMonth", month);
        model.addAttribute("monthDto", monthDto);
        model.addAttribute("orderingList", orderingList);
        return new ModelAndView("work/insert", model);
    }

    /**
     * 作業を新規追加(確認)する。
     *
     * @param workDto       作業
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid WorkDto workDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/work/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("work/insertConfirm", model);
    }

    /**
     * 作業を新規追加(終了)する。
     *
     * @param workDto       作業
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid WorkDto workDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/work/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //作業を新規追加する。
        int ret = workService.insertSelective(workDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/work/insertFinish", model);
    }

    /**
     * 作業を編集する。
     *
     * @param workId 作業ID
     * @param model  モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{workId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer workId, ModelMap model) {
        //プライマリーキーで作業を検索する。
        List<WorkDto> workDtos = workService.selectAllByPrimaryKey(workId);
        //DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        //MonthDto monthDto = DateTimeUtil.getMonthStartAndEnd(month);
        WorkDto workDto = workDtos.get(0);
        model.addAttribute("yearMonth", workDto.getWorkMonth());
        model.addAttribute("workDto", workDto);
        return new ModelAndView("work/edit", model);
    }

    /**
     * 作業を編集(確認)する。
     *
     * @param workDto       作業
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid WorkDto workDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/work/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("work/editConfirm", model);
    }

    /**
     * 作業を編集(終了)する。
     *
     * @param workDto       作業
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid WorkDto workDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/work/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで作業を更新する。
        int ret = workService.updateByPrimaryKeySelective(workDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("work/editFinish", model);
    }

    /**
     * 作業一覧画面を表示する。
     *
     * @param workDto 作業
     * @param model   モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("workDto") WorkDto workDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        String userName = "";
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("userName", userName);


        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();

        if (appUser.getUserType() == 0) {
            return new ModelAndView("work/list", model);
        } else {
            return new ModelAndView("workForStaff/list", model);
        }

    }

}
