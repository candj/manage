package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.Utils.ReportUtil;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping(value = "/report")
@Controller
public class ReportController {

    /**スタッフテンプレート*/
    private static final String OFFER_TEMPLATE = "template/offerTemplate.xlsx";
    /**勤務テンプレート*/
    private static final String ORDER_TEMPLATE = "template/orderTemplate.xlsx";
    private static final String INVOICE_TEMPLATE = "template/invoiceTemplate.xlsx";

    @Autowired
    OfferService offerService;

    @Autowired
    OrderingService orderingService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    InvoiceDetailService invoiceDetailService;

    @Autowired
    CompanyService companyService;

    @Autowired
    AppUserService appUserService;

    @Autowired
    ApplicationContext ac;

    @RequestMapping(value = "/offer/export", method = RequestMethod.GET)
    @ResponseBody
    public void exportOffer(HttpServletRequest request, HttpServletResponse response, OfferDto dto,
                            @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth)
            throws Exception {

        DateTime startOfMonth = new DateTime();
        if(yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        dto.setWorkStartDate(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        dto.setWorkEndDate(endOfMonth);

        boolean companySelectNoResultFlag = false;
        if (dto.getSendCompanyName() != null&& !dto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getSendCompanyName());
            if (!companyDtoList.isEmpty()){
                dto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        if (dto.getReceiveCompanyName() != null && !dto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                dto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        List<OfferDto> OfferDtoList = new ArrayList<OfferDto>();
        if(companySelectNoResultFlag == false){
            OfferDtoList = offerService.selectAllByExample(dto);
        }
        String dirName = "見積書";

        //一時フォルダーを作る
        String dirPath = ac.getResource("classpath:").getURL().getPath() + dirName;
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdirs();
        }

        for(OfferDto o : OfferDtoList) {
            AppUserDto a = new AppUserDto();
            a.setCompanyId(o.getSendCompany().getSendCompanyId());
            a.setUserType(2);
            List<AppUserDto> appUserDtoList = appUserService.selectAllByExample(a);
            AppUserDto guest = new AppUserDto();
            if(!appUserDtoList.isEmpty()){
                guest = appUserDtoList.get(0);
            }

            OfferDetailDto offerDetailDto = new OfferDetailDto();
            offerDetailDto.setOfferId(o.getOfferId());
            List<OfferDetailDto> offerDetailDtoList = offerDetailService.selectAllByExample(offerDetailDto);
            Map<String, Object> model = new HashMap<String, Object>();
            if(o.getAfterPaymentPattern().equals("00")) {
                o.setAfterPaymentPatternName("精算なし");
            }else if(o.getAfterPaymentPattern().equals("01")){
                o.setAfterPaymentPatternName("定額");
            }else if(o.getAfterPaymentPattern().equals("10")){
                o.setAfterPaymentPatternName("時間幅上下割");
            }else if(o.getAfterPaymentPattern().equals("11")){
                o.setAfterPaymentPatternName("時間幅中間割");
            }
            model.put("offer", o);
            model.put("offerDetailList", offerDetailDtoList);
            model.put("guest", guest);
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            ReportUtil.outputExcelForZip(ac, OFFER_TEMPLATE, dirPath, "見積書_" + o.getOfferNumber().replace(" ", "_") + "_" + uuid + ".xlsx", model);
        }

        String zipFile = String.format("attachment; filename=\"%s%s\"", URLEncoder.encode(dirName,"UTF-8"), ".zip");
        ReportUtil.outputZip(response, dir, zipFile);
        File[] listFiles = dir.listFiles();
        //一時フォルダーを削除する
        for (int i = 0; i < listFiles.length; i++) {
            listFiles[i].delete();
        }
        dir.delete();
    }

    @RequestMapping(value = "/ordering/export", method = RequestMethod.GET)
    @ResponseBody
    public void exportOrdering(HttpServletRequest request, HttpServletResponse response, OrderingDto dto,
                            @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                            @RequestParam(name = "projectName", required = false) String projectName)
            throws Exception {

        DateTime startOfMonth = new DateTime();
        if(yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        dto.setWorkStartDate(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        dto.setWorkEndDate(endOfMonth);
        if(projectName.equals("")){
            projectName = null;
        }

        boolean companySelectNoResultFlag = false;
        if (dto.getSendCompanyName() != null&& !dto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getSendCompanyName());
            if (!companyDtoList.isEmpty()){
                dto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        if (dto.getReceiveCompanyName() != null && !dto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                dto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        List<OrderingDto> OrderingDtoList = new ArrayList<OrderingDto>();
        if(companySelectNoResultFlag == false){
            OrderingDtoList = orderingService.selectAllByExample(dto,projectName);
        }
        String dirName = "注文書";

        //一時フォルダーを作る
        String dirPath = ac.getResource("classpath:").getURL().getPath() + dirName;
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdirs();
        }

        for(OrderingDto o : OrderingDtoList) {
            OfferDetailDto offerDetailDto = new OfferDetailDto();
            offerDetailDto.setOfferId(o.getOfferId());
            List<OfferDetailDto> offerDetailDtoList = offerDetailService.selectAllByExample(offerDetailDto);
            Map<String, Object> model = new HashMap<String, Object>();
            if(o.getOffer().getAfterPaymentPattern().equals("00")) {
                o.getOffer().setAfterPaymentPatternName("精算なし");
            }else if(o.getOffer().getAfterPaymentPattern().equals("01")){
                o.getOffer().setAfterPaymentPatternName("定額");
            }else if(o.getOffer().getAfterPaymentPattern().equals("10")){
                o.getOffer().setAfterPaymentPatternName("時間幅上下割");
            }
            else if(o.getOffer().getAfterPaymentPattern().equals("11")){
                o.getOffer().setAfterPaymentPatternName("時間幅中間割");
            }
            model.put("ordering", o);
            model.put("offerDetailList", offerDetailDtoList);
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            ReportUtil.outputExcelForZip(ac, ORDER_TEMPLATE, dirPath, "注文書_" + o.getOrderNumber().replace(" ", "_") + "_" + uuid + ".xlsx", model);
        }

        String zipFile = String.format("attachment; filename=\"%s%s\"", URLEncoder.encode(dirName,"UTF-8"), ".zip");
        ReportUtil.outputZip(response, dir, zipFile);
        File[] listFiles = dir.listFiles();
        //一時フォルダーを削除する
        for (int i = 0; i < listFiles.length; i++) {
            listFiles[i].delete();
        }
        dir.delete();
    }

    @RequestMapping(value = "/invoice/export", method = RequestMethod.GET)
    @ResponseBody
    public void exportInvoice(HttpServletRequest request, HttpServletResponse response, InvoiceDto dto,
                               @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                               @RequestParam(name = "projectName", required = false) String projectName)
            throws Exception {

        DateTime startOfMonth = new DateTime();
        if(yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        dto.setWorkStartDate(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        dto.setWorkEndDate(endOfMonth);
        if(projectName.equals("")){
            projectName = null;
        }

        boolean companySelectNoResultFlag = false;
        if (dto.getSendCompanyName() != null&& !dto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getSendCompanyName());
            if (!companyDtoList.isEmpty()){
                dto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        if (dto.getReceiveCompanyName() != null && !dto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(dto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                dto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                companySelectNoResultFlag = true;
            }
        }

        List<InvoiceDto> InvoiceDtoList = new ArrayList<InvoiceDto>();
        if(companySelectNoResultFlag == false){
            InvoiceDtoList = invoiceService.selectAllByExample(dto,projectName);
        }
        String dirName = "請求書";

        //一時フォルダーを作る
        String dirPath = ac.getResource("classpath:").getURL().getPath() + dirName;
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdirs();
        }

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for(InvoiceDto i : InvoiceDtoList) {
            i.setDeliveryDate(DateTimeUtil.dateTimeToStringDate(i.getInvoiceDeliveryDate()));

            AppUserDto a = new AppUserDto();
            a.setCompanyId(i.getOrdering().getOffer().getSendCompany().getSendCompanyId());
            a.setUserType(2);
            List<AppUserDto> appUserDtoList = appUserService.selectAllByExample(a);
            AppUserDto guest = new AppUserDto();
            if(!appUserDtoList.isEmpty()){
                guest = appUserDtoList.get(0);
            }

            InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
            invoiceDetailDto.setInvoiceId(i.getInvoiceId());
            List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailService.selectAllByExample(invoiceDetailDto);
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("invoice", i);
            model.put("invoiceDetailList", invoiceDetailDtoList);
            model.put("guest", guest);
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            ReportUtil.outputExcelForZip(ac, INVOICE_TEMPLATE, dirPath, "請求書_" + i.getInvoiceNumber().replace(" ", "_") + "_" +  uuid + ".xlsx", model);
        }

        String zipFile = String.format("attachment; filename=\"%s%s\"", URLEncoder.encode(dirName,"UTF-8"), ".zip");
        ReportUtil.outputZip(response, dir, zipFile);
        File[] listFiles = dir.listFiles();
        //一時フォルダーを削除する
        for (int i = 0; i < listFiles.length; i++) {
            listFiles[i].delete();
        }
        dir.delete();
    }

    @RequestMapping(value = "/offer/exportOne", method = RequestMethod.GET)
    @ResponseBody
    public void exportOfferOne(HttpServletRequest request, HttpServletResponse response, OfferDto dto) throws Exception {

        OfferDto offerDto = offerService.selectAllByPrimaryKey(dto.getOfferId()).get(0);

        AppUserDto a = new AppUserDto();
        a.setCompanyId(offerDto.getSendCompany().getSendCompanyId());
        a.setUserType(2);
        List<AppUserDto> appUserDtoList = appUserService.selectAllByExample(a);
        AppUserDto guest = new AppUserDto();
        if(!appUserDtoList.isEmpty()){
            guest = appUserDtoList.get(0);
        }

        OfferDetailDto offerDetailDto = new OfferDetailDto();
        offerDetailDto.setOfferId(offerDto.getOfferId());
        List<OfferDetailDto> offerDetailDtoList = offerDetailService.selectAllByExample(offerDetailDto);
        Map<String, Object> model = new HashMap<String, Object>();
        if(offerDto.getAfterPaymentPattern().equals("00")) {
            offerDto.setAfterPaymentPatternName("精算なし");
        }else if(offerDto.getAfterPaymentPattern().equals("01")){
            offerDto.setAfterPaymentPatternName("定額");
        }else if(offerDto.getAfterPaymentPattern().equals("10")){
            offerDto.setAfterPaymentPatternName("時間幅上下割");
        }
        else if(offerDto.getAfterPaymentPattern().equals("11")){
            offerDto.setAfterPaymentPatternName("時間幅中間割");
        }
        model.put("offer", offerDto);
        model.put("offerDetailList", offerDetailDtoList);
        model.put("guest", guest);
        ReportUtil.outputExcelForHttp(ac, OFFER_TEMPLATE, response, "見積書_" + offerDto.getOfferNumber().replace(" ", "_") + ".xlsx", model);

    }

    @RequestMapping(value = "/order/exportOne", method = RequestMethod.GET)
    @ResponseBody
    public void exportOrderOne(HttpServletRequest request, HttpServletResponse response, OrderingDto dto) throws Exception {

        OrderingDto orderingDto = orderingService.selectAllByPrimaryKey(dto.getOrderId()).get(0);

        OfferDetailDto offerDetailDto = new OfferDetailDto();
        offerDetailDto.setOfferId(orderingDto.getOfferId());
        List<OfferDetailDto> offerDetailDtoList = offerDetailService.selectAllByExample(offerDetailDto);
        Map<String, Object> model = new HashMap<String, Object>();
        if(orderingDto.getOffer().getAfterPaymentPattern().equals("00")) {
            orderingDto.getOffer().setAfterPaymentPatternName("精算なし");
        }else if(orderingDto.getOffer().getAfterPaymentPattern().equals("01")){
            orderingDto.getOffer().setAfterPaymentPatternName("定額");
        }else if(orderingDto.getOffer().getAfterPaymentPattern().equals("10")){
            orderingDto.getOffer().setAfterPaymentPatternName("時間幅上下割");
        }
        else if(orderingDto.getOffer().getAfterPaymentPattern().equals("11")){
            orderingDto.getOffer().setAfterPaymentPatternName("時間幅中間割");
        }
        model.put("ordering", orderingDto);
        model.put("offerDetailList", offerDetailDtoList);
        ReportUtil.outputExcelForHttp(ac, ORDER_TEMPLATE, response, "注文書_" + orderingDto.getOrderNumber().replace(" ", "_") + ".xlsx", model);

    }

    @RequestMapping(value = "/invoice/exportOne", method = RequestMethod.GET)
    @ResponseBody
    public void exportInvoiceOne(HttpServletRequest request, HttpServletResponse response, InvoiceDto dto) throws Exception {

        InvoiceDto invoiceDto = invoiceService.selectAllByPrimaryKey(dto.getInvoiceId()).get(0);
        invoiceDto.setDeliveryDate(DateTimeUtil.dateTimeToStringDate(invoiceDto.getInvoiceDeliveryDate()));

        AppUserDto a = new AppUserDto();
        a.setCompanyId(invoiceDto.getOrdering().getOffer().getSendCompany().getSendCompanyId());
        a.setUserType(2);
        List<AppUserDto> appUserDtoList = appUserService.selectAllByExample(a);
        AppUserDto guest = new AppUserDto();
        if(!appUserDtoList.isEmpty()){
            guest = appUserDtoList.get(0);
        }

        InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
        invoiceDetailDto.setInvoiceId(invoiceDto.getInvoiceId());
        List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailService.selectAllByExample(invoiceDetailDto);
        for(InvoiceDetailDto i : invoiceDetailDtoList){
            if(i.getCommutingCostAmount() > 0){
                i.setCommutingCostString("交通費：" + i.getCommutingCostAmount().toString() + "円");
            }
        }
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("invoice", invoiceDto);
        model.put("invoiceDetailList", invoiceDetailDtoList);
        model.put("guest", guest);
        ReportUtil.outputExcelForHttp(ac, INVOICE_TEMPLATE, response, "請求書_" + invoiceDto.getInvoiceNumber().replace(" ", "_") + ".xlsx", model);

    }

}
