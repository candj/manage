package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * プロジェクト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class ProjectDto {
    /** プロジェクトID */
    @Digits(integer=10, fraction=0)
    private Integer projectId;

    /** 見積 */
    private java.util.List<OfferDto> offer;

    /** プロジェクト名 */
    @Length(max=128)
    private String projectName;

    /** 開発言語 */
    @Length(max=64)
    private String programmingLanguage;

    /** デ—タベ—ス */
    @Length(max=64)
    private String projectDatabase;

    /** プログラム規模 */
    @Digits(integer=10, fraction=0)
    private Integer programSize;

    /** プロジェクト摘要 */
    @Length(max=1000)
    private String projectDescription;

    /** 開始期日 */
    private org.joda.time.DateTime projectStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime projectEndDate;

    /** 元会社ID */
    @Digits(integer=10, fraction=0)
    private Integer sendCompanyId;

    /** 元会社 */
    private com.candj.crm.mapper.model.SendCompany sendCompany;

    /** 先会社ID */
    @Digits(integer=10, fraction=0)
    private Integer receiveCompanyId;

    /** 先会社 */
    private com.candj.crm.mapper.model.ReceiveCompany receiveCompany;

    /** プロジェクトステータス */
    @Length(max=2)
    private String projectStatus;

    /** 見積作成FLAG */
    private boolean haveOfferFlag;

    /** 削除FLAG */
    private boolean projectDeleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    private  String sendCompanyName;

    private String receiveCompanyName;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
