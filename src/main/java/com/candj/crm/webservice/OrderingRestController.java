package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 注文情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/orderingRest")
@RestController
public class OrderingRestController {

    @Autowired
    OrderingService orderingService;


    @Autowired
    InvoiceService invoiceService;

    @Autowired
    OfferService offerService;

    @Autowired
    CompanyService companyService;

    /**
     * 注文を新規追加する。
     * @param orderId 注文ID
     * @return 結果
     */
    @RequestMapping(value = "{orderId}", method = RequestMethod.GET)
    public OrderingDto get(@PathVariable Integer orderId) {
        //プライマリーキーで注文を検索する。
        OrderingDto ret = orderingService.selectByPrimaryKey(orderId);
        return ret;
    }

    /**
     * 注文を新規追加する。
     * @param orderingDto 注文
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody OrderingDto orderingDto, Errors rrrors) {

        orderingDto.setOrderDeleteFlag(true);

        //注文番号を生成する
        OfferDto offerDto = offerService.selectAllByPrimaryKey(orderingDto.getOfferId()).get(0);
        orderingDto.setOrderNumber(offerDto.getReceiveCompany().getReceiveCompanyNameAbbr()
                + " "
                + DateTimeUtil.dateTimeToStringNumber(orderingDto.getOrderDate()));

        //注文を新規追加する。
        int ret = orderingService.insertSelective(orderingDto);
        return ret;
    }

    /**
     * 注文を変更する。
     * @param orderingDto 注文
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody OrderingDto orderingDto, Errors rrrors) {
        orderingDto.setOrderDeleteFlag(true);

        //注文番号を生成する
        OfferDto offerDto = offerService.selectAllByPrimaryKey(orderingDto.getOfferId()).get(0);
        orderingDto.setOrderNumber(offerDto.getReceiveCompany().getReceiveCompanyNameAbbr()
                + " "
                + DateTimeUtil.dateTimeToStringNumber(orderingDto.getOrderDate()));

        //プライマリーキーで注文を更新する。
        int ret = orderingService.updateByPrimaryKeySelective(orderingDto);
        return ret;
    }

    /**
     * 注文を新規追加する。
     * @param orderId 注文ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{orderId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer orderId) {

        OrderingDto orderingDto = orderingService.selectByPrimaryKey(orderId);

        //連携削除
        List<InvoiceDto> invoiceDtoList = invoiceService.selectByOrderId(orderId);
        if(!invoiceDtoList.isEmpty()){
            InvoiceDto invoiceDto = invoiceDtoList.get(0);
            invoiceDto.setInvoiceDeleteFlag(false);
            //請求を削除する
            invoiceService.updateByPrimaryKey(invoiceDto);
        }

        orderingDto.setOrderDeleteFlag(false);

        //注文を削除する。
        int ret = orderingService.updateByPrimaryKey(orderingDto);
        return ret;
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderingDto 注文
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<OrderingDto> list(OrderingDto orderingDto,
                                      @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                      @RequestParam(name = "projectName", required = false) String projectName,
                                      JqgridPageReq pageReq) {

        //条件で注文を検索する。（連携情報含む）
        DateTime startOfMonth = new DateTime();
        if(yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);

        if (orderingDto.getSendCompanyName() != null && !orderingDto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(orderingDto.getSendCompanyName());
            if (!companyDtoList.isEmpty()){
                orderingDto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<OrderingDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        if (orderingDto.getReceiveCompanyName() != null && !orderingDto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(orderingDto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                orderingDto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            }else {
                Page<OrderingDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0){
            Page<OrderingDto> page = PageUtil.startPage(pageReq);
            orderingDto.setWorkStartDate(startOfMonth);
            orderingDto.setWorkEndDate(endOfMonth);
            if(projectName.equals("")){
                projectName = null;
            }
            List<OrderingDto> ret = orderingService.selectAllByExample(orderingDto, projectName);
            return PageUtil.resp(page);
        }else {
            Page<OrderingDto> page = PageUtil.startPage(pageReq);
            orderingDto.setWorkStartDate(startOfMonth);
            orderingDto.setWorkEndDate(endOfMonth);
            List<OrderingDto> ret = orderingService.selectByMyExample(orderingDto, appUser.getUserId());
            return PageUtil.resp(page);
        }
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderingDto 注文
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<OrderingDto> listAll(OrderingDto orderingDto, JqgridPageReq pageReq) {
        Page<OrderingDto> page = PageUtil.startPage(pageReq);
        //条件で注文を検索する。（連携情報含む）
        List<OrderingDto> ret = orderingService.selectByExample(orderingDto);
        return PageUtil.resp(page);
    }
}
