var colNames = [
    "個人所得税率ID",
    "報酬月額下限",
    "報酬月額上限",
    "扶養人数0",
    "扶養人数1",
    "扶養人数2",
    "扶養人数3",
    "扶養人数4",
    "扶養人数5",
    "扶養人数6",
    "扶養人数7",
    "料率"
];

var colModel = [
    {
        name: "personalIncomeTaxId",
        index: "personalIncomeTaxId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "minMonthlyPayment",
        index: "minMonthlyPayment",
        width: 110,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "maxMonthlyPayment",
        index: "maxMonthlyPayment",
        width: 110,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport0",
        index: "numberOfSupport0",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport1",
        index: "numberOfSupport1",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport2",
        index: "numberOfSupport2",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport3",
        index: "numberOfSupport3",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport4",
        index: "numberOfSupport4",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport5",
        index: "numberOfSupport5",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport6",
        index: "numberOfSupport6",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "numberOfSupport7",
        index: "numberOfSupport7",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "tax",
        index: "tax",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
];

var postData = {
    yearMonth: $('#yearMonth').val(),
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData);
});	
	