package com.candj.crm.webservice;

import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.OrderDto;
import com.candj.crm.service.OrderService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注文情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/orderRest")
@RestController
public class OrderRestController {
    @Autowired
    OrderService orderService;

    /**
     * 注文を新規追加する。
     * @param orderId 注文ID
     * @return 結果
     */
    @RequestMapping(value = "{orderId}", method = RequestMethod.GET)
    public OrderDto get(@PathVariable Integer orderId) {
        //プライマリーキーで注文を検索する。
        OrderDto ret = orderService.selectByPrimaryKey(orderId);
        return ret;
    }

    /**
     * 注文を新規追加する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody OrderDto orderDto, Errors rrrors) {
        //注文を新規追加する。
        int ret = orderService.insertSelective(orderDto);
        return ret;
    }

    /**
     * 注文を変更する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody OrderDto orderDto, Errors rrrors) {
        //プライマリーキーで注文を更新する。
        int ret = orderService.updateByPrimaryKeySelective(orderDto);
        return ret;
    }

    /**
     * 注文を新規追加する。
     * @param orderId 注文ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{orderId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer orderId) {
        //注文を削除する。
        int ret = orderService.deleteByPrimaryKey(orderId);
        return ret;
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderDto 注文
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<OrderDto> list(OrderDto orderDto, JqgridPageReq pageReq) {
        Page<OrderDto> page = PageUtil.startPage(pageReq);
        //条件で注文を検索する。（連携情報含む）
        List<OrderDto> ret = orderService.selectByExample(orderDto);
        return PageUtil.resp(page);
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderDto 注文
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<OrderDto> listAll(OrderDto orderDto, JqgridPageReq pageReq) {
        Page<OrderDto> page = PageUtil.startPage(pageReq);
        //条件で注文を検索する。（連携情報含む）
        List<OrderDto> ret = orderService.selectAllByExample(orderDto);
        return PageUtil.resp(page);
    }
}
