package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 費用
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Cost {
    /** ID */
    private Integer costId;

    /** 費用日付 */
    private org.joda.time.DateTime costDate;

    /** 摘要 */
    private String costSummary;

    /** 収入金额 */
    private Integer incomeAmount;

    /** 支出金额 */
    private Integer expendAmount;

    /** 事務費の入金 */
    private Integer businessIncomeAmount;

    /** 事務費の支出 */
    private Integer businessExpendAmount;

    /** 当月分費用 */
    private Integer costTotalAmount;

    /** コメント */
    private String remark;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
