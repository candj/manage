package com.candj.crm.controller;

import com.candj.crm.dto.PaySiteDto;
import com.candj.crm.service.PaySiteService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 支払いサイト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/paySite")
@Controller
public class PaySiteController {
    @Autowired
    PaySiteService paySiteService;

    /**
     * 支払いサイトを新規追加する。
     * @param paySiteDto 支払いサイト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(PaySiteDto paySiteDto, ModelMap model) {
        return new ModelAndView("paySite/insert", model);
    }

    /**
     * 支払いサイトを新規追加(確認)する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid PaySiteDto paySiteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/paySite/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("paySite/insertConfirm", model);
    }

    /**
     * 支払いサイトを新規追加(終了)する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid PaySiteDto paySiteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/paySite/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //支払いサイトを新規追加する。
        int ret = paySiteService.insertSelective(paySiteDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/paySite/insertFinish", model);
    }

    /**
     * 支払いサイトを編集する。
     * @param paySiteId 支払いサイトID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{paySiteId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer paySiteId, ModelMap model) {
        //プライマリーキーで支払いサイトを検索する。
        PaySiteDto aySiteDto = paySiteService.selectByPrimaryKey(paySiteId);
        model.addAttribute("aySiteDto", aySiteDto);
        return new ModelAndView("paySite/edit", model);
    }

    /**
     * 支払いサイトを編集(確認)する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid PaySiteDto paySiteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/paySite/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("paySite/editConfirm", model);
    }

    /**
     * 支払いサイトを編集(終了)する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid PaySiteDto paySiteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/paySite/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで支払いサイトを更新する。
        int ret = paySiteService.updateByPrimaryKeySelective(paySiteDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("paySite/editFinish", model);
    }

    /**
     * 支払いサイト一覧画面を表示する。
     * @param paySiteDto 支払いサイト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("paySiteDto") PaySiteDto paySiteDto, ModelMap model) {
        return new ModelAndView("paySite/list", model);
    }
}
