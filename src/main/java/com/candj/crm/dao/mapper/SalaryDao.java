package com.candj.crm.dao.mapper;

import com.candj.crm.dto.MonthDto;
import com.candj.crm.mapper.model.Salary;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 給料情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface SalaryDao {
    /**
     * プライマリーキーで給料を検索する。
     * @param salaryId ID
     * @return 結果
     */
    Salary selectByPrimaryKey(Integer salaryId);

    /**
     * プライマリーキーで給料検索する。（連携情報含む）
     * @param salaryId ID
     * @return 結果
     */
    List<Salary> selectAllByPrimaryKey(Integer salaryId);

    /**
     * 条件で給料を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Salary> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で給料を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Salary> selectByExample(@Param("example") WhereCondition example);

    /**
     * 給料を新規追加する。
     * @param record 給料
     * @return 結果
     */
    int insertSelective(Salary record);

    /**
     * 給料を新規追加する。
     * @param record 給料
     * @return 結果
     */
    int insert(Salary record);

    /**
     * プライマリーキーで給料を更新する。
     * @param record 給料
     * @return 結果
     */
    int updateByPrimaryKey(Salary record);

    /**
     * プライマリーキーで給料を更新する。
     * @param record 給料
     * @return 結果
     */
    int updateByPrimaryKeySelective(Salary record);

    /**
     * 給料を更新する。
     * @param record 給料
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Salary record, @Param("example") WhereCondition example);

    /**
     * 給料を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Salary example);

    /**
     * 給料を削除する。
     * @param salaryId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer salaryId);

    /**
     * プライマリーキーで給料を検索する。
     * @param record ID
     * @return 結果
     */
    List<Salary> selectByStaffAndMonth(MonthDto record);
}
