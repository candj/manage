package com.candj.crm.batch;

import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.util.DateUtil;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Component
public class salaryTask {
    private static final Logger logger = LoggerFactory.getLogger(salaryTask.class);

    @Autowired
    SalaryService salaryService;

    @Autowired
    AppUserService appuserService;

    @Autowired
    WorkService workService;

    @Autowired
    CommuteService commuteService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    CompanyService companyService;

    @Autowired
    StaffSalaryInfoService staffSalaryInfoService;

    public static String MONTH_FORMAT = "yyyy-MM";

    // エクセルexportのパースを取得する
    //@Value("${spring.businessHour.batchPath}")
    //String filePath;

    private File outputFolder = null;

    @Scheduled(cron = "0 00 12 10 * ?")//0/10 * * * * ?
    public void taskCycle() throws IOException {

        // 毎日12時00分にE001の機能で前日分の統計データを自動生成
        DateTime lastMonth = DateTime.parse(DateUtil.now().minusMonths(1).toString(MONTH_FORMAT));

        List<AppUserDto> appUserDtoList = appuserService.selectThisStaffByExample(null);
        for (AppUserDto appUserDto : appUserDtoList) {
            SalaryDto salaryDto = new SalaryDto();
            salaryDto.setUserId(appUserDto.getUserId());

            //作業を選択する
            salaryDto.setSalaryMonth(lastMonth);
            WorkDto workDto = new WorkDto();
            workDto.setUserId(appUserDto.getUserId());
            workDto.setWorkMonth(lastMonth);
            List<WorkDto> workDtoList = workService.selectAllByUserAndMonth(workDto);

            //交通費
            CommuteDto commuteDto = new CommuteDto();
            commuteDto.setUserId(salaryDto.getUserId());
            commuteDto.setCommuteDate(lastMonth);
            List<CommuteDto> commuteDtos = commuteService.selectByCommuteDto(commuteDto);

            int commutingCost = 0;
            if (!commuteDtos.isEmpty()) {

                for (CommuteDto commute : commuteDtos) {
                    if ("01".equals(commute.getCommuteAdmitStatus())) {
                        commutingCost = commute.getCommuteAmount();
                    } else {
                        commutingCost = 0;
                    }

                    if (salaryDto.getCommutingCost() == null) {
                        salaryDto.setCommutingCost(commutingCost);
                    } else {
                        salaryDto.setCommutingCost(commutingCost + salaryDto.getCommutingCost());
                    }
                }
            }else{
                salaryDto.setCommutingCost(0);
            }

            salaryDto.setSalaryAmount(salaryDto.getSalaryAmount() == null ? commutingCost : salaryDto.getSalaryAmount() + commutingCost);

            DateTime endOfMonth = lastMonth.plusMonths(1).minusSeconds(1);
            StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
            staffSalaryInfoDto.setSalaryStartDate(lastMonth);
            staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
            staffSalaryInfoDto.setUserId(salaryDto.getUserId());
            List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
            salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());

            if (!staffSalaryInfoDtoList.isEmpty()) {
                salaryService.makeSalary(workDtoList, lastMonth, staffSalaryInfoDtoList.get(0), salaryDto);
            } else {
                salaryService.makeSalary(workDtoList, lastMonth, null, salaryDto);
            }




            CompanyDto companyDto = new CompanyDto();
            companyDto.setCompanyId(appUserDto.getCompanyId());

            List<CompanyDto> companyDtoList = companyService.selectByExample(companyDto);
            if (!companyDtoList.isEmpty()) {
                DateTime dateTime = new DateTime();
                if (companyDtoList.get(0).getSalaryPayTime().intValue() == 0) {
                    DateTime theEndDataOfMonth = dateTime.dayOfMonth().withMaximumValue();
                    salaryDto.setPlanPayDate(theEndDataOfMonth);
                } else if (companyDtoList.get(0).getSalaryPayTime().intValue() == 1) {
                    DateTime theFirstDateOfMonth = dateTime.dayOfMonth().withMinimumValue().plusDays(14);
                    salaryDto.setPlanPayDate(theFirstDateOfMonth);
                } else {
                    DateTime theFirstDateOfMonth = dateTime.dayOfMonth().withMinimumValue().plusDays(24);
                    salaryDto.setPlanPayDate(theFirstDateOfMonth);
                }
            }

            try {
                salaryService.insertSelective(salaryDto);
                System.out.println("給料作成完了");
            } catch (Exception ex) {
                logger.error("例外をキャッチしました。", ex);
            }
        }
		/*WorkTaskDto dto = new WorkTaskDto();
		dto.setWorkStartTime(yesterday);
		String filename = String.format("OverAllMgr_Ver0.3_%s.xlsx", yesterday.toString(DateUtil.BM_DATE_FORMAT));

		File file = new File(outputFolder, filename);

		OutputStream os = new FileOutputStream(file);

		try {
			reportService.outputBusinessDayHourFile(dto, os);
		} catch (Exception ex) {
			logger.error("例外をキャッチしました。", ex);
		} finally {
			if (os != null) {
				os.close();
			}

		}*/
    }
}
