

			var colNames = [
				"注文ID",
				"見積 ID",
				"注文番号",
				"ステータス",
			];
			var colModel = [
				{
					name: "orderId",
					index: "orderId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "offerId",
					index: "offerId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "orderNumber",
					index: "orderNumber",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "orderStatus",
					index: "orderStatus",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	