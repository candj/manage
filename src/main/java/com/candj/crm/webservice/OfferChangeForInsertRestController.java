package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.dto.StaffSalaryInfoDto;
import com.candj.crm.service.OfferTemporaryService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OfferChangeForInsertRestController {

    @Autowired
    OfferTemporaryService offerTemporaryService;

    /**
     *
     * @param receiveCompanyId スタッフID
     * @return 結果
     */
    @RequestMapping(value = "offer/insert/offerChangeForInsert", method = RequestMethod.POST)
    public Object changeCompanyForInsert(Integer receiveCompanyId) throws Exception{
        //条件で注文を検索する。（連携情報含む）
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        offerDto.setReceiveCompanyId(receiveCompanyId);
        offerTemporaryService.updateByPrimaryKeySelective(offerDto);

        return offerDto;
    }

    /**
     *
     * @param startDate 開始時間
     * @return 結果
     */
    @RequestMapping(value = "offer/insert/offerChangeStartDateForInsert", method = RequestMethod.POST)
    public Object changeStartDateForInsert(String startDate) throws Exception{
        DateTime date = DateTimeUtil.StringToDateTimeDate(startDate);
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        offerDto.setWorkStartDate(date);
        offerTemporaryService.updateByPrimaryKeySelective(offerDto);

        return offerDto;
    }

    /**
     *
     * @param endDate 開始時間
     * @return 結果
     */
    @RequestMapping(value = "offer/insert/offerChangeEndDateForInsert", method = RequestMethod.POST)
    public Object changeEndDateForInsert(String endDate) throws Exception{
        DateTime date = DateTimeUtil.StringToDateTimeDate(endDate);
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        offerDto.setWorkEndDate(date);
        offerTemporaryService.updateByPrimaryKeySelective(offerDto);

        return offerDto;
    }

    /**
     *
     * @param workName 開始時間
     * @return 結果
     */
    @RequestMapping(value = "offer/insert/offerChangeWorkNameForInsert", method = RequestMethod.POST)
    public Object changeWorkNameForInsert(String workName) throws Exception{
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        offerDto.setWorkName(workName);
        offerTemporaryService.updateByPrimaryKeySelective(offerDto);

        return offerDto;
    }


}
