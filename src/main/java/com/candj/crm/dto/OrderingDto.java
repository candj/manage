package com.candj.crm.dto;

import javax.validation.constraints.Digits;

import com.candj.crm.mapper.model.InvoiceColumnOnly;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 注文
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OrderingDto {
    /** 注文ID */
    @Digits(integer=10, fraction=0)
    private Integer orderId;

    /** 請求 */
    private java.util.List<InvoiceDto> invoice;

    /** 請求 */
    private InvoiceColumnOnly invoiceDto;

    /** 見積 ID */
    @Digits(integer=10, fraction=0)
    private Integer offerId;

    /** 請求 ID */
    @Digits(integer=10, fraction=0)
    private Integer invoiceId;

    /** 見積 */
    private com.candj.crm.mapper.model.Offer offer;

    /** 元会社ID */
    @Digits(integer=10, fraction=0)
    private Integer sendCompanyId;

    /** 先会社ID */
    @Digits(integer=10, fraction=0)
    private Integer receiveCompanyId;

    /** プロジェクトID */
    @Digits(integer=10, fraction=0)
    private Integer projectId;

    /** 注文番号 */
    @Length(max=64)
    private String orderNumber;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 日付 */
    private org.joda.time.DateTime orderDate;

    /** ステータス */
    @Length(max=2)
    private String orderStatus;

    /** 請求作成FLAG */
    private boolean haveInvoiceFlag;

    /** 削除FLAG */
    private boolean orderDeleteFlag;

    private String sendCompanyName;

    private String receiveCompanyName;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
