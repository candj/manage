package com.candj.crm.Utils;

import com.candj.crm.dto.LabourInsuranceDto;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.mapper.model.LabourInsurance;
import com.candj.crm.service.LabourInsuranceService;
import com.candj.crm.service.TaxService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class Calculate {

    @Autowired
    TaxService taxService;

    @Autowired
    LabourInsuranceService labourInsuranceService;

    public Integer salaryAmountCalculate(Integer salary, BigDecimal manHour){
        Double mh = manHour.doubleValue();
        Double s = salary * mh;
        return s.intValue();
    }

    public Integer salaryExtraCalculate(Integer salaryExtra, Integer salaryDeduction, BigDecimal manHour){
        Double mh = manHour.doubleValue();
        Double s;
        if(mh > 0) {
            s = salaryExtra * mh;
        }
        else if(mh < 0){
            s = salaryDeduction * mh;
        } else{
            s = 0.0;
        }
        return s.intValue();
    }

    public Integer consumptionTax(Integer amountWithoutTax, TaxDto dto, DateTime startDate){
        List<TaxDto> taxDtos = taxService.selectByNameAndLimit(dto);
        Double taxAmount = 0.00;
        for(TaxDto t : taxDtos){
            boolean isBefore = startDate.isBefore(t.getTaxStartMonth());
            if(!isBefore){
                taxAmount = t.getTaxRate().doubleValue() * amountWithoutTax * 0.01;
                break;
            }
        }
        return taxAmount.intValue();
    }

    public Integer taxAmount(Integer amountWithoutTax, TaxDto dto, DateTime yearMonth){
        List<TaxDto> taxDtos = taxService.selectByNameAndLimit(dto);
        Double taxAmount = 0.00;
        for(TaxDto t : taxDtos){
            if(!yearMonth.isBefore(t.getTaxStartMonth())){
                taxAmount = t.getTaxRate().doubleValue() * amountWithoutTax * 0.01;
                break;
            }
        }
        return taxAmount.intValue();
    }

    public Integer labourInsuranceAmount(Integer basePay, DateTime yearMonth, String type){
        LabourInsuranceDto labourInsuranceDto = new LabourInsuranceDto();
        labourInsuranceDto.setLabourInsuranceStartMonth(yearMonth);
        labourInsuranceDto.setLabourInsuranceEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
        LabourInsuranceDto dto = labourInsuranceService.selectAllByExample(labourInsuranceDto).get(0);
        Double taxAmount = 0.00;
        if(type.equals("personal")) {
            taxAmount = dto.getLabourInsuranceRatePersonal().doubleValue() * basePay * 0.01;
        } else if(type.equals("company")){
            taxAmount = dto.getLabourInsuranceRateCompany().doubleValue() * basePay * 0.01;
        }

        return taxAmount.intValue();
    }
}
