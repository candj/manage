package com.candj.crm.Utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JxlsUtils {

	private static final String SHIFT_TYPE1 = "朝";
	private static final String SHIFT_TYPE2 = "昼";
	private static final String SHIFT_TYPE3 = "夜";

	private Map<String, Integer> countMap = new HashMap<>();

	public Integer count(String var) {
		if (var == null)
			return null;
		if (countMap.containsKey(var)) {
			Integer t = countMap.get(var);
			t += 1;
			countMap.replace(var, t);
			return t;
		} else {
			countMap.put(var, 1);
		}
		return 1;
	}

	public static Double divide(Double val) {
		if (val == null) {
			return 0.0;
		}
		BigDecimal bg = new BigDecimal(val * 1.0 / 60);
		Double d = bg.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
		return d;
	}

	public Double divide(Double val, int div) {
		if (val == null) {
			return 0.0;
		}
		BigDecimal bg = new BigDecimal(val * 1.0 / div);
		Double d = bg.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
		return d;
	}

	public Date formatDate(DateTime source) {
		if (source == null) {
			return null;
		}
		return source.toLocalDate().toDate();
	}

	public LocalTime formatTime(DateTime source) {
		if (source == null) {
			return null;
		}
		return LocalTime.of(source.toLocalTime().get(DateTimeFieldType.hourOfDay())
				, source.toLocalTime().get(DateTimeFieldType.minuteOfHour()));
	}

	public Date formatDateTime(DateTime source) {
		if (source == null) {
			return null;
		}
		return source.toLocalDateTime().toDate();
	}

	public String formatDate(DateTime source, String format) {
		if (source == null) {
			return null;
		}
		return source.toString(DateTimeFormat.forPattern(format));
	}


	public String formatStartWorkTime(DateTime startWorkTime, String formatStr) {
		if (startWorkTime == null) {
			return "";
		}
		return startWorkTime.toString(DateTimeFormat.forPattern(formatStr));
	}

	public String formatEndWorkTime(DateTime startWorkTime, DateTime endWorkTime, String formatStr) {
		if (startWorkTime == null || endWorkTime == null) {
			return "";
		}
		if (endWorkTime.get(DateTimeFieldType.dayOfMonth()) != startWorkTime.get(DateTimeFieldType.dayOfMonth())) {
			String time = endWorkTime.toString(DateTimeFormat.forPattern(formatStr));
			String[] arr = time.split(":");
			return (Integer.parseInt(arr[0]) + 24) + ":" + arr[1];
		} else {
			return endWorkTime.toString(DateTimeFormat.forPattern(formatStr));
		}
	}

	public static void main(String[] args) {
		DateTime start = new DateTime("2013-08-01T12:00:00.666");
		DateTime end = new DateTime("2013-08-01T13:50:00.999");
		// Periodで期間を定義
		Period period = new Period(start, end);
		System.out.println(String.format("%0" + 2 + "d", period.getHours()));
	}

	public String getShiftType(DateTime startWorkTime) {

		DateTime dt1 = startWorkTime.withHourOfDay(12).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0);
		DateTime dt2 = startWorkTime.withHourOfDay(21).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0);

		if (startWorkTime.isBefore(dt1)) {
			return SHIFT_TYPE1;
		} else if (startWorkTime.isBefore(dt2)) {
			return SHIFT_TYPE2;
		} else {
			return SHIFT_TYPE3;
		}
	}
}
