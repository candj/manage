var context_path = $('meta[name=basepath]').attr('content');

var colNames = [
    "注文ID",
    "請求ID",
    "請求番号",
    "作成</br>ステータス",
    "開始時間",
    "終了時間",
    "作業時間基準",
    "作業時間</br>精算標準",
    "支払いサイト",
    "精算方法",
    "税込み合計</br>金額(円)",
    "税抜き合計</br>金額(円)",
    "消費税(円)",
    "発注元</br>会社名",
    "発注先</br>会社名",
    "種類",
    "業務名",
    "注文番号",
    "見積ID",
    "見積番号",
];
var colModel = [
    {
        name: "orderId",
        index: "orderId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
		name: "invoiceDto.invoiceId",
		index: "invoiceDto.invoiceId",
		width: 100,
		search : false,
        hidden: true,
		align: "right",
	},
    {
        name: "invoiceDto.invoiceNumber",
        index: "invoiceDto.invoiceNumber",
        width: 95,
        search: false,
        formatter : function(cellvalue, options, rowObject) {
          if(cellvalue != null && cellvalue != undefined){
            var selectHtml = '<a href="javascript:void(0);" onclick="openOrderDetail('
              + rowObject.invoiceDto.invoiceId
              + ')">'
              + cellvalue
              + '</a>';
          }else {
            selectHtml = '<label style="" data-url="'
        	  + context_path
        	  + 'invoice/insert/'
        	  + rowObject.orderId
        	  + '" class="emptyCell"><i class="fa fa-plus"> 追加</i></label>'
          }
            return selectHtml;
        },
        align: "center",
    },
    {
        name: "orderStatus",
        index: "orderStatus",
        width: 80,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '<b style="color:red">×<b>';
            } else if (cellvalue == '01') {
                return '○';
            }
        },
        align: "center",
    },
    {
        name: "offer.workStartDate",
        index: "offer.workStartDate",
        width: 100,
        search : false,
        formatter : function(cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },

    {
        name: "offer.workEndDate",
        index: "offer.workEndDate",
        width: 100,
        search : false,
        formatter : function(cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },

    {
        name: "offer.workTimeLowerLimit",
        index: "offer.workTimeLowerLimit",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == null) {
                return '';
            }
            if (rowObject.offer.workTimeUpperLimit != null) {
                var lower = cellvalue;
                var upper = rowObject.offer.workTimeUpperLimit;
                return cellvalue + 'h~' + upper + 'h';
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
					name: "offer.workTimeCalculateStandard",
					index: "offer.workTimeCalculateStandard",
					width: 80,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue == '00') {
							return '15分';
						} else if (cellvalue == '01') {
							return '30分';
						}
					},
					align: "left",
	},
    {
        name: "offer.paySite.paySiteName",
        index: "offer.paySite.paySiteName",
        width: 130,
        search: false,
        align: "left",
    },
    {
        name: "offer.afterPaymentPattern",
        index: "offer.afterPaymentPattern",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '精算なし';
            } else if (cellvalue == '01') {
                return '定額';
            } else if (cellvalue == '10') {
                return '時間幅上下割';
            }else if (cellvalue == '11') {
                return '時間幅中間割';
            }
        },
        align: "left",
    },
    {
					name: "offer.offerTotalAmount",
					index: "offer.offerTotalAmount",
					width: 110,
					search : false,
					formatter : "integer",

					align: "right",
	},
    {
        name: "offer.offerAmountWithoutTax",
        index: "offer.offerAmountWithoutTax",
        width: 110,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
					name: "offer.offerConsumptionTax",
					index: "offer.offerConsumptionTax",
					width: 80,
					search : false,
					formatter : "integer",

					align: "right",
	},
    {
        name: "offer.project.receiveCompany.receiveCompanyNameAbbr",
        index: "offer.project.receiveCompany.receiveCompanyNameAbbr",
        width: 60,
        search: false,
        align: "left",
    },
    {
        name: "offer.project.sendCompany.sendCompanyNameAbbr",
        index: "offer.project.sendCompany.sendCompanyNameAbbr",
        width: 60,
        search: false,
        align: "left",
    },
    {
        name: "offer.offerType",
        index: "offer.offerType",
        width: 70,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '派遣';
            } else if (cellvalue == '01') {
                return '請負';
            } else if (cellvalue == '10') {
                return '社内開発';
            }
        },
        align: "left",
    },
    {
        name: "offer.workName",
        index: "offer.workName",
        width: 120,
        search: false,
        align: "left",
    },
    {
        name: "orderNumber",
        index: "orderNumber",
        width: 120,
        search: false,
        align: "left",
    },
    {
        name: "offer.offerId",
        index: "offer.offerId",
        width: 100,
        search: false,
        hidden: true,
        align: "left",
    },
    {
        name: "offer.offerNumber",
        index: "offer.offerNumber",
        width: 120,
        search: false,
        align: "left",
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null && cellvalue != undefined) {
                var selectHtml = '<a href="javascript:void(0);" onclick="openDetail('
                    + rowObject.offer.offerId
                    + ')">'
                    + cellvalue
                    + '</a>';
            } else {
                var selectHtml = cellvalue;
            }
            return selectHtml;
        },
    },
];

var postData = {
    yearMonth: $('#yearMonth').val(),
    projectName: $('#projectName').val(),
};

function openDetail(offerId) {
    var width = '900px'
    var height = '90%'
    var url = "/offer/edit/" + offerId;
    layer.open({
        type: 2,
        title: ' ',
        shadeClose: false,
        shade: 0.6,
        area: [width, height],
        content: [url],
    })
    return false;
};

function openOrderDetail(orderId) {
		var width = '900px'
		var height = '90%'
		var url = "/invoice/edit/" + orderId;
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ url ],
		})
		return false;

};

var shrinkToFit = false;

var gridComplete = function () {
    $("#grid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "scroll"});

    $('.emptyCell').each(
			function(index, element) {
				$(this).closest('td').addClass('btn-update emptyTdCell').attr(
						'data-url', $(this).attr('data-url'));
			})
	$('.btn-update').click(function() {
		var width = '900px'
		var height = '90%'
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ $(this).attr("data-url") ],
		})
		return false;

	});
}

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete);
});	
	