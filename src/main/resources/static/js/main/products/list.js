

			var colNames = [
				"物件ID",
				"物件名称",
				"物件种类",
				"价格",
				"折扣价格",
				"物件整体图片",
				"成約キャンペーン対象物件",
				"房屋描述",
				"公司",
				"物件出售状态(1:公开，2:卖完)",
				"削除フラグ",
				"推荐点",
				"コメント",
			];
			var colModel = [
				{
					name: "productsId",
					index: "productsId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "productsName",
					index: "productsName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "productsSystemConfigId",
					index: "productsSystemConfigId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "productsPrice",
					index: "productsPrice",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "productsDiscountPrice",
					index: "productsDiscountPrice",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "pictureUrl",
					index: "pictureUrl",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "contractProduct",
					index: "contractProduct",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "description",
					index: "description",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "productsCompanyId",
					index: "productsCompanyId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "saleStatus",
					index: "saleStatus",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "recomentPoint",
					index: "recomentPoint",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "remarks",
					index: "remarks",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	