package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 見積詳細 
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OfferDetail {
    /**  */
    private Integer offerDetailId;

    /** 見積 */
    private Offer offer;

    /** 見積ID */
    private Integer offerId;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** ユーザー */
    private AppUser appUser;

    /** 作業員id */
    private Integer userId;

    /** 業務名 */
    private String workName;

    /** 単価 */
    private Integer amount;

    /** 工数 */
    private java.math.BigDecimal manHour;

    /** 合計金額 */
    private Integer totalAmount;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
