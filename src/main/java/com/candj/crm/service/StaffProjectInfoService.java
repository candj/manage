package com.candj.crm.service;

import com.candj.crm.dao.mapper.StaffProjectInfoDao;
import com.candj.crm.dto.StaffProjectInfoDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.StaffProjectInfo;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * スタッフプロジェクト情報情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class StaffProjectInfoService {
    @Autowired
    StaffProjectInfoDao staffProjectInfoDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでスタッフプロジェクト情報を検索する。
     *
     * @param staffProjectInfoId ID
     * @return 結果
     */
    public StaffProjectInfoDto selectByPrimaryKey(Integer staffProjectInfoId) {
        //プライマリーキーでスタッフプロジェクト情報を検索する。
        StaffProjectInfo ret = staffProjectInfoDao.selectByPrimaryKey(staffProjectInfoId);
        return DozerHelper.map(mapper, ret, StaffProjectInfoDto.class);
    }

    /**
     * プライマリーキーでスタッフプロジェクト情報を検索する。（連携情報含む）
     *
     * @param staffProjectInfoId ID
     * @return 結果
     */
    public List<StaffProjectInfoDto> selectAllByPrimaryKey(Integer staffProjectInfoId) {
        //プライマリーキーでスタッフプロジェクト情報検索する。（連携情報含む）
        List<StaffProjectInfo> ret = staffProjectInfoDao.selectAllByPrimaryKey(staffProjectInfoId);
        return DozerHelper.mapList(mapper, ret, StaffProjectInfoDto.class);
    }

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffProjectInfoDto> selectByMyExample1(StaffProjectInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        List<StaffProjectInfo> ret = new ArrayList<StaffProjectInfo>();
        if (dto != null) {

            if(dto.getStaffInProject() != null) {
                if (dto.getStaffInProject().equals("01")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
                }
                else if (dto.getStaffInProject().equals("00")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    whereCondition.or()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andIsNull("a.staff_in_project")
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample2(whereCondition);
                }
            }else{
                whereCondition.createCriteria()
                        .andLike("app_user.user_name", dto.getAppUser().getUserName())
                        .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                        .andEqualTo("a.user_id", dto.getUserId())
                        .andEqualTo("a.project_name", dto.getProjectName())
                        .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                        .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                        .andEqualTo("a.update_user", dto.getUpdateUser())
                        .andEqualTo("a.create_time", dto.getCreateTime())
                        .andEqualTo("a.update_time", dto.getUpdateTime());
                //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
            }

        }

        if (!ret.isEmpty()) {
            DateTime dateTime = new DateTime();
            DateTime nowTime = dateTime.plusMonths(1);
            for (StaffProjectInfo staff : ret) {
                if (staff.getProjectEndDate() != null) {
                    boolean bool = nowTime.getMillis() >= staff.getProjectEndDate().getMillis();
                    staff.setStaffInProjectDateISNear(bool ? "01" : "00");
                } else {
                    staff.setStaffInProjectDateISNear("01");
                }
            }

            for (StaffProjectInfo staff : ret) {
                if (staff.getStaffInProjectDateISNear().equals("00")) {
                    for (StaffProjectInfo staffProjectInfo : ret) {
                        if (staff.getUserId().equals(staffProjectInfo.getUserId())) {
                            staffProjectInfo.setStaffInProjectDateISNear("00");
                        }
                    }
                }
            }

        }

        DateTime yearMonth = new DateTime();
        for (StaffProjectInfo staffProjectInfo : ret) {
            if (staffProjectInfo.getStaffProjectInfoId() == null) {
                staffProjectInfo.setUserId(staffProjectInfo.getAppUser().getUserId());
                staffProjectInfo.setStaffInProject("00");
            } else {
                if (yearMonth.getMillis() <= staffProjectInfo.getProjectEndDate().getMillis() && yearMonth.getMillis() >= staffProjectInfo.getProjectStartDate().getMillis()) {
                    staffProjectInfo.setStaffInProject("01");
                } else {
                    staffProjectInfo.setStaffInProject("00");
                }
                staffProjectInfoDao.updateByPrimaryKey(staffProjectInfo);
            }
        }
        return DozerHelper.mapList(mapper, ret, StaffProjectInfoDto.class);
    }

    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffProjectInfoDto> selectByMyExample2(StaffProjectInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        List<StaffProjectInfo> ret = new ArrayList<StaffProjectInfo>();
        if (dto != null) {

            if(dto.getStaffInProject() != null) {
                if (dto.getStaffInProject().equals("01")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
                }
                else if (dto.getStaffInProject().equals("00")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    whereCondition.or()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andIsNull("a.staff_in_project")
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample2(whereCondition);
                }
            }else{
                whereCondition.createCriteria()
                        .andLike("app_user.user_name", dto.getAppUser().getUserName())
                        .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                        .andEqualTo("a.user_id", dto.getUserId())
                        .andEqualTo("a.project_name", dto.getProjectName())
                        .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                        .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                        .andEqualTo("a.update_user", dto.getUpdateUser())
                        .andEqualTo("a.create_time", dto.getCreateTime())
                        .andEqualTo("a.update_time", dto.getUpdateTime());
                //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
            }

        }

        if (!ret.isEmpty()) {
            DateTime dateTime = new DateTime();
            DateTime nowTime = dateTime.plusMonths(1);
            for (StaffProjectInfo staff : ret) {
                if (staff.getProjectEndDate() != null) {
                    boolean bool = nowTime.getMillis() >= staff.getProjectEndDate().getMillis();
                    staff.setStaffInProjectDateISNear(bool ? "01" : "00");
                } else {
                    staff.setStaffInProjectDateISNear("01");
                }
            }

            for (StaffProjectInfo staff : ret) {
                if (staff.getStaffInProjectDateISNear().equals("00")) {
                    for (StaffProjectInfo staffProjectInfo : ret) {
                        if (staff.getUserId().equals(staffProjectInfo.getUserId())) {
                            staffProjectInfo.setStaffInProjectDateISNear("00");
                        }
                    }
                }
            }

        }

        DateTime yearMonth = new DateTime();
        for (StaffProjectInfo staffProjectInfo : ret) {
            if (staffProjectInfo.getStaffProjectInfoId() == null) {
                staffProjectInfo.setUserId(staffProjectInfo.getAppUser().getUserId());
                staffProjectInfo.setStaffInProject("00");
            } else {
                if (yearMonth.getMillis() <= staffProjectInfo.getProjectEndDate().getMillis() && yearMonth.getMillis() >= staffProjectInfo.getProjectStartDate().getMillis()) {
                    staffProjectInfo.setStaffInProject("01");
                } else {
                    staffProjectInfo.setStaffInProject("00");
                }
                staffProjectInfoDao.updateByPrimaryKey(staffProjectInfo);
            }
        }
        return DozerHelper.mapList(mapper, ret, StaffProjectInfoDto.class);
    }


    /**
     * 条件でスタッフプロジェクト情報を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffProjectInfoDto> selectAllByExample(StaffProjectInfoDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        List<StaffProjectInfo> ret = new ArrayList<StaffProjectInfo>();
        if (dto != null) {

            if(dto.getStaffInProject() != null) {
                if (dto.getStaffInProject().equals("01")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
                }
                else if (dto.getStaffInProject().equals("00")) {
                    whereCondition.createCriteria()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    whereCondition.or()
                            .andLike("app_user.user_name", dto.getAppUser().getUserName())
                            .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                            .andEqualTo("a.user_id", dto.getUserId())
                            .andEqualTo("a.project_name", dto.getProjectName())
                            .andIsNull("a.staff_in_project")
                            .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                            .andEqualTo("a.update_user", dto.getUpdateUser())
                            .andEqualTo("a.create_time", dto.getCreateTime())
                            .andEqualTo("a.update_time", dto.getUpdateTime());
                    //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                    ret = staffProjectInfoDao.selectAllByExample2(whereCondition);
                }
            }else{
                whereCondition.createCriteria()
                        .andLike("app_user.user_name", dto.getAppUser().getUserName())
                        .andEqualTo("a.staff_project_info_id", dto.getStaffProjectInfoId())
                        .andEqualTo("a.user_id", dto.getUserId())
                        .andEqualTo("a.project_name", dto.getProjectName())
                        .andEqualTo("a.staff_in_project", dto.getStaffInProject())
                        .andGreaterThanOrEqualTo("a.project_end_date", dto.getProjectStartDate())
                        .andEqualTo("a.update_user", dto.getUpdateUser())
                        .andEqualTo("a.create_time", dto.getCreateTime())
                        .andEqualTo("a.update_time", dto.getUpdateTime());
                //条件でスタッフプロジェクト情報を検索する。（連携情報含む）)
                ret = staffProjectInfoDao.selectAllByExample1(whereCondition);
            }

        }


        if (!ret.isEmpty()) {
            DateTime dateTime = new DateTime();
            DateTime nowTime = dateTime.plusMonths(1);
            for (StaffProjectInfo staff : ret) {
                if (staff.getProjectEndDate() != null) {
                    boolean bool = nowTime.getMillis() >= staff.getProjectEndDate().getMillis();
                    staff.setStaffInProjectDateISNear(bool ? "01" : "00");
                } else {
                    staff.setStaffInProjectDateISNear("01");
                }
            }

            for (StaffProjectInfo staff : ret) {
                if (staff.getStaffInProjectDateISNear().equals("00")) {
                    for (StaffProjectInfo staffProjectInfo : ret) {
                        if (staff.getUserId().equals(staffProjectInfo.getUserId())) {
                            staffProjectInfo.setStaffInProjectDateISNear("00");
                        }
                    }
                }
            }

        }

        DateTime yearMonth = new DateTime();
        for (StaffProjectInfo staffProjectInfo : ret) {
            if (staffProjectInfo.getStaffProjectInfoId() == null) {
                staffProjectInfo.setUserId(staffProjectInfo.getAppUser().getUserId());
                staffProjectInfo.setStaffInProject("00");
            } else {
                if (yearMonth.getMillis() <= staffProjectInfo.getProjectEndDate().getMillis() && yearMonth.getMillis() >= staffProjectInfo.getProjectStartDate().getMillis()) {
                    staffProjectInfo.setStaffInProject("01");
                } else {
                    staffProjectInfo.setStaffInProject("00");
                }
                staffProjectInfoDao.updateByPrimaryKey(staffProjectInfo);
            }
        }

        return DozerHelper.mapList(mapper, ret, StaffProjectInfoDto.class);
    }

    /**
     * スタッフプロジェクト情報を新規追加する。
     *
     * @param dto スタッフプロジェクト情報
     * @return 結果
     */
    public int insertSelective(StaffProjectInfoDto dto) {
        StaffProjectInfo staffProjectInfo = mapper.map(dto, StaffProjectInfo.class);
        //スタッフプロジェクト情報を新規追加する。
        int ret = staffProjectInfoDao.insertSelective(staffProjectInfo);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を新規追加する。
     *
     * @param dto スタッフプロジェクト情報
     * @return 結果
     */
    public int insert(StaffProjectInfoDto dto) {
        StaffProjectInfo staffProjectInfo = mapper.map(dto, StaffProjectInfo.class);
        //スタッフプロジェクト情報を新規追加する。
        int ret = staffProjectInfoDao.insert(staffProjectInfo);
        return ret;
    }

    /**
     * プライマリーキーでスタッフプロジェクト情報を更新する。
     *
     * @param dto スタッフプロジェクト情報
     * @return 結果
     */
    public int updateByPrimaryKey(StaffProjectInfoDto dto) {
        StaffProjectInfo staffProjectInfo = mapper.map(dto, StaffProjectInfo.class);
        //プライマリーキーでスタッフプロジェクト情報を更新する。
        int ret = staffProjectInfoDao.updateByPrimaryKey(staffProjectInfo);
        return ret;
    }

    /**
     * プライマリーキーでスタッフプロジェクト情報を更新する。
     *
     * @param dto スタッフプロジェクト情報
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffProjectInfoDto dto) {
        StaffProjectInfo staffProjectInfo = mapper.map(dto, StaffProjectInfo.class);
        //プライマリーキーでスタッフプロジェクト情報を更新する。
        int ret = staffProjectInfoDao.updateByPrimaryKeySelective(staffProjectInfo);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を削除する。
     *
     * @param staffProjectInfoId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer staffProjectInfoId) {
        //スタッフプロジェクト情報を削除する。
        int ret = staffProjectInfoDao.deleteByPrimaryKey(staffProjectInfoId);
        return ret;
    }

    /**
     * スタッフプロジェクト情報を削除する。
     *
     * @param offerId ID
     * @return 結果
     */
    public int deleteByOfferId(Integer offerId) {
        //スタッフプロジェクト情報を削除する。
        int ret = staffProjectInfoDao.deleteByOfferId(offerId);
        return ret;
    }
}
