package com.candj.crm.service;

import com.candj.crm.dao.mapper.OrderDao;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.OrderDto;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.mapper.model.Order;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 注文情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class OrderService {
    @Autowired
    OrderDao orderDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで注文を検索する。
     * @param orderId 注文ID
     * @return 結果
     */
    public OrderDto selectByPrimaryKey(Integer orderId) {
        //プライマリーキーで注文を検索する。
        Order ret = orderDao.selectByPrimaryKey(orderId);
        return DozerHelper.map(mapper, ret, OrderDto.class);
    }

    /**
     * プライマリーキーで注文を検索する。（連携情報含む）
     * @param orderId 注文ID
     * @return 結果
     */
    public List<OrderDto> selectAllByPrimaryKey(Integer orderId) {
        //プライマリーキーで注文検索する。（連携情報含む）
        List<Order> ret = orderDao.selectAllByPrimaryKey(orderId);
        return DozerHelper.mapList(mapper, ret, OrderDto.class);
    }

    /**
     * 条件で注文を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OrderDto> selectByExample(OrderDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("order.order_id", dto.getOrderId())
            .andEqualTo("order.offer_id", dto.getOfferId())
            .andEqualTo("order.order_number", dto.getOrderNumber())
            .andEqualTo("order.order_date", dto.getOrderDate())
            .andEqualTo("order.order_status", dto.getOrderStatus())
            .andEqualTo("order.create_user", dto.getCreateUser())
            .andEqualTo("order.update_user", dto.getUpdateUser())
            .andEqualTo("order.create_time", dto.getCreateTime())
            .andEqualTo("order.update_time", dto.getUpdateTime());
        }
        //条件で注文を検索する。（連携情報含む）)
        List<Order> ret = orderDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OrderDto.class);
    }

    /**
     * 条件で注文を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<OrderDto> selectAllByExample(OrderDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("order.order_id", dto.getOrderId())
            .andEqualTo("order.offer_id", dto.getOfferId())
            .andEqualTo("order.order_number", dto.getOrderNumber())
            .andEqualTo("order.order_date", dto.getOrderDate())
            .andEqualTo("order.order_status", dto.getOrderStatus())
            .andEqualTo("order.create_user", dto.getCreateUser())
            .andEqualTo("order.update_user", dto.getUpdateUser())
            .andEqualTo("order.create_time", dto.getCreateTime())
            .andEqualTo("order.update_time", dto.getUpdateTime());
        }
        //条件で注文を検索する。（連携情報含む）)
        List<Order> ret = orderDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OrderDto.class);
    }

    /**
     * 注文を新規追加する。
     * @param dto 注文
     * @return 結果
     */
    public int insertSelective(OrderDto dto) {
        Order order  =  mapper.map(dto, Order.class);
        //注文を新規追加する。
        int ret = orderDao.insertSelective(order);
        return ret;
    }

    /**
     * 注文を新規追加する。
     * @param dto 注文
     * @return 結果
     */
    public int insert(OrderDto dto) {
        Order order  =  mapper.map(dto, Order.class);
        //注文を新規追加する。
        int ret = orderDao.insert(order);
        return ret;
    }

    /**
     * プライマリーキーで注文を更新する。
     * @param dto 注文
     * @return 結果
     */
    public int updateByPrimaryKey(OrderDto dto) {
        Order order  =  mapper.map(dto, Order.class);
        //プライマリーキーで注文を更新する。
        int ret = orderDao.updateByPrimaryKey(order);
        return ret;
    }

    /**
     * プライマリーキーで注文を更新する。
     * @param dto 注文
     * @return 結果
     */
    public int updateByPrimaryKeySelective(OrderDto dto) {
        Order order  =  mapper.map(dto, Order.class);
        //プライマリーキーで注文を更新する。
        int ret = orderDao.updateByPrimaryKeySelective(order);
        return ret;
    }

    /**
     * 注文を削除する。
     * @param orderId 注文ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer orderId) {
        //注文を削除する。
        int ret = orderDao.deleteByPrimaryKey(orderId);
        return ret;
    }

    /**
     * 条件で注文を検索する。（連携情報含む）
     * @param monthDto 検索条件
     * @return 結果
     */
    public List<OrderDto> selectByUserId(MonthDto monthDto) {
        //条件で注文を検索する。（連携情報含む）)
        List<Order> ret = orderDao.selectByUserId(monthDto);
        return DozerHelper.mapList(mapper, ret, OrderDto.class);
    }
}
