

			var colNames = [
				"ID",
				"会社区分",
				"会社名",
				"会社名略称",
				"給料支払日付",
				"住所",
				"郵便番号",
				"電話番号",
				"FAX番号",
				"登録時間",
			];
			var colModel = [
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
                	name: "companyType",
                	index: "companyType",
                	width: 100,
                	search : false,
                	formatter : function(cellvalue, options, rowObject) {
                    		if (cellvalue == 0) {
                    			return '本社';
                    		} else if (cellvalue == 1) {
                    			return '他社';
                    		}
                    },
                	align: "left",
                },
				{
					name: "companyName",
					index: "companyName",
					width: 100,
					search : false,
					align: "left",
				},

				{
					name: "companyNameAbbr",
					index: "companyNameAbbr",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "salaryPayTime",
					index: "salaryPayTime",
					width: 100,
					search : false,
					hidden : true,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue === 0) {
							return '翌月末日';
						} else if (cellvalue === 1) {
							return '翌月15日';
						}else {
							return '翌月25日';
						}
					},
					align: "left",
				},
				{
					name: "companyAdress",
					index: "companyAdress",
					width: 130,
					search : false,
					align: "left",
				},
				{
					name: "companyPostCode",
					index: "companyPostCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "companyTelephoneNumber",
					index: "companyTelephoneNumber",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "companyFaxNumber",
					index: "salary",
					width: 100,
					search : false,
					align: "left",
				},
				{
                	name: "createTime",
                	index: "createTime",
                	width: 100,
                	search : false,
                	formatter : function(cellvalue, options, rowObject) {
                         if (cellvalue != null) {
                              var date = new Date(cellvalue);
                              return date.Format('yyyy-MM-dd');
                         } else {
                              return '';
                         }
                    },
                	align: "left",
                },
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

