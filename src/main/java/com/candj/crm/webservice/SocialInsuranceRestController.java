package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.SocialInsuranceDto;
import com.candj.crm.dto.SocialInsuranceViewDto;
import com.candj.crm.jsonUtils.CalcuteStaffSalary;
import com.candj.crm.jsonUtils.ReadExcel;
import com.candj.crm.service.SocialInsuranceService;
import com.candj.crm.service.SocialInsuranceTempService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 税率、保険料率情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/socialInsuranceRest")
@RestController
public class SocialInsuranceRestController {

    @Autowired
    SocialInsuranceService socialInsuranceService;

    @Autowired
    SocialInsuranceTempService socialInsuranceTempService;

    @Value("${spring.tax.socialPath}")
    String socialPath;

    private static final String DEFAULT_LOCATION = "東京";

    static List<String> socialInsuranceName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    @RequestMapping(value = "{socialInsuranceId}", method = RequestMethod.GET)
    public SocialInsuranceDto get(@PathVariable Integer socialInsuranceId) {
        //プライマリーキーで税率、保険料率を検索する。
        SocialInsuranceDto ret = socialInsuranceService.selectByPrimaryKey(socialInsuranceId);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param socialInsuranceDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(SocialInsuranceDto socialInsuranceDto,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) throws Exception {
        if (attachment != null) {
            String excelSavePath = null;
            InputStream in = null;
            OutputStream os = null;
            try {
                //Excelファイルを一時保存する
                String fileName = attachment.getOriginalFilename();
                String filePath = ResourceUtils.getURL("classpath:").getPath() + "socialInsuranceTemporary/";
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                attachment.transferTo(dest);
                //開始時間を取得する
                DateTime yearMonth = ReadExcel.getYearMonthBySocial(realPath);
                excelSavePath = socialPath + "/" + DateTimeUtil.dateTimeToString(yearMonth) + "/" + fileName;
                File save = new File(excelSavePath);
                //フォルダがいないの場合、新規する
                if (!save.getParentFile().exists()) {
                    save.getParentFile().mkdirs();
                } else {
                    //フォルダがいるの場合、記録は更新しない、ファイルだけ保存する
                    in = new FileInputStream(realPath);
                    os = new FileOutputStream(excelSavePath);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) != -1) {
                        os.write(buf, 0, len);
                    }
                    return 1;
                }
                socialInsuranceDto.setSocialInsuranceStartMonth(yearMonth);
                socialInsuranceDto.setSocialInsuranceExcelPath(excelSavePath);
                //Excelファイルを保存する
                in = new FileInputStream(realPath);
                os = new FileOutputStream(excelSavePath);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) != -1) {
                    os.write(buf, 0, len);
                }
                //一時ファイルを削除する
                dest.delete();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                in.close();
                os.close();
            }
            //JSONファイルを生成する
            String jsonPath = ReadExcel.readExcel(excelSavePath, "social");
            socialInsuranceDto.setSocialInsuranceJsonPath(jsonPath);
        }
        //税率、保険料率を新規追加する。
        int ret = socialInsuranceService.insertSelective(socialInsuranceDto);
        socialInsuranceService.setSocialInsuranceEndMonth(socialInsuranceDto);
        return ret;
    }

    /**
     * 税率、保険料率を変更する。
     *
     * @param socialInsuranceDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody SocialInsuranceDto socialInsuranceDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(socialInsuranceDto.getYearMonth());
        socialInsuranceDto.setSocialInsuranceStartMonth(month);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = socialInsuranceService.updateByPrimaryKeySelective(socialInsuranceDto);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{socialInsuranceId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer socialInsuranceId) {

        SocialInsuranceDto socialInsuranceDto = socialInsuranceService.selectByPrimaryKey(socialInsuranceId);
        String path = socialInsuranceDto.getSocialInsuranceExcelPath();

        File file = new File(path);
        File[] listFiles = file.getParentFile().listFiles();
        //一時フォルダーを削除する
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].listFiles() != null) {
                File[] jsonList = listFiles[i].listFiles();
                for (File f : jsonList) {
                    f.delete();
                }
            }
            listFiles[i].delete();
        }
        file.getParentFile().delete();

        //社会保険料率を削除する。
        int ret = socialInsuranceService.deleteByPrimaryKey(socialInsuranceId);
        return ret;
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     *
     * @param socialInsuranceDto 税率、保険料率
     * @param pageReq            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<SocialInsuranceViewDto> listAll(SocialInsuranceDto socialInsuranceDto,
                                                    @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                                    JqgridPageReq pageReq) {

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        socialInsuranceDto.setSocialInsuranceStartMonth(month);
        socialInsuranceDto.setSocialInsuranceEndMonth(month.plusMonths(1).minusSeconds(1));
        List<SocialInsuranceDto> socialInsuranceDtoList = socialInsuranceService.selectAllByMyExample(socialInsuranceDto);

        if (!socialInsuranceDtoList.isEmpty()) {

            SocialInsuranceViewDto socialInsuranceView = new SocialInsuranceViewDto();
            List<SocialInsuranceViewDto> socialInsuranceViewDtoList = socialInsuranceTempService.selectAllByExample(socialInsuranceView);
            if (!socialInsuranceViewDtoList.isEmpty()) {
                for (SocialInsuranceViewDto socialInsurance :
                        socialInsuranceViewDtoList) {
                    socialInsuranceTempService.deleteByPrimaryKey(socialInsurance.getSocialInsuranceId());
                }
            }

            String jsonPath = socialInsuranceDtoList.get(0).getSocialInsuranceJsonPath();
            String filePath;
            if (socialInsuranceDto.getLocation() != null && socialInsuranceDto.getLocation().length() > 0) {
                int index = Integer.parseInt(socialInsuranceDto.getLocation());
                filePath = jsonPath + "/" + LOCATION_ADDRESS[index] + ".JSON";
            } else {
                filePath = jsonPath + "/" + DEFAULT_LOCATION + ".JSON";
            }
            String s = CalcuteStaffSalary.readJsonFile(filePath);
            JSONArray jArr = JSONArray.fromObject(s);
            for (int i = 0; i < jArr.size(); i++) {
                JSONObject key = (JSONObject) jArr.get(i);
                String grade = (String) key.get("等級");
                String monthly = (String) key.get("月額");
                Integer minMonthlyPayment = key.get("報酬月額下限").equals(JSONNull.getInstance()) ? 0 : Integer.valueOf((String) key.get("報酬月額下限"));
                Integer maxMonthlyPayment = key.get("報酬月額上限").equals(JSONNull.getInstance()) ? null : Integer.valueOf((String) key.get("報酬月額上限"));
                Double healthInsuranceFull = key.get("健康保険（全額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("健康保険（全額）"));
                Double healthInsuranceHalf = key.get("健康保険（折半額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("健康保険（折半額）"));
                Double healthAndNursingCareFull = key.get("健康保険＋介護保険（全額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("健康保険＋介護保険（全額）"));
                Double healthAndNursingCareHalf = key.get("健康保険＋介護保険（折半額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("健康保険＋介護保険（折半額）"));
                Double welfarePensionFull = key.get("厚生年金保険（全額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("厚生年金保険（全額）"));
                Double welfarePensionHalf = key.get("厚生年金保険（折半額）").equals(JSONNull.getInstance()) ? null : Double.valueOf((String) key.get("厚生年金保険（折半額）"));
                SocialInsuranceViewDto socialInsuranceViewDto = new SocialInsuranceViewDto();
                socialInsuranceViewDto.setGrade(grade);
                socialInsuranceViewDto.setMonthly(monthly);
                socialInsuranceViewDto.setMinMonthlyPayment(minMonthlyPayment);
                socialInsuranceViewDto.setMaxMonthlyPayment(maxMonthlyPayment);
                socialInsuranceViewDto.setHealthInsuranceFull(healthInsuranceFull);
                socialInsuranceViewDto.setHealthInsuranceHalf(healthInsuranceHalf);
                socialInsuranceViewDto.setHealthAndNursingCareFull(healthAndNursingCareFull);
                socialInsuranceViewDto.setHealthAndNursingCareHalf(healthAndNursingCareHalf);
                socialInsuranceViewDto.setWelfarePensionFull(welfarePensionFull);
                socialInsuranceViewDto.setWelfarePensionHalf(welfarePensionHalf);
                socialInsuranceTempService.insert(socialInsuranceViewDto);
            }
        }
        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<SocialInsuranceViewDto> page = PageUtil.startPage(pageReq);
        SocialInsuranceViewDto socialInsuranceViewDto = new SocialInsuranceViewDto();
        List<SocialInsuranceViewDto> socialInsuranceViewDtoList = socialInsuranceTempService.selectAllByExample(socialInsuranceViewDto);

        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     *
     * @param socialInsuranceDto 税率、保険料率
     * @param pageReq            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/fileList", method = RequestMethod.POST)
    public PageInfo<SocialInsuranceDto> fileList(SocialInsuranceDto socialInsuranceDto,
                                                 JqgridPageReq pageReq) {
        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<SocialInsuranceDto> page = PageUtil.startPage(pageReq);
        List<SocialInsuranceDto> ret = socialInsuranceService.selectByExample(socialInsuranceDto);
        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     *
     * @param socialInsuranceDto 税率、保険料率
     * @param pageReq            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<SocialInsuranceDto> list(SocialInsuranceDto socialInsuranceDto, JqgridPageReq pageReq) {
        Page<SocialInsuranceDto> page = PageUtil.startPage(pageReq);
        //条件で税率、保険料率を検索する。（連携情報含む）
        List<SocialInsuranceDto> ret = socialInsuranceService.selectByExample(socialInsuranceDto);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/download/{socialInsuranceId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(HttpServletRequest request,
                                           @PathVariable("socialInsuranceId") Integer socialInsuranceId,
                                           @RequestHeader("User-Agent") String userAgent,
                                           ModelMap model) throws Exception {
        // ファイルパース
        SocialInsuranceDto socialInsuranceDto = socialInsuranceService.selectByPrimaryKey(socialInsuranceId);
        String path = socialInsuranceDto.getSocialInsuranceExcelPath();

        File file = new File(path);
        String fileName = file.getName();

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        // 内容サイズ
        builder.contentLength(file.length());
        // application-octet-stream ： 二进制データ
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        // URLEncoder.encodeでファイルをエンコードする
        fileName = URLEncoder.encode(fileName, "UTF-8");
        if (userAgent.indexOf("MSIE") > 0) {
            // IE
            builder.header("Content-Disposition", "attachment; filename=" + fileName);
        } else {
            // FireFox、Chrome
            builder.header("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);
        }
        return builder.body(FileUtils.readFileToByteArray(file));
    }


    private static final String[] LOCATION_ADDRESS = {
            "東京", "埼玉", "千葉", "神奈川", "茨城", "愛媛", "愛知", "北海道",
            "兵庫", "長崎", "長野", "沖縄", "大阪", "大分", "島根", "徳島", "福島",
            "福岡", "福井", "富山", "岡山", "高知", "宮城", "宮崎", "広島", "和歌山",
            "京都", "静岡", "栃木", "鹿児島", "奈良", "鳥取", "岐阜", "青森", "秋田",
            "群馬", "三重", "山口", "山梨", "山形", "石川", "香川", "新潟", "熊本",
            "岩手", "滋賀", "佐賀"
    };
}
