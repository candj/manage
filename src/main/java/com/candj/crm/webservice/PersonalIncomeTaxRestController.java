package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.PersonalIncomeTaxDto;
import com.candj.crm.dto.PersonalIncomeTaxTempDto;
import com.candj.crm.jsonUtils.CalcuteStaffSalary;
import com.candj.crm.jsonUtils.ReadExcel;
import com.candj.crm.mapper.model.PersonalIncomeTempTax;
import com.candj.crm.service.PersonalIncomeTaxService;
import com.candj.crm.service.PersonalIncomeTaxTempService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 税率、保険料率情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/personalIncomeTaxRest")
@RestController
public class PersonalIncomeTaxRestController {

    @Autowired
    PersonalIncomeTaxService personalIncomeTaxService;

    @Autowired
    PersonalIncomeTaxTempService personalIncomeTaxTempService;

    @Value("${spring.tax.personalPath}")
    String personalPath;

    static List<String> personalIncomeTaxName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * 税率、保険料率を新規追加する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    @RequestMapping(value = "{personalIncomeTaxId}", method = RequestMethod.GET)
    public PersonalIncomeTaxDto get(@PathVariable Integer personalIncomeTaxId) {
        //プライマリーキーで税率、保険料率を検索する。
        PersonalIncomeTaxDto ret = personalIncomeTaxService.selectByPrimaryKey(personalIncomeTaxId);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(PersonalIncomeTaxDto personalIncomeTaxDto,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) throws Exception{
        if (attachment != null) {
            String excelSavePath = null;
            InputStream in = null;
            OutputStream os = null;
            try {
                //Excelファイルを一時保存する
                String fileName = attachment.getOriginalFilename();
                String filePath = ResourceUtils.getURL("classpath:").getPath() + "personalIncomeTaxTemporary/";
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                attachment.transferTo(dest);
                //開始時間を取得する
                DateTime yearMonth = ReadExcel.getYearMonthByPersonal(realPath);
                excelSavePath = personalPath + "/" + DateTimeUtil.dateTimeToString(yearMonth) + "/" + fileName;
                File save = new File(excelSavePath);
                //フォルダがいないの場合、新規する
                if (!save.getParentFile().exists()) {
                    save.getParentFile().mkdirs();
                } else{
                    //フォルダがいるの場合、記録は更新しない、ファイルだけ保存する
                    in = new FileInputStream(realPath);
                    os = new FileOutputStream(excelSavePath);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) != -1) {
                        os.write(buf, 0, len);
                    }
                    return 1;
                }
                personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(yearMonth);
                personalIncomeTaxDto.setPersonalIncomeTaxExcelPath(excelSavePath);
                //Excelファイルを保存する
                in = new FileInputStream(realPath);
                os = new FileOutputStream(excelSavePath);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) != -1) {
                    os.write(buf, 0, len);
                }
                //一時ファイルを削除する
                dest.delete();
            } catch (IOException e) {
                e.printStackTrace();
            } finally{
                in.close();
                os.close();
            }
            //JSONファイルを生成する
            String jsonPath = ReadExcel.readExcel(excelSavePath, "personal");
            personalIncomeTaxDto.setPersonalIncomeTaxJsonPath(jsonPath);
        }
        //税率、保険料率を新規追加する。
        int ret = personalIncomeTaxService.insertSelective(personalIncomeTaxDto);
        personalIncomeTaxService.setPersonalIncomeTaxEndMonth(personalIncomeTaxDto);
        return ret;
    }

    /**
     * 税率、保険料率を変更する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody PersonalIncomeTaxDto personalIncomeTaxDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(personalIncomeTaxDto.getYearMonth());
        personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(month);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = personalIncomeTaxService.updateByPrimaryKeySelective(personalIncomeTaxDto);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{personalIncomeTaxId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer personalIncomeTaxId) {

        PersonalIncomeTaxDto personalIncomeTaxDto = personalIncomeTaxService.selectByPrimaryKey(personalIncomeTaxId);
        String path = personalIncomeTaxDto.getPersonalIncomeTaxExcelPath();

        File file = new File(path);
        File[] listFiles = file.getParentFile().listFiles();
        //一時フォルダーを削除する
        for (int i = 0; i < listFiles.length; i++) {
            if(listFiles[i].listFiles() != null) {
                File[] jsonList = listFiles[i].listFiles();
                for (File f : jsonList) {
                    f.delete();
                }
            }
            listFiles[i].delete();
        }
        file.getParentFile().delete();

        //社会保険料率を削除する。
        int ret = personalIncomeTaxService.deleteByPrimaryKey(personalIncomeTaxId);
        return ret;
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<PersonalIncomeTempTax> listAll(PersonalIncomeTaxDto personalIncomeTaxDto,
                                                   @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                                   JqgridPageReq pageReq) {

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(month);
        personalIncomeTaxDto.setPersonalIncomeTaxEndMonth(month.plusMonths(1).minusSeconds(1));
        List<PersonalIncomeTaxDto> personalIncomeTaxDtoList = personalIncomeTaxService.selectAllByExample(personalIncomeTaxDto);

        if (!personalIncomeTaxDtoList.isEmpty()) {

            PersonalIncomeTaxTempDto personalIncomeTaxTempDto = new PersonalIncomeTaxTempDto();
            List<PersonalIncomeTaxTempDto> personalIncomeTaxTempDtoList = personalIncomeTaxTempService.selectByExample(personalIncomeTaxTempDto);
            if (!personalIncomeTaxTempDtoList.isEmpty()) {
                for (PersonalIncomeTaxTempDto incomeTaxTempDto :
                        personalIncomeTaxTempDtoList) {
                    personalIncomeTaxTempService.deleteByPrimaryKey(incomeTaxTempDto.getPersonalIncomeTaxId());
                }
            }

            String jsonPath = personalIncomeTaxDtoList.get(0).getPersonalIncomeTaxJsonPath();
            String filePath = jsonPath + "/" + "月額表.json";
            String s = CalcuteStaffSalary.readJsonFile(filePath);
            JSONArray jArr = JSONArray.fromObject(s);
            JSONArray array1 = (JSONArray) jArr.get(0);
            JSONArray array2 = (JSONArray) jArr.get(1);
            for (int i = 0; i < array1.size(); i++) {
                JSONObject key = (JSONObject) array1.get(i);
                Integer minMonthlyPayment = key.get("報酬月額下限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額下限");
                Integer maxMonthlyPayment = key.get("報酬月額上限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額上限");
                Integer numberOfSupport0 = key.get("扶養人数0").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数0");
                Integer numberOfSupport1 = key.get("扶養人数1").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数1");
                Integer numberOfSupport2 = key.get("扶養人数2").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数2");
                Integer numberOfSupport3 = key.get("扶養人数3").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数3");
                Integer numberOfSupport4 = key.get("扶養人数4").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数4");
                Integer numberOfSupport5 = key.get("扶養人数5").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数5");
                Integer numberOfSupport6 = key.get("扶養人数6").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数6");
                Integer numberOfSupport7 = key.get("扶養人数7").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数7");


                PersonalIncomeTaxTempDto incomeTaxTempDto = new PersonalIncomeTaxTempDto();
                incomeTaxTempDto.setMinMonthlyPayment(minMonthlyPayment);
                incomeTaxTempDto.setMaxMonthlyPayment(maxMonthlyPayment);
                incomeTaxTempDto.setNumberOfSupport0(numberOfSupport0);
                incomeTaxTempDto.setNumberOfSupport1(numberOfSupport1);
                incomeTaxTempDto.setNumberOfSupport2(numberOfSupport2);
                incomeTaxTempDto.setNumberOfSupport3(numberOfSupport3);
                incomeTaxTempDto.setNumberOfSupport4(numberOfSupport4);
                incomeTaxTempDto.setNumberOfSupport5(numberOfSupport5);
                incomeTaxTempDto.setNumberOfSupport6(numberOfSupport6);
                incomeTaxTempDto.setNumberOfSupport7(numberOfSupport7);
                personalIncomeTaxTempService.insertSelective(incomeTaxTempDto);
            }

            for (int i = 0; i < array2.size(); i++) {
                JSONObject key = (JSONObject) array2.get(i);
                Integer minMonthlyPayment = key.get("報酬月額下限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額下限");
                Integer maxMonthlyPayment = key.get("報酬月額上限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額上限");
                Integer numberOfSupport0 = key.get("扶養人数0").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数0");
                Integer numberOfSupport1 = key.get("扶養人数1").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数1");
                Integer numberOfSupport2 = key.get("扶養人数2").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数2");
                Integer numberOfSupport3 = key.get("扶養人数3").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数3");
                Integer numberOfSupport4 = key.get("扶養人数4").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数4");
                Integer numberOfSupport5 = key.get("扶養人数5").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数5");
                Integer numberOfSupport6 = key.get("扶養人数6").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数6");
                Integer numberOfSupport7 = key.get("扶養人数7").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("扶養人数7");
                BigDecimal tax = key.get("料率").equals(JSONNull.getInstance()) ? BigDecimal.ZERO : new BigDecimal((Double) key.get("料率")).setScale(5, BigDecimal.ROUND_HALF_UP);

                PersonalIncomeTaxTempDto incomeTaxTempDto = new PersonalIncomeTaxTempDto();
                incomeTaxTempDto.setMinMonthlyPayment(minMonthlyPayment);
                incomeTaxTempDto.setMaxMonthlyPayment(maxMonthlyPayment);
                incomeTaxTempDto.setNumberOfSupport0(numberOfSupport0);
                incomeTaxTempDto.setNumberOfSupport1(numberOfSupport1);
                incomeTaxTempDto.setNumberOfSupport2(numberOfSupport2);
                incomeTaxTempDto.setNumberOfSupport3(numberOfSupport3);
                incomeTaxTempDto.setNumberOfSupport4(numberOfSupport4);
                incomeTaxTempDto.setNumberOfSupport5(numberOfSupport5);
                incomeTaxTempDto.setNumberOfSupport6(numberOfSupport6);
                incomeTaxTempDto.setNumberOfSupport7(numberOfSupport7);
                incomeTaxTempDto.setTax(tax);
                personalIncomeTaxTempService.insertSelective(incomeTaxTempDto);
            }

        }

        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<PersonalIncomeTempTax> page = PageUtil.startPage(pageReq);
        PersonalIncomeTaxTempDto personalIncomeTaxTempDto = new PersonalIncomeTaxTempDto();
        List<PersonalIncomeTaxTempDto> personalIncomeTaxTempDtoList = personalIncomeTaxTempService.selectAllByExample(personalIncomeTaxTempDto);
        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/fileList", method = RequestMethod.POST)
    public PageInfo<PersonalIncomeTaxDto> fileList(PersonalIncomeTaxDto personalIncomeTaxDto,
                                                JqgridPageReq pageReq) {
        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<PersonalIncomeTaxDto> page = PageUtil.startPage(pageReq);
        List<PersonalIncomeTaxDto> ret = personalIncomeTaxService.selectByExample(personalIncomeTaxDto);
        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param personalIncomeTaxDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<PersonalIncomeTaxDto> list(PersonalIncomeTaxDto personalIncomeTaxDto, JqgridPageReq pageReq) {
        Page<PersonalIncomeTaxDto> page = PageUtil.startPage(pageReq);
        //条件で税率、保険料率を検索する。（連携情報含む）
        List<PersonalIncomeTaxDto> ret = personalIncomeTaxService.selectByExample(personalIncomeTaxDto);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/download/{personalIncomeTaxId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(HttpServletRequest request,
                                           @PathVariable("personalIncomeTaxId") Integer personalIncomeTaxId,
                                           @RequestHeader("User-Agent") String userAgent,
                                           ModelMap model) throws Exception {
        // ファイルパース
        PersonalIncomeTaxDto personalIncomeTaxDto = personalIncomeTaxService.selectByPrimaryKey(personalIncomeTaxId);
        String path = personalIncomeTaxDto.getPersonalIncomeTaxExcelPath();

        File file = new File(path);
        String fileName = file.getName();

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        // 内容サイズ
        builder.contentLength(file.length());
        // application-octet-stream ： 二进制データ
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        // URLEncoder.encodeでファイルをエンコードする
        fileName = URLEncoder.encode(fileName, "UTF-8");
        if (userAgent.indexOf("MSIE") > 0) {
            // IE
            builder.header("Content-Disposition", "attachment; filename=" + fileName);
        } else {
            // FireFox、Chrome
            builder.header("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);
        }
        return builder.body(FileUtils.readFileToByteArray(file));
    }
}
