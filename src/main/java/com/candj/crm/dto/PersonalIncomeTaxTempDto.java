package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Getter
@Setter
public class PersonalIncomeTaxTempDto {

    /** 個人所得税ID */
    @Digits(integer=10, fraction=0)
    private Integer personalIncomeTaxId;

    /*報酬月額下限*/
    private Integer minMonthlyPayment;

    /** 報酬月額上限 */
    private Integer maxMonthlyPayment;

    /** 扶養人数0 */
    private  Integer numberOfSupport0;
    /** 扶養人数1 */
    private  Integer numberOfSupport1;
    /** 扶養人数2 */
    private  Integer numberOfSupport2;
    /** 扶養人数3 */
    private  Integer numberOfSupport3;
    /** 扶養人数4 */
    private  Integer numberOfSupport4;
    /** 扶養人数5 */
    private  Integer numberOfSupport5;
    /** 扶養人数6 */
    private  Integer numberOfSupport6;
    /** 扶養人数7 */
    private  Integer numberOfSupport7;

    @Digits(integer=10, fraction=5)
    private java.math.BigDecimal tax;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
