package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonalIncomeTax {

    /** 税率ID */
    private Integer personalIncomeTaxId;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** 適用年月開始時間 */
    private org.joda.time.DateTime personalIncomeTaxStartMonth;

    /** 適用年月終了時間 */
    private org.joda.time.DateTime personalIncomeTaxEndMonth;

    /** Excelパース */
    private String personalIncomeTaxExcelPath;

    /** Excelファイル名 */
    private String personalIncomeTaxExcelName;

    /** JSONパース */
    private String personalIncomeTaxJsonPath;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

}
