package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkDurationDto {

    private Integer userId;

    private Integer offerId;

    /** 開始時間 */
    private org.joda.time.DateTime startDate;

    /** 終了時間 */
    private org.joda.time.DateTime EndDate;

}
