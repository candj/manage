package com.candj.crm.webservice;

import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.RoleDto;
import com.candj.crm.dto.UserRoleDto;
import com.candj.crm.service.AppUserService;
import com.candj.crm.service.RoleService;
import com.candj.crm.service.UserRoleService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ユーザー情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/appUserRest")
@RestController
public class AppUserRestController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    RoleService roleService;

    /**
     * ユーザーを新規追加する。
     *
     * @param userId ユーザーID
     * @return 結果
     */
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public AppUserDto get(@PathVariable Integer userId) {
        //プライマリーキーでユーザーを検索する。
        AppUserDto ret = appUserService.selectByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     *
     * @param appUserDto    ユーザー
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {

        appUserDto.setUserDeleteFlag(true);
        appUserDto.setUserType(0);
        appUserDto.setEmploymentStatus("00");

        //ユーザーを新規追加する。
        int ret = appUserService.insertSelective(appUserDto);

        RoleDto roleDto = new RoleDto();
        roleDto.setRoleCode(appUserDto.getUserType() + "");
        List<RoleDto> roleDtoList = roleService.selectByExample(roleDto);
        List<AppUserDto> appUserDtos = appUserService.selectByExample(appUserDto);
        if (!roleDtoList.isEmpty() && !appUserDtos.isEmpty()) {
            UserRoleDto userRoleDto = new UserRoleDto();
            userRoleDto.setUserId(appUserDtos.get(0).getUserId());
            userRoleDto.setRoleId(roleDtoList.get(0).getRoleId());
            userRoleService.insert(userRoleDto);
        }
        return ret;
    }

    /**
     * ユーザーを変更する。
     *
     * @param appUserDto    ユーザー
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        appUserDto.setUserDeleteFlag(true);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserService.updateByPrimaryKeySelective(appUserDto);

        RoleDto roleDto = new RoleDto();
        roleDto.setRoleCode(appUserDto.getUserType() + "");
        List<RoleDto> roleDtoList = roleService.selectByExample(roleDto);
        if (!roleDtoList.isEmpty()) {
            UserRoleDto userRoleDto = new UserRoleDto();
            userRoleDto.setUserId(appUserDto.getUserId());

            List<UserRoleDto> userRoleDtoList = userRoleService.selectByExample(userRoleDto);
            if (!userRoleDtoList.isEmpty()){
                UserRoleDto userRoleDto1 = userRoleDtoList.get(0);
                userRoleDto1.setRoleId(roleDtoList.get(0).getRoleId());
                userRoleService.updateByPrimaryKeySelective(userRoleDto1);
            }
        }
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     *
     * @param userId ユーザーID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer userId) {

        AppUserDto appUserDto = appUserService.selectByPrimaryKey(userId);
        appUserDto.setUserDeleteFlag(false);

        //ユーザーを削除する。
        int ret = appUserService.updateByPrimaryKey(appUserDto);
        return ret;
    }

    /**
     * ユーザー一覧画面を表示する。
     *
     * @param appUserDto ユーザー
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<AppUserDto> list(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        appUserService.selectByExample(appUserDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザー一覧画面を表示する。
     *
     * @param appUserDto ユーザー
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<AppUserDto> listAll(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        List<AppUserDto> ret = appUserService.selectAllByExample(appUserDto);
        return PageUtil.resp(page);
    }
}
