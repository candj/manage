package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 費用
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class CostDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer costId;

    /** 費用日付 */
    private org.joda.time.DateTime costDate;

    /** 摘要 */
    @Length(max=1000)
    private String costSummary;

    /** 収入金额 */
    @Digits(integer=10, fraction=0)
    private Integer incomeAmount;

    /** 支出金额 */
    @Digits(integer=10, fraction=0)
    private Integer expendAmount;

    /** 事務費の入金 */
    @Digits(integer=10, fraction=0)
    private Integer businessIncomeAmount;

    /** 事務費の支出 */
    @Digits(integer=10, fraction=0)
    private Integer businessExpendAmount;

    /** 当月分費用 */
    @Digits(integer=10, fraction=0)
    private Integer costTotalAmount;

    /** コメント */
    @Length(max=255)
    private String remark;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
