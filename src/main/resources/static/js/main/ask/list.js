

			var colNames = [
				"用户ID",
				"お名前（漢字）",
				"お名前（カナ）",
				"电话",
				"邮箱",
				"咨询内容",
				"咨询房屋",
				"咨询/预约",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "askId",
					index: "askId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "nameKanji",
					index: "nameKanji",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "nameKana",
					index: "nameKana",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "askPhone",
					index: "askPhone",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "askEmail",
					index: "askEmail",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "comment",
					index: "comment",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "askProductsId",
					index: "askProductsId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "askKind",
					index: "askKind",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	