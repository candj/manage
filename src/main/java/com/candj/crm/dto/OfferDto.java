package com.candj.crm.dto;

import javax.validation.constraints.Digits;

import com.candj.crm.mapper.model.OrderingColumnOnly;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

/**
 * 見積
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OfferDto {
    /** 見積 ID */
    @Digits(integer=10, fraction=0)
    private Integer offerId;

    /** 注文 ID */
    @Digits(integer=10, fraction=0)
    private Integer orderId;

    /** 見積詳細  */
    private java.util.List<OfferDetailDto> offerDetail;

    /** 注文  */
    private java.util.List<OrderingDto> ordering;

    /** 注文  */
    private OrderingColumnOnly orderingDto;

    /** 見積番号 */
    @Length(max=128)
    private String offerNumber;

    /** 注文番号 */
    @Length(max=128)
    private String orderNumber;

    /** 種類 */
    @Length(max=2)
    private String offerType;

    /** 元会社ID */
    @Digits(integer=10, fraction=0)
    private Integer sendCompanyId;

    /** 元会社 */
    private com.candj.crm.mapper.model.SendCompany sendCompany;

    /** 先会社ID */
    @Digits(integer=10, fraction=0)
    private Integer receiveCompanyId;

    /** 先会社 */
    private com.candj.crm.mapper.model.ReceiveCompany receiveCompany;

    /** プロジェクト摘要 */
    @Length(max=1000)
    private String projectDescription;

    /** プロジェクトID */
    @Digits(integer=10, fraction=0)
    private Integer projectId;

    /** 見積 */
    private com.candj.crm.mapper.model.Project project;

    /** 日付 */
    private org.joda.time.DateTime date;

    /** 業務名 */
    @Length(max=128)
    private String workName;

    /** 作業時間基準（下限） */
    @Digits(integer=10, fraction=0)
    private Integer workTimeLowerLimit;

    /** 作業時間基準（上限） */
    @Digits(integer=10, fraction=0)
    private Integer workTimeUpperLimit;

    /** 種類 */
    @Length(max=2)
    private String workTimeCalculateStandard;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 納入場所 */
    @Length(max=128)
    private String workPlace;

    /** 有効期限 */
    @Digits(integer=10, fraction=0)
    private Integer offerPeriodOfValidity;

    /** 納期 */
    private org.joda.time.DateTime offerDeliveryDate;

    /** 支払いサイトID */
    @Digits(integer=10, fraction=0)
    private Integer paySiteId;

    /** 支払いサイト */
    private com.candj.crm.mapper.model.PaySite paySite;

    /** 税抜き合計金額 */
    @Digits(integer=10, fraction=0)
    private Integer offerAmountWithoutTax;

    /** 消費税率 */
    @Digits(integer=3, fraction=2)
    private BigDecimal offerConsumptionTaxRate;

    /** 消費税 */
    @Digits(integer=10, fraction=0)
    private Integer offerConsumptionTax;

    /** 税込み合計金額 */
    @Digits(integer=10, fraction=0)
    private Integer offerTotalAmount;

    /** 精算方法 */
    @Length(max=2)
    private String afterPaymentPattern;

    /** 精算方法 */
    private String afterPaymentPatternName;

    /** 見積ステータス */
    @Length(max=2)
    private String offerStatus;

    /** 注文作成FLAG */
    private boolean haveOrderFlag;

    /** 削除FLAG */
    private boolean offerDeleteFlag;

    private String sendCompanyName;

    private String receiveCompanyName;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
