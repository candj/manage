package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.CostDto;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.service.CostService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 費用情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/costRest")
@RestController
public class CostRestController {
    @Autowired
    CostService costService;

    /**
     * 費用を新規追加する。
     * @param costId ID
     * @return 結果
     */
    @RequestMapping(value = "{costId}", method = RequestMethod.GET)
    public CostDto get(@PathVariable Integer costId) {
        //プライマリーキーで費用を検索する。
        CostDto ret = costService.selectByPrimaryKey(costId);
        return ret;
    }

    /**
     * 費用を新規追加する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody CostDto costDto, Errors rrrors) {
        //費用を新規追加する。
        int ret = costService.insertSelective(costDto);
        return ret;
    }

    /**
     * 費用を変更する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody CostDto costDto, Errors rrrors) {
        //プライマリーキーで費用を更新する。
        int ret = costService.updateByPrimaryKeySelective(costDto);
        return ret;
    }

    /**
     * 費用を新規追加する。
     * @param costId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{costId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer costId) {
        //費用を削除する。
        int ret = costService.deleteByPrimaryKey(costId);
        return ret;
    }

    /**
     * 費用一覧画面を表示する。
     * @param costDto 費用
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<CostDto> list(CostDto costDto,
                                  @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                  JqgridPageReq pageReq) {
        Page<CostDto> page = PageUtil.startPage(pageReq);
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        MonthDto monthDto = new MonthDto();
        monthDto.setStartOfMonth(month);
        monthDto.setEndOfMonth(month.plusMonths(1).minusSeconds(1));
        //条件で費用を検索する。（連携情報含む）
        List<CostDto> ret = costService.selectByExample(costDto, monthDto);
        return PageUtil.resp(page);
    }

    /**
     * 費用一覧画面を表示する。
     * @param costDto 費用
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<CostDto> listAll(CostDto costDto, JqgridPageReq pageReq) {
        Page<CostDto> page = PageUtil.startPage(pageReq);
        //条件で費用を検索する。（連携情報含む）
        List<CostDto> ret = costService.selectAllByExample(costDto);
        return PageUtil.resp(page);
    }
}
