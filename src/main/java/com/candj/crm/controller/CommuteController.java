package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.Utils.FileUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.CommuteDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.CommuteService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import javax.validation.Valid;

import org.apache.commons.fileupload.FileItem;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * 交通費情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/commute")
@Controller
public class CommuteController {
    @Autowired
    CommuteService commuteService;

    /**
     * 交通費を新規追加する。
     * @param commuteDto 交通費
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{yearMonth}", method = RequestMethod.GET)
    public ModelAndView insert(CommuteDto commuteDto, @PathVariable YearMonth yearMonth, ModelMap model) {
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        model.addAttribute("yearMonth", month);
        return new ModelAndView("commute/insert", model);
    }

    /**
     * 交通費を新規追加(確認)する。
     * @param commuteDto 交通費
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid CommuteDto commuteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/commute/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("commute/insertConfirm", model);
    }

    /**
     * 交通費を新規追加(終了)する。
     * @param commuteDto 交通費
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid CommuteDto commuteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/commute/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //交通費を新規追加する。
        int ret = commuteService.insertSelective(commuteDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/commute/insertFinish", model);
    }

    /**
     * 交通費を編集する。
     * @param commuteId 交通費ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{yearMonth}/{commuteId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer commuteId, @PathVariable YearMonth yearMonth, ModelMap model) {
        //プライマリーキーで交通費を検索する。
        CommuteDto commuteDto = commuteService.selectByPrimaryKey(commuteId);
        //String fileName = commuteDto.getCommuteAttachmentName();
        //String path = commuteDto.getCommuteAttachmentPath();
        //File file = new File(path);
        //FileItem fileItem = FileUtil.createFileItem(file);
        //MultipartFile attachment = new CommonsMultipartFile(fileItem);
        model.addAttribute("commuteDto", commuteDto);
        //model.addAttribute("attachment", attachment);
        return new ModelAndView("commute/edit", model);
    }

    /**
     * 交通費を編集(確認)する。
     * @param commuteDto 交通費
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid CommuteDto commuteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/commute/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("commute/editConfirm", model);
    }

    /**
     * 交通費を編集(終了)する。
     * @param commuteDto 交通費
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid CommuteDto commuteDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/commute/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで交通費を更新する。
        int ret = commuteService.updateByPrimaryKeySelective(commuteDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("commute/editFinish", model);
    }

    /**
     * 交通費一覧画面を表示する。exception
     * @param commuteDto 交通費
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("commuteDto") CommuteDto commuteDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        String userName = "";
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("userName", userName);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0){
            return new ModelAndView("commute/list", model);
        }else {
            return new ModelAndView("commuteForStaff/list", model);
        }
    }

}
