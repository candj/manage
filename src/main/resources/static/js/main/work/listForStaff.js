

			var colNames = [
				"作業ID",
				"注文番号",
				"スタッフ名前",
				"会社",
				"作業種類",
                "作業開始期日",
                "作業終了期日",
                "作業時間",
                "勤務表",
                "処理ステータス",
				"ステータス",
			];
			var colModel = [
				{
					name: "workId",
					index: "workId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
                    name: "ordering.orderNumber",
                    index: "ordering.orderNumber",
                    width: 100,
                    key: false,
                    search : false,
                    align: "left",
                },
				{
					name: "appUser.userName",
					index: "appUser.userName",
					width: 100,
					search : false,
					align: "left",
				},
                {
                    name: "appUser.company.companyName",
                    index: "appUser.company.companyName",
                    width: 100,
                    search : false,
                    align: "left",
                },
                {
                					name: "workType",
                					index: "workType",
                					width: 100,
                					search : false,
                					hidden : true,
                					formatter : function(cellvalue, options, rowObject) {
                                      if (cellvalue == '00') {
                                        return '派遣';
                                      } else if(cellvalue == '01'){
                                        return '社内作業';
                                      }
                                    },
                					align: "left",
                				},
                                {
                                    name: "workStartDate",
                                    index: "workStartDate",
                                    width: 100,
                                    search : false,
                                    formatter : function(cellvalue, options, rowObject) {
                                      if (cellvalue != null) {
                                        var date = new Date(cellvalue);
                                        return date.Format('yyyy-MM-dd');
                                      } else {
                                        return '';
                                      }
                                    },
                                    align: "left",
                                },
                                {
                                	name: "workEndDate",
                                	index: "workEndDate",
                                	width: 100,
                                	search : false,
                                	formatter : function(cellvalue, options, rowObject) {
                                      if (cellvalue != null) {
                                      var date = new Date(cellvalue);
                                        return date.Format('yyyy-MM-dd');
                                      } else {
                                        return '';
                                      }
                                    },
                                	align: "left",
                                },
                				{
                                	name: "workHour",
                                	index: "workHour",
                                	width: 100,
                                	search : false,
                                	align: "right",
                                },
                {
                					name: "workAttachmentName",
                					index: "workAttachmentName",
                					width: 100,
                					search : false,
                					formatter : function(cellvalue, options, rowObject) {
                                    if(cellvalue != null && cellvalue != undefined){
                                      var selectHtml = '<a href="/api/workRest/download/'
                                                       + rowObject.workId
                                                       + '">'
                                                       + cellvalue
                                                       + '</a>';
                                    }else {
                                      var selectHtml = "";
                                    }
                                      return selectHtml;
                                    },
                					align: "left",
                				},
                {
                					name: "workAdmitStatus",
                					index: "workAdmitStatus",
                					width: 100,
                					formatter : function(cellvalue, options, rowObject) {
                                        if (cellvalue == "00") {
                                            return '未承認';
                                        } else if(cellvalue == "01"){
                                            return '承認済';
                                        } else{
                                            return cellvalue;
                                        }
                                    },
                                    //editable : true,
                                    //edittype : "select",
                                    //editoptions : {value : "00:未承認;01:承認済"},
                				},
				{
					name: "workStatus",
					index: "workStatus",
					width: 100,
					search : false,
					hidden : true,
					align: "right",
				},
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
	userName : $('#userName').val(),
};

var lastsel2;

var onSelectRow = function(id) {
      if (id && id !== lastsel2) {
        jQuery('#grid').jqGrid('restoreRow', lastsel2);
        jQuery('#grid').jqGrid('editRow', id, true);
        //if(lastsel2){
        //   $.ajax({url:"api/workRest/"+lastsel2, type:"GET", success:function(e){
        //      if (e != null) {
        //         $("#grid").setCell(e.workId,"workAdmitStatus",e.workAdmitStatus);
        //      }
        //   }});
        //}
        lastsel2 = id;
      }
};
var editurl = "/api/workRest/editStatus";

var shrinkToFit = true;

var gridComplete;

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete, onSelectRow, editurl);
});

	