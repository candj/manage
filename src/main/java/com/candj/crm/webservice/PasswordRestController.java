package com.candj.crm.webservice;

import com.candj.crm.Utils.HashkeyUtils;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.ChangePassword;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.AppUserService;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ユーザー情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/password")
@RestController
public class PasswordRestController {

    @Autowired
    AppUserService passwordService;

    /**
     * ユーザーを新規追加(確認)する。
     *
     * @param changePassword ユーザー
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/change_password", method = RequestMethod.POST)
    public int insertConfirm(@ModelAttribute("changePassword") ChangePassword changePassword) {
        if (changePassword.getNewPassword().equals(changePassword.getSure_newPassword())) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
            AppUser appUser = appAuthenticationUser.getAppUser();
            AppUserDto myAppUserDto = new AppUserDto();
            myAppUserDto.setUserName(appUser.getUserName());
            List<AppUserDto> appUserDtoList = passwordService.selectByExample(myAppUserDto);
            if (!appUserDtoList.isEmpty()) {
                AppUserDto appUserDto = appUserDtoList.get(0);
                if (appUserDto.getPassword().equals(changePassword.getOldPassword())) {
                    String hashKeyPassword = HashkeyUtils.hashKeyForDisk(changePassword.getNewPassword());
                    appUserDto.setPassword(hashKeyPassword);
                    return passwordService.updateByPrimaryKey(appUserDto);
                } else {
                    //古いパスワードエラー
                    throw new UncheckedLogicException("errors.error.password_error1");
                }
            } else {
                //ユーザーは存在しません
                throw new UncheckedLogicException("errors.error.password_error2");
            }
        } else {
            //2つのパスワードに一貫性がありません
            throw new UncheckedLogicException("errors.error.password_error3");
        }

    }

}
