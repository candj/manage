package com.candj.crm.jsonUtils;

import com.candj.crm.dto.IntegerChangeDto;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

import java.io.*;
import java.math.BigDecimal;

public class CalcuteStaffSalary {

    //jsonファイルをロードする
    public static String readJsonFile(String fileName) {
        FileInputStream file = null;
        BufferedReader reader = null;
        InputStreamReader inputFileReader = null;
        StringBuilder content = new StringBuilder();
        String tempString = null;
        try {
            file = new FileInputStream(fileName);
            inputFileReader = new InputStreamReader(file, "utf-8");
            reader = new BufferedReader(inputFileReader);
            // readLine 終了までロードする
            while ((tempString = reader.readLine()) != null) {
                content.append(tempString);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return content.toString();
    }


    public static void socialInsuranceCalculate(String path, Integer basePay, String location,
                                                IntegerChangeDto integerChangeDto, Integer age) {
        String filePath = path + "/" + location + ".JSON";
        String s = readJsonFile(filePath);
        JSONArray jArr = JSONArray.fromObject(s);
        BigDecimal socialInsuranceCompanyAmount = BigDecimal.ZERO;
        BigDecimal socialInsurancePersonalAmount = BigDecimal.ZERO;

        for (int i = 0 ; i < jArr.size();i++){
            JSONObject key = (JSONObject)jArr.get(i);
            Integer lowerLimit = key.get("報酬月額下限").equals(JSONNull.getInstance())? 0 : Integer.valueOf((String)key.get("報酬月額下限"));
            Integer upperLimit = key.get("報酬月額上限").equals(JSONNull.getInstance())? null : Integer.valueOf((String)key.get("報酬月額上限"));
            if(upperLimit != null) {
                if (basePay >= lowerLimit && basePay < upperLimit) {
                    if(age < 40 || age > 64) {
                        socialInsuranceCompanyAmount = new BigDecimal((String) key.get("健康保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setA(socialInsuranceCompanyAmount.intValue());
                        socialInsurancePersonalAmount = new BigDecimal((String) key.get("健康保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setB(socialInsurancePersonalAmount.intValue());
                        break;
                    } else{
                        socialInsuranceCompanyAmount = new BigDecimal((String) key.get("健康保険＋介護保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setA(socialInsuranceCompanyAmount.intValue());
                        socialInsurancePersonalAmount = new BigDecimal((String) key.get("健康保険＋介護保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setB(socialInsurancePersonalAmount.intValue());
                        break;
                    }
                }
            } else{
                if (basePay >= lowerLimit) {
                    if(age < 40 || age > 64) {
                        socialInsuranceCompanyAmount = new BigDecimal((String) key.get("健康保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setA(socialInsuranceCompanyAmount.intValue());
                        socialInsurancePersonalAmount = new BigDecimal((String) key.get("健康保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setB(socialInsurancePersonalAmount.intValue());
                        break;
                    } else{
                        socialInsuranceCompanyAmount = new BigDecimal((String) key.get("健康保険＋介護保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setA(socialInsuranceCompanyAmount.intValue());
                        socialInsurancePersonalAmount = new BigDecimal((String) key.get("健康保険＋介護保険（折半額）"))
                                .add(new BigDecimal((String) key.get("厚生年金保険（折半額）")));
                        integerChangeDto.setB(socialInsurancePersonalAmount.intValue());
                        break;
                    }
                }
            }
        }

    }

    public static void personalIncomeTaxCalculate(String path, Integer basePay, Integer bringUpNumber,
                                                IntegerChangeDto integerChangeDto) {
        String filePath = path + "/" + "月額表.JSON";
        String s = readJsonFile(filePath);
        JSONArray jArr = JSONArray.fromObject(s);
        BigDecimal personalIncomeTaxAmount = BigDecimal.ZERO;

        JSONArray Array1 = (JSONArray) jArr.get(0);
        JSONArray Array2 = (JSONArray) jArr.get(1);
        int i = 0;
        JSONObject key = (JSONObject) Array1.get(i);
        Integer lowerLimit = key.get("報酬月額下限").equals(JSONNull.getInstance())? 0 : (Integer)key.get("報酬月額下限");
        Integer upperLimit = key.get("報酬月額上限").equals(JSONNull.getInstance())? null : (Integer)key.get("報酬月額上限");
        if(basePay < lowerLimit){
            personalIncomeTaxAmount = BigDecimal.ZERO;
            integerChangeDto.setA(personalIncomeTaxAmount.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());

        } else {

            for (; i < Array1.size(); i++) {
                key = (JSONObject) Array1.get(i);
                lowerLimit = key.get("報酬月額下限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額下限");
                upperLimit = key.get("報酬月額上限").equals(JSONNull.getInstance()) ? null : (Integer) key.get("報酬月額上限");
                if (upperLimit != null) {
                    if (basePay >= lowerLimit && basePay < upperLimit) {
                        if (bringUpNumber <= 7) {
                            personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数" + Integer.toString(bringUpNumber)));
                        } else {
                            personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数7"))
                                    .subtract(new BigDecimal(1610).multiply(new BigDecimal(bringUpNumber - 7)));
                        }
                        if(personalIncomeTaxAmount.intValue() < 0){
                            personalIncomeTaxAmount = BigDecimal.ZERO;
                        }
                        integerChangeDto.setA(personalIncomeTaxAmount.intValue());
                        break;
                    }
                }
            }
            if (i == Array1.size()) {
                for (i = 0; i < Array2.size(); i++) {
                    key = (JSONObject) Array2.get(i);
                    lowerLimit = key.get("報酬月額下限").equals(JSONNull.getInstance()) ? 0 : (Integer) key.get("報酬月額下限");
                    upperLimit = key.get("報酬月額上限").equals(JSONNull.getInstance()) ? null : (Integer) key.get("報酬月額上限");
                    if (upperLimit != null) {
                        if (basePay >= lowerLimit && basePay < upperLimit) {
                            if (bringUpNumber <= 7) {
                                personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数" + Integer.toString(bringUpNumber)));
                            } else {
                                personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数7"))
                                        .subtract(new BigDecimal(1610).multiply(new BigDecimal(bringUpNumber - 7)));
                            }

                            BigDecimal extraTax = new BigDecimal((Double) key.get("料率") * (basePay - lowerLimit));

                            personalIncomeTaxAmount = personalIncomeTaxAmount.add(extraTax);
                            if(personalIncomeTaxAmount.intValue() < 0){
                                personalIncomeTaxAmount = BigDecimal.ZERO;
                            }
                            integerChangeDto.setA(personalIncomeTaxAmount.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                            break;
                        }
                    } else {
                        if (bringUpNumber <= 7) {
                            personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数" + Integer.toString(bringUpNumber)));
                        } else {
                            personalIncomeTaxAmount = new BigDecimal((Integer) key.get("扶養人数7"))
                                    .subtract(new BigDecimal(1610).multiply(new BigDecimal(bringUpNumber - 7)));
                        }

                        BigDecimal extraTax = new BigDecimal((Double) key.get("料率") * (basePay - lowerLimit));

                        personalIncomeTaxAmount = personalIncomeTaxAmount.add(extraTax);
                        if(personalIncomeTaxAmount.intValue() < 0){
                            personalIncomeTaxAmount = BigDecimal.ZERO;
                        }
                        integerChangeDto.setA(personalIncomeTaxAmount.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                        break;
                    }
                }
            }
        }

    }

}
