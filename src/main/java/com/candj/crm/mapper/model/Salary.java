package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 給料
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Salary {
    /** ID */
    private Integer salaryId;

    /** ユーザー */
    private AppUser appUser;

    /** スタッフID */
    private Integer userId;

    /** 年月 */
    private org.joda.time.DateTime salaryMonth;

    /** 基本給 */
    private Integer basePay;

    /** 残業代/営業費用/食費 */
    private Integer overtimePay;

    /** 交通費 */
    private Integer commutingCost;

    /** 社会保険料(会社分) */
    private Integer socialInsuranceCompany;

    /** 社会保険料(個人分) */
    private Integer socialInsurancePersonal;

    /** 住宅補助金、（その他） */
    private Integer housingSubsidies;

    /** 労働保険料(会社分) */
    private Integer labourInsuranceCompany;

    /** 労働保険料(個人分) */
    private Integer labourInsurancePersonal;

    /** ボーナス */
    private Integer bonus;

    /** 個人所得税 */
    private Integer personalIncomeTax;

    /** 実給 */
    private Integer salaryAmount;

    /** 扶養人数 */
    private Integer bringUpNumber;

    /** 支払い予定日付 */
    private org.joda.time.DateTime planPayDate;

    /** 支払い日付 */
    private org.joda.time.DateTime payDate;

    private Boolean playPayDateISNear;

    /** ステータス */
    private Byte status;

    /** 備考 */
    private String remark;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
