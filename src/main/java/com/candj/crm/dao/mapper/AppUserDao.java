package com.candj.crm.dao.mapper;

import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.WorkDurationDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface AppUserDao {
    /**
     * プライマリーキーでユーザーを検索する。
     * @param userId ユーザーID
     * @return 結果
     */
    AppUser selectByPrimaryKey(Integer userId);

    /**
     * プライマリーキーでユーザー検索する。（連携情報含む）
     * @param userId ユーザーID
     * @return 結果
     */
    List<AppUser> selectAllByPrimaryKey(Integer userId);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<AppUser> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<AppUser> selectByExample(@Param("example") WhereCondition example);

    /**
     * ユーザーを新規追加する。
     * @param record ユーザー
     * @return 結果
     */
    int insertSelective(AppUser record);

    /**
     * ユーザーを新規追加する。
     * @param record ユーザー
     * @return 結果
     */
    int insert(AppUser record);

    /**
     * プライマリーキーでユーザーを更新する。
     * @param record ユーザー
     * @return 結果
     */
    int updateByPrimaryKey(AppUser record);

    /**
     * プライマリーキーでユーザーを更新する。
     * @param record ユーザー
     * @return 結果
     */
    int updateByPrimaryKeySelective(AppUser record);

    /**
     * ユーザーを更新する。
     * @param record ユーザー
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(AppUser record, @Param("example") WhereCondition example);

    /**
     * ユーザーを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") AppUser example);

    /**
     * ユーザーを削除する。
     * @param userId ユーザーID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer userId);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param dto 検索条件
     * @return 結果
     */
    List<AppUser> selectStaffWorkByYearMonth(WorkDurationDto dto);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param dto 検索条件
     * @return 結果
     */
    List<AppUser> selectStaffSalaryByYearMonth(MonthDto dto);
}
