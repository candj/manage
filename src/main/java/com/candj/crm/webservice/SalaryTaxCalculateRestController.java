package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.mapper.model.StaffSalaryInfo;
import com.candj.crm.service.*;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SalaryTaxCalculateRestController {

    @Autowired
    SalaryService salaryService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    StaffSalaryInfoService staffSalaryInfoService;

    @Autowired
    CompanyService companyService;

    @Autowired
    CommuteService commuteService;

    @Autowired
    WorkService workService;

    @Autowired
    Calculate calculate;

    /**
     * 注文一覧画面を表示する。
     * @param salaryDto 注文
     * @param salaryDto 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "salary/taxCalculate", method = RequestMethod.POST)
    public Object taxCalculateInsert(SalaryDto salaryDto, @RequestParam YearMonth yearMonth) throws Exception{

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);

        WorkDto workDto = new WorkDto();
        workDto.setUserId(salaryDto.getUserId());
        workDto.setWorkMonth(month);
        List<WorkDto> workDtoList = workService.selectAllByUserAndMonth(workDto);

        //交通費
        salaryDto.setSalaryAmount(salaryDto.getCommutingCost());

        Integer basePay = salaryDto.getBasePay();
        salaryDto.setBonus(0);
        salaryDto.setHousingSubsidies(0);

        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);
        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        staffSalaryInfoDto.setSalaryStartDate(month);
        staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
        staffSalaryInfoDto.setUserId(salaryDto.getUserId());
        List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
        salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());

        if (!staffSalaryInfoDtoList.isEmpty()) {
            salaryService.makeSalaryForEdit(workDtoList, month, staffSalaryInfoDtoList.get(0), salaryDto, basePay);
        } else {
            salaryService.makeSalary(workDtoList, month, null, salaryDto);
        }




        return salaryDto;
    }

    /**
     * 注文一覧画面を表示する。
     * @param salaryDto 注文
     * @param salaryDto 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "salary/edit/{salaryId}/taxCalculate", method = RequestMethod.POST)
    public Object taxCalculateEdit(SalaryDto salaryDto, @RequestParam YearMonth yearMonth) throws Exception{
        //交通費
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        WorkDto workDto = new WorkDto();
        workDto.setUserId(salaryDto.getUserId());
        workDto.setWorkMonth(month);
        List<WorkDto> workDtoList = workService.selectAllByUserAndMonth(workDto);
        salaryDto.setSalaryAmount(salaryDto.getCommutingCost());

        Integer basePay = salaryDto.getBasePay();
        salaryDto.setBonus(0);
        salaryDto.setHousingSubsidies(0);

        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);
        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        staffSalaryInfoDto.setSalaryStartDate(month);
        staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
        staffSalaryInfoDto.setUserId(salaryDto.getUserId());
        List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
        salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());

        if (!staffSalaryInfoDtoList.isEmpty()) {
            salaryService.makeSalaryForEdit(workDtoList, month, staffSalaryInfoDtoList.get(0), salaryDto, basePay);
        } else {
            salaryService.makeSalary(workDtoList, month, null, salaryDto);
        }



        return salaryDto;
    }

    /**
     * 注文一覧画面を表示する。
     * @param salaryDto 注文
     * @param yearMonth 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "salary/taxDefault", method = RequestMethod.POST)
    public Object taxDefaultInsert(SalaryDto salaryDto, @RequestParam YearMonth yearMonth) throws Exception{

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);

        WorkDto workDto = new WorkDto();
        workDto.setUserId(salaryDto.getUserId());
        workDto.setWorkMonth(month);
        List<WorkDto> workDtoList = new ArrayList<WorkDto>();
        if(salaryDto.getUserId() != null) {
            workDtoList = workService.selectAllByUserAndMonth(workDto);
        }
        //交通費
        CommuteDto commuteDto = new CommuteDto();
        commuteDto.setUserId(salaryDto.getUserId());
        commuteDto.setCommuteDate(month);
        List<CommuteDto> commuteDtos = new ArrayList<CommuteDto>();
        if(salaryDto.getUserId() != null) {
            commuteDtos = commuteService.selectByCommuteDto(commuteDto);
        }

        int commutingCost = 0;
        if (!commuteDtos.isEmpty()) {

            for (CommuteDto commute : commuteDtos) {
                if ("01".equals(commute.getCommuteAdmitStatus())) {
                    commutingCost = commute.getCommuteAmount();
                } else {
                    commutingCost = 0;
                }

                if (salaryDto.getCommutingCost() == null) {
                    salaryDto.setCommutingCost(commutingCost);
                } else {
                    salaryDto.setCommutingCost(commutingCost + salaryDto.getCommutingCost());
                }
            }
        }else{
            salaryDto.setCommutingCost(0);
        }

        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        staffSalaryInfoDto.setSalaryStartDate(month);
        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);
        staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
        staffSalaryInfoDto.setUserId(salaryDto.getUserId());
        List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
        salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());
        salaryDto.setSalaryAmount(salaryDto.getCommutingCost());

        salaryDto.setBasePay(0);
        salaryDto.setBonus(0);
        salaryDto.setHousingSubsidies(0);

        if (!staffSalaryInfoDtoList.isEmpty()) {
            salaryService.makeSalary(workDtoList, month, staffSalaryInfoDtoList.get(0), salaryDto);
        } else {
            salaryService.makeSalary(workDtoList, month, staffSalaryInfoDtoList.get(0), salaryDto);
        }
        return salaryDto;
    }

    /**
     * 注文一覧画面を表示する。
     * @param salaryDto 注文
     * @param salaryDto 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "salary/amountCalculate", method = RequestMethod.POST)
    public Object amountCalculateInsert(SalaryDto salaryDto, @RequestParam YearMonth yearMonth) throws Exception{

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);

        WorkDto workDto = new WorkDto();
        workDto.setUserId(salaryDto.getUserId());
        workDto.setWorkMonth(month);
        List<WorkDto> workDtoList = workService.selectAllByUserAndMonth(workDto);

        //交通費
        salaryDto.setSalaryAmount(salaryDto.getCommutingCost());

        Integer basePay = salaryDto.getBasePay();
        Integer overTimePay = salaryDto.getOvertimePay();
        Integer bonus = salaryDto.getBonus();
        Integer bringUpNumber = salaryDto.getBringUpNumber();
        Integer housingSubsidies = salaryDto.getHousingSubsidies();

        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);

        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        staffSalaryInfoDto.setSalaryStartDate(month);
        staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
        staffSalaryInfoDto.setUserId(salaryDto.getUserId());
        List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
        salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());

        salaryService.makeSalaryForAmount(month, staffSalaryInfoDtoList.get(0), salaryDto, basePay, overTimePay, bonus, bringUpNumber, housingSubsidies);


        return salaryDto;
    }

    /**
     * 注文一覧画面を表示する。
     * @param salaryDto 注文
     * @param salaryDto 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "salary/edit/{salaryId}/amountCalculate", method = RequestMethod.POST)
    public Object amountCalculateEdit(SalaryDto salaryDto, @RequestParam YearMonth yearMonth) throws Exception{

        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);

        WorkDto workDto = new WorkDto();
        workDto.setUserId(salaryDto.getUserId());
        workDto.setWorkMonth(month);
        List<WorkDto> workDtoList = workService.selectAllByUserAndMonth(workDto);

        //交通費
        salaryDto.setSalaryAmount(salaryDto.getCommutingCost());

        Integer basePay = salaryDto.getBasePay();
        Integer overTimePay = salaryDto.getOvertimePay();
        Integer bonus = salaryDto.getBonus();
        Integer bringUpNumber = salaryDto.getBringUpNumber();
        Integer housingSubsidies = salaryDto.getHousingSubsidies();

        DateTime endOfMonth = month.plusMonths(1).minusSeconds(1);

        StaffSalaryInfoDto staffSalaryInfoDto = new StaffSalaryInfoDto();
        staffSalaryInfoDto.setSalaryStartDate(month);
        staffSalaryInfoDto.setSalaryEndDate(endOfMonth);
        staffSalaryInfoDto.setUserId(salaryDto.getUserId());
        List<StaffSalaryInfoDto> staffSalaryInfoDtoList = staffSalaryInfoService.selectAllByMyExample(staffSalaryInfoDto);
        salaryDto.setBringUpNumber(staffSalaryInfoDtoList.get(0).getSalaryBringUpNumber());

        salaryService.makeSalaryForAmount(month, staffSalaryInfoDtoList.get(0), salaryDto, basePay, overTimePay, bonus, bringUpNumber, housingSubsidies);


        return salaryDto;
    }

}
