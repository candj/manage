package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.InvoiceDetail;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 作業情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/workRest")
@RestController
public class WorkRestController {

    @Autowired
    WorkService workService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    AppUserService appUserService;

    @Autowired
    SalaryService salaryService;

    @Autowired
    Calculate calculate;

    @Autowired
    InvoiceDetailService invoiceDetailService;


    /**
     * 作業を新規追加する。
     *
     * @param workId 作業ID
     * @return 結果
     */
    @RequestMapping(value = "{workId}", method = RequestMethod.GET)
    public WorkDto get(@PathVariable Integer workId) {
        //プライマリーキーで作業を検索する。
        WorkDto ret = workService.selectByPrimaryKey(workId);
        return ret;
    }

    /**
     * 作業を新規追加する。
     *
     * @param workDto 作業
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(WorkDto workDto,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) {
        //注文ID、スタッフID、時間で見積詳細を検索する
        DateTime startOfMonth = DateTimeUtil.StringToDateTime(workDto.getYearMonth());
        OfferDetailForWorkDto offerDetailForWorkDto = new OfferDetailForWorkDto();
        offerDetailForWorkDto.setOrderId(workDto.getOrderId());
        offerDetailForWorkDto.setUserId(workDto.getUserId());
        offerDetailForWorkDto.setStartOfMonth(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        offerDetailForWorkDto.setEndOfMonth(endOfMonth);

        List<OfferDetail> offerDetails = offerDetailService.selectByStaffAndMonth(offerDetailForWorkDto);
        //デフォルト値の設定
        workDto.setWorkMonth(startOfMonth);
        workDto.setWorkAdmitStatus("00");
        if (!offerDetails.isEmpty()) {
            workDto.setUnitPrice(offerDetails.get(0).getAmount());
            workDto.setWorkManHour(offerDetails.get(0).getManHour());
        }
        //ファイルをアプロードしたら、保存する
        if (attachment != null) {
            try {
                String fileName = attachment.getOriginalFilename();
                workDto.setWorkAttachmentName(fileName);
                String filePath = ResourceUtils.getURL("classpath:").getPath() + "attachment/";
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                attachment.transferTo(dest);
                workDto.setWorkAttachmentPath(realPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        OfferDetail offerDetail = offerDetails.get(0);
        String workTimeCalculateStandard = offerDetail.getOffer().getWorkTimeCalculateStandard();
        BigDecimal m_workHour = workDto.getWorkHour();
        int workHourInt = (int) (m_workHour.doubleValue() * 60);
        if (workTimeCalculateStandard.equals("00")) {//15分
            int tempInt = (workHourInt / 15) * 15;
            double tempDouble = tempInt / 60.0;
            m_workHour = new BigDecimal(tempDouble);
            workDto.setWorkHour(m_workHour);
        } else if (workTimeCalculateStandard.equals("01")) {//30分
            int tempInt = (workHourInt / 30) * 30;
            double tempDouble = tempInt / 60.0;
            m_workHour = new BigDecimal(tempDouble);
            workDto.setWorkHour(m_workHour);
        }

        //作業を新規追加する。
        int ret = workService.insertSelective(workDto);

        InvoiceDetailDto invoiceDetail = new InvoiceDetailDto();
        invoiceDetail.setWorkStartDate(startOfMonth);
        invoiceDetail.setWorkEndDate(endOfMonth);
        invoiceDetail.setUserId(workDto.getUserId());

        List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailService.selectByExample(invoiceDetail);

        if (!invoiceDetailDtoList.isEmpty()) {
            InvoiceDetailDto invoiceDetailDto = invoiceDetailDtoList.get(0);
            if (invoiceDetailDto.getPersonMonth().intValue() == 0 && invoiceDetailDto.getSalaryPersonMonth() == 0) {
                invoiceDetailDto.setWorkStartDate(workDto.getWorkStartDate());
                invoiceDetailDto.setWorkEndDate(workDto.getWorkEndDate());
                invoiceDetailDto.setSalaryPersonMonth(workDto.getUnitPrice());
                invoiceDetailDto.setPersonMonth(workDto.getWorkManHour());
                invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(workDto.getUnitPrice(), workDto.getWorkManHour()));
                if (offerDetail.getOffer().getAfterPaymentPattern().equals("10")) {
                    Double workHour = workDto.getWorkHour().doubleValue();
                    //工数によって、月作業時間基準を計算する
                    Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit()
                            * workDto.getWorkManHour().doubleValue();
                    Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit()
                            * workDto.getWorkManHour().doubleValue();
                    //超過の場合
                    if (workHour > workTimeUpperLimit) {
                        Double overTime = workHour - workTimeUpperLimit;
                        BigDecimal time = new BigDecimal(overTime);
                        invoiceDetailDto.setOverTime(time);
                        Double extraUnitPrice = workDto.getUnitPrice() / workTimeUpperLimit;
                        invoiceDetailDto.setSalaryExtra(extraUnitPrice.intValue());
                        Double amountExtra = extraUnitPrice * overTime;
                        invoiceDetailDto.setAmountExtra(amountExtra.intValue());
                        invoiceDetailDto.setSalaryDeduction(0);
                    }
                    //不足の場合
                    else if (workHour < workTimeLowerLimit) {
                        Double lessTime = workHour - workTimeLowerLimit;
                        BigDecimal time = new BigDecimal(lessTime);
                        invoiceDetailDto.setOverTime(time);
                        Double deductionUnitPrice = workDto.getUnitPrice() / workTimeLowerLimit;
                        invoiceDetailDto.setSalaryDeduction(deductionUnitPrice.intValue());
                        Double amountDeduction = deductionUnitPrice * lessTime;
                        invoiceDetailDto.setSalaryDeduction(amountDeduction.intValue());
                        invoiceDetailDto.setAmountExtra(0);
                    }
                }else if (offerDetail.getOffer().getAfterPaymentPattern().equals("11")){
                    Double workHour = workDto.getWorkHour().doubleValue();
                    //工数によって、月作業時間基準を計算する
                    Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit()
                            * workDto.getWorkManHour().doubleValue();
                    Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit()
                            * workDto.getWorkManHour().doubleValue();
                    //超過の場合
                    if (workHour > workTimeUpperLimit) {
                        Double overTime = workHour - workTimeUpperLimit;
                        BigDecimal time = new BigDecimal(overTime);
                        invoiceDetailDto.setOverTime(time);
                        double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                        Double extraUnitPrice = workDto.getUnitPrice() / workTimeLimit;
                        invoiceDetailDto.setSalaryExtra(extraUnitPrice.intValue());
                        Double amountExtra = extraUnitPrice * overTime;
                        invoiceDetailDto.setAmountExtra(amountExtra.intValue());
                        invoiceDetailDto.setSalaryDeduction(0);
                    }
                    //不足の場合
                    else if (workHour < workTimeLowerLimit) {
                        Double lessTime = workHour - workTimeLowerLimit;
                        BigDecimal time = new BigDecimal(lessTime);
                        invoiceDetailDto.setOverTime(time);
                        double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                        Double deductionUnitPrice = workDto.getUnitPrice() / workTimeLimit;
                        invoiceDetailDto.setSalaryDeduction(deductionUnitPrice.intValue());
                        Double amountDeduction = deductionUnitPrice * lessTime;
                        invoiceDetailDto.setSalaryDeduction(amountDeduction.intValue());
                        invoiceDetailDto.setAmountExtra(0);
                    }
                }
                //請求詳細を新規追加する。
                invoiceDetailService.updateByPrimaryKeySelective(invoiceDetailDto);
            }
        }
        return ret;
    }


    /**
     * 作業を変更する。
     *
     * @param workDto 作業
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(WorkDto workDto,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() != null && appUserSql.getUserType() == 1) {
            List<WorkDto> workDtoList = workService.selectAllByExample(workDto);
            if (!workDtoList.isEmpty()) {
                WorkDto workDto1 = workDtoList.get(0);
                if ("01".equals(workDto1.getWorkAdmitStatus())) {
                    throw new UncheckedLogicException("errors.error.work_error1");
                }
            } else {
                throw new UncheckedLogicException("errors.error.work_error2");
            }
        }
        //ファイルをアプロードしたら、保存する
        if (attachment != null) {
            try {
                String fileName = attachment.getOriginalFilename();
                workDto.setWorkAttachmentName(fileName);
                if (workDto.getWorkAttachmentPath() != null) {
                    File oldFile = new File(workDto.getWorkAttachmentPath());
                    oldFile.delete();
                }
                String filePath = ResourceUtils.getURL("classpath:").getPath() + "attachment/";
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                attachment.transferTo(dest);
                workDto.setWorkAttachmentPath(realPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        OfferDetailForWorkDto offerDetailForWorkDto = new OfferDetailForWorkDto();
        DateTime startOfMonth = workDto.getWorkStartDate().withDayOfMonth(1);
        offerDetailForWorkDto.setOrderId(workDto.getOrderId());
        offerDetailForWorkDto.setUserId(workDto.getUserId());
        offerDetailForWorkDto.setStartOfMonth(startOfMonth);
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);
        offerDetailForWorkDto.setEndOfMonth(endOfMonth);

        List<OfferDetail> offerDetails = offerDetailService.selectByStaffAndMonth(offerDetailForWorkDto);

        OfferDetail offerDetail = offerDetails.get(0);
        String workTimeCalculateStandard = offerDetail.getOffer().getWorkTimeCalculateStandard();
        BigDecimal m_workHour = workDto.getWorkHour();
        int workHourInt = (int) (m_workHour.doubleValue() * 60);
        if (workTimeCalculateStandard.equals("00")) {//15分
            int tempInt = (workHourInt / 15) * 15;
            double tempDouble = tempInt / 60.0;
            m_workHour = new BigDecimal(tempDouble);
            workDto.setWorkHour(m_workHour);
        } else if (workTimeCalculateStandard.equals("01")) {//30分
            int tempInt = (workHourInt / 30) * 30;
            double tempDouble = tempInt / 60.0;
            m_workHour = new BigDecimal(tempDouble);
            workDto.setWorkHour(m_workHour);
        }

        WorkDto workData = workService.selectByPrimaryKey(workDto.getWorkId());
        //プライマリーキーで作業を更新する。
        int ret = workService.updateByPrimaryKeySelective(workDto);
        if (workData.getWorkHour().intValue() != workDto.getWorkHour().intValue()) {

            InvoiceDetailDto invoiceDetail = new InvoiceDetailDto();
            invoiceDetail.setWorkStartDate(startOfMonth);
            invoiceDetail.setWorkEndDate(endOfMonth);
            invoiceDetail.setUserId(workDto.getUserId());

            List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailService.selectByExample(invoiceDetail);

            if (!invoiceDetailDtoList.isEmpty()) {
                InvoiceDetailDto invoiceDetailDto = invoiceDetailDtoList.get(0);
                if (invoiceDetailDto.getPersonMonth().intValue() == 0 && invoiceDetailDto.getSalaryPersonMonth() == 0) {
                    invoiceDetailDto.setWorkStartDate(workDto.getWorkStartDate());
                    invoiceDetailDto.setWorkEndDate(workDto.getWorkEndDate());
                    invoiceDetailDto.setSalaryPersonMonth(workDto.getUnitPrice());
                    invoiceDetailDto.setPersonMonth(workDto.getWorkManHour());
                    invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(workDto.getUnitPrice(), workDto.getWorkManHour()));
                    if (offerDetail.getOffer().getAfterPaymentPattern().equals("10")) {
                        Double workHour = workDto.getWorkHour().doubleValue();
                        //工数によって、月作業時間基準を計算する
                        Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit()
                                * workDto.getWorkManHour().doubleValue();
                        Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit()
                                * workDto.getWorkManHour().doubleValue();
                        //超過の場合
                        if (workHour > workTimeUpperLimit) {
                            Double overTime = workHour - workTimeUpperLimit;
                            BigDecimal time = new BigDecimal(overTime);
                            invoiceDetailDto.setOverTime(time);
                            Double extraUnitPrice = workDto.getUnitPrice() / workTimeUpperLimit;
                            invoiceDetailDto.setSalaryExtra(extraUnitPrice.intValue());
                            Double amountExtra = extraUnitPrice * overTime;
                            invoiceDetailDto.setAmountExtra(amountExtra.intValue());
                            invoiceDetailDto.setSalaryDeduction(0);
                        }
                        //不足の場合
                        else if (workHour < workTimeLowerLimit) {
                            Double lessTime = workHour - workTimeLowerLimit;
                            BigDecimal time = new BigDecimal(lessTime);
                            invoiceDetailDto.setOverTime(time);
                            Double deductionUnitPrice = workDto.getUnitPrice() / workTimeLowerLimit;
                            invoiceDetailDto.setSalaryDeduction(deductionUnitPrice.intValue());
                            Double amountDeduction = deductionUnitPrice * lessTime;
                            invoiceDetailDto.setSalaryDeduction(amountDeduction.intValue());
                            invoiceDetailDto.setAmountExtra(0);
                        }
                    }else if (offerDetail.getOffer().getAfterPaymentPattern().equals("11")){
                        Double workHour = workDto.getWorkHour().doubleValue();
                        //工数によって、月作業時間基準を計算する
                        Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit()
                                * workDto.getWorkManHour().doubleValue();
                        Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit()
                                * workDto.getWorkManHour().doubleValue();
                        //超過の場合
                        if (workHour > workTimeUpperLimit) {
                            Double overTime = workHour - workTimeUpperLimit;
                            BigDecimal time = new BigDecimal(overTime);
                            invoiceDetailDto.setOverTime(time);
                            double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                            Double extraUnitPrice = workDto.getUnitPrice() / workTimeLimit;
                            invoiceDetailDto.setSalaryExtra(extraUnitPrice.intValue());
                            Double amountExtra = extraUnitPrice * overTime;
                            invoiceDetailDto.setAmountExtra(amountExtra.intValue());
                            invoiceDetailDto.setSalaryDeduction(0);
                        }
                        //不足の場合
                        else if (workHour < workTimeLowerLimit) {
                            Double lessTime = workHour - workTimeLowerLimit;
                            BigDecimal time = new BigDecimal(lessTime);
                            invoiceDetailDto.setOverTime(time);
                            double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                            Double deductionUnitPrice = workDto.getUnitPrice() / workTimeLimit;
                            invoiceDetailDto.setSalaryDeduction(deductionUnitPrice.intValue());
                            Double amountDeduction = deductionUnitPrice * lessTime;
                            invoiceDetailDto.setSalaryDeduction(amountDeduction.intValue());
                            invoiceDetailDto.setAmountExtra(0);
                        }
                    }
                    //請求詳細を新規追加する。
                    invoiceDetailService.updateByPrimaryKeySelective(invoiceDetailDto);
                }
            }
            return ret;
        }

        return 1;
    }

    /**
     * 作業を新規追加する。
     *
     * @param workId 作業ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{workId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer workId) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() != null && appUserSql.getUserType() == 1) {
            WorkDto workDto = workService.selectByPrimaryKey(workId);
            if ("01".equals(workDto.getWorkAdmitStatus())) {
                throw new UncheckedLogicException("errors.error.work_error1");
            }
        }
        //作業を削除する。
        return workService.deleteByPrimaryKey(workId);
    }

    /**
     * 作業一覧画面を表示する。
     *
     * @param workDto 作業
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<WorkDto> list(WorkDto workDto,
                                  @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                  @RequestParam(name = "userName", required = false) String userName,
                                  JqgridPageReq pageReq) {
        Page<WorkDto> page = PageUtil.startPage(pageReq);
        DateTime month = new DateTime();
        if (yearMonth != null) {
            month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        workDto.setWorkMonth(month);

        AppUser appUser = new AppUser();
        appUser.setUserName(userName);


        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() == 1) {
            appUser.setUserName(appUserSql.getUserName());
            appUser.setUserId(appUserSql.getUserId());
        }

        workDto.setAppUser(appUser);

        //条件で作業を検索する。（連携情報含む）
        List<WorkDto> ret = workService.selectAllByExample(workDto);
        return PageUtil.resp(page);
    }

    /**
     * 作業一覧画面を表示する。
     *
     * @param workDto 作業
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list/{orderId}", method = RequestMethod.POST)
    public PageInfo<WorkDto> listOfInvoice(WorkDto workDto,
                                           @PathVariable Integer orderId,
                                           JqgridPageReq pageReq) {
        Page<WorkDto> page = PageUtil.startPage(pageReq);
        //条件で作業を検索する。（連携情報含む）
        List<WorkDto> ret = workService.selectByOrderId(orderId);
        return PageUtil.resp(page);
    }

    /**
     * 作業一覧画面を表示する。
     *
     * @param workDto 作業
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<WorkDto> listAll(WorkDto workDto, JqgridPageReq pageReq) {
        Page<WorkDto> page = PageUtil.startPage(pageReq);
        //条件で作業を検索する。（連携情報含む）
        List<WorkDto> ret = workService.selectAllByExample(workDto);
        return PageUtil.resp(page);
    }

    /**
     * 勤務一覧画面を表示する。
     *
     * @param appUserDto ユーザー
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/workList", method = RequestMethod.POST)
    public PageInfo<AppUserDto> workList(AppUserDto appUserDto,
                                         @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                         JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        WorkDurationDto workDurationDto = new WorkDurationDto();
        workDurationDto.setUserId(appUserDto.getUserId());
        workDurationDto.setStartDate(month);
        List<AppUserDto> ret = appUserService.selectStaffWorkByYearMonth(workDurationDto, month);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/download/{workId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(HttpServletRequest request,
                                           @PathVariable("workId") Integer workId,
                                           @RequestHeader("User-Agent") String userAgent,
                                           ModelMap model) throws Exception {
        // ファイルパース
        WorkDto workDto = workService.selectByPrimaryKey(workId);
        String fileName = workDto.getWorkAttachmentName();
        String path = workDto.getWorkAttachmentPath();
        File file = new File(path);

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        // 内容サイズ
        builder.contentLength(file.length());
        // application-octet-stream ： 二进制データ
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        // URLEncoder.encodeでファイルをエンコードする
        fileName = URLEncoder.encode(fileName, "UTF-8");
        if (userAgent.indexOf("MSIE") > 0) {
            // IE
            builder.header("Content-Disposition", "attachment; filename=" + fileName);
        } else {
            // FireFox、Chrome
            builder.header("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);
        }
        return builder.body(FileUtils.readFileToByteArray(file));
    }

    /**
     * 勤務の承認状態を変更する。
     *
     * @param workDto 勤務
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/editStatus", method = RequestMethod.POST)
    public int editStatus(WorkDto workDto, Errors rrrors) {
        //承認状態を更新する
        int ret = workService.updateByPrimaryKeySelective(workDto);
        return ret;
    }
}
