package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkDurationInvoiceIdDto {

    private Integer userId;

    private Integer invoiceId;

    /** 開始時間 */
    private org.joda.time.DateTime startDate;

    /** 終了時間 */
    private org.joda.time.DateTime EndDate;

}
