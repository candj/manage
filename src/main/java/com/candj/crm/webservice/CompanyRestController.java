package com.candj.crm.webservice;

import com.candj.crm.dto.CompanyDto;
import com.candj.crm.service.CompanyService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 会社情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/companyRest")
@RestController
public class CompanyRestController {
    @Autowired
    CompanyService companyService;

    /**
     * 会社を新規追加する。
     * @param companyId 会社ID
     * @return 結果
     */
    @RequestMapping(value = "{companyId}", method = RequestMethod.GET)
    public CompanyDto get(@PathVariable Integer companyId) {
        //プライマリーキーで会社を検索する。
        CompanyDto ret = companyService.selectByPrimaryKey(companyId);
        return ret;
    }

    /**
     * 会社を新規追加する。
     * @param companyDto 会社
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody CompanyDto companyDto, Errors rrrors) {
        companyDto.setCompanyDeleteFlag(true);
        //会社を新規追加する。
        int ret = companyService.insertSelective(companyDto);
        return ret;
    }

    /**
     * 会社を変更する。
     * @param companyDto 会社
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody CompanyDto companyDto, Errors rrrors) {
        companyDto.setCompanyDeleteFlag(true);
        //プライマリーキーで会社を更新する。
        int ret = companyService.updateByPrimaryKeySelective(companyDto);
        return ret;
    }

    /**
     * 会社を新規追加する。
     * @param companyId 会社ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{companyId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer companyId) {
        CompanyDto companyDto = companyService.selectByPrimaryKey(companyId);
        companyDto.setCompanyDeleteFlag(false);
        //会社を削除する。
        int ret = companyService.updateByPrimaryKey(companyDto);
        return ret;
    }

    /**
     * 会社一覧画面を表示する。
     * @param companyDto 会社
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<CompanyDto> list(CompanyDto companyDto, JqgridPageReq pageReq) {
        Page<CompanyDto> page = PageUtil.startPage(pageReq);
        //条件で会社を検索する。（連携情報含む）
        List<CompanyDto> ret = companyService.selectByExample(companyDto);
        return PageUtil.resp(page);
    }

    /**
     * 会社一覧画面を表示する。
     * @param companyDto 会社
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<CompanyDto> listAll(CompanyDto companyDto, JqgridPageReq pageReq) {
        Page<CompanyDto> page = PageUtil.startPage(pageReq);
        //条件で会社を検索する。（連携情報含む）
        List<CompanyDto> ret = companyService.selectAllByExample(companyDto);
        return PageUtil.resp(page);
    }
}
