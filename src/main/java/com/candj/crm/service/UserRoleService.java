package com.candj.crm.service;

import com.candj.crm.dao.mapper.UserRoleDao;
import com.candj.crm.dto.UserRoleDto;
import com.candj.crm.mapper.model.UserRole;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ユーザーロール情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class UserRoleService {
    @Autowired
    UserRoleDao userRoleDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでユーザーロールを検索する。
     * @param userRoleId ID
     * @return 結果
     */
    public UserRoleDto selectByPrimaryKey(Integer userRoleId) {
        //プライマリーキーでユーザーロールを検索する。
        UserRole ret = userRoleDao.selectByPrimaryKey(userRoleId);
        return DozerHelper.map(mapper, ret, UserRoleDto.class);
    }

    /**
     * プライマリーキーでユーザーロールを検索する。（連携情報含む）
     * @param userRoleId ID
     * @return 結果
     */
    public List<UserRoleDto> selectAllByPrimaryKey(Integer userRoleId) {
        //プライマリーキーでユーザーロール検索する。（連携情報含む）
        List<UserRole> ret = userRoleDao.selectAllByPrimaryKey(userRoleId);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * 条件でユーザーロールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserRoleDto> selectByExample(UserRoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user_role.user_role_id", dto.getUserRoleId())
            .andEqualTo("user_role.user_id", dto.getUserId())
            .andEqualTo("user_role.role_id", dto.getRoleId())
            .andEqualTo("user_role.user_role_status", dto.getUserRoleStatus())
            .andEqualTo("user_role.create_user", dto.getCreateUser())
            .andEqualTo("user_role.update_user", dto.getUpdateUser())
            .andEqualTo("user_role.create_time", dto.getCreateTime())
            .andEqualTo("user_role.update_time", dto.getUpdateTime());
        }
        //条件でユーザーロールを検索する。（連携情報含む）)
        List<UserRole> ret = userRoleDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * 条件でユーザーロールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserRoleDto> selectAllByExample(UserRoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user_role.user_role_id", dto.getUserRoleId())
            .andEqualTo("user_role.user_id", dto.getUserId())
            .andEqualTo("user_role.role_id", dto.getRoleId())
            .andEqualTo("user_role.user_role_status", dto.getUserRoleStatus())
            .andEqualTo("user_role.create_user", dto.getCreateUser())
            .andEqualTo("user_role.update_user", dto.getUpdateUser())
            .andEqualTo("user_role.create_time", dto.getCreateTime())
            .andEqualTo("user_role.update_time", dto.getUpdateTime());
        }
        //条件でユーザーロールを検索する。（連携情報含む）)
        List<UserRole> ret = userRoleDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * ユーザーロールを新規追加する。
     * @param dto ユーザーロール
     * @return 結果
     */
    public int insertSelective(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //ユーザーロールを新規追加する。
        int ret = userRoleDao.insertSelective(userRole);
        return ret;
    }

    /**
     * ユーザーロールを新規追加する。
     * @param dto ユーザーロール
     * @return 結果
     */
    public int insert(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //ユーザーロールを新規追加する。
        int ret = userRoleDao.insert(userRole);
        return ret;
    }

    /**
     * プライマリーキーでユーザーロールを更新する。
     * @param dto ユーザーロール
     * @return 結果
     */
    public int updateByPrimaryKey(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //プライマリーキーでユーザーロールを更新する。
        int ret = userRoleDao.updateByPrimaryKey(userRole);
        return ret;
    }

    /**
     * プライマリーキーでユーザーロールを更新する。
     * @param dto ユーザーロール
     * @return 結果
     */
    public int updateByPrimaryKeySelective(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //プライマリーキーでユーザーロールを更新する。
        int ret = userRoleDao.updateByPrimaryKeySelective(userRole);
        return ret;
    }

    /**
     * ユーザーロールを削除する。
     * @param userRoleId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer userRoleId) {
        //ユーザーロールを削除する。
        int ret = userRoleDao.deleteByPrimaryKey(userRoleId);
        return ret;
    }
}
