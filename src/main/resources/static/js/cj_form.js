;
(function(factory) {
	'use strict'
	if (typeof define === 'function' && define.amd) {
		// using AMD; register as anon module
		define([ 'jquery' ], factory)
	} else {
		// no AMD; invoke directly
		factory((typeof (jQuery) != 'undefined') ? jQuery : window.Zepto)
	}
}
(function($) {
	'use strict'
	$.fn.cjform = function(options) {
		var defaults = {
			postingHTML : '<i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i>サブミット中．．．',

			// 提交前
			beforeSubmit : function() {
				console.log('beforeSubmit')
			},

					// 提交错误
					error : function(e) {

						var message = null
						var res = e.responseText
						if (res) {
							message = $.parseJSON(res).errors[0].message;
							if (top.swal)
								top.swal(message, '', 'warning')
						} else {
							message = 'システムエラーが発生しました。'
							if (top.swal)
								top.swal(message, '', 'error')
						}
					},

			// 提交成功
			success : function(response) {
				const toast = top.toastr;
				if (toast)
					top.toastr.success(response.data,'操作完了しました。')

				form.trigger('success')

				// reload table
				$.fn.parentPage().find('.glyphicon-refresh').click()
			},

			// 提交完成
			complete : function(xhr, status) {
				resetSubmit();
			},

			ajaxForm : function() {
				disabledSubmit()

				var submitData = $('form').serializeJSON();
				//var submitUrl = $('form').attr("action");
				//TODO:format form data checkbox,radio
				form.ajaxSubmit({
					//dataType : 'json',
					type : defaults.type,
					contentType : "application/json",
					data : submitData,
					beforeSubmit : defaults.beforeSubmit,
					success: defaults.success,
					complete : defaults.complete,
					error : defaults.error
				})
			},

			submitHandler : function() {
				if (!form.attr('action')) {
					return false
				}
				if ($.isFunction(defaults.beforeTriggerSubmit)) {
					if (defaults.beforeTriggerSubmit(form)) {
						defaults.ajaxForm()
					}
				} else {
					defaults.ajaxForm()
				}
				return false
			}
		}

		defaults = $.extend(defaults, options)

		var form = this

		var submit = $('.btn-submit', form)

		// 窗口打开完成
		$.metadata.setType('attr', 'validate')

		// 增加扩展验证方法
		if (defaults.rules) {
			$.each(defaults.rules, function(index, item) {
				$.validator.addMethod(item.name, item.method,
						item.message)
			})
		}

		var validate = form.validate({
			submitHandler : defaults.submitHandler
		})

		// 重置按钮
		var resetSubmit = function() {
			submit.html(submit.attr('data-html'))
					.removeAttr('disabled')
		}

		// 禁用按钮
		var disabledSubmit = function() {
			submit.attr('data-html', submit.html())
			submit.html(defaults.postingHTML).attr('disabled',
					'disabled')
		}

		form.data('validate', validate)
		return this
	}

	// 親ページを取得する
	$.fn.parentPage = function() {
		var activeTab = parent.$('.J_menuTab.active')
		var doc
		if (activeTab.length > 0) {
			var dataId = activeTab.data('id')
			parent.$('.J_mainContent .J_iframe').each(function() {
				if ($(this).data('id') == dataId) {
					doc = $(this.contentWindow.document)
					return false
				}
			})
		}

		return doc
	}

	$.fn.parentWinPage = function() {
    		var activeTab = parent.$('.layer.active')
    		var doc
    		if (activeTab.length > 0) {
    			var dataId = activeTab.data('id')
    			parent.$('.J_mainContent .J_iframe').each(function() {
    				if ($(this).data('id') == dataId) {
    					doc = $(this.contentWindow.document)
    					return false
    				}
    			})
    		}

    		return doc
    	}
}))

$(document).ready(function() {
	var height = $(document.body).height();
	var btnHeight = $('.buttons').outerHeight() + 10
	var content = $('.wrapper').height(height - btnHeight)
	var title = $('title').text() || '操作'
	var length = parent.$('.layui-layer-title').length
	if(length <= 1){
	    parent.$('.layui-layer-title').text(title);
	} else{
	    var i = length - 1
	    parent.$('.layui-layer-title')[i].innerText = title;
	}

	$('.i-checks').iCheck({
		checkboxClass : 'icheckbox_square-green',
		radioClass : 'iradio_square-green',
	});
	
	var btn = $(':button[data-submit-method], :submit[data-submit-method]');
	btn.click(function() {
		$('form').submit();
	});
			
	var submitMethod = btn.data("submit-method");
	$('form').cjform({type:submitMethod});

	$('button[type="reset"]').click(function() {
		$('form')[0].reset();
		return false;
	});
	
})
