package com.candj.crm.webservice;

import com.candj.crm.dto.UserRoleDto;
import com.candj.crm.service.UserRoleService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ユーザーロール情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/userRoleRest")
@RestController
public class UserRoleRestController {

    @Autowired
    UserRoleService userRoleService;

    /**
     * ユーザーロールを新規追加する。
     * @param userRoleId ID
     * @return 結果
     */
    @RequestMapping(value = "{userRoleId}", method = RequestMethod.GET)
    public UserRoleDto get(@PathVariable Integer userRoleId) {
        //プライマリーキーでユーザーロールを検索する。
        UserRoleDto ret = userRoleService.selectByPrimaryKey(userRoleId);
        return ret;
    }

    /**
     * ユーザーロールを新規追加する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody UserRoleDto userRoleDto, Errors rrrors) {
        //ユーザーロールを新規追加する。
        int ret = userRoleService.insertSelective(userRoleDto);
        return ret;
    }

    /**
     * ユーザーロールを変更する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody UserRoleDto userRoleDto, Errors rrrors) {
        //プライマリーキーでユーザーロールを更新する。
        int ret = userRoleService.updateByPrimaryKeySelective(userRoleDto);
        return ret;
    }

    /**
     * ユーザーロールを新規追加する。
     * @param userRoleId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{userRoleId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer userRoleId) {
        //ユーザーロールを削除する。
        int ret = userRoleService.deleteByPrimaryKey(userRoleId);
        return ret;
    }

    /**
     * ユーザーロール一覧画面を表示する。
     * @param userRoleDto ユーザーロール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<UserRoleDto> list(UserRoleDto userRoleDto, JqgridPageReq pageReq) {
        Page<UserRoleDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーロールを検索する。（連携情報含む）
        List<UserRoleDto> ret = userRoleService.selectByExample(userRoleDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザーロール一覧画面を表示する。
     * @param userRoleDto ユーザーロール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<UserRoleDto> listAll(UserRoleDto userRoleDto, JqgridPageReq pageReq) {
        Page<UserRoleDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーロールを検索する。（連携情報含む）
        List<UserRoleDto> ret = userRoleService.selectAllByExample(userRoleDto);
        return PageUtil.resp(page);
    }
}
