package com.candj.crm.dto;

import javax.validation.constraints.Digits;

import com.candj.crm.mapper.model.Salary;
import com.candj.crm.mapper.model.Work;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUserDto {
    /** ユーザーID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** 年月 */
    private org.joda.time.DateTime month;

    /** 請求詳細 */
    private java.util.List<InvoiceDetailDto> invoiceDetail;

    /** 作業実際 */
    private java.util.List<Work> work;

    /** 給料 */
    private java.util.List<Salary> salaryList;

    /** 給料 */
    private Salary salary;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** ユーザー */
    private com.candj.crm.mapper.model.Company company;

    /** タイプ */
    @Digits(integer=10, fraction=0)
    private Integer userType;

    /** 名前 */
    @Length(max=64)
    private String userName;

    /** 性別 */
    @Length(max=5)
    private String userSex;

    /** 生年月日 */
    private org.joda.time.DateTime birthDate;

    /** 部門 */
    @Length(max=128)
    private String userDepartment;

    /** イーメールアドレス */
    @Length(max=255)
    private String userEmail;

    /** 電話番号 */
    @Length(max=64)
    private String userTelephoneNumber;

    /** 入社日時 */
    private org.joda.time.DateTime employmentDate;

    /** 退社日付 */
    private org.joda.time.DateTime leavingDate;

    /** 在職状態 */
    @Length(max=2)
    private String employmentStatus;

    /** 基本給料 */
    @Digits(integer=10, fraction=0)
    private Integer userBaseSalary;

    /** パスワード */
    @Length(max=64)
    private String password;

    /** ゲスト種類 */
    @Length(max=64)
    private String guestType;

    /** 作業登録マック */
    private boolean register;

    /** ステータス */
    private boolean userDeleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
