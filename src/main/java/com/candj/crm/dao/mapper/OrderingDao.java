package com.candj.crm.dao.mapper;

import com.candj.crm.dto.MonthDto;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.mapper.model.Order;
import com.candj.crm.mapper.model.Ordering;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;

/**
 * 注文情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface OrderingDao {
    /**
     * プライマリーキーで注文を検索する。
     * @param orderId 注文ID
     * @return 結果
     */
    Ordering selectByPrimaryKey(Integer orderId);

    /**
     * プライマリーキーで注文検索する。（連携情報含む）
     * @param orderId 注文ID
     * @return 結果
     */
    List<Ordering> selectAllByPrimaryKey(Integer orderId);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Ordering> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Ordering> selectByMyExample(@Param("example") WhereCondition example);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Ordering> selectByExample(@Param("example") WhereCondition example);

    /**
     * 注文を新規追加する。
     * @param record 注文
     * @return 結果
     */
    int insertSelective(Ordering record);

    /**
     * 注文を新規追加する。
     * @param record 注文
     * @return 結果
     */
    int insert(Ordering record);

    /**
     * プライマリーキーで注文を更新する。
     * @param record 注文
     * @return 結果
     */
    int updateByPrimaryKey(Ordering record);

    /**
     * プライマリーキーで注文を更新する。
     * @param record 注文
     * @return 結果
     */
    int updateByPrimaryKeySelective(Ordering record);

    /**
     * 注文を更新する。
     * @param record 注文
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Ordering record, @Param("example") WhereCondition example);

    /**
     * 注文を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Ordering example);

    /**
     * 注文を削除する。
     * @param orderId 注文ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer orderId);

    /**
     * 見積IDで注文検索する。（連携情報含む）
     * @param offerDetailId 見積ID
     * @return 結果
     */
    Ordering selectAllByOfferDetailId(Integer offerDetailId);

    /**
     * スタッフIDで注文検索する。（連携情報含む）
     * @param userId スタッフID
     * @return 結果
     */
    List<Ordering> selectByStaffId(Integer userId);

    /**
     * 年月で注文検索する。（連携情報含む）
     * @param record 年月
     * @return 結果
     */
    List<Ordering> selectByMonth(MonthDto record);

    /**
     * 条件で注文を検索する。（連携情報含む）)
     * @param record 検索条件
     * @return 結果
     */
    List<Ordering> selectByUserId(MonthDto record);

    /**
     * 見積IDで注文検索する。（連携情報含む）
     * @param projectId 見積 ID
     * @return 結果
     */
    List<Ordering> selectByOfferId(Integer projectId);

    /**
     * プライマリーキーで注文検索する。（連携情報含む）
     * @param orderId 注文ID
     * @return 結果
     */
    List<Ordering> selectAllByInvoiceId(Integer invoiceId);
}
