package com.candj.crm.service;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dao.mapper.*;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.*;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 請求詳細情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class InvoiceDetailService {

    @Autowired
    InvoiceDetailDao invoiceDetailDao;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    InvoiceDao invoiceDao;

    @Autowired
    OrderingDao orderingDao;

    @Autowired
    OfferDetailDao offerDetailDao;

    @Autowired
    WorkDao workDao;

    @Autowired
    Calculate calculate;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで請求詳細を検索する。
     *
     * @param invoiceDetailId ID
     * @return 結果
     */
    public InvoiceDetailDto selectByPrimaryKey(Integer invoiceDetailId) {
        //プライマリーキーで請求詳細を検索する。
        InvoiceDetail ret = invoiceDetailDao.selectByPrimaryKey(invoiceDetailId);
        return DozerHelper.map(mapper, ret, InvoiceDetailDto.class);
    }

    /**
     * プライマリーキーで請求詳細を検索する。（連携情報含む）
     *
     * @param invoiceDetailId ID
     * @return 結果
     */
    public List<InvoiceDetailDto> selectAllByPrimaryKey(Integer invoiceDetailId) {
        //プライマリーキーで請求詳細検索する。（連携情報含む）
        List<InvoiceDetail> ret = invoiceDetailDao.selectAllByPrimaryKey(invoiceDetailId);
        return DozerHelper.mapList(mapper, ret, InvoiceDetailDto.class);
    }

    /**
     * 条件で請求詳細を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<InvoiceDetailDto> selectByExample(InvoiceDetailDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("invoice_detail.invoice_detail_id", dto.getInvoiceDetailId())
                    .andEqualTo("invoice_detail.invoice_id", dto.getInvoiceId())
                    .andLessThanOrEqualTo("invoice_detail.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("invoice_detail.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("invoice_detail.user_id", dto.getUserId())
                    .andEqualTo("invoice_detail.person_month", dto.getPersonMonth())
                    .andEqualTo("invoice_detail.salary_person_month", dto.getSalaryPersonMonth())
                    .andEqualTo("invoice_detail.amount_month", dto.getAmountMonth())
                    .andEqualTo("invoice_detail.over_time_work", dto.getOverTimeWork())
                    .andEqualTo("invoice_detail.over_time", dto.getOverTime())
                    .andEqualTo("invoice_detail.salary_extra", dto.getSalaryExtra())
                    .andEqualTo("invoice_detail.salary_deduction", dto.getSalaryDeduction())
                    .andEqualTo("invoice_detail.amount_extra", dto.getAmountExtra())
                    .andEqualTo("invoice_detail.remark", dto.getRemark())
                    .andEqualTo("invoice_detail.create_user", dto.getCreateUser())
                    .andEqualTo("invoice_detail.update_user", dto.getUpdateUser())
                    .andEqualTo("invoice_detail.create_time", dto.getCreateTime())
                    .andEqualTo("invoice_detail.update_time", dto.getUpdateTime());
        }
        //条件で請求詳細を検索する。（連携情報含む）)
        List<InvoiceDetail> ret = invoiceDetailDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, InvoiceDetailDto.class);
    }

    /**
     * 条件で見積詳細 を検索する。（連携情報含む）
     *
     * @param invoiceId 検索条件
     * @return 結果
     */
    public List<InvoiceDetailDto> selectByInvoiceId(Integer invoiceId) {
        WhereCondition whereCondition = new WhereCondition();
        //条件で見積詳細 を検索する。（連携情報含む）)
        List<InvoiceDetail> ret = invoiceDetailDao.selectByInvoiceId(invoiceId);
        ret.forEach(r -> {
            Invoice invoice = invoiceDao.selectAllByPrimaryKey(invoiceId).get(0);
            r.setInvoice(invoice);
            List<Work> workList = workDao.selectByOrderId(invoice.getOrderId());
            if(workList.isEmpty()){
                r.setWorkHour(BigDecimal.ZERO);
            }
            else {
                for (Work w : workList) {
                    if (w.getUserId() == r.getUserId()) {
                        r.setWorkHour(w.getWorkHour());
                        break;
                    } else {
                        r.setWorkHour(BigDecimal.ZERO);
                    }
                }
            }
            r.setTotalAmount(r.getAmountMonth() + r.getAmountExtra() + r.getCommutingCostAmount());
        });
        return DozerHelper.mapList(mapper, ret, InvoiceDetailDto.class);
    }

    /**
     * 条件で請求詳細を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<InvoiceDetailDto> selectAllByExample(InvoiceDetailDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("invoice_detail.invoice_detail_id", dto.getInvoiceDetailId())
                    .andEqualTo("invoice_detail.invoice_id", dto.getInvoiceId())
                    .andEqualTo("invoice_detail.work_start_date", dto.getWorkStartDate())
                    .andEqualTo("invoice_detail.work_end_date", dto.getWorkEndDate())
                    .andEqualTo("invoice_detail.user_id", dto.getUserId())
                    .andEqualTo("invoice_detail.person_month", dto.getPersonMonth())
                    .andEqualTo("invoice_detail.salary_person_month", dto.getSalaryPersonMonth())
                    .andEqualTo("invoice_detail.amount_month", dto.getAmountMonth())
                    .andEqualTo("invoice_detail.over_time_work", dto.getOverTimeWork())
                    .andEqualTo("invoice_detail.over_time", dto.getOverTime())
                    .andEqualTo("invoice_detail.salary_extra", dto.getSalaryExtra())
                    .andEqualTo("invoice_detail.salary_deduction", dto.getSalaryDeduction())
                    .andEqualTo("invoice_detail.amount_extra", dto.getAmountExtra())
                    .andEqualTo("invoice_detail.remark", dto.getRemark())
                    .andEqualTo("invoice_detail.create_user", dto.getCreateUser())
                    .andEqualTo("invoice_detail.update_user", dto.getUpdateUser())
                    .andEqualTo("invoice_detail.create_time", dto.getCreateTime())
                    .andEqualTo("invoice_detail.update_time", dto.getUpdateTime());
        }
        //条件で請求詳細を検索する。（連携情報含む）)
        List<InvoiceDetail> ret = invoiceDetailDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, InvoiceDetailDto.class);
    }

    /**
     * 請求詳細を新規追加する。
     *
     * @param dto 請求詳細
     * @return 結果
     */
    public int insertSelective(InvoiceDetailDto dto) {
        InvoiceDetail invoiceDetail = mapper.map(dto, InvoiceDetail.class);
        //請求詳細を新規追加する。
        int ret = invoiceDetailDao.insertSelective(invoiceDetail);
        return ret;
    }

    /**
     * 請求詳細を新規追加する。
     *
     * @param dto 請求詳細
     * @return 結果
     */
    public int insert(InvoiceDetailDto dto) {
        InvoiceDetail invoiceDetail = mapper.map(dto, InvoiceDetail.class);
        //請求詳細を新規追加する。
        int ret = invoiceDetailDao.insert(invoiceDetail);
        return ret;
    }

    /**
     * 請求詳細を新規追加する。
     *
     * @param invoiceDto 請求詳細
     * @return 結果
     */
    public int makeInvoice(InvoiceDto invoiceDto) {

        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;

        List<OfferDetail> offerDetails = offerDetailDao.selectByOfferId(invoiceDto.getOrdering().getOfferId());
        if (!offerDetails.isEmpty()) {
            for (OfferDetail o : offerDetails) {
                WorkDurationDto workDurationDto = new WorkDurationDto();
                workDurationDto.setUserId(o.getUserId());
                workDurationDto.setOfferId(o.getOfferId());
                workDurationDto.setStartDate(o.getWorkStartDate());
                workDurationDto.setEndDate(o.getWorkEndDate());
                List<Work> works = workDao.selectWorkByStaffAndMonth(workDurationDto);
                if (!works.isEmpty()) {
                    for (Work work : works) {
                        InvoiceDetail invoiceDetail = new InvoiceDetail();
                        invoiceDetail.setInvoiceId(invoiceDto.getInvoiceId());
                        invoiceDetail.setWorkStartDate(work.getWorkStartDate());
                        invoiceDetail.setWorkEndDate(work.getWorkEndDate());
                        invoiceDetail.setUserId(work.getUserId());
                        invoiceDetail.setSalaryPersonMonth(work.getUnitPrice());
                        invoiceDetail.setPersonMonth(work.getWorkManHour());
                        invoiceDetail.setAmountMonth(calculate.salaryAmountCalculate(work.getUnitPrice(), work.getWorkManHour()));
                        invoiceDetail.setAmountExtra(0);
                        invoiceDetail.setSalaryDeduction(0);
                        invoiceDetail.setSalaryExtra(0);
                        invoiceDetail.setOverTime(BigDecimal.ZERO);
                        //上下精算
                        if (invoiceDto.getOrdering().getOffer().getAfterPaymentPattern().equals("10")) {
                            Double workHour = work.getWorkHour().doubleValue();
                            //工数によって、月作業時間基準を計算する
                            Double workTimeUpperLimit = invoiceDto.getOrdering().getOffer().getWorkTimeUpperLimit()
                                    * work.getWorkManHour().doubleValue();
                            Double workTimeLowerLimit = invoiceDto.getOrdering().getOffer().getWorkTimeLowerLimit()
                                    * work.getWorkManHour().doubleValue();
                            //超過の場合
                            if (workHour > workTimeUpperLimit) {
                                Double overTime = workHour - workTimeUpperLimit;
                                BigDecimal time = new BigDecimal(overTime);
                                invoiceDetail.setOverTime(time);
                                Double extraUnitPrice = work.getUnitPrice() / workTimeUpperLimit;
                                invoiceDetail.setSalaryExtra(extraUnitPrice.intValue());
                                Double amountExtra = extraUnitPrice * overTime;
                                invoiceDetail.setAmountExtra(amountExtra.intValue());
                                invoiceDetail.setSalaryDeduction(0);
                            }
                            //不足の場合
                            else if (workHour < workTimeLowerLimit) {
                                Double lessTime = workHour - workTimeLowerLimit;
                                BigDecimal time = new BigDecimal(lessTime);
                                invoiceDetail.setOverTime(time);
                                Double deductionUnitPrice = work.getUnitPrice() / workTimeLowerLimit;
                                invoiceDetail.setSalaryDeduction(deductionUnitPrice.intValue());
                                Double amountDeduction = deductionUnitPrice * lessTime;
                                invoiceDetail.setAmountExtra(amountDeduction.intValue());
                                invoiceDetail.setSalaryExtra(0);
                            }
                            //時間幅中間割
                        } else if (invoiceDto.getOrdering().getOffer().getAfterPaymentPattern().equals("11")) {
                            Double workHour = work.getWorkHour().doubleValue();
                            //工数によって、月作業時間基準を計算する
                            Double workTimeUpperLimit = invoiceDto.getOrdering().getOffer().getWorkTimeUpperLimit()
                                    * work.getWorkManHour().doubleValue();
                            Double workTimeLowerLimit = invoiceDto.getOrdering().getOffer().getWorkTimeLowerLimit()
                                    * work.getWorkManHour().doubleValue();
                            //超過の場合
                            if (workHour > workTimeUpperLimit) {
                                Double overTime = workHour - workTimeUpperLimit;
                                BigDecimal time = new BigDecimal(overTime);
                                invoiceDetail.setOverTime(time);
                                double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                                Double extraUnitPrice = work.getUnitPrice() / workTimeLimit;
                                invoiceDetail.setSalaryExtra(extraUnitPrice.intValue());
                                Double amountExtra = extraUnitPrice * overTime;
                                invoiceDetail.setAmountExtra(amountExtra.intValue());
                                invoiceDetail.setSalaryDeduction(0);
                            }
                            //不足の場合
                            else if (workHour < workTimeLowerLimit) {
                                Double lessTime = workHour - workTimeLowerLimit;
                                BigDecimal time = new BigDecimal(lessTime);
                                invoiceDetail.setOverTime(time);
                                double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                                Double deductionUnitPrice = work.getUnitPrice() / workTimeLimit;
                                invoiceDetail.setSalaryDeduction(deductionUnitPrice.intValue());
                                Double amountDeduction = deductionUnitPrice * lessTime;
                                invoiceDetail.setAmountExtra(amountDeduction.intValue());
                                invoiceDetail.setSalaryExtra(0);
                            }
                        }
                        //請求詳細を新規追加する。
                        invoiceDetailDao.insertSelective(invoiceDetail);
                        //請求総金額の計算
                        totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetail.getAmountMonth()
                                + invoiceDetail.getAmountExtra();
                    }
                } else {
                    InvoiceDetail invoiceDetail = new InvoiceDetail();
                    invoiceDetail.setInvoiceId(invoiceDto.getInvoiceId());
                    invoiceDetail.setWorkStartDate(o.getWorkStartDate());
                    invoiceDetail.setWorkEndDate(o.getWorkEndDate());
                    invoiceDetail.setUserId(o.getUserId());
                    invoiceDetail.setOverTime(BigDecimal.ZERO);
                    invoiceDetail.setSalaryPersonMonth(0);
                    invoiceDetail.setPersonMonth(BigDecimal.ZERO);
                    invoiceDetail.setAmountMonth(0);
                    invoiceDetail.setSalaryDeduction(0);
                    invoiceDetail.setAmountExtra(0);

                    //請求詳細を新規追加する。
                    invoiceDetailDao.insertSelective(invoiceDetail);
                    //請求総金額の計算
                    totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetail.getAmountMonth() + invoiceDetail.getAmountExtra();
                }
            }
            invoiceDto.setInvoiceAmountWithoutTax(totalAmountWithoutTax);
            //消費税
            TaxDto dto = new TaxDto();
            dto.setTaxLowerLimit(totalAmountWithoutTax);
            dto.setTaxName("01");
            Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
            invoiceDto.setInvoiceConsumptionTax(tax);
            //税込み合計金額
            invoiceDto.setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
            invoiceService.updateByPrimaryKeySelective(invoiceDto);
        }
        int ret = 1;
        return ret;
    }

    public List<InvoiceDetailDto> makeInvoiceDetailList(Ordering ordering, InvoiceDto invoiceDto) {

        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        List<InvoiceDetailDto> invoiceDetailDtoList = new ArrayList<InvoiceDetailDto>();
        Invoice invoice = new Invoice();
        invoice.setOrdering(ordering);

        List<OfferDetail> offerDetails = offerDetailDao.selectByOfferId(ordering.getOfferId());
        int i = 0;
        if (!offerDetails.isEmpty()) {
            for (OfferDetail o : offerDetails) {
                WorkDurationDto workDurationDto = new WorkDurationDto();
                workDurationDto.setUserId(o.getUserId());
                workDurationDto.setOfferId(o.getOfferId());
                workDurationDto.setStartDate(o.getWorkStartDate());
                workDurationDto.setEndDate(o.getWorkEndDate());
                List<Work> works = workDao.selectWorkByStaffAndMonth(workDurationDto);
                if (!works.isEmpty()) {
                    for (Work work : works) {
                        InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
                        invoiceDetailDto.setInvoiceDetailId(i);
                        invoiceDetailDto.setAppUser(o.getAppUser());
                        invoiceDetailDto.setWorkStartDate(work.getWorkStartDate());
                        invoiceDetailDto.setWorkEndDate(work.getWorkEndDate());
                        invoiceDetailDto.setUserId(work.getUserId());
                        invoiceDetailDto.setSalaryPersonMonth(work.getUnitPrice());
                        invoiceDetailDto.setPersonMonth(work.getWorkManHour());
                        invoiceDetailDto.setAmountMonth(calculate.salaryAmountCalculate(work.getUnitPrice(), work.getWorkManHour()));
                        invoiceDetailDto.setAmountExtra(0);
                        invoiceDetailDto.setSalaryDeduction(0);
                        invoiceDetailDto.setSalaryExtra(0);
                        invoiceDetailDto.setOverTime(BigDecimal.ZERO);
                        invoiceDetailDto.setInvoice(invoice);
                        invoiceDetailDto.setWorkHour(work.getWorkHour());
                        //上下精算
                        if (ordering.getOffer().getAfterPaymentPattern().equals("10")) {
                            Double workHour = work.getWorkHour().doubleValue();
                            //工数によって、月作業時間基準を計算する
                            Double workTimeUpperLimit = ordering.getOffer().getWorkTimeUpperLimit()
                                    * work.getWorkManHour().doubleValue();
                            Double workTimeLowerLimit = ordering.getOffer().getWorkTimeLowerLimit()
                                    * work.getWorkManHour().doubleValue();
                            //超過の場合
                            if (workHour > workTimeUpperLimit) {
                                Double overTime = workHour - workTimeUpperLimit;
                                BigDecimal time = new BigDecimal(overTime);
                                invoiceDetailDto.setOverTime(time);
                                Double extraUnitPrice = work.getUnitPrice() / ordering.getOffer().getWorkTimeUpperLimit().doubleValue();
                                invoiceDetailDto.setSalaryExtra(extraUnitPrice.intValue());
                                Double amountExtra = extraUnitPrice * overTime;
                                invoiceDetailDto.setAmountExtra(amountExtra.intValue());
                                invoiceDetailDto.setSalaryDeduction(0);
                            }
                            //不足の場合
                            else if (workHour < workTimeLowerLimit) {
                                Double lessTime = workHour - workTimeLowerLimit;
                                BigDecimal time = new BigDecimal(lessTime);
                                invoiceDetailDto.setOverTime(time);
                                Double deductionUnitPrice = work.getUnitPrice() / ordering.getOffer().getWorkTimeLowerLimit().doubleValue();
                                invoiceDetailDto.setSalaryDeduction(deductionUnitPrice.intValue());
                                Double amountDeduction = deductionUnitPrice * lessTime;
                                invoiceDetailDto.setAmountExtra(amountDeduction.intValue());
                                invoiceDetailDto.setSalaryExtra(0);
                            }
                        }
                        //請求詳細を新規追加する。
                        invoiceDetailDto.setTotalAmount(invoiceDetailDto.getAmountMonth()
                                + invoiceDetailDto.getAmountExtra());
                        invoiceDetailDtoList.add(invoiceDetailDto);
                        //請求総金額の計算
                        totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetailDto.getAmountMonth()
                                + invoiceDetailDto.getAmountExtra();
                    }
                } else {
                    InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
                    invoiceDetailDto.setInvoiceDetailId(i);
                    invoiceDetailDto.setAppUser(o.getAppUser());
                    invoiceDetailDto.setWorkStartDate(o.getWorkStartDate());
                    invoiceDetailDto.setWorkEndDate(o.getWorkEndDate());
                    invoiceDetailDto.setUserId(o.getUserId());
                    invoiceDetailDto.setOverTime(BigDecimal.ZERO);
                    invoiceDetailDto.setSalaryPersonMonth(0);
                    invoiceDetailDto.setPersonMonth(BigDecimal.ZERO);
                    invoiceDetailDto.setAmountMonth(0);
                    invoiceDetailDto.setSalaryDeduction(0);
                    invoiceDetailDto.setAmountExtra(0);
                    invoiceDetailDto.setInvoice(invoice);
                    invoiceDetailDto.setWorkHour(BigDecimal.ZERO);
                    invoiceDetailDto.setTotalAmount(0);

                    //請求詳細を新規追加する。
                    invoiceDetailDtoList.add(invoiceDetailDto);
                    //請求総金額の計算
                    totalAmountWithoutTax = totalAmountWithoutTax + invoiceDetailDto.getAmountMonth()
                            + invoiceDetailDto.getAmountExtra();
                }
                i++;
            }
            invoiceDto.setInvoiceAmountWithoutTax(totalAmountWithoutTax);
            //消費税
            TaxDto dto = new TaxDto();
            dto.setTaxLowerLimit(totalAmountWithoutTax);
            dto.setTaxName("01");
            Integer tax = 0;
            if (invoiceDto.getWorkStartDate() != null) {
                tax = calculate.consumptionTax(totalAmountWithoutTax, dto, invoiceDto.getWorkStartDate());
            }
            invoiceDto.setInvoiceConsumptionTax(tax);
            //税込み合計金額
            invoiceDto.setInvoiceAmountWithTax(totalAmountWithoutTax + tax);
        }
        return invoiceDetailDtoList;
    }

    public List<InvoiceDetail> jsonToInvoiceDetailDtoList(String jsonData, Integer invoiceId) {
        JSONArray jsonArray = JSONArray.fromObject(jsonData);
        JSONArray invoiceDetailArray = new JSONArray();
        List<DateTime> startDate = new ArrayList<DateTime>();
        List<DateTime> endDate = new ArrayList<DateTime>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
            startDate.add(DateTimeUtil.StringToDateTimeDate(jsonObject1.get("workStartDate").toString()));
            endDate.add(DateTimeUtil.StringToDateTimeDate(jsonObject1.get("workEndDate").toString()));
            invoiceDetailArray.add(jsonObject1.discard("invoiceDetailId")
                    .discard("appUser.userName")
                    .discard("invoice.ordering.offer.workTimeLowerLimit")
                    .discard("invoice.ordering.offer.workTimeCalculateStandard")
                    .discard("workStartDate")
                    .discard("workEndDate"));
        }
        List<InvoiceDetail> invoiceDetailDtoList = (List<InvoiceDetail>) JSONArray.toList(jsonArray, InvoiceDetail.class);
        int index = 0;
        for (InvoiceDetail i : invoiceDetailDtoList) {
            i.setWorkStartDate(startDate.get(index));
            i.setWorkEndDate(endDate.get(index));
            index++;
            i.setInvoiceId(invoiceId);
            invoiceDetailDao.insertSelective(i);
        }
        return invoiceDetailDtoList;
    }

    public List<InvoiceDetailDto> addWorkHour(List<InvoiceDetailDto> invoiceDetailDtoList) {

        for (InvoiceDetailDto i : invoiceDetailDtoList) {
            WorkDurationInvoiceIdDto workDurationInvoiceIdDto = new WorkDurationInvoiceIdDto();
            workDurationInvoiceIdDto.setUserId(i.getUserId());
            workDurationInvoiceIdDto.setInvoiceId(i.getInvoiceId());
            workDurationInvoiceIdDto.setStartDate(i.getWorkStartDate());
            workDurationInvoiceIdDto.setEndDate(i.getWorkEndDate());
            List<Work> works = workDao.selectWorkByInvoiceStaffAndMonth(workDurationInvoiceIdDto);
            if (!works.isEmpty()) {
                i.setWorkHour(works.get(0).getWorkHour());
            } else {
                i.setWorkHour(BigDecimal.ZERO);
            }
        }
        return invoiceDetailDtoList;
    }

    /**
     * プライマリーキーで請求詳細を更新する。
     *
     * @param dto 請求詳細
     * @return 結果
     */
    public int updateByPrimaryKey(InvoiceDetailDto dto) {
        InvoiceDetail invoiceDetail = mapper.map(dto, InvoiceDetail.class);
        //プライマリーキーで請求詳細を更新する。
        int ret = invoiceDetailDao.updateByPrimaryKey(invoiceDetail);
        return ret;
    }

    /**
     * プライマリーキーで請求詳細を更新する。
     *
     * @param dto 請求詳細
     * @return 結果
     */
    public int updateByPrimaryKeySelective(InvoiceDetailDto dto) {
        InvoiceDetail invoiceDetail = mapper.map(dto, InvoiceDetail.class);
        //プライマリーキーで請求詳細を更新する。
        int ret = invoiceDetailDao.updateByPrimaryKeySelective(invoiceDetail);
        return ret;
    }

    /**
     * 請求詳細を削除する。
     *
     * @param invoiceDetailId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer invoiceDetailId) {
        //請求詳細を削除する。
        int ret = invoiceDetailDao.deleteByPrimaryKey(invoiceDetailId);
        return ret;
    }
}
