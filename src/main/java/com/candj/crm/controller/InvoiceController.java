package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.InvoiceDetailDto;
import com.candj.crm.dto.InvoiceDto;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.mapper.model.Ordering;
import com.candj.crm.service.*;

import java.util.Date;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 請求情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/invoice")
@Controller
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;

    @Autowired
    InvoiceDetailService invoiceDetailService;

    @Autowired
    InvoiceDetailTemporaryService invoiceDetailTemporaryService;

    @Autowired
    OrderingService orderingService;

    /**
     * 請求を新規追加する。
     * @param invoiceDto 請求
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{orderId}", method = RequestMethod.GET)
    public ModelAndView insert(InvoiceDto invoiceDto, ModelMap model) {
        List<InvoiceDto> invoiceDtos = invoiceService.selectByOrderId(invoiceDto.getOrderId());
        if(invoiceDtos.isEmpty()) {
            List<Ordering> orderings = orderingService.selectOrderByPrimaryKey(invoiceDto.getOrderId());
            invoiceDto.setOrdering(orderings.get(0));
            invoiceDto.setWorkStartDate(orderings.get(0).getOffer().getWorkStartDate());
            invoiceDto.setWorkEndDate(orderings.get(0).getOffer().getWorkEndDate());
            invoiceDto.setInvoiceDate(new DateTime());
            InvoiceDetailDto invoiceDetailDto = new InvoiceDetailDto();
            List<InvoiceDetailDto> invoiceDetailDtoList = invoiceDetailTemporaryService.selectByExample(invoiceDetailDto);
            for(InvoiceDetailDto i : invoiceDetailDtoList){
                invoiceDetailTemporaryService.deleteByPrimaryKey(i.getInvoiceDetailId());
            }
            invoiceDetailTemporaryService.makeInvoiceDetailList(orderings.get(0), invoiceDto);
            model.addAttribute("invoiceDto", invoiceDto);
            return new ModelAndView("invoice/insert", model);
        } else{
            if(!invoiceDtos.get(0).isInvoiceDeleteFlag()){
                List<Ordering> orderings = orderingService.selectOrderByPrimaryKey(invoiceDto.getOrderId());
                invoiceDto.setOrdering(orderings.get(0));
                invoiceDto.setWorkStartDate(orderings.get(0).getOffer().getWorkStartDate());
                invoiceDto.setWorkEndDate(orderings.get(0).getOffer().getWorkEndDate());
                invoiceDto.setInvoiceDate(new DateTime());
                invoiceDetailTemporaryService.makeInvoiceDetailList(orderings.get(0), invoiceDto);
                model.addAttribute("invoiceDto", invoiceDto);
                return new ModelAndView("invoice/insert", model);
            }
            else {
                model.addAttribute("invoiceDto", invoiceDtos.get(0));
                return new ModelAndView("invoice/edit", model);
            }
        }
    }

    /**
     * 請求を新規追加(確認)する。
     * @param invoiceDto 請求
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid InvoiceDto invoiceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoice/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("invoice/insertConfirm", model);
    }

    /**
     * 請求を新規追加(終了)する。
     * @param invoiceDto 請求
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid InvoiceDto invoiceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoice/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //請求を新規追加する。
        int ret = invoiceService.insertSelective(invoiceDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/invoice/insertFinish", model);
    }

    /**
     * 請求を編集する。
     * @param invoiceId 請求ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{invoiceId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer invoiceId, ModelMap model) {
        //プライマリーキーで請求を検索する。
        List<InvoiceDto> invoiceDtos = invoiceService.selectAllByPrimaryKey(invoiceId);
        model.addAttribute("invoiceDto", invoiceDtos.get(0));
        return new ModelAndView("invoice/edit", model);
    }

    /**
     * 請求を編集する。
     * @param invoiceId 請求ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/copy/{invoiceId}", method = RequestMethod.GET)
    public ModelAndView copy(@PathVariable Integer invoiceId, ModelMap model) {
        //プライマリーキーで請求を検索する。
        List<InvoiceDto> invoiceDtos = invoiceService.selectAllByPrimaryKey(invoiceId);
        model.addAttribute("invoiceDto", invoiceDtos.get(0));
        return new ModelAndView("invoice/copy", model);
    }

    /**
     * 請求を編集(確認)する。
     * @param invoiceDto 請求
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid InvoiceDto invoiceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoice/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("invoice/editConfirm", model);
    }

    /**
     * 請求を編集(終了)する。
     * @param invoiceDto 請求
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid InvoiceDto invoiceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoice/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで請求を更新する。
        int ret = invoiceService.updateByPrimaryKeySelective(invoiceDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("invoice/editFinish", model);
    }

    /**
     * 請求一覧画面を表示する。
     * @param invoiceDto 請求
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("invoiceDto") InvoiceDto invoiceDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        String projectName = "";
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("projectName", projectName);


        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0){
            return new ModelAndView("invoice/list", model);
        }else {
            return new ModelAndView("invoiceForStaff/list", model);
        }

    }
}
