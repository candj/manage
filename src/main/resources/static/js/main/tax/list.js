var colNames = [
    "税率ID",
    "税名",
    "税率(%)",
    // "所得区間下限",
    // "所得区間上限",
    "適用年月開始時間",
];
var colModel = [
    {
        name: "taxId",
        index: "taxId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "taxName",
        index: "taxName",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '01') {
                return '消費税';
            } else if (cellvalue == '02') {
                return '個人所得税';
            } else if (cellvalue == '03') {
                return '社会保険料（会社分）';
            } else if (cellvalue == '04') {
                return '社会保険料（個人分）';
            } else if (cellvalue == '05') {
                return '労働保険料（会社分）';
            } else if (cellvalue == '06') {
                return '労働保険料（個人分）';
            }
        },
        align: "left",
    },
    {
        name: "taxRate",
        index: "taxRate",
        width: 100,
        search: false,
        align: "left",
    },
    // {
    //     name: "taxLowerLimit",
    //     index: "taxLowerLimit",
    //     width: 100,
    //     search: false,
    //     formatter: "integer",
    //     formatoptions: {
    //         prefix: '$',
    //         suffix: '円',
    //         thousandsSeparator: ','
    //     },
    //     align: "left",
    // },
    // {
    //     name: "taxUpperLimit",
    //     index: "taxUpperLimit",
    //     width: 100,
    //     search: false,
    //     formatter: "integer",
    //     formatoptions: {
    //         prefix: '$',
    //         suffix: '円',
    //         thousandsSeparator: ','
    //     },
    //     align: "left",
    // },
    {
        name: "taxStartMonth",
        index: "taxStartMonth",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM');
            } else {
                return '';
            }
        },
        align: "left",
    },
];

var postData = {
    yearMonth: $('#yearMonth').val(),
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData);
});	
	