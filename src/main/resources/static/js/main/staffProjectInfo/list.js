var colNames = [
    "スタッフプロジェクト情報ID",
    "スタッフ名前",
    "作業状況",
    "業務名",
    "プロジェクト開始時間",
    "プロジェクト終了時間",
    "備考"
];
var colModel = [
    {
        name: "staffProjectInfoId",
        index: "staffProjectInfoId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "appUser.userName",
        index: "appUser.userName",
        width: 100,
        search: false,
        align: "left",
    },

    {
        name: "staffInProject",
        index: "staffInProject",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == "00") {
                return '仕事探し';
            } else if (cellvalue == "01") {
                return '仕事中';
            } else if (cellvalue == "02") {
                return '仕事確定';
            }
        },
        align: "left",
    },
    {
        name: "projectName",
        index: "projectName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "projectStartDate",
        index: "projectStartDate",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
        name: "projectEndDate",
        index: "projectEndDate",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
        cellattr: addCellAttr
    },
    {
        name: "staffInProjectDateISNear",
        index: "staffInProjectDateISNear",
        width: 300,
        search: false,
        hidden: true,
        align: "left"
    }
];

var postData = {
    yearMonth: $('#yearMonth').val(),
};

function addCellAttr(rowId, val, rawObject, cm, rdata) {
    var value = rawObject.staffInProjectDateISNear;
    if (value === '01') {
        return "style='background-color:red'";
    }
}

var gridComplete = function () {

    // var ids = $("#grid").jqGrid("getDataIDs");
    // var rowDatas = $("#grid").jqGrid("getRowData");
    // for (var ii = 0; ii < rowDatas.length; ii++) {
    //     var rowData = rowDatas[ii];
    //     var staffInProjectDateISNear = rowData.staffInProjectDateISNear;
    //     if (staffInProjectDateISNear === "01") {
    //         $("#" + ids[ii] + " td").css("background-color", "red");
    //         $("#" + ids[ii] + " td").ce
    //     }
    // }

}

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData, true, gridComplete);
});	
	

