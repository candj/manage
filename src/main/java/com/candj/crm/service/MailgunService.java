package com.candj.crm.service;

import com.google.common.base.Charsets;
import com.google.common.io.BaseEncoding;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gmasdata.com
 */
@Component
public class MailgunService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailgunService.class.getName());

    @Value("${mailgun.url}")
    private String endPoint;

    @Value("${mailgun.apiKey}")
    private String appKey;

    @Value("${mailgun.from}")
    private String from;

    @SuppressWarnings("unchecked")
    public void mailTo(String to, String subject, String body) {
        Map<String, Object> params = new HashMap<>();
        params.put("from", from);
        params.put("to", to);
        params.put("subject", subject);
        params.put("html", body);

        try {
            HttpResponse<JsonNode> result = Unirest.post(endPoint + "/messages")
                    .header("Authorization", "Basic " + BaseEncoding.base64().encode(("api:" + appKey).getBytes(Charsets.US_ASCII)))
                    .fields(params)
                    .asJson();
            LOGGER.info(String.format("mail: %s %s %s %s ",to ,subject,body , result.getBody().toString()));
        } catch (UnirestException e) {
            LOGGER.error("Failed to send mail", e);
        }
    }
}
