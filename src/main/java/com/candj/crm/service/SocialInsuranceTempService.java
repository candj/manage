package com.candj.crm.service;

import com.candj.crm.dao.mapper.SocialInsuranceTempDao;
import com.candj.crm.dto.SocialInsuranceViewDto;
import com.candj.crm.mapper.model.SocialInsuranceTemp;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class SocialInsuranceTempService {

    @Autowired
    SocialInsuranceTempDao socialInsurancetempDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで税率、保険料率を検索する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public SocialInsuranceViewDto selectByPrimaryKey(Integer socialInsuranceId) {
        //プライマリーキーで税率、保険料率を検索する。
        SocialInsuranceTemp ret = socialInsurancetempDao.selectByPrimaryKey(socialInsuranceId);
        return DozerHelper.map(mapper, ret, SocialInsuranceViewDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public List<SocialInsuranceViewDto> selectAllByPrimaryKey(Integer socialInsuranceId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<SocialInsuranceTemp> ret = socialInsurancetempDao.selectAllByPrimaryKey(socialInsuranceId);
        return DozerHelper.mapList(mapper, ret, SocialInsuranceViewDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SocialInsuranceViewDto> selectByExample(SocialInsuranceViewDto dto) {
        WhereCondition whereCondition = new WhereCondition();

        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<SocialInsuranceTemp> ret = socialInsurancetempDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, SocialInsuranceViewDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SocialInsuranceViewDto> selectAllByExample(SocialInsuranceViewDto dto) {
        WhereCondition whereCondition = new WhereCondition();

        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<SocialInsuranceTemp> socialInsuranceList = socialInsurancetempDao.selectAllByExample(whereCondition);

        return DozerHelper.mapList(mapper, socialInsuranceList, SocialInsuranceViewDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(SocialInsuranceViewDto dto) {
        SocialInsuranceTemp socialInsurance = mapper.map(dto, SocialInsuranceTemp.class);
        //税率、保険料率を新規追加する。
        int ret = socialInsurancetempDao.insertSelective(socialInsurance);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(SocialInsuranceViewDto dto) {
        SocialInsuranceTemp socialInsurance = mapper.map(dto, SocialInsuranceTemp.class);
        //税率、保険料率を新規追加する。
        int ret = socialInsurancetempDao.insert(socialInsurance);
        return ret;
    }


    /**
     * 税率、保険料率を削除する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer socialInsuranceId) {
        //税率、保険料率を削除する。
        int ret = socialInsurancetempDao.deleteByPrimaryKey(socialInsuranceId);
        return ret;
    }


}
