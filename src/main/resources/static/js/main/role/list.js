

			var colNames = [
				"ロールID",
				"ロールコード",
				"ロール名",
				"ステータス",
			];
			var colModel = [
				{
					name: "roleId",
					index: "roleId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "roleCode",
					index: "roleCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "roleName",
					index: "roleName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "status",
					index: "status",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

