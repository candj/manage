package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.LabourInsurance;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface LabourInsuranceDao {
    /**
     * プライマリーキーで費用を検索する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    LabourInsurance selectByPrimaryKey(Integer labourInsuranceId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param labourInsuranceId ID
     * @return 結果
     */
    List<LabourInsurance> selectAllByPrimaryKey(Integer labourInsuranceId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<LabourInsurance> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<LabourInsurance> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(LabourInsurance record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(LabourInsurance record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(LabourInsurance record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(LabourInsurance record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(LabourInsurance record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") LabourInsurance example);

    /**
     * 費用を削除する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer labourInsuranceId);
}
