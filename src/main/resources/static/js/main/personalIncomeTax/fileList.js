

			var colNames = [
			    "個人所得税ID",
				"適用年月(開始時間)",
				"Excelファイル",
			];
			var colModel = [
				{
					name: "personalIncomeTaxId",
					index: "personalIncomeTaxId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
                {
                    name: "personalIncomeTaxStartMonth",
                    index: "personalIncomeTaxStartMonth",
                    width: 50,
                    search : false,
                    formatter : function(cellvalue, options, rowObject) {
                       if (cellvalue != null) {
                           var date = new Date(cellvalue);
                           return date.Format('yyyy-MM');
                       } else {
                           return '';
                       }
                    },
                    align: "left",
                },
                {
                        name: "personalIncomeTaxExcelName",
                        index: "personalIncomeTaxExcelName",
                        width: 100,
                        search: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue != null && cellvalue != undefined) {
                                var selectHtml = '<a href="/api/personalIncomeTaxRest/download/'
                                    + rowObject.personalIncomeTaxId
                                    + '">'
                                    + cellvalue
                                    + '</a>';
                            } else {
                                var selectHtml = "";
                            }
                            return selectHtml;
                        },
                        align: "left",
                },
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	