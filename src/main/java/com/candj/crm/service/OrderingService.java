package com.candj.crm.service;

import com.candj.crm.dao.mapper.OrderingDao;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.dto.OrderDto;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.mapper.model.*;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 注文情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class OrderingService {
    @Autowired
    OrderingDao orderingDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで注文を検索する。
     *
     * @param orderId 注文ID
     * @return 結果
     */
    public OrderingDto selectByPrimaryKey(Integer orderId) {
        //プライマリーキーで注文を検索する。
        Ordering ret = orderingDao.selectByPrimaryKey(orderId);
        return DozerHelper.map(mapper, ret, OrderingDto.class);
    }

    /**
     * プライマリーキーで注文を検索する。（連携情報含む）
     *
     * @param orderId 注文ID
     * @return 結果
     */
    public List<OrderingDto> selectAllByPrimaryKey(Integer orderId) {
        //プライマリーキーで注文検索する。（連携情報含む）
        List<Ordering> ret = orderingDao.selectAllByPrimaryKey(orderId);
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }

    /**
     * 条件で注文を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<OrderingDto> selectByExample(OrderingDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("ordering.order_id", dto.getOrderId())
                    .andEqualTo("ordering.offer_id", dto.getOfferId())
                    .andEqualTo("send_company_id", dto.getSendCompanyId())
                    .andEqualTo("receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("ordering.order_number", dto.getOrderNumber())
                    .andEqualTo("ordering.order_date", dto.getOrderDate())
                    .andEqualTo("ordering.order_status", dto.getOrderStatus())
                    .andEqualTo("ordering.create_user", dto.getCreateUser())
                    .andEqualTo("ordering.update_user", dto.getUpdateUser())
                    .andEqualTo("ordering.create_time", dto.getCreateTime())
                    .andEqualTo("ordering.update_time", dto.getUpdateTime());
        }
        //条件で注文を検索する。（連携情報含む）)
        List<Ordering> ret = orderingDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }


    /**
     * 条件で注文を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<OrderingDto> selectByMyExample(OrderingDto dto, Integer userId) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("ordering.order_id", dto.getOrderId())
                    .andEqualTo("ordering.offer_id", dto.getOfferId())
                    .andEqualTo("offer.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("offer.receive_company_id", dto.getReceiveCompanyId())
                    .andLike("ordering.order_number", dto.getOrderNumber())
                    .andEqualTo("ordering.order_date", dto.getOrderDate())
                    .andEqualTo("ordering.order_status", dto.getOrderStatus())
                    .andEqualTo("ordering.create_user", dto.getCreateUser())
                    .andEqualTo("ordering.update_user", dto.getUpdateUser())
                    .andEqualTo("ordering.create_time", dto.getCreateTime())
                    .andEqualTo("ordering.update_time", dto.getUpdateTime())
                    .andEqualTo("ordering.order_delete_flag", 1)
                    .andLessThanOrEqualTo("offer.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("offer.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("offer_detail.user_id", userId);
        }
        //条件で注文を検索する。（連携情報含む）)
        List<Ordering> ret = orderingDao.selectByMyExample(whereCondition);
        ret.forEach(o -> {
            if (o.getInvoice().get(0).getInvoiceId() != null) {
                for(InvoiceColumnOnly i : o.getInvoice()) {
                    if(i.isInvoiceDeleteFlag() != false) {
                        o.setInvoiceDto(i);
                        break;
                    }
                }
            }
        });
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }


    /**
     * 条件で注文を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<OrderingDto> selectAllByExample(OrderingDto dto, String projectName) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("ordering.order_id", dto.getOrderId())
                    .andEqualTo("ordering.offer_id", dto.getOfferId())
                    .andEqualTo("offer.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("offer.receive_company_id", dto.getReceiveCompanyId())
                    .andLike("ordering.order_number", dto.getOrderNumber())
                    .andEqualTo("ordering.order_date", dto.getOrderDate())
                    .andEqualTo("ordering.order_status", dto.getOrderStatus())
                    .andEqualTo("ordering.create_user", dto.getCreateUser())
                    .andEqualTo("ordering.update_user", dto.getUpdateUser())
                    .andEqualTo("ordering.create_time", dto.getCreateTime())
                    .andEqualTo("ordering.update_time", dto.getUpdateTime())
                    .andEqualTo("ordering.order_delete_flag", 1)
                    .andLessThanOrEqualTo("offer.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("offer.work_end_date", dto.getWorkStartDate());
        }
        //条件で注文を検索する。（連携情報含む）)
        List<Ordering> ret = orderingDao.selectAllByExample(whereCondition);
        ret.forEach(o -> {
            if (o.getInvoice().get(0).getInvoiceId() != null) {
                for(InvoiceColumnOnly i : o.getInvoice()) {
                    if(i.isInvoiceDeleteFlag() != false) {
                        o.setInvoiceDto(i);
                        break;
                    }
                }
            }
        });
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }

    /**
     * 注文を新規追加する。
     *
     * @param dto 注文
     * @return 結果
     */
    public int insertSelective(OrderingDto dto) {
        Ordering ordering = mapper.map(dto, Ordering.class);
        //注文を新規追加する。
        int ret = orderingDao.insertSelective(ordering);
        return ret;
    }

    /**
     * 注文を新規追加する。
     *
     * @param dto 注文
     * @return 結果
     */
    public int insert(OrderingDto dto) {
        Ordering ordering = mapper.map(dto, Ordering.class);
        //注文を新規追加する。
        int ret = orderingDao.insert(ordering);
        return ret;
    }

    /**
     * プライマリーキーで注文を更新する。
     *
     * @param dto 注文
     * @return 結果
     */
    public int updateByPrimaryKey(OrderingDto dto) {
        Ordering ordering = mapper.map(dto, Ordering.class);
        //プライマリーキーで注文を更新する。
        int ret = orderingDao.updateByPrimaryKey(ordering);
        return ret;
    }

    /**
     * プライマリーキーで注文を更新する。
     *
     * @param dto 注文
     * @return 結果
     */
    public int updateByPrimaryKeySelective(OrderingDto dto) {
        Ordering ordering = mapper.map(dto, Ordering.class);
        //プライマリーキーで注文を更新する。
        int ret = orderingDao.updateByPrimaryKeySelective(ordering);
        return ret;
    }

    /**
     * 注文を削除する。
     *
     * @param orderId 注文ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer orderId) {
        //注文を削除する。
        int ret = orderingDao.deleteByPrimaryKey(orderId);
        return ret;
    }

    /**
     * プライマリーキーで注文を検索する。（連携情報含む）
     *
     * @param orderId 注文ID
     * @return 結果
     */
    public List<Ordering> selectOrderByPrimaryKey(Integer orderId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Ordering> ret = orderingDao.selectAllByPrimaryKey(orderId);
        return ret;
    }

    /**
     * 見積IDで注文を検索する。（連携情報含む）
     *
     * @param offerDetailId 注文ID
     * @return 結果
     */
    public Ordering selectOrderByOfferDetailId(Integer offerDetailId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        Ordering ret = orderingDao.selectAllByOfferDetailId(offerDetailId);
        return ret;
    }

    /**
     * スタッフIDで注文を検索する。（連携情報含む）
     *
     * @param userId 注文ID
     * @return 結果
     */
    public List<Ordering> selectByStaffId(Integer userId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Ordering> ret = orderingDao.selectByStaffId(userId);
        return ret;
    }

    /**
     * 年月で注文を検索する。（連携情報含む）
     *
     * @param monthDto 年月
     * @return 結果
     */
    public List<Ordering> selectByMonth(MonthDto monthDto) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Ordering> ret = orderingDao.selectByMonth(monthDto);
        return ret;
    }

    /**
     * 条件で注文を検索する。（連携情報含む）
     *
     * @param monthDto 検索条件
     * @return 結果
     */
    public List<OrderingDto> selectByUserId(MonthDto monthDto) {
        //条件で注文を検索する。（連携情報含む）)
        List<Ordering> ret = orderingDao.selectByUserId(monthDto);
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }

    /**
     * 見積IDで注文を検索する。
     *
     * @param offerId 見積 ID
     * @return 結果
     */
    public List<OrderingDto> selectByOfferId(Integer offerId) {
        //プライマリーキーで見積を検索する。
        List<Ordering> ret = orderingDao.selectByOfferId(offerId);
        return DozerHelper.mapList(mapper, ret, OrderingDto.class);
    }
}
