package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザーロール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserRoleDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer userRoleId;

    /** ユーザーID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** ユーザーロール */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** ロールID */
    @Digits(integer=10, fraction=0)
    private Integer roleId;

    /** ユーザーロール */
    private com.candj.crm.mapper.model.Role role;

    /** ステータス */
    @Digits(integer=3, fraction=0)
    private Byte userRoleStatus;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
