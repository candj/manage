package com.candj.crm.service;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.dao.mapper.SalaryDao;
import com.candj.crm.dto.*;
import com.candj.crm.jsonUtils.CalcuteStaffSalary;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.mapper.model.Salary;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 給料情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class SalaryService {
    @Autowired
    SalaryDao salaryDao;

    @Autowired
    TaxService taxService;

    @Autowired
    AppUserService appUserService;

    @Autowired
    SocialInsuranceService socialInsuranceService;

    @Autowired
    PersonalIncomeTaxService personalIncomeTaxService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    CompanyService companyService;

    @Autowired
    Calculate calculate;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで給料を検索する。
     *
     * @param salaryId ID
     * @return 結果
     */
    public SalaryDto selectByPrimaryKey(Integer salaryId) {
        //プライマリーキーで給料を検索する。
        Salary ret = salaryDao.selectByPrimaryKey(salaryId);
        return DozerHelper.map(mapper, ret, SalaryDto.class);
    }

    /**
     * プライマリーキーで給料を検索する。（連携情報含む）
     *
     * @param salaryId ID
     * @return 結果
     */
    public List<SalaryDto> selectAllByPrimaryKey(Integer salaryId) {
        //プライマリーキーで給料検索する。（連携情報含む）
        List<Salary> ret = salaryDao.selectAllByPrimaryKey(salaryId);
        return DozerHelper.mapList(mapper, ret, SalaryDto.class);
    }

    /**
     * 条件で給料を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SalaryDto> selectByExample(SalaryDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("salary.salary_id", dto.getSalaryId())
                    .andEqualTo("salary.user_id", dto.getUserId())
                    .andEqualTo("salary.salary_month", dto.getSalaryMonth())
                    .andEqualTo("salary.base_pay", dto.getBasePay())
                    .andEqualTo("salary.overtime_pay", dto.getOvertimePay())
                    .andEqualTo("salary.commuting_cost", dto.getCommutingCost())
                    .andEqualTo("salary.social_insurance_company", dto.getSocialInsuranceCompany())
                    .andEqualTo("salary.social_insurance_personal", dto.getSocialInsurancePersonal())
                    .andEqualTo("salary.housing_subsidies", dto.getHousingSubsidies())
                    .andEqualTo("salary.labour_insurance_company", dto.getLabourInsuranceCompany())
                    .andEqualTo("salary.labour_insurance_personal", dto.getLabourInsurancePersonal())
                    .andEqualTo("salary.bonus", dto.getBonus())
                    .andEqualTo("salary.personal_income_tax", dto.getPersonalIncomeTax())
                    .andEqualTo("salary.salary_amount", dto.getSalaryAmount())
                    .andEqualTo("salary.bring_up_number", dto.getBringUpNumber())
                    .andEqualTo("salary.plan_pay_date", dto.getPlanPayDate())
                    .andEqualTo("salary.pay_date", dto.getPayDate())
                    .andEqualTo("salary.status", dto.getStatus())
                    .andEqualTo("salary.remark", dto.getRemark())
                    .andEqualTo("salary.create_user", dto.getCreateUser())
                    .andEqualTo("salary.update_user", dto.getUpdateUser())
                    .andEqualTo("salary.create_time", dto.getCreateTime())
                    .andEqualTo("salary.update_time", dto.getUpdateTime());
        }
        //条件で給料を検索する。（連携情報含む）)
        List<Salary> ret = salaryDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, SalaryDto.class);
    }

    /**
     * 条件で給料を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SalaryDto> selectAllByExample(SalaryDto dto, String userName) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("salary.salary_id", dto.getSalaryId())
                    .andEqualTo("salary.user_id", dto.getUserId())
                    .andLike("app_user.user_name", userName)
                    .andEqualTo("salary.salary_month", dto.getSalaryMonth())
                    .andEqualTo("salary.base_pay", dto.getBasePay())
                    .andEqualTo("salary.overtime_pay", dto.getOvertimePay())
                    .andEqualTo("salary.commuting_cost", dto.getCommutingCost())
                    .andEqualTo("salary.social_insurance_company", dto.getSocialInsuranceCompany())
                    .andEqualTo("salary.social_insurance_personal", dto.getSocialInsurancePersonal())
                    .andEqualTo("salary.housing_subsidies", dto.getHousingSubsidies())
                    .andEqualTo("salary.labour_insurance_company", dto.getLabourInsuranceCompany())
                    .andEqualTo("salary.labour_insurance_personal", dto.getLabourInsurancePersonal())
                    .andEqualTo("salary.bonus", dto.getBonus())
                    .andEqualTo("salary.personal_income_tax", dto.getPersonalIncomeTax())
                    .andEqualTo("salary.salary_amount", dto.getSalaryAmount())
                    .andEqualTo("salary.bring_up_number", dto.getBringUpNumber())
                    .andEqualTo("salary.plan_pay_date", dto.getPlanPayDate())
                    .andEqualTo("salary.pay_date", dto.getPayDate())
                    .andEqualTo("salary.status", dto.getStatus())
                    .andEqualTo("salary.remark", dto.getRemark())
                    .andEqualTo("salary.create_user", dto.getCreateUser())
                    .andEqualTo("salary.update_user", dto.getUpdateUser())
                    .andEqualTo("salary.create_time", dto.getCreateTime())
                    .andEqualTo("salary.update_time", dto.getUpdateTime());
        }
        //条件で給料を検索する。（連携情報含む）)
        List<Salary> ret = salaryDao.selectAllByExample(whereCondition);
        /*ret.forEach(r -> {

        });*/
        return DozerHelper.mapList(mapper, ret, SalaryDto.class);
    }

    /**
     * 給料を新規追加する。
     *
     * @param dto 給料
     * @return 結果
     */
    public int insertSelective(SalaryDto dto) {
        Salary salary = mapper.map(dto, Salary.class);
        //給料を新規追加する。
        int ret = salaryDao.insertSelective(salary);
        return ret;
    }

    /**
     * 給料を新規追加する。
     *
     * @param dto 給料
     * @return 結果
     */
    public int insert(SalaryDto dto) {
        Salary salary = mapper.map(dto, Salary.class);
        //給料を新規追加する。
        int ret = salaryDao.insert(salary);
        return ret;
    }

    /**
     * プライマリーキーで給料を更新する。
     *
     * @param dto 給料
     * @return 結果
     */
    public int updateByPrimaryKey(SalaryDto dto) {
        Salary salary = mapper.map(dto, Salary.class);
        //プライマリーキーで給料を更新する。
        int ret = salaryDao.updateByPrimaryKey(salary);
        return ret;
    }

    /**
     * プライマリーキーで給料を更新する。
     *
     * @param dto 給料
     * @return 結果
     */
    public int updateByPrimaryKeySelective(SalaryDto dto) {
        Salary salary = mapper.map(dto, Salary.class);
        //プライマリーキーで給料を更新する。
        int ret = salaryDao.updateByPrimaryKeySelective(salary);
        return ret;
    }

    /**
     * 給料を削除する。
     *
     * @param salaryId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer salaryId) {
        //給料を削除する。
        int ret = salaryDao.deleteByPrimaryKey(salaryId);
        return ret;
    }

    /**
     * 給料を新規追加する。
     *
     * @param salaryDto 給料
     * @return 結果
     */
    public SalaryDto makeSalary(List<WorkDto> workDtoList, DateTime yearMonth, StaffSalaryInfoDto staffSalaryInfoDto, SalaryDto salaryDto) {

        int basePay;
        int basePayValue = 0;
        int mOverTime = 0;
        int commutingCost = salaryDto.getCommutingCost();
        if (!workDtoList.isEmpty()) {
            for (WorkDto workData : workDtoList) {
                if ("01".equals(workData.getWorkAdmitStatus())) {
                    OfferDetailForWorkDto offerDetailForWorkDto = new OfferDetailForWorkDto();
                    offerDetailForWorkDto.setOrderId(workData.getOrderId());
                    offerDetailForWorkDto.setUserId(workData.getUserId());
                    offerDetailForWorkDto.setStartOfMonth(yearMonth);
                    DateTime endOfMonth = yearMonth.plusMonths(1).minusSeconds(1);
                    offerDetailForWorkDto.setEndOfMonth(endOfMonth);

                    List<OfferDetail> offerDetails = offerDetailService.selectByStaffAndMonth(offerDetailForWorkDto);
                    OfferDetail offerDetail = new OfferDetail();
                    if (!offerDetails.isEmpty()) {
                        offerDetail = offerDetails.get(0);
                    }
                    if (offerDetail != null && staffSalaryInfoDto != null && workData.getWorkType().equals("00")) {
//                      basePay = calculate.salaryAmountCalculate(workDto.getUnitPrice(), workDto.getWorkManHour());
//                      BigDecimal b = workData.getWorkManHour().multiply(new BigDecimal(staffSalaryInfoDto.getSalaryPay()));
                        basePay = staffSalaryInfoDto.getSalaryPay();
                        if (offerDetail.getOffer().getAfterPaymentPattern().equals("10")) {
                            //上下精算
                            Double manHour = offerDetail.getManHour().doubleValue();
                            Double workHour = workData.getWorkHour().doubleValue();
                            Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit().doubleValue();
                            Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit().doubleValue();
                            Double base = basePay * manHour;
                            //超過の場合
                            if (workHour > workTimeUpperLimit * manHour) {
                                Double overTime = workHour - workTimeUpperLimit * manHour;
                                Double extraUnitPrice = basePay / workTimeUpperLimit;
                                Double amountExtra = extraUnitPrice * overTime;
                                basePayValue = basePayValue + base.intValue();
                                mOverTime = amountExtra.intValue();
                                Integer overtimePayValue = salaryDto.getOvertimePay() == null ? amountExtra.intValue() : salaryDto.getOvertimePay() + amountExtra.intValue();
                                salaryDto.setOvertimePay(overtimePayValue);
                            }
                            //不足の場合
                            else if (workHour < workTimeLowerLimit * manHour) {

                                Double lessTime = workHour - workTimeLowerLimit * manHour;
                                Double deductionUnitPrice = basePay / workTimeLowerLimit;
                                Double amountDeduction = deductionUnitPrice * lessTime;
                                basePayValue = basePayValue + base.intValue() + amountDeduction.intValue();
                            }
                            else{
                                basePayValue = basePayValue + base.intValue();
                            }
                        } else if (offerDetail.getOffer().getAfterPaymentPattern().equals("11")) {
                            //上下精算
                            Double manHour = offerDetail.getManHour().doubleValue();
                            Double workHour = workData.getWorkHour().doubleValue();
                            Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit().doubleValue();
                            Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit().doubleValue();
                            Double base = basePay * manHour;
                            //超過の場合
                            if (workHour > workTimeUpperLimit * manHour) {
                                Double overTime = workHour - workTimeUpperLimit * manHour;
                                double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                                Double extraUnitPrice = basePay / workTimeLimit;
                                Double amountExtra = extraUnitPrice * overTime;
                                basePayValue = basePayValue + base.intValue();
                                mOverTime = amountExtra.intValue();
                                Integer overtimePayValue = salaryDto.getOvertimePay() == null ? amountExtra.intValue() : salaryDto.getOvertimePay() + amountExtra.intValue();
                                salaryDto.setOvertimePay(overtimePayValue);
                            }
                            //不足の場合
                            else if (workHour < workTimeLowerLimit * manHour) {
                                Double lessTime = workHour - workTimeLowerLimit * manHour;
                                double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                                Double deductionUnitPrice = basePay / workTimeLimit;
                                Double amountDeduction = deductionUnitPrice * lessTime;
                                basePayValue = basePayValue + base.intValue() + amountDeduction.intValue();
                            }
                            else{
                                basePayValue = basePayValue + base.intValue();
                            }
                        }
                    } else {
                        basePayValue = 0;
                    }
                }
            }
        } else{
            salaryDto.setBasePay(0);
            salaryDto.setOvertimePay(0);
            salaryDto.setSocialInsurancePersonal(0);
            salaryDto.setSocialInsuranceCompany(0);
            salaryDto.setPersonalIncomeTax(0);
            salaryDto.setLabourInsuranceCompany(0);
            salaryDto.setLabourInsurancePersonal(0);
        }

        //デフォルト値を設置する
        salaryDto.setUserId(salaryDto.getUserId());

        salaryDto.setBasePay(basePayValue);
        salaryDto.setOvertimePay(mOverTime);
        basePayValue = basePayValue + mOverTime + commutingCost;

        //社会保険料
        Integer socialInsuranceCompanyCalculate = 0;
        Integer socialInsurancePersonalCalculate = 0;
        if(staffSalaryInfoDto != null) {
            if (staffSalaryInfoDto.isSocialInsuranceFlag()) {
                SocialInsuranceDto socialInsuranceDto = new SocialInsuranceDto();
                socialInsuranceDto.setSocialInsuranceStartMonth(yearMonth);
                socialInsuranceDto.setSocialInsuranceEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
                String socialInsuranceJsonPath = socialInsuranceService.selectAllByExample(socialInsuranceDto).get(0).getSocialInsuranceJsonPath();

                IntegerChangeDto integerChangeDto = new IntegerChangeDto();
                integerChangeDto.setA(0);
                integerChangeDto.setB(0);
                AppUserDto appUserDto = appUserService.selectByPrimaryKey(staffSalaryInfoDto.getUserId());
                Integer age = 0;
                if(appUserDto.getBirthDate() != null){
                    DateTime now = new DateTime();
                    DateTime birthDate = appUserDto.getBirthDate();
                    age = now.getYear() - birthDate.getYear();
                    if(now.getMonthOfYear() < birthDate.getMonthOfYear()){
                        age--;
                    }else if(now.getMonthOfYear() == birthDate.getMonthOfYear()){
                        if(now.getDayOfMonth() < birthDate.getDayOfMonth()){
                            age--;
                        }
                    }
                }
                CalcuteStaffSalary.socialInsuranceCalculate(socialInsuranceJsonPath, basePayValue, staffSalaryInfoDto.getUserLocation(),
                        integerChangeDto, age);
                socialInsuranceCompanyCalculate = integerChangeDto.getA();
                socialInsurancePersonalCalculate = integerChangeDto.getB();
            }
        }
        //（会社分）
        Integer socialInsuranceCompanyValue = salaryDto.getSocialInsuranceCompany() == null ?
                socialInsuranceCompanyCalculate : socialInsuranceCompanyCalculate + salaryDto.getSocialInsuranceCompany();
        salaryDto.setSocialInsuranceCompany(socialInsuranceCompanyValue);
        //（個人分）
        Integer socialInsurancePersonalValue = salaryDto.getSocialInsurancePersonal() == null ?
                socialInsurancePersonalCalculate : socialInsurancePersonalCalculate + salaryDto.getSocialInsurancePersonal();
        salaryDto.setSocialInsurancePersonal(socialInsurancePersonalValue);

        //労働保険料(会社分)
        Integer labourInsuranceCompanyCalculate = calculate.labourInsuranceAmount(basePayValue, yearMonth, "personal");
        Integer labourInsuranceCompanyValue = salaryDto.getLabourInsuranceCompany() == null ? labourInsuranceCompanyCalculate : labourInsuranceCompanyCalculate + salaryDto.getLabourInsuranceCompany();
        salaryDto.setLabourInsuranceCompany(labourInsuranceCompanyValue);
        //労働保険料(個人分)
        Integer labourInsurancePersonalCalculate = calculate.labourInsuranceAmount(basePayValue, yearMonth, "company");
        Integer labourInsurancePersonalValue = salaryDto.getLabourInsurancePersonal() == null ? labourInsurancePersonalCalculate : labourInsurancePersonalCalculate + salaryDto.getLabourInsurancePersonal();
        salaryDto.setLabourInsurancePersonal(labourInsurancePersonalValue);

        int salaryAmount = basePayValue - (socialInsurancePersonalCalculate + labourInsurancePersonalCalculate);
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmount = salaryAmount - salaryDto.getCommutingCost();
        }

        //個人所得税
        Integer personalIncomeTaxCalculate = 0;
        PersonalIncomeTaxDto personalIncomeTaxDto = new PersonalIncomeTaxDto();
        personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(yearMonth);
        personalIncomeTaxDto.setPersonalIncomeTaxEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
        String personalIncomeTaxJsonPath = personalIncomeTaxService.selectAllByExample(personalIncomeTaxDto).get(0).getPersonalIncomeTaxJsonPath();

        IntegerChangeDto integerChangeDto = new IntegerChangeDto();
        integerChangeDto.setA(0);
        CalcuteStaffSalary.personalIncomeTaxCalculate(personalIncomeTaxJsonPath, salaryAmount, staffSalaryInfoDto.getSalaryBringUpNumber(),
                    integerChangeDto);
        personalIncomeTaxCalculate = integerChangeDto.getA();
        Integer personalIncomeTaxValue = salaryDto.getPersonalIncomeTax() == null ? personalIncomeTaxCalculate : salaryDto.getPersonalIncomeTax() + personalIncomeTaxCalculate;
        salaryDto.setPersonalIncomeTax(personalIncomeTaxValue);


        salaryAmount = salaryAmount - personalIncomeTaxCalculate;
        Integer salaryAmountValue = 0;
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmountValue = salaryDto.getSalaryAmount() == null ? salaryAmount : salaryAmount + salaryDto.getSalaryAmount();
        }
        else{
            salaryAmountValue = salaryAmount;
        }
        if(salaryAmountValue < 0){
            salaryDto.setSalaryAmount(0);
        } else {
            salaryDto.setSalaryAmount(salaryAmountValue);
        }

        CompanyDto companyDto = companyService.selectByUserId(salaryDto.getUserId()).get(0);
        DateTime planDate = yearMonth.plusMonths(1);
        if(companyDto.getSalaryPayTime() == 0){
            salaryDto.setPlanPayDate(planDate.plusMonths(1).minusDays(1));
        } else if(companyDto.getSalaryPayTime() == 1){
            salaryDto.setPlanPayDate(new DateTime(planDate.getYear(), planDate.getMonthOfYear(), 15, 0, 0, 0, 0));
        } else if(companyDto.getSalaryPayTime() == 2){
            salaryDto.setPlanPayDate(new DateTime(planDate.getYear(), planDate.getMonthOfYear(), 25, 0, 0, 0, 0));
        }

        return salaryDto;
    }

    /**
     * 給料を新規追加する。
     *
     * @param salaryDto 給料
     * @return 結果
     */
    public SalaryDto makeSalaryForEdit(List<WorkDto> workDtoList, DateTime yearMonth, StaffSalaryInfoDto staffSalaryInfoDto, SalaryDto salaryDto, Integer basePay) {

        int basePayValue = 0;
        int mOverTime = 0;
        int commutingCost = salaryDto.getCommutingCost();
        if (!workDtoList.isEmpty()) {
            for (WorkDto workData : workDtoList) {
                if ("01".equals(workData.getWorkAdmitStatus())) {
                    OfferDetailForWorkDto offerDetailForWorkDto = new OfferDetailForWorkDto();
                    offerDetailForWorkDto.setOrderId(workData.getOrderId());
                    offerDetailForWorkDto.setUserId(workData.getUserId());
                    offerDetailForWorkDto.setStartOfMonth(yearMonth);
                    DateTime endOfMonth = yearMonth.plusMonths(1).minusSeconds(1);
                    offerDetailForWorkDto.setEndOfMonth(endOfMonth);

                    List<OfferDetail> offerDetails = offerDetailService.selectByStaffAndMonth(offerDetailForWorkDto);
                    OfferDetail offerDetail = new OfferDetail();
                    if (!offerDetails.isEmpty()) {
                        offerDetail = offerDetails.get(0);
                    }
                    if (offerDetail.getOffer().getAfterPaymentPattern().equals("10")) {
                        //上下精算
                        Double manHour = offerDetail.getManHour().doubleValue();
                        Double workHour = workData.getWorkHour().doubleValue();
                        Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit().doubleValue();
                        Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit().doubleValue();
                        Double base = basePay * manHour;
                        //超過の場合
                        if (workHour > workTimeUpperLimit * manHour) {
                            Double overTime = workHour - workTimeUpperLimit * manHour;
                            Double extraUnitPrice = basePay / workTimeUpperLimit;
                            Double amountExtra = extraUnitPrice * overTime;
                            basePayValue = basePayValue + base.intValue();
                            mOverTime = amountExtra.intValue();
                            Integer overtimePayValue = salaryDto.getOvertimePay() == null ? amountExtra.intValue() : salaryDto.getOvertimePay() + amountExtra.intValue();
                            salaryDto.setOvertimePay(overtimePayValue);
                        }
                        //不足の場合
                        else if (workHour < workTimeLowerLimit * manHour) {

                            Double lessTime = workHour - workTimeLowerLimit * manHour;
                            Double deductionUnitPrice = basePay / workTimeLowerLimit;
                            Double amountDeduction = deductionUnitPrice * lessTime;
                            basePayValue = basePayValue + base.intValue() + amountDeduction.intValue();
                        }
                        else{
                            basePayValue = basePayValue + base.intValue();
                        }
                    } else if (offerDetail.getOffer().getAfterPaymentPattern().equals("11")) {
                        //上下精算
                        Double manHour = offerDetail.getManHour().doubleValue();
                        Double workHour = workData.getWorkHour().doubleValue();
                        Double workTimeUpperLimit = offerDetail.getOffer().getWorkTimeUpperLimit().doubleValue();
                        Double workTimeLowerLimit = offerDetail.getOffer().getWorkTimeLowerLimit().doubleValue();
                        Double base = basePay * manHour;
                        //超過の場合
                        if (workHour > workTimeUpperLimit * manHour) {
                            Double overTime = workHour - workTimeUpperLimit * manHour;
                            double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                            Double extraUnitPrice = basePay / workTimeLimit;
                            Double amountExtra = extraUnitPrice * overTime;
                            basePayValue = basePayValue + base.intValue();
                            mOverTime = amountExtra.intValue();
                            Integer overtimePayValue = salaryDto.getOvertimePay() == null ? amountExtra.intValue() : salaryDto.getOvertimePay() + amountExtra.intValue();
                            salaryDto.setOvertimePay(overtimePayValue);
                        }
                        //不足の場合
                        else if (workHour < workTimeLowerLimit * manHour) {
                            Double lessTime = workHour - workTimeLowerLimit * manHour;
                            double workTimeLimit = (workTimeUpperLimit + workTimeLowerLimit) / 2;
                            Double deductionUnitPrice = basePay / workTimeLimit;
                            Double amountDeduction = deductionUnitPrice * lessTime;
                            basePayValue = basePayValue + base.intValue() + amountDeduction.intValue();
                        }
                        else{
                            basePayValue = basePayValue + base.intValue();
                        }
                    }
                }
            }
        } else{
            salaryDto.setBasePay(0);
            salaryDto.setOvertimePay(0);
            salaryDto.setSocialInsurancePersonal(0);
            salaryDto.setSocialInsuranceCompany(0);
            salaryDto.setPersonalIncomeTax(0);
            salaryDto.setLabourInsuranceCompany(0);
            salaryDto.setLabourInsurancePersonal(0);
        }

        //デフォルト値を設置する
        salaryDto.setUserId(salaryDto.getUserId());
        salaryDto.setBasePay(basePayValue);
        salaryDto.setOvertimePay(mOverTime);
        basePayValue = basePayValue + mOverTime + commutingCost;

        //社会保険料
        Integer socialInsuranceCompanyCalculate = 0;
        Integer socialInsurancePersonalCalculate = 0;
        if(staffSalaryInfoDto.isSocialInsuranceFlag()) {
            SocialInsuranceDto socialInsuranceDto = new SocialInsuranceDto();
            socialInsuranceDto.setSocialInsuranceStartMonth(yearMonth);
            socialInsuranceDto.setSocialInsuranceEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
            String socialInsuranceJsonPath = socialInsuranceService.selectAllByExample(socialInsuranceDto).get(0).getSocialInsuranceJsonPath();

            IntegerChangeDto integerChangeDto = new IntegerChangeDto();
            integerChangeDto.setA(0);
            integerChangeDto.setB(0);
            AppUserDto appUserDto = appUserService.selectByPrimaryKey(staffSalaryInfoDto.getUserId());
            Integer age = 0;
            if(appUserDto.getBirthDate() != null){
                DateTime now = new DateTime();
                DateTime birthDate = appUserDto.getBirthDate();
                age = now.getYear() - birthDate.getYear();
                if(now.getMonthOfYear() < birthDate.getMonthOfYear()){
                    age--;
                }else if(now.getMonthOfYear() == birthDate.getMonthOfYear()){
                    if(now.getDayOfMonth() < birthDate.getDayOfMonth()){
                        age--;
                    }
                }
            }
            CalcuteStaffSalary.socialInsuranceCalculate(socialInsuranceJsonPath, basePayValue, staffSalaryInfoDto.getUserLocation(),
                    integerChangeDto, age);
            socialInsuranceCompanyCalculate = integerChangeDto.getA();
            socialInsurancePersonalCalculate = integerChangeDto.getB();
        }
        //（会社分）
        Integer socialInsuranceCompanyValue = salaryDto.getSocialInsuranceCompany() == null ?
                socialInsuranceCompanyCalculate : socialInsuranceCompanyCalculate + salaryDto.getSocialInsuranceCompany();
        salaryDto.setSocialInsuranceCompany(socialInsuranceCompanyValue);
        //（個人分）
        Integer socialInsurancePersonalValue = salaryDto.getSocialInsurancePersonal() == null ?
                socialInsurancePersonalCalculate : socialInsurancePersonalCalculate + salaryDto.getSocialInsurancePersonal();
        salaryDto.setSocialInsurancePersonal(socialInsurancePersonalValue);

        //労働保険料(会社分)
        Integer labourInsuranceCompanyCalculate = calculate.labourInsuranceAmount(basePayValue, yearMonth, "personal");
        Integer labourInsuranceCompanyValue = salaryDto.getLabourInsuranceCompany() == null ? labourInsuranceCompanyCalculate : labourInsuranceCompanyCalculate + salaryDto.getLabourInsuranceCompany();
        salaryDto.setLabourInsuranceCompany(labourInsuranceCompanyValue);
        //労働保険料(個人分)
        Integer labourInsurancePersonalCalculate = calculate.labourInsuranceAmount(basePayValue, yearMonth, "company");
        Integer labourInsurancePersonalValue = salaryDto.getLabourInsurancePersonal() == null ? labourInsurancePersonalCalculate : labourInsurancePersonalCalculate + salaryDto.getLabourInsurancePersonal();
        salaryDto.setLabourInsurancePersonal(labourInsurancePersonalValue);

        int salaryAmount = basePayValue - (socialInsurancePersonalCalculate + labourInsurancePersonalCalculate);
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmount = salaryAmount - salaryDto.getCommutingCost();
        }

        //個人所得税
        Integer personalIncomeTaxCalculate = 0;
        PersonalIncomeTaxDto personalIncomeTaxDto = new PersonalIncomeTaxDto();
        personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(yearMonth);
        personalIncomeTaxDto.setPersonalIncomeTaxEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
        String personalIncomeTaxJsonPath = personalIncomeTaxService.selectAllByExample(personalIncomeTaxDto).get(0).getPersonalIncomeTaxJsonPath();

        IntegerChangeDto integerChangeDto = new IntegerChangeDto();
        integerChangeDto.setA(0);
        CalcuteStaffSalary.personalIncomeTaxCalculate(personalIncomeTaxJsonPath, salaryAmount, staffSalaryInfoDto.getSalaryBringUpNumber(),
                integerChangeDto);
        personalIncomeTaxCalculate = integerChangeDto.getA();
        Integer personalIncomeTaxValue = salaryDto.getPersonalIncomeTax() == null ? personalIncomeTaxCalculate : salaryDto.getPersonalIncomeTax() + personalIncomeTaxCalculate;
        salaryDto.setPersonalIncomeTax(personalIncomeTaxValue);


        salaryAmount = salaryAmount - personalIncomeTaxCalculate;
        Integer salaryAmountValue = 0;
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmountValue = salaryDto.getSalaryAmount() == null ? salaryAmount : salaryAmount + salaryDto.getSalaryAmount();
        }
        else{
            salaryAmountValue = salaryAmount;
        }
        if(salaryAmount < 0){
            salaryDto.setSalaryAmount(0);
        } else {
            salaryDto.setSalaryAmount(salaryAmountValue);
        }

        return salaryDto;
    }

    /**
     * 給料を新規追加する。
     *
     *
     * @return 結果
     */
    public SalaryDto makeSalaryForAmount( DateTime yearMonth, StaffSalaryInfoDto staffSalaryInfoDto,
                                                 SalaryDto salaryDto, Integer basePay, Integer mOverTime,
                                                 Integer bonus, Integer bringUpNumber, Integer housingSubsidies) {

        //デフォルト値を設置する
        salaryDto.setBasePay(basePay);
        salaryDto.setOvertimePay(mOverTime);
        salaryDto.setBonus(bonus);
        salaryDto.setBringUpNumber(bringUpNumber);
        salaryDto.setHousingSubsidies(housingSubsidies);
        basePay = basePay + mOverTime + bonus + housingSubsidies + salaryDto.getCommutingCost();

        //社会保険料
        Integer socialInsuranceCompanyCalculate = 0;
        Integer socialInsurancePersonalCalculate = 0;
        if(staffSalaryInfoDto.isSocialInsuranceFlag()) {
            SocialInsuranceDto socialInsuranceDto = new SocialInsuranceDto();
            socialInsuranceDto.setSocialInsuranceStartMonth(yearMonth);
            socialInsuranceDto.setSocialInsuranceEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
            String socialInsuranceJsonPath = socialInsuranceService.selectAllByExample(socialInsuranceDto).get(0).getSocialInsuranceJsonPath();

            IntegerChangeDto integerChangeDto = new IntegerChangeDto();
            integerChangeDto.setA(0);
            integerChangeDto.setB(0);
            AppUserDto appUserDto = appUserService.selectByPrimaryKey(staffSalaryInfoDto.getUserId());
            Integer age = 0;
            if(appUserDto.getBirthDate() != null){
                DateTime now = new DateTime();
                DateTime birthDate = appUserDto.getBirthDate();
                age = now.getYear() - birthDate.getYear();
                if(now.getMonthOfYear() < birthDate.getMonthOfYear()){
                    age--;
                }else if(now.getMonthOfYear() == birthDate.getMonthOfYear()){
                    if(now.getDayOfMonth() < birthDate.getDayOfMonth()){
                        age--;
                    }
                }
            }
            CalcuteStaffSalary.socialInsuranceCalculate(socialInsuranceJsonPath, basePay, staffSalaryInfoDto.getUserLocation(),
                    integerChangeDto, age);
            socialInsuranceCompanyCalculate = integerChangeDto.getA();
            socialInsurancePersonalCalculate = integerChangeDto.getB();
        }
        //（会社分）
        Integer socialInsuranceCompanyValue = salaryDto.getSocialInsuranceCompany() == null ?
                socialInsuranceCompanyCalculate : socialInsuranceCompanyCalculate + salaryDto.getSocialInsuranceCompany();
        salaryDto.setSocialInsuranceCompany(socialInsuranceCompanyValue);
        //（個人分）
        Integer socialInsurancePersonalValue = salaryDto.getSocialInsurancePersonal() == null ?
                socialInsurancePersonalCalculate : socialInsurancePersonalCalculate + salaryDto.getSocialInsurancePersonal();
        salaryDto.setSocialInsurancePersonal(socialInsurancePersonalValue);

        //労働保険料(会社分)
        Integer labourInsuranceCompanyCalculate = calculate.labourInsuranceAmount(basePay, yearMonth, "personal");
        Integer labourInsuranceCompanyValue = salaryDto.getLabourInsuranceCompany() == null ? labourInsuranceCompanyCalculate : labourInsuranceCompanyCalculate + salaryDto.getLabourInsuranceCompany();
        salaryDto.setLabourInsuranceCompany(labourInsuranceCompanyValue);
        //労働保険料(個人分)
        Integer labourInsurancePersonalCalculate = calculate.labourInsuranceAmount(basePay, yearMonth, "company");
        Integer labourInsurancePersonalValue = salaryDto.getLabourInsurancePersonal() == null ? labourInsurancePersonalCalculate : labourInsurancePersonalCalculate + salaryDto.getLabourInsurancePersonal();
        salaryDto.setLabourInsurancePersonal(labourInsurancePersonalValue);

        int salaryAmount = basePay - (socialInsurancePersonalCalculate + labourInsurancePersonalCalculate);
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmount = salaryAmount - salaryDto.getCommutingCost();
        }

        //個人所得税
        Integer personalIncomeTaxCalculate = 0;
        PersonalIncomeTaxDto personalIncomeTaxDto = new PersonalIncomeTaxDto();
        personalIncomeTaxDto.setPersonalIncomeTaxStartMonth(yearMonth);
        personalIncomeTaxDto.setPersonalIncomeTaxEndMonth(yearMonth.plusMonths(1).minusSeconds(1));
        String personalIncomeTaxJsonPath = personalIncomeTaxService.selectAllByExample(personalIncomeTaxDto).get(0).getPersonalIncomeTaxJsonPath();

        IntegerChangeDto integerChangeDto = new IntegerChangeDto();
        integerChangeDto.setA(0);
        CalcuteStaffSalary.personalIncomeTaxCalculate(personalIncomeTaxJsonPath, salaryAmount, bringUpNumber,
                integerChangeDto);
        personalIncomeTaxCalculate = integerChangeDto.getA();
        Integer personalIncomeTaxValue = salaryDto.getPersonalIncomeTax() == null ? personalIncomeTaxCalculate : salaryDto.getPersonalIncomeTax() + personalIncomeTaxCalculate;
        salaryDto.setPersonalIncomeTax(personalIncomeTaxValue);


        salaryAmount = salaryAmount - personalIncomeTaxCalculate;
        Integer salaryAmountValue = 0;
        if(salaryDto.getCommutingCost() <= 100000){
            salaryAmountValue = salaryDto.getSalaryAmount() == null ? salaryAmount : salaryAmount + salaryDto.getSalaryAmount();
        }
        else{
            salaryAmountValue = salaryAmount;
        }
        if(salaryAmount < 0){
            salaryDto.setSalaryAmount(0);
        } else {
            salaryDto.setSalaryAmount(salaryAmountValue);
        }

        return salaryDto;
    }
}
