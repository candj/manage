

			var colNames = [
				"作業ID",
				"作業種類",
				"注文番号",
				"作業開始期日",
				"作業終了期日",
				"作業時間",
				"ステータス",
			];
			var colModel = [
				{
					name: "workId",
					index: "workId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "workType",
					index: "workType",
					width: 100,
					search : false,
					align: "left",
				},
				{
                    name: "ordering.orderNumber",
                    index: "ordering.orderNumber",
                    width: 100,
                    search : false,
                    align: "left",
                },
                {
                    name: "workStartDate",
                    index: "workStartDate",
                    width: 100,
                    search : false,
                    formatter : function(cellvalue, options, rowObject) {
                      if (cellvalue != null) {
                        var date = new Date(cellvalue);
                        return date.Format('yyyy-MM-dd');
                      } else {
                        return '';
                      }
                    },
                    align: "left",
                },
                {
                	name: "workEndDate",
                	index: "workEndDate",
                	width: 100,
                	search : false,
                	formatter : function(cellvalue, options, rowObject) {
                      if (cellvalue != null) {
                      var date = new Date(cellvalue);
                        return date.Format('yyyy-MM-dd');
                      } else {
                        return '';
                      }
                    },
                	align: "left",
                },
				{
                	name: "workHour",
                	index: "workHour",
                	width: 100,
                	search : false,
                	align: "left",
                },
				{
					name: "workStatus",
					index: "workStatus",
					width: 100,
					search : false,
					hidden : true,
					align: "right",
				},
			];



$(document).ready(function(){

    var parent = $.fn.parentPage();
    var yearMonth = parent.find('#yearMonth').val();
    $('#yearMonth').val(yearMonth);

    var postData = {
    	userId : $('#userId').val(),
    	yearMonth : $('#yearMonth').val(),
    };

	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	