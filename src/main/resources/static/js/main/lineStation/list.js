

			var colNames = [
				"线路上的车站ID",
				"线路ID",
				"车站ID",
				"车站顺序",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "lineStationId",
					index: "lineStationId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "lineStationLineId",
					index: "lineStationLineId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "lineStationStationId",
					index: "lineStationStationId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "stationOrder",
					index: "stationOrder",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	