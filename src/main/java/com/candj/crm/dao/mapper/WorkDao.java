package com.candj.crm.dao.mapper;

import com.candj.crm.dto.WorkDurationDto;
import com.candj.crm.dto.WorkDurationInvoiceIdDto;
import com.candj.crm.mapper.model.Work;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 作業情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface WorkDao {
    /**
     * プライマリーキーで作業を検索する。
     * @param workId 作業ID
     * @return 結果
     */
    Work selectByPrimaryKey(Integer workId);

    /**
     * プライマリーキーで作業検索する。（連携情報含む）
     * @param workId 作業ID
     * @return 結果
     */
    List<Work> selectAllByPrimaryKey(Integer workId);

    /**
     * 条件で作業を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Work> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で作業を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Work> selectByExample(@Param("example") WhereCondition example);

    /**
     * 作業を新規追加する。
     * @param record 作業
     * @return 結果
     */
    int insertSelective(Work record);

    /**
     * 作業を新規追加する。
     * @param record 作業
     * @return 結果
     */
    int insert(Work record);

    /**
     * プライマリーキーで作業を更新する。
     * @param record 作業
     * @return 結果
     */
    int updateByPrimaryKey(Work record);

    /**
     * プライマリーキーで作業を更新する。
     * @param record 作業
     * @return 結果
     */
    int updateByPrimaryKeySelective(Work record);

    /**
     * 作業を更新する。
     * @param record 作業
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Work record, @Param("example") WhereCondition example);

    /**
     * 作業を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Work example);

    /**
     * 作業を削除する。
     * @param workId 作業ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer workId);

    /**
     * 条件で作業を検索する。（連携情報含む）)
     * @param record 検索条件
     * @return 結果
     */
    List<Work> selectWorkByStaffAndMonth(WorkDurationDto record);

    /**
     * 条件で作業を検索する。（連携情報含む）)
     * @param record 検索条件
     * @return 結果
     */
    List<Work> selectWorkByInvoiceStaffAndMonth(WorkDurationInvoiceIdDto record);

    /**
     * プライマリーキーで作業検索する。（連携情報含む）
     * @param workId 作業ID
     * @return 結果
     */
    List<Work> selectByOrderId(Integer workId);
}
