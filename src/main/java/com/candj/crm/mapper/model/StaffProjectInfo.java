package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフプロジェクト情報
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffProjectInfo {
    /** ID */
    private Integer staffProjectInfoId;

    /** ユーザー */
    private AppUser appUser;

    /** 担当者ID */
    private Integer userId;

    /** offerID */
    private Integer offerId;

    /** プロジェクト名 */
    private String projectName;

    /** プロジェクトにあり */
    private String staffInProject;

    private String staffInProjectDateISNear;


    /** 開始期日 */
    private org.joda.time.DateTime projectStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime projectEndDate;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
