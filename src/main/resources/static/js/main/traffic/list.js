

			var colNames = [
				"交通ID",
				"土地ID",
				"交通（线路，车站）",
				"步行时间",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "trafficId",
					index: "trafficId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "trafficLandId",
					index: "trafficLandId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "trafficLineStationId",
					index: "trafficLineStationId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "walkTime",
					index: "walkTime",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	