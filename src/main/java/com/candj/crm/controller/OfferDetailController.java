package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.InvoiceDetailDto;
import com.candj.crm.dto.OfferDetailDto;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.service.*;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 見積詳細 情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "offerDetail")
@Controller
public class OfferDetailController {
    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    OfferService offerService;

    @Autowired
    OfferTemporaryService offerTemporaryService;

    @Autowired
    OfferDetailTemporaryService offerDetailTemporaryService;

    @Autowired
    AppUserService appUserService;

    /**
     * 見積詳細 を新規追加する。
     * @param offerDetailDto 見積詳細 
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{offerId}", method = RequestMethod.GET)
    public ModelAndView insert(OfferDetailDto offerDetailDto, ModelMap model) {
        OfferDto offerDto = offerService.selectByPrimaryKey(offerDetailDto.getOfferId());
        offerDetailDto.setWorkName(offerDto.getWorkName());
        offerDetailDto.setWorkStartDate(offerDto.getWorkStartDate());
        offerDetailDto.setWorkEndDate(offerDto.getWorkEndDate());
        return new ModelAndView("offerDetail/insert", model);
    }

    /**
     * 見積詳細 を新規追加(確認)する。
     * @param offerDetailDto 見積詳細 
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid OfferDetailDto offerDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offerDetail/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("offerDetail/insertConfirm", model);
    }

    /**
     * 見積詳細 を新規追加(終了)する。
     * @param offerDetailDto 見積詳細 
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid OfferDetailDto offerDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offerDetail/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //見積詳細 を新規追加する。
        int ret = offerDetailService.insertSelective(offerDetailDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/offerDetail/insertFinish", model);
    }

    /**
     * 見積詳細 を編集する。
     * @param offerDetailId 
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{offerDetailId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer offerDetailId, ModelMap model) {
        //プライマリーキーで見積詳細 を検索する。
        OfferDetailDto offerDetailDto = offerDetailService.selectAllByPrimaryKey(offerDetailId).get(0);
        model.addAttribute("offerDetailDto", offerDetailDto);
        return new ModelAndView("offerDetail/edit", model);
    }

    /**
     * 見積詳細 を編集(確認)する。
     * @param offerDetailDto 見積詳細 
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid OfferDetailDto offerDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offerDetail/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("offerDetail/editConfirm", model);
    }

    /**
     * 見積詳細 を編集(終了)する。
     * @param offerDetailDto 見積詳細 
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid OfferDetailDto offerDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/offerDetail/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで見積詳細 を更新する。
        int ret = offerDetailService.updateByPrimaryKeySelective(offerDetailDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("offerDetail/editFinish", model);
    }

    /**
     * 見積詳細 一覧画面を表示する。
     * @param offerDetailDto 見積詳細 
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list/{offerId}", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("offerDetailDto") OfferDetailDto offerDetailDto, ModelMap model) {
        return new ModelAndView("offerDetail/list", model);
    }

    /**
     * 請求詳細を編集する。
     * @param offerDetailDto 見積詳細
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/addForInsert", method = RequestMethod.GET)
    public ModelAndView addForInsert(OfferDetailDto offerDetailDto,
                                      ModelMap model) {
        DateTime now = new DateTime();
        OfferDto o = new OfferDto();
        OfferDto offerDto = offerTemporaryService.selectByExample(o).get(0);
        DateTime startOfMonth = new DateTime(now.getYear(), now.getMonthOfYear(), 1, 0, 0, 0, 0);
        if(offerDto.getWorkStartDate() != null){
            offerDetailDto.setWorkStartDate(offerDto.getWorkStartDate());
        } else {
            offerDetailDto.setWorkStartDate(startOfMonth);
        }
        if(offerDto.getWorkEndDate() != null){
            offerDetailDto.setWorkEndDate(offerDto.getWorkEndDate());
        } else {
            offerDetailDto.setWorkEndDate(startOfMonth.plusMonths(1).minusSeconds(1));
        }
        if(offerDto.getWorkName() != null){
            offerDetailDto.setWorkName(offerDto.getWorkName());
        } else {
            offerDetailDto.setWorkName("システム開発支援");
        }
        return new ModelAndView("offerDetail/addForInsert", model);
    }

    /**
     * 見積詳細を編集する。
     * @param offerDetailId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/editForInsert/{offerDetailId}", method = RequestMethod.GET)
    public ModelAndView editForInsert(@PathVariable Integer offerDetailId, ModelMap model) {
        //プライマリーキーで請求詳細を検索する。
        OfferDetailDto offerDetailDto = offerDetailTemporaryService.selectByPrimaryKey(offerDetailId);
        AppUserDto appUserDto = appUserService.selectByPrimaryKey(offerDetailDto.getUserId());
        offerDetailDto.setAppUserDto(appUserDto);
        model.addAttribute("offerDetailDto", offerDetailDto);
        return new ModelAndView("offerDetail/editForInsert", model);
    }

}
