package com.candj.crm.webservice;

import com.candj.crm.dto.UserDto;
import com.candj.crm.service.UserService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ユーザー情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/userRest")
@RestController
public class UserRestController {
    @Autowired
    UserService userService;

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public UserDto get(@PathVariable Integer userId) {
        //プライマリーキーでユーザーを検索する。
        UserDto ret = userService.selectByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody UserDto userDto, Errors rrrors) {
        //ユーザーを新規追加する。
        int ret = userService.insertSelective(userDto);
        return ret;
    }

    /**
     * ユーザーを変更する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody UserDto userDto, Errors rrrors) {
        //プライマリーキーでユーザーを更新する。
        int ret = userService.updateByPrimaryKeySelective(userDto);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer userId) {
        //ユーザーを削除する。
        int ret = userService.deleteByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param userDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<UserDto> list(UserDto userDto, JqgridPageReq pageReq) {
        Page<UserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        List<UserDto> ret = userService.selectByExample(userDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param userDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<UserDto> listAll(UserDto userDto, JqgridPageReq pageReq) {
        Page<UserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        List<UserDto> ret = userService.selectAllByExample(userDto);
        return PageUtil.resp(page);
    }
}
