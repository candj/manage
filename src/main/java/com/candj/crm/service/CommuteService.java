package com.candj.crm.service;

import com.candj.crm.dao.mapper.CommuteDao;
import com.candj.crm.dto.CommuteDto;
import com.candj.crm.mapper.model.Commute;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 交通費情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class CommuteService {
    @Autowired
    CommuteDao commuteDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで交通費を検索する。
     * @param commuteId 交通費ID
     * @return 結果
     */
    public CommuteDto selectByPrimaryKey(Integer commuteId) {
        //プライマリーキーで交通費を検索する。
        Commute ret = commuteDao.selectByPrimaryKey(commuteId);
        return DozerHelper.map(mapper, ret, CommuteDto.class);
    }

    /**
     * プライマリーキーで交通費を検索する。（連携情報含む）
     * @param commuteId 交通費ID
     * @return 結果
     */
    public List<CommuteDto> selectAllByPrimaryKey(Integer commuteId) {
        //プライマリーキーで交通費検索する。（連携情報含む）
        List<Commute> ret = commuteDao.selectAllByPrimaryKey(commuteId);
        return DozerHelper.mapList(mapper, ret, CommuteDto.class);
    }

    /**
     * 条件で交通費を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CommuteDto> selectByExample(CommuteDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("commute.commute_id", dto.getCommuteId())
            .andEqualTo("commute.user_id", dto.getUserId())
            .andEqualTo("commute.commute_apply_month", dto.getCommuteApplyMonth())
            .andEqualTo("commute.commute_date", dto.getCommuteDate())
            .andEqualTo("commute.commute_get_on_station", dto.getCommuteGetOnStation())
            .andEqualTo("commute.commute_get_off_station", dto.getCommuteGetOffStation())
            .andEqualTo("commute.commute_amount", dto.getCommuteAmount())
            .andEqualTo("commute.commute_remark", dto.getCommuteRemark())
            .andEqualTo("commute.commute_attachment_path", dto.getCommuteAttachmentPath())
            .andEqualTo("commute.commute_admit_status", dto.getCommuteAdmitStatus())
            .andEqualTo("commute.commute_status", dto.getCommuteStatus())
            .andEqualTo("commute.create_user", dto.getCreateUser())
            .andEqualTo("commute.update_user", dto.getUpdateUser())
            .andEqualTo("commute.create_time", dto.getCreateTime())
            .andEqualTo("commute.update_time", dto.getUpdateTime());
        }
        //条件で交通費を検索する。（連携情報含む）)
        List<Commute> ret = commuteDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CommuteDto.class);
    }

    /**
     * 条件で交通費を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CommuteDto> selectByCommuteDto(CommuteDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("commute.user_id", dto.getUserId())
                    .andGreaterThanOrEqualTo("commute.commute_date", dto.getCommuteDate());
        }
        //条件で交通費を検索する。（連携情報含む）)
        List<Commute> ret = commuteDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CommuteDto.class);
    }

    /**
     * 条件で交通費を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CommuteDto> selectAllByExample(CommuteDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andLike("app_user.user_name", dto.getAppUser().getUserName())
            .andEqualTo("commute.commute_id", dto.getCommuteId())
            .andEqualTo("commute.user_id", dto.getUserId())
            .andEqualTo("commute.commute_apply_month", dto.getCommuteApplyMonth())
            .andEqualTo("commute.commute_date", dto.getCommuteDate())
            .andEqualTo("commute.commute_get_on_station", dto.getCommuteGetOnStation())
            .andEqualTo("commute.commute_get_off_station", dto.getCommuteGetOffStation())
            .andEqualTo("commute.commute_amount", dto.getCommuteAmount())
            .andEqualTo("commute.commute_remark", dto.getCommuteRemark())
            .andEqualTo("commute.commute_attachment_path", dto.getCommuteAttachmentPath())
            .andEqualTo("commute.commute_admit_status", dto.getCommuteAdmitStatus())
            .andEqualTo("commute.commute_status", dto.getCommuteStatus())
            .andEqualTo("commute.create_user", dto.getCreateUser())
            .andEqualTo("commute.update_user", dto.getUpdateUser())
            .andEqualTo("commute.create_time", dto.getCreateTime())
            .andEqualTo("commute.update_time", dto.getUpdateTime());
        }
        //条件で交通費を検索する。（連携情報含む）)
        List<Commute> ret = commuteDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CommuteDto.class);
    }

    /**
     * 交通費を新規追加する。
     * @param dto 交通費
     * @return 結果
     */
    public int insertSelective(CommuteDto dto) {
        Commute commute  =  mapper.map(dto, Commute.class);
        //交通費を新規追加する。
        int ret = commuteDao.insertSelective(commute);
        return ret;
    }

    /**
     * 交通費を新規追加する。
     * @param dto 交通費
     * @return 結果
     */
    public int insert(CommuteDto dto) {
        Commute commute  =  mapper.map(dto, Commute.class);
        //交通費を新規追加する。
        int ret = commuteDao.insert(commute);
        return ret;
    }

    /**
     * プライマリーキーで交通費を更新する。
     * @param dto 交通費
     * @return 結果
     */
    public int updateByPrimaryKey(CommuteDto dto) {
        Commute commute  =  mapper.map(dto, Commute.class);
        //プライマリーキーで交通費を更新する。
        int ret = commuteDao.updateByPrimaryKey(commute);
        return ret;
    }

    /**
     * プライマリーキーで交通費を更新する。
     * @param dto 交通費
     * @return 結果
     */
    public int updateByPrimaryKeySelective(CommuteDto dto) {
        Commute commute  =  mapper.map(dto, Commute.class);
        //プライマリーキーで交通費を更新する。
        int ret = commuteDao.updateByPrimaryKeySelective(commute);
        return ret;
    }

    /**
     * 交通費を削除する。
     * @param commuteId 交通費ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer commuteId) {
        //交通費を削除する。
        int ret = commuteDao.deleteByPrimaryKey(commuteId);
        return ret;
    }
}
