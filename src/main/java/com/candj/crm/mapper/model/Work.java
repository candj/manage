package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

/**
 * 作業
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Work {
    /** 作業ID */
    private Integer workId;

    /** スタッフ */
    private AppUser appUser;

    /** スタッフID */
    private Integer userId;

    /** 種類 */
    private String workType;

    /** 注文 */
    private Ordering ordering;

    /** 注文ID */
    private Integer orderId;

    /** 年月 */
    private org.joda.time.DateTime workMonth;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 単価 */
    private Integer unitPrice;

    /** 工数 */
    private java.math.BigDecimal workManHour;

    /** 作業時間 */
    private java.math.BigDecimal workHour;

    /** 勤務表パース */
    private String workAttachmentPath;

    /** 勤務表パース */
    private String workAttachmentName;

    /** 処理状態 */
    private String workAdmitStatus;

    /** ステータス */
    private Byte workStatus;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
