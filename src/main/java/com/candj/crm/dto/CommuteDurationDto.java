package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommuteDurationDto {

    private Integer userId;

    /** 開始時間 */
    private org.joda.time.DateTime startDate;

    /** 終了時間 */
    private org.joda.time.DateTime EndDate;

}
