

			var colNames = [
				"ID",
				"業務名",
				"業務名「中国語」",
				"業務コード",
				"暗証化コード",
				"ブランド",
				"業務区別",
				"マーク",
				"業務モード",
				"業務区分ID",
			];
			var colModel = [
				{
					name: "businessId",
					index: "businessId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "businessName",
					index: "businessName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessNameCn",
					index: "businessNameCn",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessCode",
					index: "businessCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessEncryptCode",
					index: "businessEncryptCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "area",
					index: "area",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "process",
					index: "process",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "mark",
					index: "mark",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "chargeType",
					index: "chargeType",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "businessTypeId",
					index: "businessTypeId",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

