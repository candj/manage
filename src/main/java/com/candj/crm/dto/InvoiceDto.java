package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 請求
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class InvoiceDto {
    /** 請求ID */
    @Digits(integer=10, fraction=0)
    private Integer invoiceId;

    /** 請求詳細 */
    private java.util.List<InvoiceDetailDto> invoiceDetail;

    /** 注文ID */
    @Digits(integer=10, fraction=0)
    private Integer orderId;

    /** 請求番号 */
    @Length(max=128)
    private String invoiceNumber;

    /** 請求 */
    private com.candj.crm.mapper.model.Ordering ordering;

    /** 日付 */
    private org.joda.time.DateTime invoiceDate;

    private String sendCompanyName;

    private String receiveCompanyName;

    /** 元会社ID */
    @Digits(integer=10, fraction=0)
    private Integer sendCompanyId;

    /** 元会社 */
    private com.candj.crm.mapper.model.Company sendCompany;

    /** 先会社ID */
    @Digits(integer=10, fraction=0)
    private Integer receiveCompanyId;

    /** 先会社 */
    private com.candj.crm.mapper.model.Company receiveCompany;

    /** 開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 税抜き合計金額 */
    @Digits(integer=10, fraction=0)
    private Integer invoiceAmountWithoutTax;

    /** 消費税 */
    @Digits(integer=10, fraction=0)
    private Integer invoiceConsumptionTax;

    /** 税込み合計金額 */
    @Digits(integer=10, fraction=0)
    private Integer invoiceAmountWithTax;

    /** 納期 */
    private org.joda.time.DateTime invoiceDeliveryDate;

    /** 納期 */
    private String deliveryDate;

    /** ステータス */
    @Length(max=2)
    private String invoiceStatus;

    /** 支払状態 */
    @Length(max=2)
    private String invoicePayStatus;

    /** 削除FLAG */
    private boolean invoiceDeleteFlag;

    /** 交通費 */
    @Digits(integer=10, fraction=0)
    private Integer invoiceCommutingCost;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
