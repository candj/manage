package com.candj.crm.controller;


import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.ProjectDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.ProjectService;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * プロジェクト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/project")
@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;


    /**
     * プロジェクトを新規追加する。
     *
     * @param projectDto プロジェクト
     * @param model      モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(ProjectDto projectDto, ModelMap model) {
        return new ModelAndView("project/insert", model);
    }

    /**
     * プロジェクトを新規追加(確認)する。
     *
     * @param projectDto    プロジェクト
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid ProjectDto projectDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/project/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("project/insertConfirm", model);
    }

    /**
     * プロジェクトを新規追加(終了)する。
     *
     * @param projectDto    プロジェクト
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid ProjectDto projectDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/project/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プロジェクトを新規追加する。
        int ret = projectService.insertSelective(projectDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/project/insertFinish", model);
    }

    /**
     * プロジェクトを編集する。
     *
     * @param projectId プロジェクトID
     * @param model     モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{projectId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer projectId, ModelMap model) {
        //プライマリーキーでプロジェクトを検索する。
        ProjectDto projectDto = projectService.selectByPrimaryKey(projectId);
        model.addAttribute("projectDto", projectDto);
        return new ModelAndView("project/edit", model);
    }

    /**
     * プロジェクトを編集(確認)する。
     *
     * @param projectDto    プロジェクト
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid ProjectDto projectDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/project/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("project/editConfirm", model);
    }

    /**
     * プロジェクトを編集(終了)する。
     *
     * @param projectDto    プロジェクト
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid ProjectDto projectDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/project/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでプロジェクトを更新する。
        int ret = projectService.updateByPrimaryKeySelective(projectDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("project/editFinish", model);
    }

    /**
     * プロジェクト一覧画面を表示する。
     *
     * @param projectDto プロジェクト
     * @param model      モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("projectDto") ProjectDto projectDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0) {
            return new ModelAndView("project/list", model);
        } else {
            return new ModelAndView("projectForStaff/list", model);
        }
    }

    /**
     * プロジェクト一覧画面を表示する。
     *
     * @param projectDto プロジェクト
     * @param model      モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/offerList/{projectId}", method = RequestMethod.GET)
    public ModelAndView offerList(ProjectDto projectDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        projectDto = projectService.selectByPrimaryKey(projectDto.getProjectId());
        model.addAttribute("projectDto", projectDto);
        return new ModelAndView("project/offerList", model);

    }
}
