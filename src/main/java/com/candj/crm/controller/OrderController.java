package com.candj.crm.controller;

import com.candj.crm.dto.OrderDto;
import com.candj.crm.service.OrderService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 注文情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/order")
@Controller
public class OrderController {
    @Autowired
    OrderService orderService;

    /**
     * 注文を新規追加する。
     * @param orderDto 注文
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(OrderDto orderDto, ModelMap model) {
        return new ModelAndView("order/insert", model);
    }

    /**
     * 注文を新規追加(確認)する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid OrderDto orderDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/order/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("order/insertConfirm", model);
    }

    /**
     * 注文を新規追加(終了)する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid OrderDto orderDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/order/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //注文を新規追加する。
        int ret = orderService.insertSelective(orderDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/order/insertFinish", model);
    }

    /**
     * 注文を編集する。
     * @param orderId 注文ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{orderId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer orderId, ModelMap model) {
        //プライマリーキーで注文を検索する。
        OrderDto orderDto = orderService.selectByPrimaryKey(orderId);
        model.addAttribute("orderDto", orderDto);
        return new ModelAndView("order/edit", model);
    }

    /**
     * 注文を編集(確認)する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid OrderDto orderDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/order/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("order/editConfirm", model);
    }

    /**
     * 注文を編集(終了)する。
     * @param orderDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid OrderDto orderDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/order/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで注文を更新する。
        int ret = orderService.updateByPrimaryKeySelective(orderDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("order/editFinish", model);
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderDto 注文
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("orderDto") OrderDto orderDto, ModelMap model) {
        return new ModelAndView("order/list", model);
    }
}
