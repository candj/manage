package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.PersonalIncomeTempTax;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PersonalIncomeTaxTempDao {

    /**
     * プライマリーキーで費用を検索する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    PersonalIncomeTempTax selectByPrimaryKey(Integer personalIncomeTaxId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    List<PersonalIncomeTempTax> selectAllByPrimaryKey(Integer personalIncomeTaxId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PersonalIncomeTempTax> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PersonalIncomeTempTax> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(PersonalIncomeTempTax record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(PersonalIncomeTempTax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(PersonalIncomeTempTax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(PersonalIncomeTempTax record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(PersonalIncomeTempTax record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") PersonalIncomeTempTax example);

    /**
     * 費用を削除する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer personalIncomeTaxId);
}
