package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.service.TaxService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 税率、保険料率情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/taxRest")
@RestController
public class TaxRestController {

    @Autowired
    TaxService taxService;

    static List<String> taxName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * 税率、保険料率を新規追加する。
     * @param taxId ID
     * @return 結果
     */
    @RequestMapping(value = "{taxId}", method = RequestMethod.GET)
    public TaxDto get(@PathVariable Integer taxId) {
        //プライマリーキーで税率、保険料率を検索する。
        TaxDto ret = taxService.selectByPrimaryKey(taxId);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param taxDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody TaxDto taxDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(taxDto.getYearMonth());
        taxDto.setTaxStartMonth(month);
        //税率、保険料率を新規追加する。
        int ret = taxService.insertSelective(taxDto);
        taxService.setTaxEndMonth(taxDto);
        return ret;
    }

    /**
     * 税率、保険料率を変更する。
     * @param taxDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody TaxDto taxDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(taxDto.getYearMonth());
        taxDto.setTaxStartMonth(month);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = taxService.updateByPrimaryKeySelective(taxDto);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param taxId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{taxId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer taxId) {
        //税率、保険料率を削除する。
        int ret = taxService.deleteByPrimaryKey(taxId);
        return ret;
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param taxDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<TaxDto> listAll(TaxDto taxDto,
                                    @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                    JqgridPageReq pageReq) {
        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<TaxDto> page = PageUtil.startPage(pageReq);
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        taxDto.setTaxStartMonth(month);
        taxDto.setTaxEndMonth(month.plusMonths(1).minusSeconds(1));
        List<TaxDto> ret = taxService.selectAllByExample(taxDto, month);
        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param taxDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<TaxDto> list(TaxDto taxDto, JqgridPageReq pageReq) {
        Page<TaxDto> page = PageUtil.startPage(pageReq);
        //条件で税率、保険料率を検索する。（連携情報含む）
        List<TaxDto> ret = taxService.selectByExample(taxDto);
        return PageUtil.resp(page);
    }
}
