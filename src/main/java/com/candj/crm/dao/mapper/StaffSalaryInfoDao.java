package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.StaffSalaryInfo;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフ基本給料情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface StaffSalaryInfoDao {
    /**
     * プライマリーキーでスタッフ基本給料を検索する。
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    StaffSalaryInfo selectByPrimaryKey(Integer staffSalaryInfoId);

    /**
     * プライマリーキーでスタッフ基本給料検索する。（連携情報含む）
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    List<StaffSalaryInfo> selectAllByPrimaryKey(Integer staffSalaryInfoId);

    /**
     * プライマリーキーでスタッフ基本給料検索する。（連携情報含む）
     * @param userId ID
     * @return 結果
     */
    List<StaffSalaryInfo> selectAllByPrimaryKeyFromUserId(Integer userId);

    /**
     * 条件でスタッフ基本給料を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffSalaryInfo> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフ基本給料を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffSalaryInfo> selectByExample(@Param("example") WhereCondition example);

    /**
     * スタッフ基本給料を新規追加する。
     * @param record スタッフ基本給料
     * @return 結果
     */
    int insertSelective(StaffSalaryInfo record);

    /**
     * スタッフ基本給料を新規追加する。
     * @param record スタッフ基本給料
     * @return 結果
     */
    int insert(StaffSalaryInfo record);

    /**
     * プライマリーキーでスタッフ基本給料を更新する。
     * @param record スタッフ基本給料
     * @return 結果
     */
    int updateByPrimaryKey(StaffSalaryInfo record);

    /**
     * プライマリーキーでスタッフ基本給料を更新する。
     * @param record スタッフ基本給料
     * @return 結果
     */
    int updateByPrimaryKeySelective(StaffSalaryInfo record);

    /**
     * スタッフ基本給料を更新する。
     * @param record スタッフ基本給料
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(StaffSalaryInfo record, @Param("example") WhereCondition example);

    /**
     * スタッフ基本給料を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") StaffSalaryInfo example);

    /**
     * スタッフ基本給料を削除する。
     * @param staffSalaryInfoId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer staffSalaryInfoId);
}
