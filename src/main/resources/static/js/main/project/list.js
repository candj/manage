

			var colNames = [
				"プロジェクトID",
				"見積状態",
				"プロジェクト名",
				"開発言語",
				"デ—タベ—ス",
				"プログラム規模",
				"開始期日",
				"終了期日",
				"発注元会社",
				"発注先会社",
				"プロジェクト摘要",
				"プロジェクトステータス",
			];
			var colModel = [
				{
					name: "projectId",
					index: "projectId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
                {
					name: "haveOfferFlag",
					index: "haveOfferFlag",
					width: 100,
					search : false,
					hidden : true,
					formatter : function(cellvalue, options, rowObject) {
                         if (cellvalue == true) {
                               return '<b style="color:green">○<b>';
                         } else if (cellvalue == false) {
                               return '<b style="color:red">×<b>';
                         }
                    },
					align: "center",
				},
				{
					name: "projectName",
					index: "projectName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "programmingLanguage",
					index: "programmingLanguage",
					width: 100,
					search : false,
					hidden: true,
					align: "left",
				},
				{
					name: "projectDatabase",
					index: "projectDatabase",
					width: 100,
					search : false,
					hidden: true,
					align: "left",
				},
				{
					name: "programSize",
					index: "programSize",
					width: 100,
					search : false,
					hidden: true,
					align: "left",
				},
				{
                                	name: "projectStartDate",
                                	index: "projectStartDate",
                                	width: 100,
                                	search : false,
                                	formatter : function(cellvalue, options, rowObject) {
                                    		if (cellvalue != null) {
                                    			var date = new Date(cellvalue);
                                    			return date.Format('yyyy-MM-dd');
                                    		} else {
                                    			return '';
                                    		}
                                    },
                                	align: "left",
                                },
                                {
                                	name: "projectEndDate",
                                	index: "projectEndDate",
                                	width: 100,
                                	search : false,
                                	formatter : function(cellvalue, options, rowObject) {
                                             if (cellvalue != null) {
                                                 var date = new Date(cellvalue);
                                                 return date.Format('yyyy-MM-dd');
                                             } else {
                                                 return '';
                                             }
                                    },
                                	align: "left",
                 },
				{
					name: "sendCompany.sendCompanyNameAbbr",
					index: "sendCompany.sendCompanyNameAbbr",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "receiveCompany.receiveCompanyNameAbbr",
					index: "receiveCompany.receiveCompanyNameAbbr",
					width: 100,
					search : false,
					align: "left",
				},
				{
                    name: "projectDescription",
                    index: "projectDescription",
                    width: 300,
                    search : false,
                    align: "left",
                },
				{
					name: "projectStatus",
					index: "projectStatus",
					width: 100,
					hidden: true,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                         if (cellvalue == '00') {
                               return '開始前';
                         } else if (cellvalue == '01') {
                               return '進行中';
                         } else if (cellvalue == '10') {
                               return '完了';
                         }
                    },
					align: "left",
				},
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	