package com.candj.crm.Utils;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.jxls.expression.JexlExpressionEvaluator;
import org.jxls.transform.Transformer;
import org.jxls.util.JxlsHelper;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ReportUtil {

	public static void outputExcelForHttp(ApplicationContext ac, String templateFile, HttpServletResponse res, String fileName,
			Map<String, Object> model) throws IOException {

		res.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		res.setHeader("content-disposition", String.format("attachment; filename=\"%s\"", URLEncoder.encode(fileName,"UTF-8")));
		res.setCharacterEncoding("UTF-8");

		outputExcel(ac, templateFile, res.getOutputStream(), model);

	}

	public static void outputExcelForZip(ApplicationContext ac, String templateFile, String dirPath, String fileName,
										  Map<String, Object> model) throws IOException {

		String filePath = dirPath + "/" + fileName;
		OutputStream os = new FileOutputStream(filePath);

		outputExcel(ac, templateFile, os, model);
	}


	public static void outputExcel(ApplicationContext ac, String templateFile, OutputStream os,
			Map<String, Object> model) throws IOException {

		InputStream in = null;

		try {
			// エクセルを生成する
			in = ac.getResource("classpath:" + templateFile).getInputStream();
			org.jxls.common.Context context = new org.jxls.common.Context();

			model.forEach((key, value) -> {
				context.putVar(key, value);
			});
			// クライアントからだダウンロードする
			JxlsHelper jxlsHelper = JxlsHelper.getInstance();
			Transformer transformer = jxlsHelper.createTransformer(in, os);
			JexlExpressionEvaluator evaluator = (JexlExpressionEvaluator) transformer.getTransformationConfig()
					.getExpressionEvaluator();
			Map<String, Object> funcs = new HashMap<String, Object>();
			funcs.put("utils", new JxlsUtils());
			JexlEngine jexl = new JexlBuilder().namespaces(funcs).create();
			evaluator.setJexlEngine(jexl);
			jxlsHelper.processTemplate(context, transformer);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void outputZip(HttpServletResponse res, File sourceFolder, String file) throws IOException {
		res.setContentType("application/zip");
		res.setHeader("content-disposition", file);
		res.setCharacterEncoding("UTF-8");

		//エクセルを圧縮、クライアントからだダウンロードする
		ZipUtils.compress(sourceFolder, res.getOutputStream());
	}



}
