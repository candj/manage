package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

@Getter
@Setter
public class SocialInsuranceDto {

    /** 税率ID */
    @Digits(integer=10, fraction=0)
    private Integer socialInsuranceId;

    /** 適用年月開始時間 */
    private String yearMonth;

    private  String location;

    /** 適用年月開始時間 */
    private org.joda.time.DateTime SocialInsuranceStartMonth;

    /** 適用年月終了時間 */
    private org.joda.time.DateTime SocialInsuranceEndMonth;

    /** Excelパース */
    @Length(max=255)
    private String SocialInsuranceExcelPath;

    /** Excelファイル名 */
    @Length(max=255)
    private String SocialInsuranceExcelName;

    /** JSONパース */
    @Length(max=255)
    private String SocialInsuranceJsonPath;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

}
