package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class ChangePassword {

    /** パスワード */
    @Length(max=64)
    private String oldPassword;

    /**　new パスワード */
    @Length(max=64)
    private String newPassword;

    /** sure new パスワード */
    @Length(max=64)
    private String sure_newPassword;
}
