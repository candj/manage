package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.User;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface UserDao {
    /**
     * プライマリーキーでユーザーを検索する。
     * @param userId ユーザーID
     * @return 結果
     */
    User selectByPrimaryKey(Integer userId);

    /**
     * プライマリーキーでユーザー検索する。（連携情報含む）
     * @param userId ユーザーID
     * @return 結果
     */
    List<User> selectAllByPrimaryKey(Integer userId);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<User> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でユーザーを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<User> selectByExample(@Param("example") WhereCondition example);

    /**
     * ユーザーを新規追加する。
     * @param record ユーザー
     * @return 結果
     */
    int insertSelective(User record);

    /**
     * ユーザーを新規追加する。
     * @param record ユーザー
     * @return 結果
     */
    int insert(User record);

    /**
     * プライマリーキーでユーザーを更新する。
     * @param record ユーザー
     * @return 結果
     */
    int updateByPrimaryKey(User record);

    /**
     * プライマリーキーでユーザーを更新する。
     * @param record ユーザー
     * @return 結果
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * ユーザーを更新する。
     * @param record ユーザー
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(User record, @Param("example") WhereCondition example);

    /**
     * ユーザーを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") User example);

    /**
     * ユーザーを削除する。
     * @param userId ユーザーID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer userId);
}
