var colNames = [
    "スタッフ給料情報ID",
    "スタッフ名前",
    "基本給",
    "適用開始時間",
    "適用終了時間"
];

var colModel = [
    {
        name: "staffSalaryInfoId",
        index: "staffSalaryInfoId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "appUser.userName",
        index: "appUser.userName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "salaryPay",
        index: "salaryPay",
        width: 100,
        search: false,
        align: "left",
    },

    {
        name: "salaryStartDate",
        index: "salaryStartDate",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
        name: "salaryEndDate",
        index: "salaryEndDate",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    }
];

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel);
});	
	

