package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

/**
 * 注文
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OrderingColumnOnly {

    /** 注文ID */
    private Integer orderId;

    /** 請求ID */
    private Integer invoiceId;

    /** 見積 ID */
    private Integer offerId;

    /** 注文番号 */
    private String orderNumber;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 日付 */
    private org.joda.time.DateTime orderDate;

    /** ステータス */
    private String orderStatus;

    /** 請求作成FLAG */
    private boolean haveInvoiceFlag;

    /** 削除FLAG */
    private boolean orderDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
