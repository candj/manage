package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.dto.OfferDetailDto;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.dto.StaffProjectInfoDto;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 見積詳細 情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/offerDetailRest")
@RestController
public class OfferDetailRestController {

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    OfferDetailTemporaryService offerDetailTemporaryService;

    @Autowired
    OfferService offerService;

    @Autowired
    Calculate calculate;

    @Autowired
    StaffProjectInfoService staffProjectInfoService;

    /**
     * 見積詳細 を新規追加する。
     *
     * @param offerDetailId
     * @return 結果
     */
    @RequestMapping(value = "{offerDetailId}", method = RequestMethod.GET)
    public OfferDetailDto get(@PathVariable Integer offerDetailId) {
        //プライマリーキーで見積詳細 を検索する。
        OfferDetailDto ret = offerDetailService.selectByPrimaryKey(offerDetailId);
        return ret;
    }

    /**
     * 見積詳細 を新規追加する。
     *
     * @param offerDetailDto 見積詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody OfferDetailDto offerDetailDto, Errors rrrors) {
        //見積詳細　該記録スタッフの金額小計
        offerDetailDto.setTotalAmount(calculate.salaryAmountCalculate(offerDetailDto.getAmount(), offerDetailDto.getManHour()));
        //見積詳細 を新規追加する。
        int ret = offerDetailService.insertSelective(offerDetailDto);
        //見積総金額の計算と更新
        int offerId = offerDetailDto.getOfferId();
        List<OfferDetailDto> offerDetailDtos = offerDetailService.selectByOfferId(offerId);
        OfferDto offerDto = offerService.selectByPrimaryKey(offerId);
        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        for (OfferDetailDto offerDetail : offerDetailDtos) {
            totalAmountWithoutTax = totalAmountWithoutTax + offerDetail.getTotalAmount();
        }
        offerDto.setOfferAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, offerDto.getWorkStartDate());
        offerDto.setOfferConsumptionTax(tax);
        //税込み合計金額
        offerDto.setOfferTotalAmount(totalAmountWithoutTax + tax);
        offerService.updateByPrimaryKeySelective(offerDto);

        StaffProjectInfoDto staffProjectInfoDto = new StaffProjectInfoDto();
        staffProjectInfoDto.setProjectName(offerDetailDto.getWorkName());
        staffProjectInfoDto.setProjectStartDate(offerDetailDto.getWorkStartDate());
        staffProjectInfoDto.setProjectEndDate(offerDetailDto.getWorkEndDate());
        staffProjectInfoDto.setUserId(offerDetailDto.getUserId());
        staffProjectInfoDto.setOfferId(offerDetailDto.getOfferId());
        DateTime dateTime = new DateTime();
        if (dateTime.getMillis() <= offerDetailDto.getWorkStartDate().getMillis()) {
            staffProjectInfoDto.setStaffInProject("02");
        } else if(dateTime.getMillis() >= offerDetailDto.getWorkStartDate().getMillis()){
            staffProjectInfoDto.setStaffInProject("00");
        } else{
            staffProjectInfoDto.setStaffInProject("01");
        }
        staffProjectInfoService.insert(staffProjectInfoDto);
        return ret;
    }

    /**
     * 見積詳細 を変更する。
     *
     * @param offerDetailDto 見積詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody OfferDetailDto offerDetailDto, Errors rrrors) {
        //見積詳細　該記録スタッフの金額小計の更新
        offerDetailDto.setTotalAmount(calculate.salaryAmountCalculate(offerDetailDto.getAmount(), offerDetailDto.getManHour()));
        //プライマリーキーで見積詳細 を更新する。
        int ret = offerDetailService.updateByPrimaryKeySelective(offerDetailDto);
        //見積総金額の計算と更新
        int offerId = offerDetailDto.getOfferId();
        List<OfferDetailDto> offerDetailDtos = offerDetailService.selectByOfferId(offerId);
        OfferDto offerDto = offerService.selectByPrimaryKey(offerId);
        //税抜き合計金額
        int totalAmountWithoutTax = 0;
        for (OfferDetailDto offerDetail : offerDetailDtos) {
            totalAmountWithoutTax = totalAmountWithoutTax + offerDetail.getTotalAmount();
        }
        offerDto.setOfferAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, offerDto.getWorkStartDate());
        offerDto.setOfferConsumptionTax(tax);
        //税込み合計金額
        offerDto.setOfferTotalAmount(totalAmountWithoutTax + tax);
        offerService.updateByPrimaryKeySelective(offerDto);

        StaffProjectInfoDto staffProjectInfoDto = new StaffProjectInfoDto();
        staffProjectInfoDto.setProjectName(offerDetailDto.getWorkName());
        staffProjectInfoDto.setProjectStartDate(offerDetailDto.getWorkStartDate());
        staffProjectInfoDto.setProjectEndDate(offerDetailDto.getWorkEndDate());
        staffProjectInfoDto.setUserId(offerDetailDto.getUserId());
        DateTime dateTime = new DateTime();
        if (dateTime.getMillis() <= offerDetailDto.getWorkStartDate().getMillis()) {
            staffProjectInfoDto.setStaffInProject("02");
        } else if(dateTime.getMillis() >= offerDetailDto.getWorkStartDate().getMillis()){
            staffProjectInfoDto.setStaffInProject("00");
        } else{
            staffProjectInfoDto.setStaffInProject("01");
        }
        staffProjectInfoService.updateByPrimaryKey(staffProjectInfoDto);
        return ret;
    }

    /**
     * 見積詳細 を新規追加する。
     *
     * @param offerDetailId
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{offerDetailId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer offerDetailId) {
        //見積詳細 を削除する。
        int ret = offerDetailService.deleteByPrimaryKey(offerDetailId);
        return ret;
    }

    /**
     * 見積詳細 一覧画面を表示する。
     *
     * @param offerDetailDto 見積詳細
     * @param pageReq        改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<OfferDetailDto> list(OfferDetailDto offerDetailDto, JqgridPageReq pageReq) {
        Page<OfferDetailDto> page = PageUtil.startPage(pageReq);
        //条件で見積詳細 を検索する。（連携情報含む）
        List<OfferDetailDto> ret = offerDetailService.selectByOfferId(offerDetailDto.getOfferId());
        return PageUtil.resp(page);
    }

    /**
     * 見積詳細 一覧画面を表示する。
     *
     * @param offerDetailDto 見積詳細
     * @param pageReq        改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<OfferDetailDto> listAll(OfferDetailDto offerDetailDto, JqgridPageReq pageReq) {
        Page<OfferDetailDto> page = PageUtil.startPage(pageReq);
        //条件で見積詳細 を検索する。（連携情報含む）
        List<OfferDetailDto> ret = offerDetailService.selectAllByExample(offerDetailDto);
        return PageUtil.resp(page);
    }

    /**
     * 見積詳細 を新規追加する。
     *
     * @param offerDetailDto 見積詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/addForInsert", method = RequestMethod.POST)
    public int addForInsert(@Valid @RequestBody OfferDetailDto offerDetailDto, Errors rrrors) {
        //見積詳細　該記録スタッフの金額小計
        offerDetailDto.setTotalAmount(calculate.salaryAmountCalculate(offerDetailDto.getAmount(), offerDetailDto.getManHour()));
        //見積詳細 を新規追加する。
        int ret = offerDetailTemporaryService.insertSelective(offerDetailDto);
        //見積総金額の計算と更新
        /*int offerId = offerDetailDto.getOfferId();
        List<OfferDetailDto> offerDetailDtos = offerDetailService.selectByOfferId(offerId);
        OfferDto offerDto = offerService.selectByPrimaryKey(offerId);
        //税抜き合計金額
        Integer totalAmountWithoutTax = 0;
        for (OfferDetailDto offerDetail : offerDetailDtos) {
            totalAmountWithoutTax = totalAmountWithoutTax + offerDetail.getTotalAmount();
        }
        offerDto.setOfferAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, offerDto.getWorkStartDate());
        offerDto.setOfferConsumptionTax(tax);
        //税込み合計金額
        offerDto.setOfferTotalAmount(totalAmountWithoutTax + tax);
        offerService.updateByPrimaryKeySelective(offerDto);*/

        return ret;
    }

    /**
     * 見積詳細 を変更する。
     *
     * @param offerDetailDto 見積詳細
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/editForInsert",method = RequestMethod.PUT)
    public int editForInsert(@Valid @RequestBody OfferDetailDto offerDetailDto, Errors rrrors) {
        //見積詳細　該記録スタッフの金額小計の更新
        offerDetailDto.setTotalAmount(calculate.salaryAmountCalculate(offerDetailDto.getAmount(), offerDetailDto.getManHour()));
        //プライマリーキーで見積詳細 を更新する。
        int ret = offerDetailService.updateByPrimaryKeySelective(offerDetailDto);
        //見積総金額の計算と更新
        /*int offerId = offerDetailDto.getOfferId();
        List<OfferDetailDto> offerDetailDtos = offerDetailService.selectByOfferId(offerId);
        OfferDto offerDto = offerService.selectByPrimaryKey(offerId);
        //税抜き合計金額
        int totalAmountWithoutTax = 0;
        for (OfferDetailDto offerDetail : offerDetailDtos) {
            totalAmountWithoutTax = totalAmountWithoutTax + offerDetail.getTotalAmount();
        }
        offerDto.setOfferAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, offerDto.getWorkStartDate());
        offerDto.setOfferConsumptionTax(tax);
        //税込み合計金額
        offerDto.setOfferTotalAmount(totalAmountWithoutTax + tax);
        offerService.updateByPrimaryKeySelective(offerDto);*/

        StaffProjectInfoDto staffProjectInfoDto = new StaffProjectInfoDto();
        staffProjectInfoDto.setProjectName(offerDetailDto.getWorkName());
        staffProjectInfoDto.setProjectStartDate(offerDetailDto.getWorkStartDate());
        staffProjectInfoDto.setProjectEndDate(offerDetailDto.getWorkEndDate());
        staffProjectInfoDto.setUserId(offerDetailDto.getUserId());
        DateTime dateTime = new DateTime();
        if (dateTime.getMillis() >= offerDetailDto.getWorkStartDate().getMillis()) {
            staffProjectInfoDto.setStaffInProject("01");
        } else {
            staffProjectInfoDto.setStaffInProject("00");
        }
        staffProjectInfoService.updateByPrimaryKey(staffProjectInfoDto);
        return ret;
    }

    /**
     * 見積詳細 を新規追加する。
     *
     * @param offerDetailId
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "deleteForInsert/{offerDetailId}", method = RequestMethod.DELETE)
    public int deleteForInsert(@PathVariable Integer offerDetailId) {
        //見積詳細 を削除する。
        int ret = offerDetailTemporaryService.deleteByPrimaryKey(offerDetailId);
        return ret;
    }

    /**
     * 見積詳細 一覧画面を表示する。
     *
     * @param offerDetailDto 見積詳細
     * @param pageReq        改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listForInsert", method = RequestMethod.POST)
    public PageInfo<OfferDetailDto> listForInsert(OfferDetailDto offerDetailDto, JqgridPageReq pageReq) {
        Page<OfferDetailDto> page = PageUtil.startPage(pageReq);
        //条件で見積詳細 を検索する。（連携情報含む）
        List<OfferDetailDto> ret = offerDetailTemporaryService.selectAllByExample(offerDetailDto);
        return PageUtil.resp(page);
    }
}
