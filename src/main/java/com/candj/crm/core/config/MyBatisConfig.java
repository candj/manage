package com.candj.crm.core.config;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.candj.crm.dao.mapper")
public class MyBatisConfig {
}
