package com.candj.crm.controller;

import com.candj.crm.dto.UserDto;
import com.candj.crm.service.UserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/user")
@Controller
public class UserController {
    @Autowired
    UserService userService;

    /**
     * ユーザーを新規追加する。
     * @param userDto ユーザー
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(UserDto userDto, ModelMap model) {
        return new ModelAndView("user/insert", model);
    }

    /**
     * ユーザーを新規追加(確認)する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid UserDto userDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/user/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("user/insertConfirm", model);
    }

    /**
     * ユーザーを新規追加(終了)する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid UserDto userDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/user/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //ユーザーを新規追加する。
        int ret = userService.insertSelective(userDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/user/insertFinish", model);
    }

    /**
     * ユーザーを編集する。
     * @param userId ユーザーID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer userId, ModelMap model) {
        //プライマリーキーでユーザーを検索する。
        UserDto serDto = userService.selectByPrimaryKey(userId);
        model.addAttribute("serDto", serDto);
        return new ModelAndView("user/edit", model);
    }

    /**
     * ユーザーを編集(確認)する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid UserDto userDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/user/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("user/editConfirm", model);
    }

    /**
     * ユーザーを編集(終了)する。
     * @param userDto ユーザー
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid UserDto userDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/user/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでユーザーを更新する。
        int ret = userService.updateByPrimaryKeySelective(userDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("user/editFinish", model);
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param userDto ユーザー
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("userDto") UserDto userDto, ModelMap model) {
        return new ModelAndView("user/list", model);
    }
}
