var colNames = [
    "ユーザーID",
    // "タイプ",
    "名前",
    "性別",
    "部署",
    "イーメールアドレス",
    "電話番号",
    // "パスワード",
];
var colModel = [
    {
        name: "userId",
        index: "userId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "userName",
        index: "userName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userSex",
        index: "userSex",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userDepartment",
        index: "userDepartment",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userEmail",
        index: "userEmail",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userTelephoneNumber",
        index: "userTelephoneNumber",
        width: 100,
        search: false,
        align: "left",
    }
];

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel);
});	
	