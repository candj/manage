

			var colNames = [
				"市区町村ID",
				"市区町村名称",
				"市区町村种类",
				"市区町村种类名称",
				"都道府县",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "cityId",
					index: "cityId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "cityName",
					index: "cityName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "cityKind",
					index: "cityKind",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "cityKindName",
					index: "cityKindName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "cityCountryId",
					index: "cityCountryId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	