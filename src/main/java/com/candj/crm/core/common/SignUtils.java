package com.candj.crm.core.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.candj.webpower.web.core.exception.NotValidException;
import com.candj.webpower.web.core.util.LogUtil;

/**
 * MD5署名作成用共通ユーティリティ.
 *
 */
public class SignUtils {

    /** MD5署名の暗号化方式. */
    private static final String SIGN_TYPE_MD5 = "1";

    /**
     * デジタル署名検証メソッド
     * 
     * @param Map
     * @param requestId APIKeyを作成するために必要
     * @param signType 署名の暗号化方式
     * @return boolean
     * @throws NotValidException
     */
    public static boolean checkDigitalSignature(Map<String, String> map, String requestId, String signType) throws NotValidException {
        // リクエストから送られてきたデジタル署名方式を比較
        if (!StringUtils.equals(signType, SIGN_TYPE_MD5)) {
            LogUtil.warn("800-00503", signType);
            throw new NotValidException("C003-0003");
        }
        String expectSign = createDigitalSignature(map, requestId);
        LogUtil.debug("期待されたSIGN ： " + expectSign);
        return StringUtils.equals(expectSign, map.get("sign"));
    }

    /**
     * デジタル署名作成メソッド MD5署名で作成
     * 
     * @param Map
     * @param requestId APIKeyを作成するために必要
     * @return MD5署名された32文字の文字列
     */
    public static String createDigitalSignature(Map<String, String> map, String requestId) {

        StringBuilder tempSign = new StringBuilder();
        String apiKey;

        // mapをキーとしてソート(大文字、小文字を区別しない)
        List<String> mapkeys = new ArrayList<>(map.keySet());
        Collections.sort(mapkeys, String.CASE_INSENSITIVE_ORDER);

        // key=value の形式に変換し、&でつなげる
        tempSign.append("{");
        for (String mapkey : mapkeys) {
            if (map.get(mapkey) != null && !(mapkey.equals("sign"))) {
                tempSign.append(mapkey);
                tempSign.append("=");
                tempSign.append(map.get(mapkey));
                tempSign.append("&");
            }
        }
        // 最後の"&"を削除
        tempSign.deleteCharAt(tempSign.length() - 1);
        // APIキーをつなげる
        apiKey = "B2B" + requestId;
        tempSign.append(apiKey);
        tempSign.append("}");
        // MD5署名
        return org.apache.commons.codec.digest.DigestUtils.md5Hex(tempSign.toString()).toUpperCase();
    }
}
