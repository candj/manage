package com.candj.crm.service;

import com.candj.crm.dao.mapper.PaySiteDao;
import com.candj.crm.dto.PaySiteDto;
import com.candj.crm.mapper.model.PaySite;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支払いサイト情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class PaySiteService {
    @Autowired
    PaySiteDao paySiteDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで支払いサイトを検索する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    public PaySiteDto selectByPrimaryKey(Integer paySiteId) {
        //プライマリーキーで支払いサイトを検索する。
        PaySite ret = paySiteDao.selectByPrimaryKey(paySiteId);
        return DozerHelper.map(mapper, ret, PaySiteDto.class);
    }

    /**
     * プライマリーキーで支払いサイトを検索する。（連携情報含む）
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    public List<PaySiteDto> selectAllByPrimaryKey(Integer paySiteId) {
        //プライマリーキーで支払いサイト検索する。（連携情報含む）
        List<PaySite> ret = paySiteDao.selectAllByPrimaryKey(paySiteId);
        return DozerHelper.mapList(mapper, ret, PaySiteDto.class);
    }

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<PaySiteDto> selectByExample(PaySiteDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("pay_site.pay_site_id", dto.getPaySiteId())
            .andEqualTo("pay_site.pay_site_name", dto.getPaySiteName())
            .andEqualTo("pay_site.create_user", dto.getCreateUser())
            .andEqualTo("pay_site.update_user", dto.getUpdateUser())
            .andEqualTo("pay_site.create_time", dto.getCreateTime())
            .andEqualTo("pay_site.update_time", dto.getUpdateTime());
        }
        //条件で支払いサイトを検索する。（連携情報含む）)
        List<PaySite> ret = paySiteDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, PaySiteDto.class);
    }

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<PaySiteDto> selectAllByExample(PaySiteDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("pay_site.pay_site_id", dto.getPaySiteId())
            .andEqualTo("pay_site.pay_site_name", dto.getPaySiteName())
            .andEqualTo("pay_site.create_user", dto.getCreateUser())
            .andEqualTo("pay_site.update_user", dto.getUpdateUser())
            .andEqualTo("pay_site.create_time", dto.getCreateTime())
            .andEqualTo("pay_site.update_time", dto.getUpdateTime());
        }
        //条件で支払いサイトを検索する。（連携情報含む）)
        List<PaySite> ret = paySiteDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, PaySiteDto.class);
    }

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）
     * @return 結果
     */
    public List<PaySiteDto> selectByMyExample(Integer orderId) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(orderId != null) {
            whereCondition.createCriteria()
                    .andEqualTo("ordering.order_id", orderId);
        }
        //条件で支払いサイトを検索する。（連携情報含む）)
        List<PaySite> ret = paySiteDao.selectByMyExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, PaySiteDto.class);
    }

    /**
     * 支払いサイトを新規追加する。
     * @param dto 支払いサイト
     * @return 結果
     */
    public int insertSelective(PaySiteDto dto) {
        PaySite paySite  =  mapper.map(dto, PaySite.class);
        //支払いサイトを新規追加する。
        int ret = paySiteDao.insertSelective(paySite);
        return ret;
    }

    /**
     * 支払いサイトを新規追加する。
     * @param dto 支払いサイト
     * @return 結果
     */
    public int insert(PaySiteDto dto) {
        PaySite paySite  =  mapper.map(dto, PaySite.class);
        //支払いサイトを新規追加する。
        int ret = paySiteDao.insert(paySite);
        return ret;
    }

    /**
     * プライマリーキーで支払いサイトを更新する。
     * @param dto 支払いサイト
     * @return 結果
     */
    public int updateByPrimaryKey(PaySiteDto dto) {
        PaySite paySite  =  mapper.map(dto, PaySite.class);
        //プライマリーキーで支払いサイトを更新する。
        int ret = paySiteDao.updateByPrimaryKey(paySite);
        return ret;
    }

    /**
     * プライマリーキーで支払いサイトを更新する。
     * @param dto 支払いサイト
     * @return 結果
     */
    public int updateByPrimaryKeySelective(PaySiteDto dto) {
        PaySite paySite  =  mapper.map(dto, PaySite.class);
        //プライマリーキーで支払いサイトを更新する。
        int ret = paySiteDao.updateByPrimaryKeySelective(paySite);
        return ret;
    }

    /**
     * 支払いサイトを削除する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer paySiteId) {
        //支払いサイトを削除する。
        int ret = paySiteDao.deleteByPrimaryKey(paySiteId);
        return ret;
    }
}
