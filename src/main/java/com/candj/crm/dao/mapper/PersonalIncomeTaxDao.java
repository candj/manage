package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.PersonalIncomeTax;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface PersonalIncomeTaxDao {
    /**
     * プライマリーキーで費用を検索する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    PersonalIncomeTax selectByPrimaryKey(Integer personalIncomeTaxId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    List<PersonalIncomeTax> selectAllByPrimaryKey(Integer personalIncomeTaxId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PersonalIncomeTax> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PersonalIncomeTax> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(PersonalIncomeTax record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(PersonalIncomeTax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(PersonalIncomeTax record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(PersonalIncomeTax record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(PersonalIncomeTax record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") PersonalIncomeTax example);

    /**
     * 費用を削除する。
     * @param personalIncomeTaxId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer personalIncomeTaxId);

}
