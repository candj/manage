package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * プロジェクト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Project {
    /** 見積 */
    private java.util.List<OfferColumnOnly> offer;

    /** プロジェクトID */
    private Integer projectId;

    /** プロジェクト名 */
    private String projectName;

    /** 開発言語 */
    private String programmingLanguage;

    /** デ—タベ—ス */
    private String projectDatabase;

    /** プログラム規模 */
    private Integer programSize;

    /** プロジェクト摘要 */
    private String projectDescription;

    /** 開始期日 */
    private org.joda.time.DateTime projectStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime projectEndDate;

    /** 元会社 */
    private SendCompany sendCompany;

    /** 元会社ID */
    private Integer sendCompanyId;

    /** 先会社 */
    private ReceiveCompany receiveCompany;

    /** 先会社ID */
    private Integer receiveCompanyId;

    /** プロジェクトステータス */
    private String projectStatus;

    /** 見積作成FLAG */
    private boolean haveOfferFlag;

    /** 削除FLAG */
    private boolean projectDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
