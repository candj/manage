package com.candj.crm.webservice;

import com.candj.crm.dto.AppUserDto;
import com.candj.crm.service.AppUserService;
import com.candj.crm.service.UserService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ユーザー情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/guestRest")
@RestController
public class GuestRestController {
    @Autowired
    AppUserService appUserService;

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public AppUserDto get(@PathVariable Integer userId) {
        //プライマリーキーでユーザーを検索する。
        AppUserDto ret = appUserService.selectByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param appUserDto ユーザー
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        //ユーザーを新規追加する。
        appUserDto.setUserType(2);
        appUserDto.setUserDeleteFlag(true);
        int ret = appUserService.insertSelective(appUserDto);
        return ret;
    }

    /**
     * ユーザーを変更する。
     * @param appUserDto ユーザー
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        appUserDto.setUserDeleteFlag(true);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserService.updateByPrimaryKeySelective(appUserDto);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer userId) {
        AppUserDto appUserDto = appUserService.selectByPrimaryKey(userId);
        appUserDto.setUserDeleteFlag(false);

        //ユーザーを削除する。
        int ret = appUserService.updateByPrimaryKey(appUserDto);
        return ret;
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param appUserDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<AppUserDto> list(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        appUserDto.setUserType(2);
        List<AppUserDto> ret = appUserService.selectAllByExample(appUserDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param appUserDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<AppUserDto> listAll(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        List<AppUserDto> ret = appUserService.selectAllByExample(appUserDto);
        return PageUtil.resp(page);
    }
}
