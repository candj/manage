package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

@Getter
@Setter
public class SocialInsurance {

    /** 税率ID */
    private Integer socialInsuranceId;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** 適用年月開始時間 */
    private org.joda.time.DateTime SocialInsuranceStartMonth;

    /** 適用年月終了時間 */
    private org.joda.time.DateTime SocialInsuranceEndMonth;

    /** Excelパース */
    private String SocialInsuranceExcelPath;

    /** Excelファイル名 */
    private String SocialInsuranceExcelName;

    /** JSONパース */
    private String SocialInsuranceJsonPath;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

}
