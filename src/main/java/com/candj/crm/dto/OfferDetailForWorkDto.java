package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfferDetailForWorkDto {

    private Integer orderId;

    private Integer userId;

    /** 月初 */
    private org.joda.time.DateTime startOfMonth;

    /** 月末 */
    private org.joda.time.DateTime EndOfMonth;

}
