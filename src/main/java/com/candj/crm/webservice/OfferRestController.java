package com.candj.crm.webservice;

import com.candj.crm.Utils.Calculate;
import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.*;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 見積情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/offerRest")
@RestController
public class OfferRestController {

    @Autowired
    OfferService offerService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    OfferDetailTemporaryService offerDetailTemporaryService;

    @Autowired
    ProjectService projectService;

    @Autowired
    OrderingService orderingService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    CompanyService companyService;

    @Autowired
    Calculate calculate;

    @Autowired
    StaffProjectInfoService staffProjectInfoService;

    /**
     * 見積を新規追加する。
     *
     * @param offerId 見積 ID
     * @return 結果
     */
    @RequestMapping(value = "{offerId}", method = RequestMethod.GET)
    public OfferDto get(@PathVariable Integer offerId) {
        //プライマリーキーで見積を検索する。
        OfferDto ret = offerService.selectByPrimaryKey(offerId);
        return ret;
    }

    /**
     * 見積を新規追加する。
     *
     * @param offerDto 見積
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody OfferDto offerDto, Errors rrrors) {

        offerDto.setOfferDeleteFlag(true);

        CompanyDto companyDto = companyService.selectByPrimaryKey(offerDto.getReceiveCompanyId());
        //見積番号を生成する
        offerDto.setOfferNumber(companyDto.getCompanyNameAbbr()
                + " "
                + DateTimeUtil.dateTimeToStringNumber(offerDto.getDate()));

        //見積を新規追加する。
        int ret = offerService.insertSelective(offerDto);
        //見積詳細を新規追加する。
        OfferDto offer = offerService.selectAllByExample(offerDto).get(0);
        OfferDetailDto offerDetailDto = new OfferDetailDto();
        List<OfferDetailDto> offerDetailDtoList = offerDetailTemporaryService.selectByExample(offerDetailDto);
        Integer totalAmountWithoutTax = 0;
        for (OfferDetailDto od : offerDetailDtoList) {
            od.setOfferDetailId(null);
            od.setOfferId(offer.getOfferId());
            offerDetailService.insertSelective(od);
            //見積総金額の計算と更新
            totalAmountWithoutTax = totalAmountWithoutTax + od.getTotalAmount();

            StaffProjectInfoDto staffProjectInfoDto = new StaffProjectInfoDto();
            staffProjectInfoDto.setProjectName(od.getWorkName());
            staffProjectInfoDto.setProjectStartDate(od.getWorkStartDate());
            staffProjectInfoDto.setProjectEndDate(od.getWorkEndDate());
            staffProjectInfoDto.setUserId(od.getUserId());
            staffProjectInfoDto.setOfferId(offer.getOfferId());
            DateTime dateTime = new DateTime();
            if (dateTime.getMillis() <= od.getWorkStartDate().getMillis()) {
                staffProjectInfoDto.setStaffInProject("02");
            } else if(dateTime.getMillis() >= od.getWorkEndDate().getMillis()){
                staffProjectInfoDto.setStaffInProject("00");
            } else{
                staffProjectInfoDto.setStaffInProject("01");
            }
            staffProjectInfoService.insert(staffProjectInfoDto);
        }
        //税抜き合計金額
        offer.setOfferAmountWithoutTax(totalAmountWithoutTax);
        //消費税
        TaxDto dto = new TaxDto();
        dto.setTaxLowerLimit(totalAmountWithoutTax);
        dto.setTaxName("01");
        Integer tax = calculate.consumptionTax(totalAmountWithoutTax, dto, offerDto.getWorkStartDate());
        offer.setOfferConsumptionTax(tax);
        //税込み合計金額
        offer.setOfferTotalAmount(totalAmountWithoutTax + tax);
        offerService.updateByPrimaryKeySelective(offer);

        return ret;
    }

    /**
     * 見積を変更する。
     *
     * @param offerDto 見積
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody OfferDto offerDto, Errors rrrors) {
        offerDto.setOfferDeleteFlag(true);

        CompanyDto companyDto = companyService.selectByPrimaryKey(offerDto.getReceiveCompanyId());
        //見積番号を生成する
        offerDto.setOfferNumber(companyDto.getCompanyNameAbbr()
                + " "
                + DateTimeUtil.dateTimeToStringNumber(offerDto.getDate()));

        //プライマリーキーで見積を更新する。
        int ret = offerService.updateByPrimaryKeySelective(offerDto);
        return ret;
    }

    /**
     * 見積を新規追加する。
     *
     * @param offerId 見積 ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{offerId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer offerId) {

        OfferDto offerDto = offerService.selectByPrimaryKey(offerId);

        //連携削除
        List<OrderingDto> orderingDtoList = orderingService.selectByOfferId(offerId);
        if (!orderingDtoList.isEmpty()) {
            OrderingDto orderingDto = orderingDtoList.get(0);
            orderingDto.setOrderDeleteFlag(false);
            //注文を削除する
            orderingService.updateByPrimaryKey(orderingDto);
            List<InvoiceDto> invoiceDtoList = invoiceService.selectByOrderId(orderingDto.getOrderId());
            if (!invoiceDtoList.isEmpty()) {
                InvoiceDto invoiceDto = invoiceDtoList.get(0);
                invoiceDto.setInvoiceDeleteFlag(false);
                //請求を削除する
                invoiceService.updateByPrimaryKey(invoiceDto);
            }
        }
        offerDto.setOfferDeleteFlag(false);

        List<OfferDetailDto> offerDetailDtoList = offerDetailService.selectByOfferId(offerId);
        if (!offerDetailDtoList.isEmpty()) {
            for (OfferDetailDto offerDetailDto : offerDetailDtoList) {
                staffProjectInfoService.deleteByOfferId(offerDetailDto.getOfferId());
            }
        }

        //見積を削除する。
        int ret = offerService.updateByPrimaryKey(offerDto);
        return ret;
    }

    /**
     * 見積一覧画面を表示する。
     *
     * @param offerDto 見積
     * @param pageReq  改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<OfferDto> list(OfferDto offerDto,
                                   @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                   JqgridPageReq pageReq) {


        //条件で見積を検索する。（連携情報含む）
        DateTime startOfMonth = new DateTime();
        if (yearMonth != null) {
            startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        DateTime endOfMonth = startOfMonth.plusMonths(1).minusSeconds(1);

        if (offerDto.getSendCompanyName() != null && !offerDto.getSendCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(offerDto.getSendCompanyName());
            if (!companyDtoList.isEmpty()) {
                offerDto.setSendCompanyId(companyDtoList.get(0).getCompanyId());
            } else {
                Page<OfferDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        if (offerDto.getReceiveCompanyName() != null && !offerDto.getReceiveCompanyName().equals("")) {
            List<CompanyDto> companyDtoList = companyService.selectAllByExampleFromSearch(offerDto.getReceiveCompanyName());
            if (!companyDtoList.isEmpty()) {
                offerDto.setReceiveCompanyId(companyDtoList.get(0).getCompanyId());
            } else {
                Page<OfferDto> page = PageUtil.startPage(pageReq);
                return PageUtil.resp(page);
            }
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();

        if (appUser.getUserType() == 0) {
            Page<OfferDto> page = PageUtil.startPage(pageReq);
            offerDto.setWorkStartDate(startOfMonth);
            offerDto.setWorkEndDate(endOfMonth);
            List<OfferDto> ret = offerService.selectAllByExample(offerDto);
            return PageUtil.resp(page);
        } else {
            Page<OfferDto> page = PageUtil.startPage(pageReq);
            offerDto.setWorkStartDate(startOfMonth);
            offerDto.setWorkEndDate(endOfMonth);
            List<OfferDto> ret = offerService.selectByMyExample(offerDto, appUser.getUserId());
            return PageUtil.resp(page);
        }
    }

    /**
     * 見積一覧画面を表示する。
     *
     * @param offerDto 見積
     * @param pageReq  改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<OfferDto> listAll(OfferDto offerDto, JqgridPageReq pageReq) {
        Page<OfferDto> page = PageUtil.startPage(pageReq);
        //条件で見積を検索する。（連携情報含む）
        List<OfferDto> ret = offerService.selectByExample(offerDto);
        return PageUtil.resp(page);
    }
}
