package com.candj.crm.service;

import com.candj.crm.dao.mapper.TaxDao;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.mapper.model.Tax;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class TaxService {

    @Autowired
    TaxDao taxDao;

    @Autowired
    Mapper mapper;

    Comparator<Tax> descWorkStartTimeComp = new Comparator<Tax>() {
        @Override
        public int compare(Tax t1, Tax t2) {
            return t2.getTaxStartMonth().compareTo(t1.getTaxStartMonth());
        }
    };

    static List<String> taxName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * プライマリーキーで税率、保険料率を検索する。
     * @param taxId ID
     * @return 結果
     */
    public TaxDto selectByPrimaryKey(Integer taxId) {
        //プライマリーキーで税率、保険料率を検索する。
        Tax ret = taxDao.selectByPrimaryKey(taxId);
        return DozerHelper.map(mapper, ret, TaxDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     * @param taxId ID
     * @return 結果
     */
    public List<TaxDto> selectAllByPrimaryKey(Integer taxId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<Tax> ret = taxDao.selectAllByPrimaryKey(taxId);
        return DozerHelper.mapList(mapper, ret, TaxDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaxDto> selectByExample(TaxDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("tax.tax_id", dto.getTaxId())
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andEqualTo("tax.tax_rate", dto.getTaxRate())
                    .andEqualTo("tax.create_user", dto.getCreateUser())
                    .andEqualTo("tax.update_user", dto.getUpdateUser())
                    .andEqualTo("tax.create_time", dto.getCreateTime())
                    .andEqualTo("tax.update_time", dto.getUpdateTime());
        }
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<Tax> ret = taxDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaxDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaxDto> selectAllByExample(TaxDto dto, DateTime yearMonth) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("tax.tax_id", dto.getTaxId())
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andEqualTo("tax.tax_rate", dto.getTaxRate())
                    .andLessThanOrEqualTo("tax.tax_start_month", dto.getTaxStartMonth())
                    .andGreaterThanOrEqualTo("tax.tax_end_month", dto.getTaxEndMonth())
                    .andEqualTo("tax.create_user", dto.getCreateUser())
                    .andEqualTo("tax.update_user", dto.getUpdateUser())
                    .andEqualTo("tax.create_time", dto.getCreateTime())
                    .andEqualTo("tax.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("tax.tax_id", dto.getTaxId())
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andEqualTo("tax.tax_rate", dto.getTaxRate())
                    .andLessThanOrEqualTo("tax.tax_start_month", dto.getTaxStartMonth())
                    .andIsNull("tax.tax_end_month")
                    .andEqualTo("tax.create_user", dto.getCreateUser())
                    .andEqualTo("tax.update_user", dto.getUpdateUser())
                    .andEqualTo("tax.create_time", dto.getCreateTime())
                    .andEqualTo("tax.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("tax.tax_name, tax.tax_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<Tax> ret = taxDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaxDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(TaxDto dto) {
        Tax tax  =  mapper.map(dto, Tax.class);
        //税率、保険料率を新規追加する。
        int ret = taxDao.insertSelective(tax);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(TaxDto dto) {
        Tax tax  =  mapper.map(dto, Tax.class);
        //税率、保険料率を新規追加する。
        int ret = taxDao.insert(tax);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKey(TaxDto dto) {
        Tax tax  =  mapper.map(dto, Tax.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = taxDao.updateByPrimaryKey(tax);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKeySelective(TaxDto dto) {
        Tax tax  =  mapper.map(dto, Tax.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = taxDao.updateByPrimaryKeySelective(tax);
        return ret;
    }

    /**
     * 税率、保険料率を削除する。
     * @param taxId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer taxId) {
        //税率、保険料率を削除する。
        int ret = taxDao.deleteByPrimaryKey(taxId);
        return ret;
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaxDto> selectByNameAndLimit(TaxDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andLessThanOrEqualTo("tax.tax_lower_limit", dto.getTaxLowerLimit())
                    .andGreaterThan("tax.tax_upper_limit", dto.getTaxLowerLimit());
            whereCondition.or()
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andIsNull("tax.tax_upper_limit")
                    .andLessThanOrEqualTo("tax.tax_lower_limit", dto.getTaxLowerLimit());
            whereCondition.or()
                    .andEqualTo("tax.tax_name", dto.getTaxName())
                    .andIsNull("tax.tax_lower_limit")
                    .andIsNull("tax.tax_upper_limit");
        }
        whereCondition.setOrderByClause("tax.tax_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<Tax> ret = taxDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaxDto.class);
    }

    public void setTaxEndMonth(TaxDto taxDto){
        List<Tax> taxList = taxDao.selectByTaxName(taxDto.getTaxName());
        taxList.sort(descWorkStartTimeComp);
        DateTime lastEndMonth = null;
        for(Tax t : taxList){
            t.setTaxEndMonth(lastEndMonth == null? null : lastEndMonth);
            lastEndMonth = t.getTaxStartMonth().minusSeconds(1);
            taxDao.updateByPrimaryKeySelective(t);
        }
    }

}
