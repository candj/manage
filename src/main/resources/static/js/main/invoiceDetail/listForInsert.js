

			var colNames = [
                "ID",
				"請求ID",
				"作業員ID",
				"作業員",
				"開始期日",
                "終了期日",
				"人月",
				"人月単価</br>(円/月)",
				"月額金額</br>(円)",
				"作業</br>時間</br>(時間)",
				"作業時間</br>基準</br>(時間)",
				"精算</br>標準",
				"超過/控</br>除時間</br>(時間)",
				"超過単価</br>(円/時間)",
				"控除単価</br>(円/時間)",
				"超過金額</br>(円)",
				"交通費</br>(円)",
				"合計金額</br>(円)",
			];
			var colModel = [
                {
					name: "invoiceDetailId",
					index: "invoiceDetailId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "invoiceId",
					index: "invoiceId",
					width: 100,
					search : false,
					hidden: true,
					align: "right",
				},
                {
					name: "userId",
					index: "userId",
					width: 100,
					search : false,
					hidden : true,
					align: "left",
				},
				{
					name: "appUser.userName",
					index: "appUser.userName",
					width: 70,
					search : false,
					align: "left",
				},
                {
                                	name: "workStartDate",
                                	index: "workStartDate",
                                	width: 100,
                                	search : false,
                                	hidden : true,
                                	formatter : function(cellvalue, options, rowObject) {
                                    		if (cellvalue != null) {
                                    			var date = new Date(cellvalue);
                                    			return date.Format('yyyy-MM-dd');
                                    		} else {
                                    			return '';
                                    		}
                                    },
                                	align: "left",
                                },
                                {
                                	name: "workEndDate",
                                	index: "workEndDate",
                                	width: 100,
                                	search : false,
                                	hidden : true,
                                	formatter : function(cellvalue, options, rowObject) {
                                             if (cellvalue != null) {
                                                 var date = new Date(cellvalue);
                                                 return date.Format('yyyy-MM-dd');
                                             } else {
                                                 return '';
                                             }
                                    },
                                	align: "left",
                 },
				{
					name: "personMonth",
					index: "personMonth",
					width: 40,
					search : false,
					align: "right",
				},
				{
					name: "salaryPersonMonth",
					index: "salaryPersonMonth",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "amountMonth",
					index: "amountMonth",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
                {
					name: "workHour",
					index: "workHour",
					width: 50,
					search : false,
					align: "right",
				},
				{
					name: "invoice.ordering.offer.workTimeLowerLimit",
					index: "invoice.ordering.offer.workTimeLowerLimit",
					width: 70,
					search : false,
					formatter : function(cellvalue, options, rowObject) {

                    		if (cellvalue == null) {
                    			return '';
                    		}

                    		if (rowObject.invoice.ordering.offer.workTimeUpperLimit != null) {
                    			var lower = cellvalue;
                    			var upper = rowObject.invoice.ordering.offer.workTimeUpperLimit;

                    			return cellvalue + '~' + upper;
                    		} else {
                    			return '';
                    		}
                    },
					align: "left",
				},
                {
					name: "invoice.ordering.offer.workTimeCalculateStandard",
					index: "invoice.ordering.offer.workTimeCalculateStandard",
					width: 50,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                         if (cellvalue == '00') {
                              return '15分';
                         } else if (cellvalue == '01') {
                              return '30分';
                         }
                    },
					align: "left",
				},
				{
					name: "overTime",
					index: "overTime",
					width: 60,
					search : false,
					align: "right",
				},
				{
					name: "salaryExtra",
					index: "salaryExtra",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "salaryDeduction",
					index: "salaryDeduction",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "amountExtra",
					index: "amountExtra",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
                {
					name: "commutingCostAmount",
					index: "commutingCostAmount",
					width: 60,
					search : false,
					formatter : "integer",

					align: "right",
				},
                {
					name: "totalAmount",
					index: "totalAmount",
					width: 70,
					search : false,
					formatter : "integer",

					align: "right",
				},
			];

var postData = {
	invoiceId : $('#invoiceId').val(),
};

var shrinkToFit = false;

var lastsel2;

var onSelectRow = function(id) {
      if (id && id !== lastsel2) {
        jQuery('#grid').jqGrid('restoreRow', lastsel2);
        jQuery('#grid').jqGrid('editRow', id, true);
        //if(lastsel2){
        //   $.ajax({url:"api/workRest/"+lastsel2, type:"GET", success:function(e){
        //      if (e != null) {
        //         $("#grid").setCell(e.workId,"workAdmitStatus",e.workAdmitStatus);
        //      }
        //   }});
        //}
        lastsel2 = id;
      }
};

var editurl = "/api/invoiceDetailRest/editRows";

var gridComplete = function(){
    $("#grid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "scroll" });
}

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	