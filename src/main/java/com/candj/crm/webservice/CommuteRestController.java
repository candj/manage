package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.CommuteDto;
import com.candj.crm.dto.WorkDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.service.CommuteService;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import org.apache.commons.io.FileUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 交通費情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/commuteRest")
@RestController
public class CommuteRestController {
    @Autowired
    CommuteService commuteService;

    /**
     * 交通費を新規追加する。
     *
     * @param commuteId 交通費ID
     * @return 結果
     */
    @RequestMapping(value = "{commuteId}", method = RequestMethod.GET)
    public CommuteDto get(@PathVariable Integer commuteId) {
        //プライマリーキーで交通費を検索する。
        CommuteDto ret = commuteService.selectByPrimaryKey(commuteId);
        return ret;
    }

    /**
     * 交通費を新規追加する。
     *
     * @param commuteDto 交通費
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "{yearMonth}", method = RequestMethod.POST)
    public int insert(CommuteDto commuteDto,
                      @PathVariable YearMonth yearMonth,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) {
        //交通費を新規追加する。
        DateTime startOfMonth = DateTimeUtil.yearMonthToDateTime(yearMonth);
        commuteDto.setCommuteAdmitStatus("00");
        commuteDto.setCommuteApplyMonth(startOfMonth);
        //ファイルをアプロードしたら、保存する
        if (attachment != null) {
            try {
                String fileName = attachment.getOriginalFilename();
                commuteDto.setCommuteAttachmentName(fileName);
                String filePath = ResourceUtils.getURL("classpath:").getPath() + "attachment/";
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                attachment.transferTo(dest);
                commuteDto.setCommuteAttachmentPath(realPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int ret = commuteService.insertSelective(commuteDto);
        return ret;
    }

    /**
     * 交通費を変更する。
     *
     * @param commuteDto 交通費
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(CommuteDto commuteDto,
                      @RequestParam(name = "attachment", required = false) MultipartFile attachment,
                      Errors rrrors) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() != null && appUserSql.getUserType() == 0) {
            //ファイルをアプロードしたら、保存する
            if (attachment != null) {
                try {
                    String fileName = attachment.getOriginalFilename();
                    commuteDto.setCommuteAttachmentName(fileName);
                    if (commuteDto.getCommuteAttachmentPath() != null) {
                        File oldFile = new File(commuteDto.getCommuteAttachmentPath());
                        oldFile.delete();
                    }
                    String filePath = ResourceUtils.getURL("classpath:").getPath() + "attachment/";
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    String realPath = filePath + uuid + fileName;
                    File dest = new File(realPath);
                    //フォルダがいないの場合、新規する
                    if (!dest.getParentFile().exists()) {
                        dest.getParentFile().mkdirs();
                    }
                    attachment.transferTo(dest);
                    commuteDto.setCommuteAttachmentPath(realPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //プライマリーキーで交通費を更新する。
            return commuteService.updateByPrimaryKeySelective(commuteDto);
        } else {

            List<CommuteDto> commuteDtoList = commuteService.selectByCommuteDto(commuteDto);
            if (!commuteDtoList.isEmpty()) {
                CommuteDto commuteDto1 = commuteDtoList.get(0);
                if ("00".equals(commuteDto1.getCommuteAdmitStatus())) {
                    //ファイルをアプロードしたら、保存する
                    if (attachment != null) {
                        try {
                            String fileName = attachment.getOriginalFilename();
                            commuteDto.setCommuteAttachmentName(fileName);
                            if (commuteDto.getCommuteAttachmentPath() != null) {
                                File oldFile = new File(commuteDto.getCommuteAttachmentPath());
                                oldFile.delete();
                            }
                            String filePath = ResourceUtils.getURL("classpath:").getPath() + "attachment/";
                            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                            String realPath = filePath + uuid + fileName;
                            File dest = new File(realPath);
                            //フォルダがいないの場合、新規する
                            if (!dest.getParentFile().exists()) {
                                dest.getParentFile().mkdirs();
                            }
                            attachment.transferTo(dest);
                            commuteDto.setCommuteAttachmentPath(realPath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    //プライマリーキーで交通費を更新する。
                    return commuteService.updateByPrimaryKeySelective(commuteDto);
                } else {
                    throw new UncheckedLogicException("errors.error.work_error1");
                }
            }
        }

        return 1;
    }

    /**
     * 交通費を新規追加する。
     *
     * @param commuteId 交通費ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{commuteId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer commuteId) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() != null && appUserSql.getUserType() == 1) {
            CommuteDto commuteDto = commuteService.selectByPrimaryKey(commuteId);
            if ("01".equals(commuteDto.getCommuteAdmitStatus())) {
                throw new UncheckedLogicException("errors.error.work_error1");
            }
        }
        //交通費を削除する。
        return commuteService.deleteByPrimaryKey(commuteId);
    }

    /**
     * 交通費一覧画面を表示する。
     *
     * @param commuteDto 交通費
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<CommuteDto> list(CommuteDto commuteDto,
                                     @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                     @RequestParam(name = "userName", required = false) String userName,
                                     JqgridPageReq pageReq) {
        Page<CommuteDto> page = PageUtil.startPage(pageReq);
        DateTime month = new DateTime();
        if (yearMonth != null) {
            month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        }
        commuteDto.setCommuteApplyMonth(month);
        AppUser appUser = new AppUser();
        appUser.setUserName(userName);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUserSql = appAuthenticationUser.getAppUser();
        if (appUserSql.getUserType() == 1) {
            appUser.setUserName(appUserSql.getUserName());
            appUser.setUserId(appUserSql.getUserId());
        }

        commuteDto.setAppUser(appUser);
        //条件で交通費を検索する。（連携情報含む）
        List<CommuteDto> ret = commuteService.selectAllByExample(commuteDto);
        return PageUtil.resp(page);
    }

    /**
     * 交通費一覧画面を表示する。
     *
     * @param commuteDto 交通費
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<CommuteDto> listAll(CommuteDto commuteDto, JqgridPageReq pageReq) {
        Page<CommuteDto> page = PageUtil.startPage(pageReq);
        //条件で交通費を検索する。（連携情報含む）
        List<CommuteDto> ret = commuteService.selectAllByExample(commuteDto);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/download/{commuteId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(HttpServletRequest request,
                                           @PathVariable("commuteId") Integer commuteId,
                                           @RequestHeader("User-Agent") String userAgent,
                                           ModelMap model) throws Exception {
        // ファイルパース
        CommuteDto commuteDto = commuteService.selectByPrimaryKey(commuteId);
        String fileName = commuteDto.getCommuteAttachmentName();
        String path = commuteDto.getCommuteAttachmentPath();
        File file = new File(path);

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        // 内容サイズ
        builder.contentLength(file.length());
        // application-octet-stream ： 二进制流数据
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        // URLEncoder.encodeでファイルをエンコードする
        fileName = URLEncoder.encode(fileName, "UTF-8");
        if (userAgent.indexOf("MSIE") > 0) {
            // IE
            builder.header("Content-Disposition", "attachment; filename=" + fileName);
        } else {
            // FireFox、Chrome
            builder.header("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);
        }
        return builder.body(FileUtils.readFileToByteArray(file));
    }

    /**
     * 交通費の承認状態を変更する。
     *
     * @param commuteDto 交通費
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/editStatus", method = RequestMethod.POST)
    public int editStatus(CommuteDto commuteDto, Errors rrrors) {
        //承認状態を更新する
        int ret = commuteService.updateByPrimaryKeySelective(commuteDto);
        return ret;
    }
}
