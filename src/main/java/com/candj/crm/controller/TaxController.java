package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.TaxDto;
import com.candj.crm.service.TaxService;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/tax")
@Controller
public class TaxController {

    @Autowired
    TaxService taxService;

    /**
     * 税率、保険料率を新規追加する。
     * @param taxDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(TaxDto taxDto, ModelMap model) {
        return new ModelAndView("tax/insert", model);
    }

    /**
     * 税率、保険料率を編集する。
     * @param taxId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{taxId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer taxId, ModelMap model) {
        //プライマリーキーで税率、保険料率を検索する。
        TaxDto taxDto = taxService.selectByPrimaryKey(taxId);
        taxDto.setYearMonth(DateTimeUtil.dateTimeToString(taxDto.getTaxStartMonth()));
        model.addAttribute("taxDto", taxDto);
        return new ModelAndView("tax/edit", model);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param taxDto 税率、保険料率
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("taxDto") TaxDto taxDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("tax/list", model);
    }
}
