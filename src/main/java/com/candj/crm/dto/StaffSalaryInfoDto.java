package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * スタッフ基本給料
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffSalaryInfoDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer staffSalaryInfoId;

    /** 担当者ID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** スタッフ基本給料 */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 基本給 */
    @Digits(integer=10, fraction=0)
    private Integer salaryPay;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** 扶養人数 */
    @Digits(integer=10, fraction=0)
    private Integer SalaryBringUpNumber;

    /** スタッフ現在地 */
    @Length(max=10)
    private String userLocation;

    /** 社会保険入りFLAG */
    private boolean socialInsuranceFlag;

    /** 适用開始期日 */
    private org.joda.time.DateTime salaryStartDate;

    /** 适用終了期日 */
    private org.joda.time.DateTime salaryEndDate;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** タイプ */
    @Digits(integer=10, fraction=0)
    private Integer userType;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
