package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 支払いサイト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class PaySiteDto {
    /** 支払いサイトID */
    @Digits(integer=10, fraction=0)
    private Integer paySiteId;

    /** プロジェクト */
    private java.util.List<ProjectDto> project;

    /** 名称 */
    @Length(max=128)
    private String paySiteName;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
