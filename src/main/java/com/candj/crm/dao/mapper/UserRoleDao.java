package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.UserRole;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * ユーザーロール情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface UserRoleDao {
    /**
     * プライマリーキーでユーザーロールを検索する。
     * @param userRoleId ID
     * @return 結果
     */
    UserRole selectByPrimaryKey(Integer userRoleId);

    /**
     * プライマリーキーでユーザーロール検索する。（連携情報含む）
     * @param userRoleId ID
     * @return 結果
     */
    List<UserRole> selectAllByPrimaryKey(Integer userRoleId);

    /**
     * 条件でユーザーロールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<UserRole> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でユーザーロールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<UserRole> selectByExample(@Param("example") WhereCondition example);

    /**
     * ユーザーロールを新規追加する。
     * @param record ユーザーロール
     * @return 結果
     */
    int insertSelective(UserRole record);

    /**
     * ユーザーロールを新規追加する。
     * @param record ユーザーロール
     * @return 結果
     */
    int insert(UserRole record);

    /**
     * プライマリーキーでユーザーロールを更新する。
     * @param record ユーザーロール
     * @return 結果
     */
    int updateByPrimaryKey(UserRole record);

    /**
     * プライマリーキーでユーザーロールを更新する。
     * @param record ユーザーロール
     * @return 結果
     */
    int updateByPrimaryKeySelective(UserRole record);

    /**
     * ユーザーロールを更新する。
     * @param record ユーザーロール
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(UserRole record, @Param("example") WhereCondition example);

    /**
     * ユーザーロールを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") UserRole example);

    /**
     * ユーザーロールを削除する。
     * @param userRoleId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer userRoleId);
}
