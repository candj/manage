

			var colNames = [
				"ID",
				"スタッフID",
				"業務ID",
			];
			var colModel = [
				{
					name: "workId",
					index: "workId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "workStaffId",
					index: "workStaffId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "workBusinessId",
					index: "workBusinessId",
					width: 100,
					search : false,
					align: "right",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

