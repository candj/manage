package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

@Getter
@Setter
public class ReceiveCompanyDto {
    /** 会社ID */
    private Integer receiveCompanyId;

    /** 会社区分 */
    private Byte receiveCompanyType;

    /** 会社名 */
    private String receiveCompanyName;

    /** 会社名（略称） */
    private String receiveCompanyNameAbbr;

    /** 銀行 */
    private String companyBank;

    /** 銀行支店名 */
    private String companyBankName;

    /** 預金種類 */
    private String companyBankAccountType;

    /** 口座番号 */
    private Integer companyBankAccountNumber;

    /** 口座名義 */
    private String companyBankAccountName;

    /** 住所 */
    private String receiveCompanyAdress;

    /** 郵便番号 */
    private String receiveCompanyPostCode;

    /** 電話番号 */
    private String receiveCompanyTelephoneNumber;

    /** FAX番号 */
    private String receiveCompanyFaxNumber;

    /** ステータス */
    private boolean receiveCompanyDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
