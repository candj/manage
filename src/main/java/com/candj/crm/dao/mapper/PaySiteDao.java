package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.PaySite;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 支払いサイト情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface PaySiteDao {
    /**
     * プライマリーキーで支払いサイトを検索する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    PaySite selectByPrimaryKey(Integer paySiteId);

    /**
     * プライマリーキーで支払いサイト検索する。（連携情報含む）
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    List<PaySite> selectAllByPrimaryKey(Integer paySiteId);

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PaySite> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PaySite> selectByMyExample(@Param("example") WhereCondition example);

    /**
     * 条件で支払いサイトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<PaySite> selectByExample(@Param("example") WhereCondition example);

    /**
     * 支払いサイトを新規追加する。
     * @param record 支払いサイト
     * @return 結果
     */
    int insertSelective(PaySite record);

    /**
     * 支払いサイトを新規追加する。
     * @param record 支払いサイト
     * @return 結果
     */
    int insert(PaySite record);

    /**
     * プライマリーキーで支払いサイトを更新する。
     * @param record 支払いサイト
     * @return 結果
     */
    int updateByPrimaryKey(PaySite record);

    /**
     * プライマリーキーで支払いサイトを更新する。
     * @param record 支払いサイト
     * @return 結果
     */
    int updateByPrimaryKeySelective(PaySite record);

    /**
     * 支払いサイトを更新する。
     * @param record 支払いサイト
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(PaySite record, @Param("example") WhereCondition example);

    /**
     * 支払いサイトを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") PaySite example);

    /**
     * 支払いサイトを削除する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer paySiteId);
}
