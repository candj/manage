package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.SalaryDto;
import com.candj.crm.dto.WorkDurationDto;
import com.candj.crm.service.AppUserService;
import com.candj.crm.service.SalaryService;
import com.candj.crm.service.SendMailService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 給料情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/api/salaryRest")
@RestController
public class SalaryRestController {
    @Autowired
    SalaryService salaryService;

    @Autowired
    AppUserService appUserService;

    @Autowired
    SendMailService sendMailService;

    /**
     * 給料を新規追加する。
     *
     * @param salaryId ID
     * @return 結果
     */
    @RequestMapping(value = "{salaryId}", method = RequestMethod.GET)
    public SalaryDto get(@PathVariable Integer salaryId) {
        //プライマリーキーで給料を検索する。
        SalaryDto ret = salaryService.selectByPrimaryKey(salaryId);
        return ret;
    }

    /**
     * 給料を新規追加する。
     *
     * @param salaryDto 給料
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody SalaryDto salaryDto,
                      Errors rrrors) {
        //給料を新規追加する。
        DateTime month = DateTimeUtil.StringToDateTime(salaryDto.getYearMonth());
        salaryDto.setSalaryMonth(month);
        int ret = salaryService.insertSelective(salaryDto);
        return ret;
    }

    /**
     * 給料を変更する。
     *
     * @param salaryDto 給料
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody SalaryDto salaryDto, Errors rrrors) throws Exception{
        //プライマリーキーで給料を更新する。
        int ret = salaryService.updateByPrimaryKeySelective(salaryDto);
        if(salaryDto.getStatus() == 1){
            AppUserDto appUserDto = appUserService.selectByPrimaryKey(salaryDto.getUserId());
            sendMailService.sendMail(appUserDto.getUserEmail(), salaryDto.getSalaryId(), vc -> {
                vc.put("name", appUserDto.getUserName());
            });
        }
        return ret;
    }

    /**
     * 給料を新規追加する。
     *
     * @param salaryId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{salaryId}/{yearMonth}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer salaryId, @PathVariable YearMonth yearMonth) {
        //給料を削除する。
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        int ret = salaryService.deleteByPrimaryKey(salaryId);
        return ret;
    }

    /**
     * 給料一覧画面を表示する。
     *
     * @param salaryDto 給料
     * @param pageReq   改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<SalaryDto> list(SalaryDto salaryDto,
                                    @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                    @RequestParam(name = "userName", required = false) String userName,
                                    JqgridPageReq pageReq) {
        Page<SalaryDto> page = PageUtil.startPage(pageReq);
        //条件で給料を検索する。（連携情報含む）
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        salaryDto.setSalaryMonth(month);
        List<SalaryDto> ret = salaryService.selectAllByExample(salaryDto, userName);
        return PageUtil.resp(page);
    }

    /**
     * 給料一覧画面を表示する。
     *
     * @param salaryDto 給料
     * @param pageReq   改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<SalaryDto> listAll(SalaryDto salaryDto,
                                       @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                       @RequestParam(name = "userName", required = false) String userName,
                                       JqgridPageReq pageReq) {
        Page<SalaryDto> page = PageUtil.startPage(pageReq);
        //条件で給料を検索する。（連携情報含む）
        List<SalaryDto> ret = salaryService.selectAllByExample(salaryDto, userName);
        return PageUtil.resp(page);
    }

    /**
     * 給料一覧画面を表示する。
     *
     * @param appUserDto ユーザー
     * @param pageReq    改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/salaryList", method = RequestMethod.POST)
    public PageInfo<AppUserDto> salaryList(AppUserDto appUserDto,
                                           @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                           JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        MonthDto monthDto = new MonthDto();
        monthDto.setUserId(appUserDto.getUserId());
        monthDto.setStartOfMonth(month);
        List<AppUserDto> appUserDtoList = appUserService.selectStaffSalaryByYearMonth(monthDto, month);

        return PageUtil.resp(page);
    }
}
