package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 請求詳細
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class InvoiceDetailDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer invoiceDetailId;

    /** 請求ID */
    @Digits(integer=10, fraction=0)
    private Integer invoiceId;

    /** 請求詳細 */
    private com.candj.crm.mapper.model.Invoice invoice;

    /** 開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 担当者ID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** 請求詳細 */
    private com.candj.crm.mapper.model.AppUser appUser;

    private AppUserDto appUserDto;

    /** 人月 */
    @Digits(integer=8, fraction=2)
    private java.math.BigDecimal personMonth;

    /** 人月単価 */
    @Digits(integer=10, fraction=0)
    private Integer salaryPersonMonth;

    /** 月額金額 */
    @Digits(integer=10, fraction=0)
    private Integer amountMonth;

    /** 残業 */
    @Length(max=10)
    private String overTimeWork;

    /** 時間 */
    @Digits(integer=9, fraction=1)
    private java.math.BigDecimal overTime;

    /** 超過単価 */
    @Digits(integer=10, fraction=0)
    private Integer salaryExtra;

    /** 控除単価 */
    @Digits(integer=10, fraction=0)
    private Integer salaryDeduction;

    /** 超過金額 */
    @Digits(integer=10, fraction=0)
    private Integer amountExtra;

    /** 総金額 */
    @Digits(integer=10, fraction=0)
    private Integer totalAmount;

    /** 摘要 */
    @Length(max=255)
    private String remark;

    /** 作業時間 */
    @Digits(integer=9, fraction=1)
    private java.math.BigDecimal workHour;

    /** 交通費 */
    @Digits(integer=10, fraction=0)
    private Integer commutingCostAmount;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    private String CommutingCostString;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
