

			var colNames = [
				"一戸建てID",
				"建物ID",
				"リフォーム実施内容",
				"建物現況",
				"中古标志",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "singleHouseId",
					index: "singleHouseId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "singleHouseBulidingId",
					index: "singleHouseBulidingId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "remodeDetail",
					index: "remodeDetail",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "singleHouseSystemConfigId",
					index: "singleHouseSystemConfigId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "singleHouseUsed",
					index: "singleHouseUsed",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	