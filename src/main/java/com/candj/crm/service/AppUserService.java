package com.candj.crm.service;

import com.candj.crm.dao.mapper.AppUserDao;
import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.MonthDto;
import com.candj.crm.dto.WorkDurationDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Salary;
import com.candj.crm.mapper.model.Work;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class AppUserService {
    @Autowired
    AppUserDao appUserDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでユーザーを検索する。
     *
     * @param userId ユーザーID
     * @return 結果
     */
    public AppUserDto selectByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザーを検索する。
        AppUser ret = appUserDao.selectByPrimaryKey(userId);
        return DozerHelper.map(mapper, ret, AppUserDto.class);
    }

    /**
     * プライマリーキーでユーザーを検索する。（連携情報含む）
     *
     * @param userId ユーザーID
     * @return 結果
     */
    public List<AppUserDto> selectAllByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザー検索する。（連携情報含む）
        List<AppUser> ret = appUserDao.selectAllByPrimaryKey(userId);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }


    /**
     * 条件でユーザーを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("app_user.user_id", dto.getUserId())
                    .andEqualTo("app_user.company_id", dto.getCompanyId())
                    .andEqualTo("app_user.user_type", dto.getUserType())
                    .andLessThan("app_user.user_type", 1)
                    .andLike("app_user.user_name", dto.getUserName())
                    .andEqualTo("app_user.user_sex", dto.getUserSex())
                    .andEqualTo("app_user.birth_date", dto.getBirthDate())
                    .andEqualTo("app_user.user_department", dto.getUserDepartment())
                    .andEqualTo("app_user.user_email", dto.getUserEmail())
                    .andEqualTo("app_user.user_telephone_number", dto.getUserTelephoneNumber())
                    .andEqualTo("app_user.employment_date", dto.getEmploymentDate())
                    .andEqualTo("app_user.employment_status", dto.getEmploymentStatus())
                    .andEqualTo("app_user.password", dto.getPassword())
                    .andEqualTo("app_user.user_delete_flag", 1)
                    .andEqualTo("app_user.create_user", dto.getCreateUser())
                    .andEqualTo("app_user.update_user", dto.getUpdateUser())
                    .andEqualTo("app_user.create_time", dto.getCreateTime())
                    .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        for (AppUser appUser : appUsers) {
            appUser.setPassword("******");
        }
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectStaffByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("app_user.company_id", dto.getCompanyId())
                .andLessThan("app_user.user_type", 2);
        whereCondition.or()
                .andEqualTo("app_user.company_id", dto.getCompanyId())
                .andEqualTo("app_user.user_type", 3);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectThisStaffByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andLessThan("app_user.user_type", 2);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectAllStaffByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andLessThan("app_user.user_type", 2);
        whereCondition.or()
                .andEqualTo("app_user.user_type", 3);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectThisCompanyStaff(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("company.company_type", 0)
                .andLessThan("app_user.user_type", 2);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectAllByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("app_user.user_id", dto.getUserId())
                    .andEqualTo("app_user.company_id", dto.getCompanyId())
                    .andEqualTo("app_user.user_type", dto.getUserType())
                    .andLike("app_user.user_name", dto.getUserName())
                    .andEqualTo("app_user.user_sex", dto.getUserSex())
                    .andEqualTo("app_user.birth_date", dto.getBirthDate())
                    .andEqualTo("app_user.user_department", dto.getUserDepartment())
                    .andEqualTo("app_user.user_email", dto.getUserEmail())
                    .andEqualTo("app_user.user_telephone_number", dto.getUserTelephoneNumber())
                    .andEqualTo("app_user.employment_date", dto.getEmploymentDate())
                    .andEqualTo("app_user.employment_status", dto.getEmploymentStatus())
                    .andEqualTo("app_user.password", dto.getPassword())
                    .andEqualTo("app_user.user_delete_flag", 1)
                    .andEqualTo("app_user.create_user", dto.getCreateUser())
                    .andEqualTo("app_user.update_user", dto.getUpdateUser())
                    .andEqualTo("app_user.create_time", dto.getCreateTime())
                    .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectStaffAllByExample(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("app_user.user_id", dto.getUserId())
                    .andEqualTo("app_user.company_id", dto.getCompanyId())
                    .andEqualTo("app_user.user_type", 1)
                    .andLike("app_user.user_name", dto.getUserName())
                    .andEqualTo("app_user.user_sex", dto.getUserSex())
                    .andEqualTo("app_user.birth_date", dto.getBirthDate())
                    .andEqualTo("app_user.user_department", dto.getUserDepartment())
                    .andEqualTo("app_user.user_email", dto.getUserEmail())
                    .andEqualTo("app_user.user_telephone_number", dto.getUserTelephoneNumber())
                    .andEqualTo("app_user.employment_date", dto.getEmploymentDate())
                    .andEqualTo("app_user.employment_status", dto.getEmploymentStatus())
                    .andEqualTo("app_user.password", dto.getPassword())
                    .andEqualTo("app_user.user_delete_flag", 1)
                    .andEqualTo("app_user.create_user", dto.getCreateUser())
                    .andEqualTo("app_user.update_user", dto.getUpdateUser())
                    .andEqualTo("app_user.create_time", dto.getCreateTime())
                    .andEqualTo("app_user.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("app_user.user_id", dto.getUserId())
                    .andEqualTo("app_user.company_id", dto.getCompanyId())
                    .andGreaterThanOrEqualTo("app_user.user_type", 3)
                    .andLessThanOrEqualTo("app_user.user_type", 4)
                    .andEqualTo("app_user.user_name", dto.getUserName())
                    .andEqualTo("app_user.user_sex", dto.getUserSex())
                    .andEqualTo("app_user.birth_date", dto.getBirthDate())
                    .andEqualTo("app_user.user_department", dto.getUserDepartment())
                    .andEqualTo("app_user.user_email", dto.getUserEmail())
                    .andEqualTo("app_user.user_telephone_number", dto.getUserTelephoneNumber())
                    .andEqualTo("app_user.employment_date", dto.getEmploymentDate())
                    .andEqualTo("app_user.employment_status", dto.getEmploymentStatus())
                    .andEqualTo("app_user.password", dto.getPassword())
                    .andEqualTo("app_user.user_delete_flag", 1)
                    .andEqualTo("app_user.create_user", dto.getCreateUser())
                    .andEqualTo("app_user.update_user", dto.getUpdateUser())
                    .andEqualTo("app_user.create_time", dto.getCreateTime())
                    .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * ユーザーを新規追加する。
     *
     * @param dto ユーザー
     * @return 結果
     */
    public int insertSelective(AppUserDto dto) {
        AppUser appUser = mapper.map(dto, AppUser.class);
        //ユーザーを新規追加する。
        int ret = appUserDao.insertSelective(appUser);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     *
     * @param dto ユーザー
     * @return 結果
     */
    public int insert(AppUserDto dto) {
        AppUser appUser = mapper.map(dto, AppUser.class);
        //ユーザーを新規追加する。
        int ret = appUserDao.insert(appUser);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     *
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKey(AppUserDto dto) {
        AppUser appUser = mapper.map(dto, AppUser.class);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserDao.updateByPrimaryKey(appUser);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     *
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKeySelective(AppUserDto dto) {
        AppUser appUser = mapper.map(dto, AppUser.class);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserDao.updateByPrimaryKeySelective(appUser);
        return ret;
    }

    /**
     * ユーザーを削除する。
     *
     * @param userId ユーザーID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer userId) {
        //ユーザーを削除する。
        int ret = appUserDao.deleteByPrimaryKey(userId);
        return ret;
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectStaffWorkByYearMonth(WorkDurationDto dto, DateTime month) {
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> staff = appUserDao.selectStaffWorkByYearMonth(dto);
        staff.forEach(s -> {
            s.setRegister(false);
            List<Work> work = new ArrayList<Work>();
            for (Work w : s.getWork()) {
                if (w.getWorkId() != null) {
                    if (w.getWorkMonth().getYear() == month.getYear() && w.getWorkMonth().getMonthOfYear() == month.getMonthOfYear()) {
                        work.add(w);
                        s.setRegister(true);
                    }
                }
            }
            s.setWork(work);
        });
        return DozerHelper.mapList(mapper, staff, AppUserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectStaffSalaryByYearMonth(MonthDto dto, DateTime month) {
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> staff = appUserDao.selectStaffSalaryByYearMonth(dto);
        staff.forEach(s -> {
            List<Salary> salaries = new ArrayList<Salary>();
            for (Salary sa : s.getSalaryList()) {
                if (sa.getSalaryId() != null) {
                    if (sa.getSalaryMonth().getYear() == month.getYear() && sa.getSalaryMonth().getMonthOfYear() == month.getMonthOfYear()) {
                        salaries.add(sa);
                        s.setSalary(sa);
                    }
                }
            }
            s.setSalaryList(salaries);
            s.setMonth(month);
        });
        if (!staff.isEmpty()) {
            DateTime dateTime = new DateTime();
            DateTime nowTime = dateTime.plusDays(2);
            for (AppUser appUserData : staff) {
                if (appUserData.getSalary() != null && appUserData.getSalary().getPayDate() == null) {
                    appUserData.setPlayPayDateISNear(nowTime.getMillis() >= appUserData.getSalary().getPayDate().getMillis());
                }
            }
        }
        return DozerHelper.mapList(mapper, staff, AppUserDto.class);
    }

    /**
     * スタッフを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectStaff(AppUserDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andLessThan("app_user.user_type", 2);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

    /**
     * スタッフを検索する。（連携情報含む）
     *
     *
     * @return 結果
     */
    public List<AppUserDto> selectNoCompanyStaff() {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andIsNull("app_user.company_id")
                .andEqualTo("app_user.user_type", 4);
        //条件でスタッフを検索する。（連携情報含む）)
        List<AppUser> appUsers = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, appUsers, AppUserDto.class);
    }

}
