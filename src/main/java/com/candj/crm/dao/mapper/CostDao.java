package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.Cost;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 費用情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface CostDao {
    /**
     * プライマリーキーで費用を検索する。
     * @param costId ID
     * @return 結果
     */
    Cost selectByPrimaryKey(Integer costId);

    /**
     * プライマリーキーで費用検索する。（連携情報含む）
     * @param costId ID
     * @return 結果
     */
    List<Cost> selectAllByPrimaryKey(Integer costId);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Cost> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で費用を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Cost> selectByExample(@Param("example") WhereCondition example);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insertSelective(Cost record);

    /**
     * 費用を新規追加する。
     * @param record 費用
     * @return 結果
     */
    int insert(Cost record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKey(Cost record);

    /**
     * プライマリーキーで費用を更新する。
     * @param record 費用
     * @return 結果
     */
    int updateByPrimaryKeySelective(Cost record);

    /**
     * 費用を更新する。
     * @param record 費用
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Cost record, @Param("example") WhereCondition example);

    /**
     * 費用を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Cost example);

    /**
     * 費用を削除する。
     * @param costId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer costId);
}
