var colNames = [
    "税率ID",
    "等級",
    "月額",
    "報酬月額下限",
    "報酬月額上限",
    "健康保険</br>（全額）",
    "健康保険</br>（折半額）",
    "健康保険＋介</br>護保険（全額）",
    "健康保険＋介</br>護保険（折半額）",
    "厚生年金保険</br>（全額）",
    "厚生年金保険</br>（折半額）"
];
var colModel = [
    {
        name: "socialInsuranceId",
        index: "socialInsuranceId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "grade",
        index: "grade",
        width: 100,
        search: false,
        key: true,
        align: "left",
    },
    {
        name: "monthly",
        index: "monthly",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "minMonthlyPayment",
        index: "minMonthlyPayment",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "maxMonthlyPayment",
        index: "maxMonthlyPayment",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "healthInsuranceFull",
        index: "healthInsuranceFull",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "healthInsuranceHalf",
        index: "healthInsuranceHalf",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "healthAndNursingCareFull",
        index: "healthAndNursingCareFull",
        width: 120,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "healthAndNursingCareHalf",
        index: "healthAndNursingCareHalf",
        width: 130,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "welfarePensionFull",
        index: "welfarePensionFull",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    },
    {
        name: "welfarePensionHalf",
        index: "welfarePensionHalf",
        width: 100,
        search: false,
        align: "left",
        formatter : "integer"
    }
];

var postData = {
    yearMonth: $('#yearMonth').val(),
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData);
});	
	