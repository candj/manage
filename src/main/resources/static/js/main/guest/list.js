var colNames = [
    "ID",
    "ゲスト名前",
    "会社",
    "ゲスト種類",
    "イーメールアドレス",
    "電話番号",
    "登録時間",
    "ステータス",
];
var colModel = [
	{
		name: "userId",
		index: "userId",
		width: 100,
		search: false,
		key: true,
		hidden: true,
		align: "right",
	},
    {
        name: "userName",
        index: "userName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "company.companyName",
        index: "company.companyName",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "guestType",
        index: "guestType",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userEmail",
        index: "userEmail",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "userTelephoneNumber",
        index: "userTelephoneNumber",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "createTime",
        index: "createTime",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        hidden: true,
        align: "left",
    },
    {
        name: "status",
        index: "status",
        width: 100,
        search: false,
        hidden: true,
        align: "left",
    },
];

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel);
});	
	

