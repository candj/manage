package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 注文
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OrderDto {
    /** 注文ID */
    @Digits(integer=10, fraction=0)
    private Integer orderId;

    /** 見積 ID */
    @Digits(integer=10, fraction=0)
    private Integer offerId;

    /** 注文番号 */
    @Length(max=64)
    private String orderNumber;

    /** 日付 */
    private org.joda.time.DateTime orderDate;

    /** ステータス */
    @Length(max=2)
    private String orderStatus;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
