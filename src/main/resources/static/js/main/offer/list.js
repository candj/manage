var context_path = $('meta[name=basepath]').attr('content');

			var colNames = [
				"見積 ID",
				"注文 ID",
				"注文番号",
				"発送</br>ステータス",
				"作業開始期日",
				"作業終了期日",
				"作業時間基準",
				"作業時間</br>精算標準",
				"支払いサイト",
				"精算方法",
				"税込み合計</br>金額(円)",
				"税抜き合計</br>金額(円)",
				"消費税(円)",
				"見積元</br>会社名",
				"見積先</br>会社名",
				"種類",
				"業務名",
				"納入場所",
				"有効</br>期限",
				"見積番号",
				"プロジェクトID",
                "プロジェクト名",
			];
			var colModel = [
				{
					name: "offerId",
					index: "offerId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
                {
					name: "orderingDto.orderId",
					index: "orderingDto.orderId",
					width: 100,
					search : false,
                    hidden: true,
					align: "right",
				},
                {
					name: "orderingDto.orderNumber",
					index: "orderingDto.orderNumber",
					width: 95,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                      if(cellvalue != null && cellvalue != undefined){
                        var selectHtml = '<a href="javascript:void(0);" onclick="openOrderDetail('
                                         + rowObject.orderingDto.orderId
                                         + ')">'
                                         + cellvalue
                                         + '</a>';
                      }else {
                        selectHtml = '<label style="" data-url="'
							+ context_path
							+ 'ordering/insert/'
							+ rowObject.offerId
							+ '" class="emptyCell"><i class="fa fa-plus"> 追加</i></label>'
                      }
                        return selectHtml;
                    },
					align: "center",
				},
				{
					name: "offerStatus",
					index: "offerStatus",
					width: 80,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                         if (cellvalue == '00') {
                              return '<b style="color:red">×<b>';
                         } else if (cellvalue == '01') {
                              return '○';
                         }
                    },
					align: "center",
				},

				{
					name: "workStartDate",
					index: "workStartDate",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue != null) {
							var date = new Date(cellvalue);
							return date.Format('yyyy-MM-dd');
						} else {
							return '';
						}
					},
					align: "left",
				},

				{
					name: "workEndDate",
					index: "workEndDate",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue != null) {
							var date = new Date(cellvalue);
							return date.Format('yyyy-MM-dd');
						} else {
							return '';
						}
					},
					align: "left",
				},

				{
					name: "workTimeLowerLimit",
					index: "workTimeLowerLimit",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {

						if (cellvalue == null) {
							return '';
						}

						if (rowObject.workTimeUpperLimit != null) {
							var lower = cellvalue;
							var upper = rowObject.workTimeUpperLimit;

							return cellvalue + 'h~' + upper + 'h';
						} else {
							return '';
						}
					},
					align: "left",
				},
				{
					name: "workTimeCalculateStandard",
					index: "workTimeCalculateStandard",
					width: 80,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue == '00') {
							return '15分';
						} else if (cellvalue == '01') {
							return '30分';
						}
					},
					align: "left",
				},

				{
					name: "paySite.paySiteName",
					index: "paySite.paySiteName",
					width: 130,
					search : false,
					align: "left",
				},

				{
					name: "afterPaymentPattern",
					index: "afterPaymentPattern",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue == '00') {
							return '精算なし';
						} else if (cellvalue == '01') {
							return '定額';
						} else if (cellvalue == '10') {
							return '時間幅上下割';
						} else if (cellvalue == '11') {
							return '時間幅中間割';
						}
					},
					align: "left",
				},
                {
					name: "offerTotalAmount",
					index: "offerTotalAmount",
					width: 110,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "offerAmountWithoutTax",
					index: "offerAmountWithoutTax",
					width: 110,
					search : false,
					formatter : "integer",

					align: "right",
				},
				{
					name: "offerConsumptionTax",
					index: "offerConsumptionTax",
					width: 80,
					search : false,
					formatter : "integer",

					align: "right",
				},

				{
					name: "sendCompany.sendCompanyNameAbbr",
					index: "sendCompany.sendCompanyNameAbbr",
					width: 60,
					search : false,
					align: "left",
				},
				{
					name: "receiveCompany.receiveCompanyNameAbbr",
					index: "receiveCompany.receiveCompanyNameAbbr",
					width: 60,
					search : false,
					align: "left",
				},
                {
					name: "offerType",
					index: "offerType",
					width: 70,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue == '00') {
							return '派遣';
						} else if (cellvalue == '01') {
							return '請負';
						} else if (cellvalue == '10') {
							return '社内開発';
						}
					},
					align: "left",
				},
				{
					name: "workName",
					index: "workName",
					width: 120,
					search : false,
					align: "left",
				},

				{
					name: "workPlace",
					index: "workPlace",
					width: 80,
					search : false,
					align: "left",
				},

				{
					name: "offerPeriodOfValidity",
					index: "offerPeriodOfValidity",
					width: 60,
					search : false,
					align: "right",
				},

				{
					name: "offerNumber",
					index: "offerNumber",
					width: 110,
					search : false,
					align: "left",
				},

				{
                    name: "project.projectId",
                    index: "project.projectId",
                    width: 100,
                    search : false,
                    hidden : true,
                    align: "left",
                },
				{
                    name: "project.projectName",
                    index: "project.projectName",
                    width: 150,
                    search : false,
                    align: "left",
                    hidden : true,
                    formatter : function(cellvalue, options, rowObject) {
                      if(cellvalue != null && cellvalue != undefined){
                        var selectHtml = '<a href="javascript:void(0);" onclick="openProjectDetail('
                                         + rowObject.project.projectId
                                         + ')">'
                                         + cellvalue
                                         + '</a>';
                      }else {
                        var selectHtml = cellvalue;
                      }
                        return selectHtml;
                    },
                },
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
	projectName : $('#projectName').val(),
};


	function openProjectDetail(projectId) {
		var width = '900px'
		var height = '90%'
		var url = "/project/edit/" + projectId;
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ url ],
		})
		return false;

	};

    function openOrderDetail(orderId) {
		var width = '900px'
		var height = '90%'
		var url = "/ordering/edit/" + orderId;
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ url ],
		})
		return false;

	};

var shrinkToFit = false;

var gridComplete = function(){
    $("#grid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "scroll" });

    $('.emptyCell').each(
			function(index, element) {
				$(this).closest('td').addClass('btn-update emptyTdCell').attr(
						'data-url', $(this).attr('data-url'));
			})
	$('.btn-update').click(function() {
		var width = '900px'
		var height = '90%'
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ $(this).attr("data-url") ],
		})
		return false;

	});
}

$(document).ready(function(){

	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete);
});	
	