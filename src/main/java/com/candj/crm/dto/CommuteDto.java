package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 交通費
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class CommuteDto {

    /** 交通費ID */
    @Digits(integer=10, fraction=0)
    private Integer commuteId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** スタッフ */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 申請月 */
    private org.joda.time.DateTime commuteApplyMonth;

    /** 乗車日 */
    private org.joda.time.DateTime commuteDate;

    /** 乗車駅 */
    @Length(max=64)
    private String commuteGetOnStation;

    /** 降車駅 */
    @Length(max=64)
    private String commuteGetOffStation;

    /** 金額 */
    @Digits(integer=10, fraction=0)
    private Integer commuteAmount;

    /** コメント */
    @Length(max=255)
    private String commuteRemark;

    /** 添付ファイルパス */
    @Length(max=128)
    private String commuteAttachmentPath;

    /** 添付ファイル名 */
    @Length(max=128)
    private String commuteAttachmentName;

    /** 処理状態 */
    @Length(max=2)
    private String commuteAdmitStatus;

    /** ステータス */
    @Digits(integer=3, fraction=0)
    private Byte commuteStatus;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** タイプ */
    @Digits(integer=10, fraction=0)
    private Integer userType;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
