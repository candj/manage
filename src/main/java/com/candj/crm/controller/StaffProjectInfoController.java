package com.candj.crm.controller;

import com.candj.crm.dto.StaffProjectInfoDto;
import com.candj.crm.service.StaffProjectInfoService;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフプロジェクト情報情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staffProjectInfo")
@Controller
public class StaffProjectInfoController {

    @Autowired
    StaffProjectInfoService staffProjectInfoService;

    /**
     * スタッフプロジェクト情報を新規追加する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffProjectInfoDto staffProjectInfoDto, ModelMap model) {
        return new ModelAndView("staffProjectInfo/insert", model);
    }

    /**
     * スタッフプロジェクト情報を新規追加(確認)する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffProjectInfoDto staffProjectInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffProjectInfo/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffProjectInfo/insertConfirm", model);
    }

    /**
     * スタッフプロジェクト情報を新規追加(終了)する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffProjectInfoDto staffProjectInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffProjectInfo/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフプロジェクト情報を新規追加する。
        int ret = staffProjectInfoService.insertSelective(staffProjectInfoDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staffProjectInfo/insertFinish", model);
    }

    /**
     * スタッフプロジェクト情報を編集する。
     * @param staffProjectInfoId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{staffProjectInfoId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer staffProjectInfoId, ModelMap model) {
        //プライマリーキーでスタッフプロジェクト情報を検索する。
        StaffProjectInfoDto staffProjectInfoDto = staffProjectInfoService.selectByPrimaryKey(staffProjectInfoId);
        model.addAttribute("staffProjectInfoDto", staffProjectInfoDto);
        return new ModelAndView("staffProjectInfo/edit", model);
    }

    /**
     * スタッフプロジェクト情報を編集(確認)する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffProjectInfoDto staffProjectInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffProjectInfo/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffProjectInfo/editConfirm", model);
    }

    /**
     * スタッフプロジェクト情報を編集(終了)する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffProjectInfoDto staffProjectInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffProjectInfo/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフプロジェクト情報を更新する。
        int ret = staffProjectInfoService.updateByPrimaryKeySelective(staffProjectInfoDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staffProjectInfo/editFinish", model);
    }

    /**
     * スタッフプロジェクト情報一覧画面を表示する。
     * @param staffProjectInfoDto スタッフプロジェクト情報
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffProjectInfoDto") StaffProjectInfoDto staffProjectInfoDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("staffProjectInfo/list", model);
    }
}
