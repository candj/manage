var colNames = [
    "請求ID",
    "作成</br>ステータス",
    "支払</br>ステータス",
    "支払期限日",
    "開始時間",
    "終了時間",
    "請求日付",
    "作業時間基準",
    "作業時間</br>精算標準",
    "支払いサイト",
    "精算方法",
    "税込み合計</br>金額(円)",
    "税抜き合計</br>金額(円)",
    "消費税(円)",
    "請求元</br>会社名",
    "請求先</br>会社名",
    "種類",
    "請求番号",
    "注文ID",
    "注文番号",
];
var colModel = [
    {
        name: "invoiceId",
        index: "invoiceId",
        width: 100,
        search: false,
        key: true,
        hidden: true,
        align: "right",
    },
    {
        name: "invoiceStatus",
        index: "invoiceStatus",
        width: 80,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '<b style="color:red">×<b>';
            } else if (cellvalue == '01') {
                return '○';
            }
        },
        align: "center",
    },
    {
        name: "invoicePayStatus",
        index: "invoicePayStatus",
        width: 80,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '<b style="color:red">×<b>';
            } else if (cellvalue == '01') {
                return '○';
            }
        },
        align: "center",
    },
    {
        name: "invoiceDeliveryDate",
        index: "invoiceDeliveryDate",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
        name: "workStartDate",
        index: "workStartDate",
        width: 100,
        search : false,
        formatter : function(cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },

    {
        name: "workEndDate",
        index: "workEndDate",
        width: 100,
        search : false,
        formatter : function(cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },

    {
        name: "invoiceDate",
        index: "invoiceDate",
        width: 100,
        search: false,
        hidden: true,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
        name: "ordering.offer.workTimeLowerLimit",
        index: "ordering.offer.workTimeLowerLimit",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == null) {
                return '';
            }
            if (rowObject.ordering.offer.workTimeUpperLimit != null) {
                var lower = cellvalue;
                var upper = rowObject.ordering.offer.workTimeUpperLimit;
                return cellvalue + 'h~' + upper + 'h';
            } else {
                return '';
            }
        },
        align: "left",
    },
    {
					name: "ordering.offer.workTimeCalculateStandard",
					index: "ordering.offer.workTimeCalculateStandard",
					width: 80,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue == '00') {
							return '15分';
						} else if (cellvalue == '01') {
							return '30分';
						}
					},
					align: "left",
	},
    {
        name: "ordering.offer.paySite.paySiteName",
        index: "ordering.offer.paySite.paySiteName",
        width: 130,
        search: false,
        align: "left",
    },
    {
        name: "ordering.offer.afterPaymentPattern",
        index: "ordering.offer.afterPaymentPattern",
        width: 100,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '精算なし';
            } else if (cellvalue == '01') {
                return '定額';
            } else if (cellvalue == '10') {
                return '時間幅上下割';
            }else if (cellvalue == '11') {
                return '時間幅中間割';
            }
        },
        align: "left",
    },
    {
        name: "invoiceAmountWithTax",
        index: "invoiceAmountWithTax",
        width: 110,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "invoiceAmountWithoutTax",
        index: "invoiceAmountWithoutTax",
        width: 110,
        search: false,
        formatter: "integer",

        align: "right",
    },
    {
        name: "invoiceConsumptionTax",
        index: "invoiceConsumptionTax",
        width: 80,
        search: false,
        formatter: "integer",

        align: "right",
    },


    {
        name: "ordering.offer.sendCompany.sendCompanyNameAbbr",
        index: "ordering.offer.sendCompany.sendCompanyNameAbbr",
        width: 60,
        search: false,
        align: "left",
    },

    {
        name: "ordering.offer.receiveCompany.receiveCompanyNameAbbr",
        index: "ordering.offer.receiveCompany.receiveCompanyNameAbbr",
        width: 60,
        search: false,
        search: false,
        align: "left",
    },

    {
        name: "ordering.offer.offerType",
        index: "ordering.offer.offerType",
        width: 60,
        search: false,
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue == '00') {
                return '派遣';
            } else if (cellvalue == '01') {
                return '請負';
            } else if (cellvalue == '10') {
                return '社内開発';
            }
        },
        align: "left",
    },
    {
        name: "invoiceNumber",
        index: "invoiceNumber",
        width: 100,
        search: false,
        align: "left",
    },
    {
        name: "ordering.orderId",
        index: "ordering.orderId",
        width: 100,
        search: false,
        hidden: true,
        align: "left",
    },
    {
        name: "ordering.orderNumber",
        index: "ordering.orderNumber",
        width: 100,
        search: false,
        align: "left",
        formatter: function (cellvalue, options, rowObject) {
            if (cellvalue != null && cellvalue != undefined) {
                var selectHtml = '<a href="javascript:void(0);" onclick="openDetail('
                    + rowObject.ordering.orderId
                    + ')">'
                    + cellvalue
                    + '</a>';
            } else {
                var selectHtml = cellvalue;
            }
            return selectHtml;
        },
    },
];

var postData = {
    yearMonth: $('#yearMonth').val(),
    projectName: $('#projectName').val(),
};

function openDetail(orderId) {
    var width = '900px'
    var height = '90%'
    var url = "/ordering/edit/" + orderId;
    layer.open({
        type: 2,
        title: ' ',
        shadeClose: false,
        shade: 0.6,
        area: [width, height],
        content: [url],
    })
    return false;
};

var shrinkToFit = false;

var gridComplete = function () {
    $("#grid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "scroll"});
}

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete);
});	
	