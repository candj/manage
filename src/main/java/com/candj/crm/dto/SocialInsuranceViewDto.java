package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

@Getter
@Setter
public class SocialInsuranceViewDto {

    /** 税率ID */
    @Digits(integer=10, fraction=0)
    private Integer socialInsuranceId;


    /** 等級 */
    @Length(max=255)
    private String grade;

    /** 月額 */
    @Length(max=255)
    private String monthly;

    /** 報酬月額下限 */
    @Length(max=255)
    private Integer minMonthlyPayment;

    /** 報酬月額上限 */
    private Integer maxMonthlyPayment;

    /** 健康保険（全額）*/
    private Double healthInsuranceFull;

    /** 健康保険（折半額）*/
    private Double healthInsuranceHalf;

    /** 健康保険＋介護保険（全額）*/
    private Double healthAndNursingCareFull;

    /** 健康保険＋介護保険（折半額）*/
    private Double healthAndNursingCareHalf;

    /** 厚生年金保険（全額）*/
    private Double welfarePensionFull;

    /** 厚生年金保険（折半額）*/
    private Double welfarePensionHalf;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
