package com.candj.crm.service;

import com.candj.crm.dao.mapper.WorkDao;
import com.candj.crm.dto.WorkDto;
import com.candj.crm.mapper.model.Work;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 作業情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class WorkService {
    @Autowired
    WorkDao workDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで作業を検索する。
     * @param workId 作業ID
     * @return 結果
     */
    public WorkDto selectByPrimaryKey(Integer workId) {
        //プライマリーキーで作業を検索する。
        Work ret = workDao.selectByPrimaryKey(workId);
        return DozerHelper.map(mapper, ret, WorkDto.class);
    }

    /**
     * プライマリーキーで作業を検索する。（連携情報含む）
     * @param workId 作業ID
     * @return 結果
     */
    public List<WorkDto> selectAllByPrimaryKey(Integer workId) {
        //プライマリーキーで作業検索する。（連携情報含む）
        List<Work> ret = workDao.selectAllByPrimaryKey(workId);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }

    /**
     * 条件で作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkDto> selectByExample(WorkDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work.work_id", dto.getWorkId())
            .andEqualTo("work.user_id", dto.getUserId())
            .andEqualTo("work.order_id", dto.getOrderId())
            .andEqualTo("work.work_start_date", dto.getWorkStartDate())
            .andEqualTo("work.work_end_date", dto.getWorkEndDate())
            .andEqualTo("work.work_month", dto.getWorkMonth())
            .andEqualTo("work.work_hour", dto.getWorkHour())
            .andEqualTo("work.work_status", dto.getWorkStatus())
            .andEqualTo("work.create_user", dto.getCreateUser())
            .andEqualTo("work.update_user", dto.getUpdateUser())
            .andEqualTo("work.create_time", dto.getCreateTime())
            .andEqualTo("work.update_time", dto.getUpdateTime());
        }
        //条件で作業を検索する。（連携情報含む）)
        List<Work> ret = workDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }

    /**
     * 条件で作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkDto> selectAllByExample(WorkDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andLike("app_user.user_name", dto.getAppUser().getUserName())
            .andEqualTo("work.work_id", dto.getWorkId())
            .andEqualTo("work.user_id", dto.getUserId())
            .andEqualTo("work.order_id", dto.getOrderId())
            .andEqualTo("work.work_month", dto.getWorkMonth())
            .andEqualTo("work.work_start_date", dto.getWorkStartDate())
            .andEqualTo("work.work_end_date", dto.getWorkEndDate())
            .andEqualTo("work.work_hour", dto.getWorkHour())
            .andEqualTo("work.work_status", dto.getWorkStatus())
            .andEqualTo("work.create_user", dto.getCreateUser())
            .andEqualTo("work.update_user", dto.getUpdateUser())
            .andEqualTo("work.create_time", dto.getCreateTime())
            .andEqualTo("work.update_time", dto.getUpdateTime());
        }
        //条件で作業を検索する。（連携情報含む）)
        List<Work> ret = workDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }

    /**
     * 条件で作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkDto> selectAllByUserAndMonth(WorkDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("work.work_id", dto.getWorkId())
                    .andEqualTo("work.user_id", dto.getUserId())
                    .andEqualTo("work.order_id", dto.getOrderId())
                    .andEqualTo("work.work_month", dto.getWorkMonth())
                    .andEqualTo("work.work_start_date", dto.getWorkStartDate())
                    .andEqualTo("work.work_end_date", dto.getWorkEndDate())
                    .andEqualTo("work.work_hour", dto.getWorkHour())
                    .andEqualTo("work.work_status", dto.getWorkStatus())
                    .andEqualTo("work.create_user", dto.getCreateUser())
                    .andEqualTo("work.update_user", dto.getUpdateUser())
                    .andEqualTo("work.create_time", dto.getCreateTime())
                    .andEqualTo("work.update_time", dto.getUpdateTime());
        }
        //条件で作業を検索する。（連携情報含む）)
        List<Work> ret = workDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }

    /**
     * 作業を新規追加する。
     * @param dto 作業
     * @return 結果
     */
    public int insertSelective(WorkDto dto) {
        Work work  =  mapper.map(dto, Work.class);
        //作業を新規追加する。
        int ret = workDao.insertSelective(work);
        return ret;
    }

    /**
     * 作業を新規追加する。
     * @param dto 作業
     * @return 結果
     */
    public int insert(WorkDto dto) {
        Work work  =  mapper.map(dto, Work.class);
        //作業を新規追加する。
        int ret = workDao.insert(work);
        return ret;
    }

    /**
     * プライマリーキーで作業を更新する。
     * @param dto 作業
     * @return 結果
     */
    public int updateByPrimaryKey(WorkDto dto) {
        Work work  =  mapper.map(dto, Work.class);
        //プライマリーキーで作業を更新する。
        int ret = workDao.updateByPrimaryKey(work);
        return ret;
    }

    /**
     * プライマリーキーで作業を更新する。
     * @param dto 作業
     * @return 結果
     */
    public int updateByPrimaryKeySelective(WorkDto dto) {
        Work work  =  mapper.map(dto, Work.class);
        //プライマリーキーで作業を更新する。
        int ret = workDao.updateByPrimaryKeySelective(work);
        return ret;
    }

    /**
     * 作業を削除する。
     * @param workId 作業ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer workId) {
        //作業を削除する。
        int ret = workDao.deleteByPrimaryKey(workId);
        return ret;
    }

    /**
     * 条件で作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkDto> selectByStaffAndMonth(WorkDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("work.work_id", dto.getWorkId())
                    .andEqualTo("work.user_id", dto.getUserId())
                    .andEqualTo("work.order_id", dto.getOrderId())
                    .andEqualTo("work.work_start_date", dto.getWorkStartDate())
                    .andEqualTo("work.work_end_date", dto.getWorkEndDate())
                    .andEqualTo("work.work_hour", dto.getWorkHour())
                    .andEqualTo("work.work_status", dto.getWorkStatus())
                    .andEqualTo("work.create_user", dto.getCreateUser())
                    .andEqualTo("work.update_user", dto.getUpdateUser())
                    .andEqualTo("work.create_time", dto.getCreateTime())
                    .andEqualTo("work.update_time", dto.getUpdateTime());
        }
        //条件で作業を検索する。（連携情報含む）)
        List<Work> ret = workDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }

    /**
     * 注文IDで作業を検索する。（連携情報含む）
     * @param orderId 作業ID
     * @return 結果
     */
    public List<WorkDto> selectByOrderId(Integer orderId) {
        //プライマリーキーで作業検索する。（連携情報含む）
        List<Work> ret = workDao.selectByOrderId(orderId);
        return DozerHelper.mapList(mapper, ret, WorkDto.class);
    }
}
