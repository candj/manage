package com.candj.crm.service;

import com.candj.crm.dao.mapper.SocialInsuranceDao;
import com.candj.crm.dto.SocialInsuranceDto;
import com.candj.crm.mapper.model.SocialInsurance;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 税率、保険料率情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class SocialInsuranceService {

    @Autowired
    SocialInsuranceDao socialInsuranceDao;

    private static final String DEFAULT_LOCATION = "東京";

    @Autowired
    Mapper mapper;

    Comparator<SocialInsurance> descWorkStartTimeComp = new Comparator<SocialInsurance>() {
        @Override
        public int compare(SocialInsurance t1, SocialInsurance t2) {
            return t2.getSocialInsuranceStartMonth().compareTo(t1.getSocialInsuranceStartMonth());
        }
    };

    static List<String> socialInsuranceName = Arrays.asList("01", "02", "03", "04", "05", "06");

    /**
     * プライマリーキーで税率、保険料率を検索する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public SocialInsuranceDto selectByPrimaryKey(Integer socialInsuranceId) {
        //プライマリーキーで税率、保険料率を検索する。
        SocialInsurance ret = socialInsuranceDao.selectByPrimaryKey(socialInsuranceId);
        return DozerHelper.map(mapper, ret, SocialInsuranceDto.class);
    }

    /**
     * プライマリーキーで税率、保険料率を検索する。（連携情報含む）
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public List<SocialInsuranceDto> selectAllByPrimaryKey(Integer socialInsuranceId) {
        //プライマリーキーで税率、保険料率検索する。（連携情報含む）
        List<SocialInsurance> ret = socialInsuranceDao.selectAllByPrimaryKey(socialInsuranceId);
        return DozerHelper.mapList(mapper, ret, SocialInsuranceDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SocialInsuranceDto> selectByExample(SocialInsuranceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("social_insurance.social_insurance_id", dto.getSocialInsuranceId())
                    .andEqualTo("social_insurance.social_insurance_excel_path", dto.getSocialInsuranceExcelPath())
                    .andEqualTo("social_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("social_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("social_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("social_insurance.update_time", dto.getUpdateTime());
        }
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<SocialInsurance> ret = socialInsuranceDao.selectByExample(whereCondition);
        ret.forEach(r -> {
            File file = new File(r.getSocialInsuranceExcelPath());
            r.setSocialInsuranceExcelName(file.getName());
        });
        return DozerHelper.mapList(mapper, ret, SocialInsuranceDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SocialInsuranceDto> selectAllByExample(SocialInsuranceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("social_insurance.social_insurance_id", dto.getSocialInsuranceId())
                    .andLessThanOrEqualTo("social_insurance.social_insurance_start_month", dto.getSocialInsuranceStartMonth())
                    .andGreaterThanOrEqualTo("social_insurance.social_insurance_end_month", dto.getSocialInsuranceEndMonth())
                    .andEqualTo("social_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("social_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("social_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("social_insurance.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("social_insurance.social_insurance_id", dto.getSocialInsuranceId())
                    .andLessThanOrEqualTo("social_insurance.social_insurance_start_month", dto.getSocialInsuranceStartMonth())
                    .andIsNull("social_insurance.social_insurance_end_month")
                    .andEqualTo("social_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("social_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("social_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("social_insurance.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("social_insurance.social_insurance_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<SocialInsurance> socialInsuranceList = socialInsuranceDao.selectAllByExample(whereCondition);


        return DozerHelper.mapList(mapper, socialInsuranceList, SocialInsuranceDto.class);
    }

    /**
     * 条件で税率、保険料率を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<SocialInsuranceDto> selectAllByMyExample(SocialInsuranceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("social_insurance.social_insurance_id", dto.getSocialInsuranceId())
                    .andLessThanOrEqualTo("social_insurance.social_insurance_start_month", dto.getSocialInsuranceStartMonth())
                    .andGreaterThanOrEqualTo("social_insurance.social_insurance_end_month", dto.getSocialInsuranceEndMonth())
                    .andEqualTo("social_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("social_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("social_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("social_insurance.update_time", dto.getUpdateTime());
            whereCondition.or()
                    .andEqualTo("social_insurance.social_insurance_id", dto.getSocialInsuranceId())
                    .andLessThanOrEqualTo("social_insurance.social_insurance_start_month", dto.getSocialInsuranceStartMonth())
                    .andIsNull("social_insurance.social_insurance_end_month")
                    .andEqualTo("social_insurance.create_user", dto.getCreateUser())
                    .andEqualTo("social_insurance.update_user", dto.getUpdateUser())
                    .andEqualTo("social_insurance.create_time", dto.getCreateTime())
                    .andEqualTo("social_insurance.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("social_insurance.social_insurance_start_month DESC");
        //条件で税率、保険料率を検索する。（連携情報含む）)
        List<SocialInsurance> socialInsuranceList = socialInsuranceDao.selectAllByExample(whereCondition);

        return DozerHelper.mapList(mapper, socialInsuranceList, SocialInsuranceDto.class);
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insertSelective(SocialInsuranceDto dto) {
        SocialInsurance socialInsurance = mapper.map(dto, SocialInsurance.class);
        //税率、保険料率を新規追加する。
        int ret = socialInsuranceDao.insertSelective(socialInsurance);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int insert(SocialInsuranceDto dto) {
        SocialInsurance socialInsurance = mapper.map(dto, SocialInsurance.class);
        //税率、保険料率を新規追加する。
        int ret = socialInsuranceDao.insert(socialInsurance);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKey(SocialInsuranceDto dto) {
        SocialInsurance socialInsurance = mapper.map(dto, SocialInsurance.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = socialInsuranceDao.updateByPrimaryKey(socialInsurance);
        return ret;
    }

    /**
     * プライマリーキーで税率、保険料率を更新する。
     *
     * @param dto 税率、保険料率
     * @return 結果
     */
    public int updateByPrimaryKeySelective(SocialInsuranceDto dto) {
        SocialInsurance socialInsurance = mapper.map(dto, SocialInsurance.class);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = socialInsuranceDao.updateByPrimaryKeySelective(socialInsurance);
        return ret;
    }

    /**
     * 税率、保険料率を削除する。
     *
     * @param socialInsuranceId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer socialInsuranceId) {
        //税率、保険料率を削除する。
        int ret = socialInsuranceDao.deleteByPrimaryKey(socialInsuranceId);
        return ret;
    }

    public void setSocialInsuranceEndMonth(SocialInsuranceDto socialInsuranceDto) {
        WhereCondition whereCondition = new WhereCondition();
        List<SocialInsurance> socialInsuranceList = socialInsuranceDao.selectByExample(whereCondition);
        socialInsuranceList.sort(descWorkStartTimeComp);
        DateTime lastEndMonth = null;
        for (SocialInsurance s : socialInsuranceList) {
            s.setSocialInsuranceEndMonth(lastEndMonth == null ? null : lastEndMonth);
            lastEndMonth = s.getSocialInsuranceStartMonth().minusSeconds(1);
            socialInsuranceDao.updateByPrimaryKeySelective(s);
        }
    }

}
