package com.candj.crm.jsonUtils;

import com.candj.webpower.web.core.exception.BusinessLogicException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReadExcel {

    public static String readExcel(String filePath, String type) {
        File excelFile = new File(filePath);
        String folderPath = excelFile.getParentFile().getPath() + "/" + "JSON";
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        FileInputStream inputStream = null;
        FileWriter writer = null;
        try{
            inputStream = new FileInputStream(filePath);
            if(type.equals("social")) {
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
                //excelが正しいかどうっかを確認
                int sheetCount = workbook.getNumberOfSheets();
                if (sheetCount < 1) {
                    throw new Exception("Not any available sheet");
                }

                //sheetを取得する
                for (int i = 0; i < sheetCount; i++) {
                    try {
                        XSSFSheet sheet = workbook.getSheetAt(i);
                        writer = new FileWriter(folderPath + "/" + sheet.getSheetName() + ".json");
                        JSONArray jsonData = new JSONArray();
                        jsonData = transferXlsxToJsonForSocial(sheet);
                        jsonData.write(writer);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (writer != null) {
                            try {
                                writer.flush();
                                writer.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            else if(type.equals("personal")) {
                HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
                //excelが正しいかどうっかを確認
                int sheetCount = workbook.getNumberOfSheets();
                if (sheetCount < 1) {
                    throw new Exception("Not any available sheet");
                }

                //sheetを取得する
                try {
                    HSSFSheet sheet = workbook.getSheetAt(0);
                    writer = new FileWriter(folderPath + "/" + sheet.getSheetName() + ".json");
                    JSONArray jsonData = new JSONArray();
                    jsonData = transferXlsxToJsonForPersonal(sheet);
                    jsonData.write(writer);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.flush();
                            writer.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (inputStream!=null){
                try {
                    inputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        return folderPath;
    }

    public static DateTime getYearMonthBySocial(String filePath) {
        FileInputStream inputStream = null;
        DateTime yearMonth = new DateTime();
        try{
            inputStream = new FileInputStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            //excelが正しいかどうっかを確認
            int sheetCount = workbook.getNumberOfSheets();
            if (sheetCount < 1) {
                throw new BusinessLogicException("正しいExcelファイルを入力して下さい");
            }

            //年月を取得する
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFRow xRow = sheet.getRow(0);
            String text = xRow.getCell(0).toString();
            if(text.equals("")){
                throw new BusinessLogicException("正しいExcelファイルを入力して下さい");
            }
            String yearBase = text.substring(0,2);
            int beginIndex;
            int overIndex;
            beginIndex = text.indexOf("年");
            String year = text.substring(2, beginIndex);
            overIndex = text.indexOf("月");
            String month = text.substring(beginIndex + 1, overIndex);
            if(yearBase.equals("令和")){
                yearMonth = new DateTime(2018 + Integer.valueOf(year), Integer.valueOf(month), 1, 0, 0, 0, 0);
            } else if(yearBase.equals("平成")){
                yearMonth = new DateTime(1988 + Integer.valueOf(year), Integer.valueOf(month), 1, 0, 0, 0, 0);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (inputStream!=null){
                try {
                    inputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return yearMonth;
    }

    public static DateTime getYearMonthByPersonal(String filePath) {
        FileInputStream inputStream = null;
        DateTime yearMonth = new DateTime();
        try{
            inputStream = new FileInputStream(filePath);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

            //excelが正しいかどうっかを確認
            int sheetCount = workbook.getNumberOfSheets();
            if (sheetCount < 1) {
                throw new BusinessLogicException("正しいExcelファイルを入力して下さい");
            }

            //年月を取得する
            HSSFSheet sheet = workbook.getSheetAt(0);
            HSSFRow xRow = sheet.getRow(0);
            String text = xRow.getCell(1).toString();
            if(text.equals("")){
                throw new BusinessLogicException("正しいExcelファイルを入力して下さい");
            }
            int index;
            index = text.indexOf("（");
            String yearBase = text.substring(index + 1, index + 3);
            int beginIndex;
            //int overIndex;
            beginIndex = text.indexOf("年");
            String year = text.substring(index + 3, beginIndex);
            //overIndex = text.indexOf("月");
            //String month = text.substring(beginIndex + 1, overIndex);
            if(yearBase.equals("令和")){
                yearMonth = new DateTime(2018 + Integer.valueOf(year), 1, 1, 0, 0, 0, 0);
            } else if(yearBase.equals("平成")){
                yearMonth = new DateTime(1988 + Integer.valueOf(year), 1, 1, 0, 0, 0, 0);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (inputStream!=null){
                try {
                    inputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return yearMonth;
    }

    private static JSONArray transferXlsxToJsonForSocial (XSSFSheet sheet) throws Exception{

        int lastRowNum = sheet.getLastRowNum();

        JSONArray jsonArr = new JSONArray();
        boolean flag = false;
        int rowIndex = 11;
        for (; rowIndex <= lastRowNum; rowIndex++) {

            XSSFRow xRow = sheet.getRow(rowIndex);

            if(xRow.getCell(9).getRawValue() == null){
                if(flag == false) {
                    continue;
                } else{
                  break;
                }
            } else{
                String allAmount = xRow.getCell(9).getRawValue();
                String halfAmount = xRow.getCell(10).getRawValue();
                for(int i = 11; i < rowIndex; i++){

                    XSSFRow x = sheet.getRow(i);

                    Map<String, String> jsonMap = new LinkedHashMap<>();

                    jsonMap.put("等級", x.getCell(0).getRawValue());
                    jsonMap.put("月額", x.getCell(1).getRawValue());
                    jsonMap.put("報酬月額下限", x.getCell(2).getRawValue());
                    jsonMap.put("報酬月額上限", x.getCell(4).getRawValue());
                    jsonMap.put("健康保険（全額）", x.getCell(5).getRawValue());
                    jsonMap.put("健康保険（折半額）", x.getCell(6).getRawValue());
                    jsonMap.put("健康保険＋介護保険（全額）", x.getCell(7).getRawValue());
                    jsonMap.put("健康保険＋介護保険（折半額）", x.getCell(8).getRawValue());
                    jsonMap.put("厚生年金保険（全額）", allAmount);
                    jsonMap.put("厚生年金保険（折半額）", halfAmount);

                    JSONObject jsonObject = JSONObject.fromObject(jsonMap);

                    jsonArr.add(jsonObject);
                }
                break;
            }
        }
        for(; rowIndex <= lastRowNum; rowIndex++) {

            XSSFRow xRow = sheet.getRow(rowIndex);

            if(xRow.getCell(9).getRawValue() == null){
                break;
            }

            Map<String, String> jsonMap = new LinkedHashMap<>();

            jsonMap.put("等級", xRow.getCell(0).toString());
            jsonMap.put("月額", xRow.getCell(1).getRawValue());
            jsonMap.put("報酬月額下限", xRow.getCell(2).getRawValue());
            jsonMap.put("報酬月額上限", xRow.getCell(4).getRawValue());
            jsonMap.put("健康保険（全額）", xRow.getCell(5).getRawValue());
            jsonMap.put("健康保険（折半額）", xRow.getCell(6).getRawValue());
            jsonMap.put("健康保険＋介護保険（全額）", xRow.getCell(7).getRawValue());
            jsonMap.put("健康保険＋介護保険（折半額）", xRow.getCell(8).getRawValue());
            jsonMap.put("厚生年金保険（全額）", xRow.getCell(9).getRawValue());
            jsonMap.put("厚生年金保険（折半額）", xRow.getCell(10).getRawValue());

            JSONObject jsonObject = JSONObject.fromObject(jsonMap);

            jsonArr.add(jsonObject);
        }

        for(; rowIndex <= lastRowNum; rowIndex++) {

            XSSFRow xRow = sheet.getRow(rowIndex);

            if(xRow.getCell(0).getRawValue() == null){
                break;
            }

            Map<String, String> jsonMap = new LinkedHashMap<>();

            jsonMap.put("等級", xRow.getCell(0).getRawValue());
            jsonMap.put("月額", xRow.getCell(1).getRawValue());
            jsonMap.put("報酬月額下限", xRow.getCell(2).getRawValue());
            jsonMap.put("報酬月額上限", xRow.getCell(4).getRawValue());
            jsonMap.put("健康保険（全額）", xRow.getCell(5).getRawValue());
            jsonMap.put("健康保険（折半額）", xRow.getCell(6).getRawValue());
            jsonMap.put("健康保険＋介護保険（全額）", xRow.getCell(7).getRawValue());
            jsonMap.put("健康保険＋介護保険（折半額）", xRow.getCell(8).getRawValue());
            jsonMap.put("厚生年金保険（全額）", "0");
            jsonMap.put("厚生年金保険（折半額）", "0");

            JSONObject jsonObject = JSONObject.fromObject(jsonMap);

            jsonArr.add(jsonObject);
        }
        return jsonArr;

    }

    private static JSONArray transferXlsxToJsonForPersonal (HSSFSheet sheet) throws Exception{

        int lastRowNum = sheet.getLastRowNum();

        JSONArray jsonArr = new JSONArray();
        JSONArray jArr1 = new JSONArray();
        JSONArray jArr2 = new JSONArray();
        int rowIndex = 9;
        for (; rowIndex <= lastRowNum; rowIndex++) {

            HSSFRow xRow = sheet.getRow(rowIndex);
            HSSFRow nextRow = sheet.getRow(rowIndex + 1);

            if(xRow.getCell(1).getCellType() == CellType.BLANK){
                if(nextRow.getCell(1).getCellType() == CellType.BLANK){
                    break;
                }
                else{
                    continue;
                }
            }else if(xRow.getCell(1).getCellType() == CellType.STRING){
                break;
            } else{
                HSSFRow x = sheet.getRow(rowIndex);

                Map<String, Double> jsonMap = new LinkedHashMap<>();

                jsonMap.put("報酬月額下限", x.getCell(1).getNumericCellValue());
                jsonMap.put("報酬月額上限", x.getCell(2).getNumericCellValue());
                jsonMap.put("扶養人数0", x.getCell(3).getNumericCellValue());
                jsonMap.put("扶養人数1", x.getCell(4).getNumericCellValue());
                jsonMap.put("扶養人数2", x.getCell(5).getNumericCellValue());
                jsonMap.put("扶養人数3", x.getCell(6).getNumericCellValue());
                jsonMap.put("扶養人数4", x.getCell(7).getNumericCellValue());
                jsonMap.put("扶養人数5", x.getCell(8).getNumericCellValue());
                jsonMap.put("扶養人数6", x.getCell(9).getNumericCellValue());
                jsonMap.put("扶養人数7", x.getCell(10).getNumericCellValue());

                JSONObject jsonObject = JSONObject.fromObject(jsonMap);

                jArr1.add(jsonObject);
            }
        }

        rowIndex += 2;
        if (sheet.getRow(rowIndex).getCell(1).getCellType() != CellType.BLANK) {
            for (; rowIndex <= lastRowNum; rowIndex+=9) {
                if((sheet.getRow(rowIndex + 2).getCell(1).getCellType() == CellType.BLANK)){
                    HSSFRow hRow = sheet.getRow(rowIndex);
                    HSSFRow rateRow = sheet.getRow(rowIndex + 6);
                    Map<String, Double> jsonMap = new LinkedHashMap<>();
                    //下限
                    String lower = hRow.getCell(1).getStringCellValue();
                    int index = lower.indexOf("円");
                    String l = lower.substring(0, index).replace(",", "");
                    Double lowerLimit = Double.valueOf(l);
                    jsonMap.put("報酬月額下限", lowerLimit);
                    jsonMap.put("報酬月額上限", null);
                    jsonMap.put("扶養人数0", hRow.getCell(3).getNumericCellValue());
                    jsonMap.put("扶養人数1", hRow.getCell(4).getNumericCellValue());
                    jsonMap.put("扶養人数2", hRow.getCell(5).getNumericCellValue());
                    jsonMap.put("扶養人数3", hRow.getCell(6).getNumericCellValue());
                    jsonMap.put("扶養人数4", hRow.getCell(7).getNumericCellValue());
                    jsonMap.put("扶養人数5", hRow.getCell(8).getNumericCellValue());
                    jsonMap.put("扶養人数6", hRow.getCell(9).getNumericCellValue());
                    jsonMap.put("扶養人数7", hRow.getCell(10).getNumericCellValue());
                    //料率
                    String rate = rateRow.getCell(3).getStringCellValue();
                    index = rate.indexOf("の");
                    int lastIndex = rate.indexOf("％");
                    String r = rate.substring(index + 1, lastIndex);
                    Double taxRate = Double.valueOf(r) * 0.01;
                    jsonMap.put("料率", taxRate);
                    jArr2.add(jsonMap);
                    break;
                }
                HSSFRow hRow = sheet.getRow(rowIndex);
                HSSFRow limitRow = sheet.getRow(rowIndex + 4);
                HSSFRow rateRow = sheet.getRow(rowIndex + 5);
                Map<String, Double> jsonMap = new LinkedHashMap<>();
                //下限
                String lower = hRow.getCell(1).getStringCellValue();
                int index = lower.indexOf("円");
                String l = lower.substring(0, index).replace(",", "");
                Double lowerLimit = Double.valueOf(l);
                jsonMap.put("報酬月額下限", lowerLimit);
                //上限
                String upper = limitRow.getCell(1).getStringCellValue();
                index = upper.indexOf("円");
                String u = upper.substring(0, index).replace(",", "");
                Double upperLimit = Double.valueOf(u);
                jsonMap.put("報酬月額上限", upperLimit);
                jsonMap.put("扶養人数0", hRow.getCell(3).getNumericCellValue());
                jsonMap.put("扶養人数1", hRow.getCell(4).getNumericCellValue());
                jsonMap.put("扶養人数2", hRow.getCell(5).getNumericCellValue());
                jsonMap.put("扶養人数3", hRow.getCell(6).getNumericCellValue());
                jsonMap.put("扶養人数4", hRow.getCell(7).getNumericCellValue());
                jsonMap.put("扶養人数5", hRow.getCell(8).getNumericCellValue());
                jsonMap.put("扶養人数6", hRow.getCell(9).getNumericCellValue());
                jsonMap.put("扶養人数7", hRow.getCell(10).getNumericCellValue());
                //料率
                String rate = rateRow.getCell(3).getStringCellValue();
                index = rate.indexOf("の");
                int lastIndex = rate.indexOf("％");
                String r = rate.substring(index + 1, lastIndex);
                Double taxRate = Double.valueOf(r) * 0.01;
                jsonMap.put("料率", taxRate);
                jArr2.add(jsonMap);
            }
        }else {
            HSSFRow hRow1 = sheet.getRow(rowIndex);
            HSSFRow limitRow1 = sheet.getRow(rowIndex + 6);
            HSSFRow rateRow1 = sheet.getRow(rowIndex + 7);
            Map<String, Double> jsonMap = new LinkedHashMap<>();
            //下限
            String lower = hRow1.getCell(1).getStringCellValue();
            int index = lower.indexOf("円");
            String l = lower.substring(0, index).replace(",", "");
            Double lowerLimit = Double.valueOf(l);
            jsonMap.put("報酬月額下限", lowerLimit);
            //上限
            String upper = limitRow1.getCell(1).getStringCellValue();
            index = upper.indexOf("円");
            String u = upper.substring(0, index).replace(",", "");
            Double upperLimit = Double.valueOf(u);
            jsonMap.put("報酬月額上限", upperLimit);
            jsonMap.put("扶養人数0", hRow1.getCell(3).getNumericCellValue());
            jsonMap.put("扶養人数1", hRow1.getCell(4).getNumericCellValue());
            jsonMap.put("扶養人数2", hRow1.getCell(5).getNumericCellValue());
            jsonMap.put("扶養人数3", hRow1.getCell(6).getNumericCellValue());
            jsonMap.put("扶養人数4", hRow1.getCell(7).getNumericCellValue());
            jsonMap.put("扶養人数5", hRow1.getCell(8).getNumericCellValue());
            jsonMap.put("扶養人数6", hRow1.getCell(9).getNumericCellValue());
            jsonMap.put("扶養人数7", hRow1.getCell(10).getNumericCellValue());
            //料率
            String rate = rateRow1.getCell(3).getStringCellValue();
            index = rate.indexOf("の");
            int lastIndex = rate.indexOf("％");
            String r = rate.substring(index + 1, lastIndex);
            Double taxRate = Double.valueOf(r) * 0.01;
            jsonMap.put("料率", taxRate);
            jArr2.add(jsonMap);
            rowIndex += 13;
            for (; rowIndex <= lastRowNum; rowIndex+=9) {
                if((sheet.getRow(rowIndex + 6).getCell(1).getCellType() == CellType.BLANK)){
                    HSSFRow hRow = sheet.getRow(rowIndex);
                    HSSFRow rateRow = sheet.getRow(rowIndex + 4);
                    jsonMap = new LinkedHashMap<>();
                    //下限
                    lower = hRow.getCell(1).getStringCellValue();
                    index = lower.indexOf("円");
                    l = lower.substring(0, index).replace(",", "");
                    lowerLimit = Double.valueOf(l);
                    jsonMap.put("報酬月額下限", lowerLimit);
                    jsonMap.put("報酬月額上限", null);
                    jsonMap.put("扶養人数0", hRow.getCell(3).getNumericCellValue());
                    jsonMap.put("扶養人数1", hRow.getCell(4).getNumericCellValue());
                    jsonMap.put("扶養人数2", hRow.getCell(5).getNumericCellValue());
                    jsonMap.put("扶養人数3", hRow.getCell(6).getNumericCellValue());
                    jsonMap.put("扶養人数4", hRow.getCell(7).getNumericCellValue());
                    jsonMap.put("扶養人数5", hRow.getCell(8).getNumericCellValue());
                    jsonMap.put("扶養人数6", hRow.getCell(9).getNumericCellValue());
                    jsonMap.put("扶養人数7", hRow.getCell(10).getNumericCellValue());
                    //料率
                    rate = rateRow.getCell(3).getStringCellValue();
                    index = rate.indexOf("の");
                    lastIndex = rate.indexOf("％");
                    r = rate.substring(index + 1, lastIndex);
                    taxRate = Double.valueOf(r) * 0.01;
                    jsonMap.put("料率", taxRate);
                    jArr2.add(jsonMap);
                    break;
                }
                HSSFRow hRow = sheet.getRow(rowIndex);
                HSSFRow limitRow = sheet.getRow(rowIndex + 4);
                HSSFRow rateRow = sheet.getRow(rowIndex + 5);
                jsonMap = new LinkedHashMap<>();
                //下限
                lower = hRow.getCell(1).getStringCellValue();
                index = lower.indexOf("円");
                l = lower.substring(0, index).replace(",", "");
                lowerLimit = Double.valueOf(l);
                jsonMap.put("報酬月額下限", lowerLimit);
                //上限
                upper = limitRow.getCell(1).getStringCellValue();
                index = upper.indexOf("円");
                u = upper.substring(0, index).replace(",", "");
                upperLimit = Double.valueOf(u);
                jsonMap.put("報酬月額上限", upperLimit);
                jsonMap.put("扶養人数0", hRow.getCell(3).getNumericCellValue());
                jsonMap.put("扶養人数1", hRow.getCell(4).getNumericCellValue());
                jsonMap.put("扶養人数2", hRow.getCell(5).getNumericCellValue());
                jsonMap.put("扶養人数3", hRow.getCell(6).getNumericCellValue());
                jsonMap.put("扶養人数4", hRow.getCell(7).getNumericCellValue());
                jsonMap.put("扶養人数5", hRow.getCell(8).getNumericCellValue());
                jsonMap.put("扶養人数6", hRow.getCell(9).getNumericCellValue());
                jsonMap.put("扶養人数7", hRow.getCell(10).getNumericCellValue());
                //料率
                rate = rateRow.getCell(3).getStringCellValue();
                index = rate.indexOf("の");
                lastIndex = rate.indexOf("％");
                r = rate.substring(index + 1, lastIndex);
                taxRate = Double.valueOf(r) * 0.01;
                jsonMap.put("料率", taxRate);
                jArr2.add(jsonMap);
            }
        }
        jsonArr.add(jArr1);
        jsonArr.add(jArr2);
        return jsonArr;
    }

}


