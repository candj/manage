package com.candj.crm.core.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.candj.crm.service.AppUserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.crm.dao.mapper.AppUserDao;
import com.candj.crm.dto.UserRoleDto;
import com.candj.crm.service.UserRoleService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    AppUserDao userDao;
    @Autowired
    UserRoleService userRoleService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        try {
            WhereCondition whereCondition = new WhereCondition();
            whereCondition.createCriteria()
                    .andEqualTo("user_name", userName);
            //条件でユーザーを検索する。（連携情報含む）)
            List<com.candj.crm.mapper.model.AppUser> users = userDao.selectByExample(whereCondition);
            com.candj.crm.mapper.model.AppUser user = users.stream().findFirst().orElseThrow(() -> {
                throw new UsernameNotFoundException("errors.error.user_no_exist");
            });

            com.candj.crm.mapper.model.AppUser appUser = users.get(0);
            if (!appUser.isUserDeleteFlag()) {
                throw new UsernameNotFoundException("errors.error.user_no_exist");
            }

            Collection<GrantedAuthority> authList = getAuthorities(user.getUserId());

            return new AppAuthenticationUser(user, authList);

        } catch (Throwable e) {
            e.printStackTrace();
            throw new UsernameNotFoundException(e.getMessage());
        }

    }

    private Collection<GrantedAuthority> getAuthorities(Integer userId) throws UsernameNotFoundException {

        UserRoleDto dto = new UserRoleDto();
        dto.setUserId(userId);
        List<UserRoleDto> list = userRoleService.selectAllByExample(dto);
        if (CollectionUtils.isEmpty(list)) {
            throw new UsernameNotFoundException("errors.error.no_authority");
        }
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        for (UserRoleDto d : list) {
            authList.add(new SimpleGrantedAuthority(d.getRole().getRoleCode()));
        }
        return authList;
    }

}
