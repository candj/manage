package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class SendCompanyDto {

    /** 会社ID */
    private Integer sendCompanyId;

    /** 会社区分 */
    private Byte sendCompanyType;

    /** 会社名 */
    private String sendCompanyName;

    /** 会社名（略称） */
    private String sendCompanyNameAbbr;

    /** 銀行 */
    private String companyBank;

    /** 銀行支店名 */
    private String companyBankName;

    /** 預金種類 */
    private String companyBankAccountType;

    /** 口座番号 */
    private Integer companyBankAccountNumber;

    /** 口座名義 */
    private String companyBankAccountName;

    /** 住所 */
    private String sendCompanyAdress;

    /** 郵便番号 */
    private String sendCompanyPostCode;

    /** 電話番号 */
    private String sendCompanyTelephoneNumber;

    /** FAX番号 */
    private String sendCompanyFaxNumber;

    /** ステータス */
    private boolean sendCompanyDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
