package com.candj.crm.service;

import com.candj.crm.dao.mapper.InvoiceDao;
import com.candj.crm.dto.InvoiceDto;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.mapper.model.Invoice;
import com.candj.crm.mapper.model.Ordering;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 請求情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class InvoiceService {

    @Autowired
    InvoiceDao invoiceDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで請求を検索する。
     *
     * @param invoiceId 請求ID
     * @return 結果
     */
    public InvoiceDto selectByPrimaryKey(Integer invoiceId) {
        //プライマリーキーで請求を検索する。
        Invoice ret = invoiceDao.selectByPrimaryKey(invoiceId);
        return DozerHelper.map(mapper, ret, InvoiceDto.class);
    }

    /**
     * プライマリーキーで請求を検索する。（連携情報含む）
     *
     * @param invoiceId 請求ID
     * @return 結果
     */
    public List<InvoiceDto> selectAllByPrimaryKey(Integer invoiceId) {
        //プライマリーキーで請求検索する。（連携情報含む）
        List<Invoice> ret = invoiceDao.selectAllByPrimaryKey(invoiceId);
        return DozerHelper.mapList(mapper, ret, InvoiceDto.class);
    }

    /**
     * 条件で請求を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<InvoiceDto> selectByExample(InvoiceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("invoice.invoice_id", dto.getInvoiceId())
                    .andEqualTo("invoice.order_id", dto.getOrderId())
                    .andEqualTo("offer.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("offer.receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("invoice.invoice_date", dto.getInvoiceDate())
                    .andEqualTo("invoice.work_start_date", dto.getWorkStartDate())
                    .andEqualTo("invoice.work_end_date", dto.getWorkEndDate())
                    .andEqualTo("invoice.invoice_amount_without_tax", dto.getInvoiceAmountWithoutTax())
                    .andEqualTo("invoice.invoice_consumption_tax", dto.getInvoiceConsumptionTax())
                    .andEqualTo("invoice.invoice_amount_with_tax", dto.getInvoiceAmountWithTax())
                    .andEqualTo("invoice.invoice_delivery_date", dto.getInvoiceDeliveryDate())
                    .andEqualTo("invoice.invoice_status", dto.getInvoiceStatus())
                    .andEqualTo("invoice.create_user", dto.getCreateUser())
                    .andEqualTo("invoice.update_user", dto.getUpdateUser())
                    .andEqualTo("invoice.create_time", dto.getCreateTime())
                    .andEqualTo("invoice.update_time", dto.getUpdateTime());
        }
        //条件で請求を検索する。（連携情報含む）)
        List<Invoice> ret = invoiceDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, InvoiceDto.class);
    }

    /**
     * 条件で請求を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<InvoiceDto> selectAllByExample(InvoiceDto dto, String projectName) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("invoice.invoice_id", dto.getInvoiceId())
                    .andEqualTo("invoice.order_id", dto.getOrderId())
                    .andEqualTo("invoice.invoice_date", dto.getInvoiceDate())
                    .andEqualTo("offer.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("offer.receive_company_id", dto.getReceiveCompanyId())
                    .andLessThanOrEqualTo("invoice.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("invoice.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("invoice.invoice_amount_without_tax", dto.getInvoiceAmountWithoutTax())
                    .andEqualTo("invoice.invoice_consumption_tax", dto.getInvoiceConsumptionTax())
                    .andEqualTo("invoice.invoice_amount_with_tax", dto.getInvoiceAmountWithTax())
                    .andEqualTo("invoice.invoice_delivery_date", dto.getInvoiceDeliveryDate())
                    .andEqualTo("invoice.invoice_status", dto.getInvoiceStatus())
                    .andEqualTo("invoice.invoice_pay_status", dto.getInvoicePayStatus())
                    .andEqualTo("invoice.create_user", dto.getCreateUser())
                    .andEqualTo("invoice.update_user", dto.getUpdateUser())
                    .andEqualTo("invoice.create_time", dto.getCreateTime())
                    .andEqualTo("invoice.update_time", dto.getUpdateTime())
                    .andEqualTo("invoice.invoice_delete_flag", 1);
        }
        //条件で請求を検索する。（連携情報含む）)
        List<Invoice> ret = invoiceDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, InvoiceDto.class);
    }

    /**
     * 条件で請求を検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<InvoiceDto> selectAllByMyExample(InvoiceDto dto, String projectName, Integer userId) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("invoice.invoice_id", dto.getInvoiceId())
                    .andEqualTo("invoice.order_id", dto.getOrderId())
                    .andEqualTo("invoice.invoice_date", dto.getInvoiceDate())
                    .andEqualTo("offer.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("offer.receive_company_id", dto.getReceiveCompanyId())
                    .andLessThanOrEqualTo("invoice.work_start_date", dto.getWorkEndDate())
                    .andGreaterThanOrEqualTo("invoice.work_end_date", dto.getWorkStartDate())
                    .andEqualTo("invoice.invoice_amount_without_tax", dto.getInvoiceAmountWithoutTax())
                    .andEqualTo("invoice.invoice_consumption_tax", dto.getInvoiceConsumptionTax())
                    .andEqualTo("invoice.invoice_amount_with_tax", dto.getInvoiceAmountWithTax())
                    .andEqualTo("invoice.invoice_delivery_date", dto.getInvoiceDeliveryDate())
                    .andEqualTo("invoice.invoice_status", dto.getInvoiceStatus())
                    .andEqualTo("invoice.create_user", dto.getCreateUser())
                    .andEqualTo("invoice.update_user", dto.getUpdateUser())
                    .andEqualTo("invoice.create_time", dto.getCreateTime())
                    .andEqualTo("invoice.update_time", dto.getUpdateTime())
                    .andEqualTo("invoice.invoice_delete_flag", 1)
                    .andEqualTo("offer_detail.user_id", userId);

        }
        //条件で請求を検索する。（連携情報含む）)
        List<Invoice> ret = invoiceDao.selectAllByMyExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, InvoiceDto.class);
    }

    /**
     * 請求を新規追加する。
     *
     * @param dto 請求
     * @return 結果
     */
    public int insertSelective(InvoiceDto dto) {
        Invoice invoice = mapper.map(dto, Invoice.class);
        //請求を新規追加する。
        int ret = invoiceDao.insertSelective(invoice);
        return ret;
    }

    /**
     * 請求を新規追加する。
     *
     * @param dto 請求
     * @return 結果
     */
    public int insert(InvoiceDto dto) {
        Invoice invoice = mapper.map(dto, Invoice.class);
        //請求を新規追加する。
        int ret = invoiceDao.insert(invoice);
        return ret;
    }

    /**
     * プライマリーキーで請求を更新する。
     *
     * @param dto 請求
     * @return 結果
     */
    public int updateByPrimaryKey(InvoiceDto dto) {
        Invoice invoice = mapper.map(dto, Invoice.class);
        //プライマリーキーで請求を更新する。
        int ret = invoiceDao.updateByPrimaryKey(invoice);
        return ret;
    }

    /**
     * プライマリーキーで請求を更新する。
     *
     * @param dto 請求
     * @return 結果
     */
    public int updateByPrimaryKeySelective(InvoiceDto dto) {
        Invoice invoice = mapper.map(dto, Invoice.class);
        //プライマリーキーで請求を更新する。
        int ret = invoiceDao.updateByPrimaryKeySelective(invoice);
        return ret;
    }

    /**
     * 請求を削除する。
     *
     * @param invoiceId 請求ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer invoiceId) {
        //請求を削除する。
        int ret = invoiceDao.deleteByPrimaryKey(invoiceId);
        return ret;
    }

    /**
     * 注文IDで請求を検索する。
     *
     * @param orderId 見積 ID
     * @return 結果
     */
    public List<InvoiceDto> selectByOrderId(Integer orderId) {
        //プライマリーキーで見積を検索する。
        List<Invoice> ret = invoiceDao.selectByOrderId(orderId);
        return DozerHelper.mapList(mapper, ret, InvoiceDto.class);
    }
}
