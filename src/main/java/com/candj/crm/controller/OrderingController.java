package com.candj.crm.controller;

import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.Offer;
import com.candj.crm.service.OfferService;
import com.candj.crm.service.OrderingService;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 注文情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/ordering")
@Controller
public class OrderingController {
    @Autowired
    OrderingService orderingService;

    @Autowired
    OfferService offerService;

    /**
     * 注文を新規追加する。
     * @param orderingDto 注文
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{offerId}", method = RequestMethod.GET)
    public ModelAndView insert(OrderingDto orderingDto, ModelMap model) {
        List<OrderingDto> orderingDtos = orderingService.selectByOfferId(orderingDto.getOfferId());
        if(orderingDtos.isEmpty()) {
            List<Offer> offers = offerService.selectOfferByPrimaryKey(orderingDto.getOfferId());
            orderingDto.setOffer(offers.get(0));
            return new ModelAndView("ordering/insert", model);
        }
        else{
            model.addAttribute("orderingDto", orderingDtos.get(0));
            return new ModelAndView("ordering/edit", model);
        }
    }

    /**
     * 注文を新規追加(確認)する。
     * @param orderingDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid OrderingDto orderingDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/ordering/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("ordering/insertConfirm", model);
    }

    /**
     * 注文を新規追加(終了)する。
     * @param orderingDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid OrderingDto orderingDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/ordering/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //注文を新規追加する。
        int ret = orderingService.insertSelective(orderingDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/ordering/insertFinish", model);
    }

    /**
     * 注文を編集する。
     * @param orderId 注文ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{orderId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer orderId, ModelMap model) {
        //プライマリーキーで注文を検索する。
        List<OrderingDto> orderingDtos = orderingService.selectAllByPrimaryKey(orderId);
        model.addAttribute("orderingDto", orderingDtos.get(0));
        return new ModelAndView("ordering/edit", model);
    }

    /**
     * 注文を編集する。
     * @param orderId 注文ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/copy/{orderId}", method = RequestMethod.GET)
    public ModelAndView copy(@PathVariable Integer orderId, ModelMap model) {
        //プライマリーキーで注文を検索する。
        List<OrderingDto> orderingDtos = orderingService.selectAllByPrimaryKey(orderId);
        model.addAttribute("orderingDto", orderingDtos.get(0));
        return new ModelAndView("ordering/copy", model);
    }

    /**
     * 注文を編集(確認)する。
     * @param orderingDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid OrderingDto orderingDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/ordering/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("ordering/editConfirm", model);
    }

    /**
     * 注文を編集(終了)する。
     * @param orderingDto 注文
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid OrderingDto orderingDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/ordering/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで注文を更新する。
        int ret = orderingService.updateByPrimaryKeySelective(orderingDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("ordering/editFinish", model);
    }

    /**
     * 注文一覧画面を表示する。
     * @param orderingDto 注文
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("orderingDto") OrderingDto orderingDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        String projectName = "";
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("projectName", projectName);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        if (appUser.getUserType() == 0){
            return new ModelAndView("ordering/list", model);
        }else {
            return new ModelAndView("orderingForStaff/list", model);
        }
    }
}
