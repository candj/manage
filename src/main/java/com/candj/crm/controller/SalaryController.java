package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.dto.*;
import com.candj.crm.mapper.model.AppUser;
import com.candj.crm.mapper.model.OfferDetail;
import com.candj.crm.service.*;

import java.util.List;
import javax.validation.Valid;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 給料情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 */
@RequestMapping(value = "/salary")
@Controller
public class SalaryController {
    @Autowired
    SalaryService salaryService;

    @Autowired
    OfferDetailService offerDetailService;

    @Autowired
    CommuteService commuteService;

    @Autowired
    WorkService workService;

    @Autowired
    CompanyService companyService;

    /**
     * 給料を新規追加する。
     *
     * @param salaryDto 給料
     * @param model     モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(SalaryDto salaryDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("salaryDto", salaryDto);
        return new ModelAndView("salary/insert", model);
    }

    /**
     * 給料を新規追加(確認)する。
     *
     * @param salaryDto     給料
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid SalaryDto salaryDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/salary/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("salary/insertConfirm", model);
    }

    /**
     * 給料を新規追加(終了)する。
     *
     * @param salaryDto     給料
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid SalaryDto salaryDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/salary/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //給料を新規追加する。
        int ret = salaryService.insertSelective(salaryDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/salary/insertFinish", model);
    }

    /**
     * 給料を編集する。
     *
     * @param salaryId ID
     * @param model  モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{salaryId}/{yearMonth}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer salaryId,
                             @PathVariable YearMonth yearMonth,
                             ModelMap model) throws Exception {
        //年月とスタッフIDで給料を検索する。
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        SalaryDto salaryDto = salaryService.selectByPrimaryKey(salaryId);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();

        CompanyDto companyDto = new CompanyDto();
        companyDto.setCompanyId(appUser.getCompanyId());

        List<CompanyDto> companyDtoList = companyService.selectByExample(companyDto);
        if (!companyDtoList.isEmpty()) {
            DateTime dateTime = new DateTime();
            if (companyDtoList.get(0).getSalaryPayTime().intValue() == 0){
                DateTime theEndDataOfMonth = dateTime.dayOfMonth().withMaximumValue();
                salaryDto.setPlanPayDate(theEndDataOfMonth);
            }else if (companyDtoList.get(0).getSalaryPayTime().intValue() == 1){
                DateTime theFirstDateOfMonth = dateTime.dayOfMonth().withMinimumValue().plusDays(14);
                salaryDto.setPlanPayDate(theFirstDateOfMonth);
            }else {
                DateTime theFirstDateOfMonth = dateTime.dayOfMonth().withMinimumValue().plusDays(24);
                salaryDto.setPlanPayDate(theFirstDateOfMonth);
            }
        }

        model.addAttribute("salaryDto", salaryDto);
        model.addAttribute("yearMonth", month);
        return new ModelAndView("salary/edit", model);
    }

    /**
     * 給料を編集(確認)する。
     *
     * @param salaryDto     給料
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid SalaryDto salaryDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/salary/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("salary/editConfirm", model);
    }

    /**
     * 給料を編集(終了)する。
     *
     * @param salaryDto     給料
     * @param bindingResult バンディング結果
     * @param model         モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid SalaryDto salaryDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/salary/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで給料を更新する。
        int ret = salaryService.updateByPrimaryKeySelective(salaryDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("salary/editFinish", model);
    }

    /**
     * 給料一覧画面を表示する。
     *
     * @param appUserDto 給料
     * @param model      モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("appUserDto") AppUserDto appUserDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("salary/list", model);
    }
}
