package com.candj.crm.controller;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.StaffSalaryInfoDto;
import com.candj.crm.service.StaffSalaryInfoService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ基本給料情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staffSalaryInfo")
@Controller
public class StaffSalaryInfoController {
    @Autowired
    StaffSalaryInfoService staffSalaryInfoService;

    /**
     * スタッフ基本給料を新規追加する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffSalaryInfoDto staffSalaryInfoDto, ModelMap model) {
        return new ModelAndView("staffSalaryInfo/insert", model);
    }

    /**
     * スタッフ基本給料を新規追加(確認)する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffSalaryInfoDto staffSalaryInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffSalaryInfo/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffSalaryInfo/insertConfirm", model);
    }

    /**
     * スタッフ基本給料を新規追加(終了)する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffSalaryInfoDto staffSalaryInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffSalaryInfo/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ基本給料を新規追加する。
        int ret = staffSalaryInfoService.insertSelective(staffSalaryInfoDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staffSalaryInfo/insertFinish", model);
    }

    /**
     * スタッフ基本給料を編集する。
     * @param staffSalaryInfoId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{staffSalaryInfoId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer staffSalaryInfoId, ModelMap model) {
        //プライマリーキーでスタッフ基本給料を検索する。
        StaffSalaryInfoDto staffSalaryInfoDto = staffSalaryInfoService.selectAllByPrimaryKey(staffSalaryInfoId).get(0);
        staffSalaryInfoDto.setYearMonth(DateTimeUtil.dateTimeToString(staffSalaryInfoDto.getSalaryStartDate()));
        model.addAttribute("staffSalaryInfoDto", staffSalaryInfoDto);
        return new ModelAndView("staffSalaryInfo/edit", model);
    }

    /**
     * スタッフ基本給料を編集(確認)する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffSalaryInfoDto staffSalaryInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffSalaryInfo/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffSalaryInfo/editConfirm", model);
    }

    /**
     * スタッフ基本給料を編集(終了)する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffSalaryInfoDto staffSalaryInfoDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffSalaryInfo/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ基本給料を更新する。
        int ret = staffSalaryInfoService.updateByPrimaryKeySelective(staffSalaryInfoDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staffSalaryInfo/editFinish", model);
    }

    /**
     * スタッフ基本給料一覧画面を表示する。
     * @param staffSalaryInfoDto スタッフ基本給料
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffSalaryInfoDto") StaffSalaryInfoDto staffSalaryInfoDto, ModelMap model) {
        return new ModelAndView("staffSalaryInfo/list", model);
    }
}
