package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiveCompany {
    /** 会社ID */
    private Integer receiveCompanyId;

    /** 会社区分 */
    private Byte receiveCompanyType;

    /** 会社名 */
    private String receiveCompanyName;

    /** 会社名（略称） */
    private String receiveCompanyNameAbbr;

    /** 銀行 */
    private String receiveCompanyBank;

    /** 銀行支店名 */
    private String receiveCompanyBankName;

    /** 預金種類 */
    private String receiveCompanyBankAccountType;

    /** 口座番号 */
    private Integer receiveCompanyBankAccountNumber;

    /** 口座名義 */
    private String receiveCompanyBankAccountName;

    private Byte salaryPayTime;

    /** 住所 */
    private String receiveCompanyAdress;

    /** 郵便番号 */
    private String receiveCompanyPostCode;

    /** 電話番号 */
    private String receiveCompanyTelephoneNumber;

    /** FAX番号 */
    private String receiveCompanyFaxNumber;

    /** ステータス */
    private boolean receiveCompanyDeleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
