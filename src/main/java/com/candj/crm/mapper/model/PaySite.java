package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 支払いサイト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class PaySite {
    /** プロジェクト */
    private java.util.List<Project> project;

    /** 支払いサイトID */
    private Integer paySiteId;

    /** 名称 */
    private String paySiteName;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
