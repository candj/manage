package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

/**
 * 請求
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Invoice {
    /** 請求詳細 */
    private java.util.List<InvoiceDetail> invoiceDetail;

    /** 請求ID */
    private Integer invoiceId;

    /** 注文 */
    private Ordering ordering;

    /** 注文ID */
    private Integer orderId;

    /** 請求番号 */
    private String invoiceNumber;

    /** 日付 */
    private org.joda.time.DateTime invoiceDate;

    /** 元会社 */
    private SendCompany sendCompany;

    /** 元会社ID */
    private Integer sendCompanyId;

    /** 先会社 */
    private ReceiveCompany receiveCompany;

    /** 先会社ID */
    private Integer receiveCompanyId;

    /** 開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 税抜き合計金額 */
    private Integer invoiceAmountWithoutTax;

    /** 消費税 */
    private Integer invoiceConsumptionTax;

    /** 税込み合計金額 */
    private Integer invoiceAmountWithTax;

    /** 納期 */
    private org.joda.time.DateTime invoiceDeliveryDate;

    /** ステータス */
    private String invoiceStatus;

    /** 支払状態 */
    private String invoicePayStatus;

    /** 削除FLAG */
    private boolean invoiceDeleteFlag;

    /** 交通費 */
    private Integer invoiceCommutingCost;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
