package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailTemplateDto {

    private Integer  id;

    private String title;

    private String content;

    private String link;

    private Integer code;

}
