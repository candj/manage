package com.candj.crm.controller;

import com.candj.crm.dto.CostDto;
import com.candj.crm.service.CostService;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 費用情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/cost")
@Controller
public class CostController {
    @Autowired
    CostService costService;

    /**
     * 費用を新規追加する。
     * @param costDto 費用
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(CostDto costDto, ModelMap model) {
        return new ModelAndView("cost/insert", model);
    }

    /**
     * 費用を新規追加(確認)する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid CostDto costDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/cost/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("cost/insertConfirm", model);
    }

    /**
     * 費用を新規追加(終了)する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid CostDto costDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/cost/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //費用を新規追加する。
        int ret = costService.insertSelective(costDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/cost/insertFinish", model);
    }

    /**
     * 費用を編集する。
     * @param costId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{costId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer costId, ModelMap model) {
        //プライマリーキーで費用を検索する。
        CostDto costDto = costService.selectByPrimaryKey(costId);
        model.addAttribute("costDto", costDto);
        return new ModelAndView("cost/edit", model);
    }

    /**
     * 費用を編集(確認)する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid CostDto costDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/cost/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("cost/editConfirm", model);
    }

    /**
     * 費用を編集(終了)する。
     * @param costDto 費用
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid CostDto costDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/cost/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで費用を更新する。
        int ret = costService.updateByPrimaryKeySelective(costDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("cost/editFinish", model);
    }

    /**
     * 費用一覧画面を表示する。
     * @param costDto 費用
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("costDto") CostDto costDto, ModelMap model) {
        DateTime yearMonth = new DateTime();
        model.addAttribute("yearMonth", yearMonth);
        return new ModelAndView("cost/list", model);
    }
}
