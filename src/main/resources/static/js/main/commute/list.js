

			var colNames = [
				"交通費ID",
				"スタッフ",
				"乗車日",
				"乗車駅",
				"降車駅",
				"金額(円)",
				"コメント",
				"添付ファイル",
				"処理状態ステータス",
				"ステータス",
			];
			var colModel = [
				{
					name: "commuteId",
					index: "commuteId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "appUser.userName",
					index: "appUser.userName",
					width: 100,
					search : false,
					align: "left",
				},
				{
                    name: "commuteDate",
                    index: "commuteDate",
                    width: 100,
                    search : false,
                    formatter : function(cellvalue, options, rowObject) {
                      if (cellvalue != null) {
                            var date = new Date(cellvalue);
                            return date.Format('yyyy-MM-dd');
                      } else {
                            return '';
                      }
                    },
                    align: "left",
                },
				{
					name: "commuteGetOnStation",
					index: "commuteGetOnStation",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "commuteGetOffStation",
					index: "commuteGetOffStation",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "commuteAmount",
					index: "commuteAmount",
					width: 100,
					search : false,

					align: "right",
				},
				{
					name: "commuteRemark",
					index: "commuteRemark",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "commuteAttachmentName",
					index: "commuteAttachmentName",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                      if(cellvalue != null && cellvalue != undefined){
                        var selectHtml = '<a href="/api/commuteRest/download/'
                                         + rowObject.commuteId
                                         + '">'
                                         + cellvalue
                                         + '</a>';
                      }else {
                        var selectHtml = "";
                      }
                        return selectHtml;
                    },
					align: "left",
				},
				{
					name: "commuteAdmitStatus",
					index: "commuteAdmitStatus",
					width: 100,
					search : false,
					formatter : function(cellvalue, options, rowObject) {
                       if (cellvalue === '00') {
                            return '未承認';
                       } else if(cellvalue === '01'){
                            return '承認済';
                       }else{
                             return cellvalue;
                       }
                    },
					editable : true,
					edittype : "select",
					editoptions : {value : "00:未承認;01:承認済"},
				},
				{
					name: "commuteStatus",
					index: "commuteStatus",
					width: 100,
					search : false,
					hidden : true,
					align: "right",
				},
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
	userName : $('#userName').val(),
};

var lastsel2;


var onSelectRow = function(id) {
      if (id && (id != lastsel2)) {
        jQuery('#grid').jqGrid('restoreRow', lastsel2);
        jQuery('#grid').jqGrid('editRow', id, true);
        lastsel2 = id;
      }
      else{
        jQuery('#grid').jqGrid('restoreRow', id);
      }
    };
var editurl = "/api/commuteRest/editStatus";

var shrinkToFit = true;

var gridComplete;

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, shrinkToFit, gridComplete, onSelectRow, editurl);
});

	