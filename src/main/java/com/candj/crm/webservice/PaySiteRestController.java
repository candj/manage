package com.candj.crm.webservice;

import com.candj.crm.dto.PaySiteDto;
import com.candj.crm.service.PaySiteService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支払いサイト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/paySiteRest")
@RestController
public class PaySiteRestController {
    @Autowired
    PaySiteService paySiteService;

    /**
     * 支払いサイトを新規追加する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    @RequestMapping(value = "{paySiteId}", method = RequestMethod.GET)
    public PaySiteDto get(@PathVariable Integer paySiteId) {
        //プライマリーキーで支払いサイトを検索する。
        PaySiteDto ret = paySiteService.selectByPrimaryKey(paySiteId);
        return ret;
    }

    /**
     * 支払いサイトを新規追加する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody PaySiteDto paySiteDto, Errors rrrors) {
        //支払いサイトを新規追加する。
        int ret = paySiteService.insertSelective(paySiteDto);
        return ret;
    }

    /**
     * 支払いサイトを変更する。
     * @param paySiteDto 支払いサイト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody PaySiteDto paySiteDto, Errors rrrors) {
        //プライマリーキーで支払いサイトを更新する。
        int ret = paySiteService.updateByPrimaryKeySelective(paySiteDto);
        return ret;
    }

    /**
     * 支払いサイトを新規追加する。
     * @param paySiteId 支払いサイトID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{paySiteId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer paySiteId) {
        //支払いサイトを削除する。
        int ret = paySiteService.deleteByPrimaryKey(paySiteId);
        return ret;
    }

    /**
     * 支払いサイト一覧画面を表示する。
     * @param paySiteDto 支払いサイト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<PaySiteDto> list(PaySiteDto paySiteDto, JqgridPageReq pageReq) {
        Page<PaySiteDto> page = PageUtil.startPage(pageReq);
        //条件で支払いサイトを検索する。（連携情報含む）
        List<PaySiteDto> ret = paySiteService.selectByExample(paySiteDto);
        return PageUtil.resp(page);
    }

    /**
     * 支払いサイト一覧画面を表示する。
     * @param paySiteDto 支払いサイト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<PaySiteDto> listAll(PaySiteDto paySiteDto, JqgridPageReq pageReq) {
        Page<PaySiteDto> page = PageUtil.startPage(pageReq);
        //条件で支払いサイトを検索する。（連携情報含む）
        List<PaySiteDto> ret = paySiteService.selectAllByExample(paySiteDto);
        return PageUtil.resp(page);
    }
}
