package com.candj.crm.controller;

import com.candj.crm.dto.AppUserDto;
import com.candj.crm.dto.InvoiceDetailDto;
import com.candj.crm.dto.InvoiceDto;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.service.AppUserService;
import com.candj.crm.service.InvoiceDetailService;
import java.util.List;
import javax.validation.Valid;

import com.candj.crm.service.InvoiceDetailTemporaryService;
import com.candj.crm.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 請求詳細情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/invoiceDetail")
@Controller
public class InvoiceDetailController {
    @Autowired
    InvoiceDetailService invoiceDetailService;

    @Autowired
    InvoiceDetailTemporaryService invoiceDetailTemporaryService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    AppUserService appUserService;

    /**
     * 請求詳細を新規追加する。
     * @param invoiceDetailDto 請求詳細
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/{invoiceId}", method = RequestMethod.GET)
    public ModelAndView insert(InvoiceDetailDto invoiceDetailDto, ModelMap model) {
        InvoiceDto invoiceDto = invoiceService.selectByPrimaryKey(invoiceDetailDto.getInvoiceId());
        invoiceDetailDto.setWorkStartDate(invoiceDto.getWorkStartDate());
        invoiceDetailDto.setWorkEndDate(invoiceDto.getWorkEndDate());
        return new ModelAndView("invoiceDetail/insert", model);
    }

    /**
     * 請求詳細を新規追加(確認)する。
     * @param invoiceDetailDto 請求詳細
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid InvoiceDetailDto invoiceDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoiceDetail/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("invoiceDetail/insertConfirm", model);
    }

    /**
     * 請求詳細を新規追加(終了)する。
     * @param invoiceDetailDto 請求詳細
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid InvoiceDetailDto invoiceDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoiceDetail/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //請求詳細を新規追加する。
        int ret = invoiceDetailService.insertSelective(invoiceDetailDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/invoiceDetail/insertFinish", model);
    }

    /**
     * 請求詳細を編集する。
     * @param invoiceDetailId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{invoiceDetailId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer invoiceDetailId, ModelMap model) {
        //プライマリーキーで請求詳細を検索する。
        InvoiceDetailDto invoiceDetailDto = invoiceDetailService.selectAllByPrimaryKey(invoiceDetailId).get(0);
        model.addAttribute("invoiceDetailDto", invoiceDetailDto);
        return new ModelAndView("invoiceDetail/edit", model);
    }

    /**
     * 請求詳細を編集する。
     * @param invoiceDetailId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/editForInsert/{invoiceDetailId}", method = RequestMethod.GET)
    public ModelAndView editForInsert(@PathVariable Integer invoiceDetailId, ModelMap model) {
        //プライマリーキーで請求詳細を検索する。
        InvoiceDetailDto invoiceDetailDto = invoiceDetailTemporaryService.selectByPrimaryKey(invoiceDetailId);
        AppUserDto appUserDto = appUserService.selectByPrimaryKey(invoiceDetailDto.getUserId());
        invoiceDetailDto.setAppUserDto(appUserDto);
        model.addAttribute("invoiceDetailDto", invoiceDetailDto);
        return new ModelAndView("invoiceDetail/editForInsert", model);
    }

    /**
     * 請求詳細を編集(確認)する。
     * @param invoiceDetailDto 請求詳細
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid InvoiceDetailDto invoiceDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoiceDetail/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("invoiceDetail/editConfirm", model);
    }

    /**
     * 請求詳細を編集(終了)する。
     * @param invoiceDetailDto 請求詳細
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid InvoiceDetailDto invoiceDetailDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/invoiceDetail/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで請求詳細を更新する。
        int ret = invoiceDetailService.updateByPrimaryKeySelective(invoiceDetailDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("invoiceDetail/editFinish", model);
    }

    /**
     * 請求詳細一覧画面を表示する。
     * @param invoiceDetailDto 請求詳細
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list/{invoiceId}", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("invoiceDetailDto") InvoiceDetailDto invoiceDetailDto, ModelMap model) {
        return new ModelAndView("invoiceDetail/list", model);
    }
}
