package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 給料
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class SalaryDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer salaryId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** 給料 */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 年月 */
    private org.joda.time.DateTime salaryMonth;

    /** 年月 */
    private String yearMonth;

    /** 基本給 */
    @Digits(integer=10, fraction=0)
    private Integer basePay;

    /** 残業代/営業費用/食費 */
    @Digits(integer=10, fraction=0)
    private Integer overtimePay;

    /** 交通費 */
    @Digits(integer=10, fraction=0)
    private Integer commutingCost;

    /** 社会保険料(会社分) */
    @Digits(integer=10, fraction=0)
    private Integer socialInsuranceCompany;

    /** 社会保険料(個人分) */
    @Digits(integer=10, fraction=0)
    private Integer socialInsurancePersonal;

    /** 住宅補助金、（その他） */
    @Digits(integer=10, fraction=0)
    private Integer housingSubsidies;

    /** 労働保険料(会社分) */
    @Digits(integer=10, fraction=0)
    private Integer labourInsuranceCompany;

    /** 労働保険料(個人分) */
    @Digits(integer=10, fraction=0)
    private Integer labourInsurancePersonal;

    /** ボーナス */
    @Digits(integer=10, fraction=0)
    private Integer bonus;

    /** 個人所得税 */
    @Digits(integer=10, fraction=0)
    private Integer personalIncomeTax;

    /** 実給 */
    @Digits(integer=10, fraction=0)
    private Integer salaryAmount;

    /** 扶養人数 */
    @Digits(integer=10, fraction=0)
    private Integer bringUpNumber;

    /** 支払い予定日付 */
    private org.joda.time.DateTime planPayDate;

    /** 支払い日付 */
    private org.joda.time.DateTime payDate;

    /** ステータス */
    @Digits(integer=3, fraction=0)
    private Byte status;

    /** 備考 */
    @Length(max=255)
    private String remark;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
