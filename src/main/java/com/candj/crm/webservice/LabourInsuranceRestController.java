package com.candj.crm.webservice;

import com.candj.crm.Utils.DateTimeUtil;
import com.candj.crm.dto.LabourInsuranceDto;
import com.candj.crm.service.LabourInsuranceService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 税率、保険料率情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/labourInsuranceRest")
@RestController
public class LabourInsuranceRestController {

    @Autowired
    LabourInsuranceService labourInsuranceService;

    /**
     * 税率、保険料率を新規追加する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    @RequestMapping(value = "{labourInsuranceId}", method = RequestMethod.GET)
    public LabourInsuranceDto get(@PathVariable Integer labourInsuranceId) {
        //プライマリーキーで税率、保険料率を検索する。
        LabourInsuranceDto ret = labourInsuranceService.selectByPrimaryKey(labourInsuranceId);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param labourInsuranceDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody LabourInsuranceDto labourInsuranceDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(labourInsuranceDto.getYearMonth());
        labourInsuranceDto.setLabourInsuranceStartMonth(month);
        //税率、保険料率を新規追加する。
        int ret = labourInsuranceService.insertSelective(labourInsuranceDto);
        labourInsuranceService.setLabourInsuranceEndMonth(labourInsuranceDto);
        return ret;
    }

    /**
     * 税率、保険料率を変更する。
     * @param labourInsuranceDto 税率、保険料率
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody LabourInsuranceDto labourInsuranceDto, Errors rrrors) {
        DateTime month = DateTimeUtil.StringToDateTime(labourInsuranceDto.getYearMonth());
        labourInsuranceDto.setLabourInsuranceStartMonth(month);
        //プライマリーキーで税率、保険料率を更新する。
        int ret = labourInsuranceService.updateByPrimaryKeySelective(labourInsuranceDto);
        return ret;
    }

    /**
     * 税率、保険料率を新規追加する。
     * @param labourInsuranceId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{labourInsuranceId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer labourInsuranceId) {
        //税率、保険料率を削除する。
        int ret = labourInsuranceService.deleteByPrimaryKey(labourInsuranceId);
        return ret;
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param labourInsuranceDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<LabourInsuranceDto> listAll(LabourInsuranceDto labourInsuranceDto,
                                    @RequestParam(name = "yearMonth", required = false) YearMonth yearMonth,
                                    JqgridPageReq pageReq) {
        //条件で税率、保険料率を検索する。（連携情報含む）
        Page<LabourInsuranceDto> page = PageUtil.startPage(pageReq);
        DateTime month = DateTimeUtil.yearMonthToDateTime(yearMonth);
        labourInsuranceDto.setLabourInsuranceStartMonth(month);
        labourInsuranceDto.setLabourInsuranceEndMonth(month.plusMonths(1).minusSeconds(1));
        List<LabourInsuranceDto> ret = labourInsuranceService.selectAllByExample(labourInsuranceDto);
        return PageUtil.resp(page);
    }

    /**
     * 税率、保険料率一覧画面を表示する。
     * @param labourInsuranceDto 税率、保険料率
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<LabourInsuranceDto> list(LabourInsuranceDto labourInsuranceDto, JqgridPageReq pageReq) {
        Page<LabourInsuranceDto> page = PageUtil.startPage(pageReq);
        //条件で税率、保険料率を検索する。（連携情報含む）
        List<LabourInsuranceDto> ret = labourInsuranceService.selectByExample(labourInsuranceDto);
        return PageUtil.resp(page);
    }
}
