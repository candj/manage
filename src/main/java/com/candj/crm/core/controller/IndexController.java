package com.candj.crm.core.controller;

import com.candj.crm.core.security.AppAuthenticationUser;
import com.candj.crm.mapper.model.AppUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * メニュー画面の遷移を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */

@RequestMapping(value = "")
@Controller
public class IndexController {

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView list() {
        return new ModelAndView("signin");
    }

	@RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView main(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AppAuthenticationUser appAuthenticationUser = (AppAuthenticationUser) principal;
        AppUser appUser = appAuthenticationUser.getAppUser();
        model.addAttribute("user", appUser);
        return new ModelAndView("main");
    }
}


