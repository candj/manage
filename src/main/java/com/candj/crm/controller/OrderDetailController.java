package com.candj.crm.controller;

import com.candj.crm.dto.OfferDetailDto;
import com.candj.crm.dto.OrderingDto;
import com.candj.crm.service.OrderService;
import com.candj.crm.service.OrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping(value = "/ordering/edit/orderDetail")
@Controller
public class OrderDetailController {
    @Autowired
    OrderingService orderingService;

    /**
     * 注文詳細 一覧画面を表示する。
     * @param offerDetailDto 見積詳細
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list/{orderId}", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("offerDetailDto") OfferDetailDto offerDetailDto,
                             @PathVariable Integer orderId, ModelMap model) {
        OrderingDto orderingDto = orderingService.selectByPrimaryKey(orderId);
        offerDetailDto.setOfferId(orderingDto.getOfferId());
        return new ModelAndView("orderDetail/list", model);
    }

}
