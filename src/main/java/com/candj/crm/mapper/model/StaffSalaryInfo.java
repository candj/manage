package com.candj.crm.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;

/**
 * スタッフ基本給料
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffSalaryInfo {
    /** ID */
    private Integer staffSalaryInfoId;

    /** ユーザー */
    private AppUser appUser;

    /** 担当者ID */
    private Integer userId;

    /** 基本給 */
    private Integer salaryPay;

    /** 扶養人数 */
    private Integer SalaryBringUpNumber;

    /** 社会保険入りFLAG */
    private boolean socialInsuranceFlag;

    /** スタッフ現在地 */
    private String userLocation;

    /** 适用開始期日 */
    private org.joda.time.DateTime salaryStartDate;

    /** 适用終了期日 */
    private org.joda.time.DateTime salaryEndDate;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
