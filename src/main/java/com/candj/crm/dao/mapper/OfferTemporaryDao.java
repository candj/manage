package com.candj.crm.dao.mapper;

import com.candj.crm.mapper.model.Offer;
import com.candj.webpower.web.core.model.WhereCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 見積情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface OfferTemporaryDao {
    /**
     * プライマリーキーで見積を検索する。
     * @param offerId 見積 ID
     * @return 結果
     */
    Offer selectByPrimaryKey(Integer offerId);

    /**
     * プライマリーキーで見積検索する。（連携情報含む）
     * @param offerId 見積 ID
     * @return 結果
     */
    List<Offer> selectAllByPrimaryKey(Integer offerId);

    /**
     * 条件で見積を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Offer> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で見積を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Offer> selectByMyExample(@Param("example") WhereCondition example);

    /**
     * 条件で見積を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Offer> selectByExample(@Param("example") WhereCondition example);

    /**
     * 見積を新規追加する。
     * @param record 見積
     * @return 結果
     */
    int insertSelective(Offer record);

    /**
     * 見積を新規追加する。
     * @param record 見積
     * @return 結果
     */
    int insert(Offer record);

    /**
     * プライマリーキーで見積を更新する。
     * @param record 見積
     * @return 結果
     */
    int updateByPrimaryKey(Offer record);

    /**
     * プライマリーキーで見積を更新する。
     * @param record 見積
     * @return 結果
     */
    int updateByPrimaryKeySelective(Offer record);

    /**
     * 見積を更新する。
     * @param record 見積
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Offer record, @Param("example") WhereCondition example);

    /**
     * 見積を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Offer example);

    /**
     * 見積を削除する。
     * @param offerId 見積 ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer offerId);

    /**
     * プロジェクトIDで見積を検索する。
     * @param projectId 見積 ID
     * @return 結果
     */
    List<Offer> selectByProjectId(Integer projectId);
}
