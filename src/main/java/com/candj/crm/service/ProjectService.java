package com.candj.crm.service;

import com.candj.crm.dao.mapper.ProjectDao;
import com.candj.crm.dto.OfferDto;
import com.candj.crm.dto.ProjectDto;
import com.candj.crm.mapper.model.Project;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * プロジェクト情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 */
@Service
@Transactional
public class ProjectService {
    @Autowired
    ProjectDao projectDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでプロジェクトを検索する。
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    public ProjectDto selectByPrimaryKey(Integer projectId) {
        //プライマリーキーでプロジェクトを検索する。
        Project ret = projectDao.selectByPrimaryKey(projectId);
        return DozerHelper.map(mapper, ret, ProjectDto.class);
    }

    /**
     * プライマリーキーでプロジェクトを検索する。（連携情報含む）
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    public List<ProjectDto> selectAllByPrimaryKey(Integer projectId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Project> ret = projectDao.selectAllByPrimaryKey(projectId);
        return DozerHelper.mapList(mapper, ret, ProjectDto.class);
    }

    /**
     * プライマリーキーでプロジェクトを検索する。（連携情報含む）
     *
     * @param offerList プロジェクトID
     * @return 結果
     */
    public List<ProjectDto> selectAllByPrimaryKey(List<OfferDto> offerList) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Project> projectList = new ArrayList<>();
        for (OfferDto offer : offerList) {
            Project project = projectDao.selectByPrimaryKey(offer.getProjectId());
            projectList.add(project);
        }
        return DozerHelper.mapList(mapper, projectList, ProjectDto.class);
    }


    /**
     * 条件でプロジェクトを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProjectDto> selectByExample(ProjectDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("project.project_id", dto.getProjectId())
                    .andEqualTo("project.project_name", dto.getProjectName())
                    .andEqualTo("project.programming_language", dto.getProgrammingLanguage())
                    .andEqualTo("project.project_database", dto.getProjectDatabase())
                    .andEqualTo("project.program_size", dto.getProgramSize())
                    .andEqualTo("project.project_description", dto.getProjectDescription())
                    .andEqualTo("project.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("project.receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("project.project_status", dto.getProjectStatus());
        }
        //条件でプロジェクトを検索する。（連携情報含む）)
        List<Project> ret = projectDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, ProjectDto.class);
    }

    /**
     * 条件でプロジェクトを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProjectDto> selectAllByExample(ProjectDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("project.project_id", dto.getProjectId())
                    .andLike("project.project_name", dto.getProjectName())
                    .andEqualTo("project.programming_language", dto.getProgrammingLanguage())
                    .andEqualTo("project.project_database", dto.getProjectDatabase())
                    .andEqualTo("project.project_description", dto.getProjectDescription())
                    .andEqualTo("project.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("project.receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("project.project_status", dto.getProjectStatus())
                    .andEqualTo("project.project_delete_flag", 1)
                    .andLessThanOrEqualTo("project.project_start_date", dto.getProjectEndDate())
                    .andGreaterThanOrEqualTo("project.project_end_date", dto.getProjectStartDate());
        }
        //条件でプロジェクトを検索する。（連携情報含む）)
        List<Project> ret = projectDao.selectAllByExample(whereCondition);
        ret.forEach(p -> {
            if (p.getOffer().get(0).getOfferId() == null) {
                p.setHaveOfferFlag(false);
            } else {
                p.setHaveOfferFlag(true);
            }
        });
        return DozerHelper.mapList(mapper, ret, ProjectDto.class);
    }

    /**
     * 条件でプロジェクトを検索する。（連携情報含む）
     *
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProjectDto> selectByMyExample(ProjectDto dto, Integer userId) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("project.project_id", dto.getProjectId())
                    .andLike("project.project_name", dto.getProjectName())
                    .andEqualTo("project.programming_language", dto.getProgrammingLanguage())
                    .andEqualTo("project.project_database", dto.getProjectDatabase())
                    .andEqualTo("project.program_size", dto.getProgramSize())
                    .andEqualTo("project.project_description", dto.getProjectDescription())
                    .andEqualTo("project.send_company_id", dto.getSendCompanyId())
                    .andEqualTo("project.receive_company_id", dto.getReceiveCompanyId())
                    .andEqualTo("project.project_status", dto.getProjectStatus())
                    .andEqualTo("project.project_delete_flag", 1)
                    .andLessThanOrEqualTo("project.project_start_date", dto.getProjectEndDate())
                    .andGreaterThanOrEqualTo("project.project_end_date", dto.getProjectStartDate())
                    .andEqualTo("offer_detail.user_id", userId);
        }
        //条件でプロジェクトを検索する。（連携情報含む）)
        List<Project> ret = projectDao.selectByMyExample(whereCondition);
        ret.forEach(p -> {
            if (p.getOffer().get(0).getOfferId() == null) {
                p.setHaveOfferFlag(false);
            } else {
                p.setHaveOfferFlag(true);
            }
        });
        return DozerHelper.mapList(mapper, ret, ProjectDto.class);
    }


    /**
     * プロジェクトを新規追加する。
     *
     * @param dto プロジェクト
     * @return 結果
     */
    public int insertSelective(ProjectDto dto) {
        Project project = mapper.map(dto, Project.class);
        //プロジェクトを新規追加する。
        int ret = projectDao.insertSelective(project);
        return ret;
    }

    /**
     * プロジェクトを新規追加する。
     *
     * @param dto プロジェクト
     * @return 結果
     */
    public int insert(ProjectDto dto) {
        Project project = mapper.map(dto, Project.class);
        //プロジェクトを新規追加する。
        int ret = projectDao.insert(project);
        return ret;
    }

    /**
     * プライマリーキーでプロジェクトを更新する。
     *
     * @param dto プロジェクト
     * @return 結果
     */
    public int updateByPrimaryKey(ProjectDto dto) {
        Project project = mapper.map(dto, Project.class);
        //プライマリーキーでプロジェクトを更新する。
        int ret = projectDao.updateByPrimaryKey(project);
        return ret;
    }

    /**
     * プライマリーキーでプロジェクトを更新する。
     *
     * @param dto プロジェクト
     * @return 結果
     */
    public int updateByPrimaryKeySelective(ProjectDto dto) {
        Project project = mapper.map(dto, Project.class);
        //プライマリーキーでプロジェクトを更新する。
        int ret = projectDao.updateByPrimaryKeySelective(project);
        return ret;
    }

    /**
     * プロジェクトを削除する。
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer projectId) {
        //プロジェクトを削除する。
        int ret = projectDao.deleteByPrimaryKey(projectId);
        return ret;
    }

    /**
     * プライマリーキーでプロジェクトを検索する。（連携情報含む）
     *
     * @param projectId プロジェクトID
     * @return 結果
     */
    public List<Project> selectProjectByPrimaryKey(Integer projectId) {
        //プライマリーキーでプロジェクト検索する。（連携情報含む）
        List<Project> ret = projectDao.selectAllByPrimaryKey(projectId);
        return ret;
    }
}
