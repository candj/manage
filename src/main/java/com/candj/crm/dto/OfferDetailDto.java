package com.candj.crm.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 見積詳細 
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class OfferDetailDto {
    /**  */
    @Digits(integer=10, fraction=0)
    private Integer offerDetailId;

    /** 見積ID */
    @Digits(integer=10, fraction=0)
    private Integer offerId;

    /** 見積  */
    private com.candj.crm.mapper.model.Offer offer;

    /** 作業開始期日 */
    private org.joda.time.DateTime workStartDate;

    /** 作業終了期日 */
    private org.joda.time.DateTime workEndDate;

    /** 作業員id */
    @Digits(integer=2, fraction=0)
    private Integer userType;

    /** 作業員id */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** 作業員id */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** 作業員  */
    private com.candj.crm.mapper.model.AppUser appUser;

    /** 作業員  */
    private AppUserDto appUserDto;

    /** 業務名 */
    @Length(max=128)
    private String workName;

    /** 単価 */
    @Digits(integer=10, fraction=0)
    private Integer amount;

    /** 工数 */
    @Digits(integer=8, fraction=2)
    private java.math.BigDecimal manHour;

    /** 合計金額 */
    @Digits(integer=10, fraction=0)
    private Integer totalAmount;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
