

			var colNames = [
				"ID",
				"摘要",
				"収入金额",
				"支出金额",
				"事務費の入金",
				"事務費の支出",
				"当月分費用",
				"コメント",
			];
			var colModel = [
				{
					name: "costId",
					index: "costId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "costSummary",
					index: "costSummary",
					width: 100,
					search : false,
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "left",
				},
				{
					name: "incomeAmount",
					index: "incomeAmount",
					width: 100,
					search : false,
					formatter : "integer",
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "right",
				},
				{
					name: "expendAmount",
					index: "expendAmount",
					width: 100,
					search : false,
					formatter : "integer",
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "right",
				},
				{
					name: "businessIncomeAmount",
					index: "businessIncomeAmount",
					width: 100,
					search : false,
					formatter : "integer",
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "right",
				},
				{
					name: "businessExpendAmount",
					index: "businessExpendAmount",
					width: 100,
					search : false,
					formatter : "integer",
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "right",
				},
				{
					name: "costTotalAmount",
					index: "costTotalAmount",
					width: 100,
					search : false,
					formatter : "integer",
					formatoptions : {
                                                            prefix : '$',
                                                            suffix : '円',
                                                            thousandsSeparator : ','
                                                        },
					align: "right",
				},
				{
					name: "remark",
					index: "remark",
					width: 100,
					search : false,
					align: "left",
				},
			];

var postData = {
	yearMonth : $('#yearMonth').val(),
};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});	
	