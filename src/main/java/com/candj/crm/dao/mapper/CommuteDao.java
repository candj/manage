package com.candj.crm.dao.mapper;

import com.candj.crm.dto.CommuteDurationDto;
import com.candj.crm.dto.WorkDurationDto;
import com.candj.crm.mapper.model.Commute;
import com.candj.crm.mapper.model.Work;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 交通費情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface CommuteDao {
    /**
     * プライマリーキーで交通費を検索する。
     * @param commuteId 交通費ID
     * @return 結果
     */
    Commute selectByPrimaryKey(Integer commuteId);

    /**
     * プライマリーキーで交通費検索する。（連携情報含む）
     * @param commuteId 交通費ID
     * @return 結果
     */
    List<Commute> selectAllByPrimaryKey(Integer commuteId);

    /**
     * 条件で交通費を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Commute> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で交通費を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Commute> selectByExample(@Param("example") WhereCondition example);

    /**
     * 交通費を新規追加する。
     * @param record 交通費
     * @return 結果
     */
    int insertSelective(Commute record);

    /**
     * 交通費を新規追加する。
     * @param record 交通費
     * @return 結果
     */
    int insert(Commute record);

    /**
     * プライマリーキーで交通費を更新する。
     * @param record 交通費
     * @return 結果
     */
    int updateByPrimaryKey(Commute record);

    /**
     * プライマリーキーで交通費を更新する。
     * @param record 交通費
     * @return 結果
     */
    int updateByPrimaryKeySelective(Commute record);

    /**
     * 交通費を更新する。
     * @param record 交通費
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Commute record, @Param("example") WhereCondition example);

    /**
     * 交通費を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Commute example);

    /**
     * 交通費を削除する。
     * @param commuteId 交通費ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer commuteId);

    /**
     * 条件で作業を検索する。（連携情報含む）)
     * @param record 検索条件
     * @return 結果
     */
    List<Commute> selectCommuteByStaffAndMonth(CommuteDurationDto record);
}
