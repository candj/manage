package com.candj.crm.controller;

import com.candj.crm.dto.UserRoleDto;
import com.candj.crm.service.UserRoleService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ユーザーロール情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/userRole")
@Controller
public class UserRoleController {
    @Autowired
    UserRoleService userRoleService;

    /**
     * ユーザーロールを新規追加する。
     * @param userRoleDto ユーザーロール
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(UserRoleDto userRoleDto, ModelMap model) {
        return new ModelAndView("userRole/insert", model);
    }

    /**
     * ユーザーロールを新規追加(確認)する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid UserRoleDto userRoleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/userRole/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("userRole/insertConfirm", model);
    }

    /**
     * ユーザーロールを新規追加(終了)する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid UserRoleDto userRoleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/userRole/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //ユーザーロールを新規追加する。
        int ret = userRoleService.insertSelective(userRoleDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/userRole/insertFinish", model);
    }

    /**
     * ユーザーロールを編集する。
     * @param userRoleId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{userRoleId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer userRoleId, ModelMap model) {
        //プライマリーキーでユーザーロールを検索する。
        UserRoleDto serRoleDto = userRoleService.selectByPrimaryKey(userRoleId);
        model.addAttribute("serRoleDto", serRoleDto);
        return new ModelAndView("userRole/edit", model);
    }

    /**
     * ユーザーロールを編集(確認)する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid UserRoleDto userRoleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/userRole/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("userRole/editConfirm", model);
    }

    /**
     * ユーザーロールを編集(終了)する。
     * @param userRoleDto ユーザーロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid UserRoleDto userRoleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/userRole/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでユーザーロールを更新する。
        int ret = userRoleService.updateByPrimaryKeySelective(userRoleDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("userRole/editFinish", model);
    }

    /**
     * ユーザーロール一覧画面を表示する。
     * @param userRoleDto ユーザーロール
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("userRoleDto") UserRoleDto userRoleDto, ModelMap model) {
        return new ModelAndView("userRole/list", model);
    }
}
