package com.candj.crm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;

import javax.validation.constraints.Digits;

/**
 * 税率
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class LabourInsuranceDto {

    /** 税率ID */
    @Digits(integer=10, fraction=0)
    private Integer labourInsuranceId;

    /** 税率(個人分)(%) */
    @Digits(integer=3, fraction=4)
    private java.math.BigDecimal labourInsuranceRatePersonal;

    /** 税率(会社分)(%) */
    @Digits(integer=3, fraction=4)
    private java.math.BigDecimal labourInsuranceRateCompany;

    /** 適用年月開始時間 */
    private String yearMonth;

    /** 適用年月開始時間 */
    private DateTime labourInsuranceStartMonth;

    /** 適用年月終了時間 */
    private DateTime labourInsuranceEndMonth;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private DateTime createTime;

    /** 更新日付 */
    private DateTime updateTime;
}
