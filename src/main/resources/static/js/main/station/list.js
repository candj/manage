

			var colNames = [
				"车站ID",
				"车站名称",
				"削除フラグ",
			];
			var colModel = [
				{
					name: "stationId",
					index: "stationId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "stationName",
					index: "stationName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "deleteFlg",
					index: "deleteFlg",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	