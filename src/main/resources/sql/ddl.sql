/*
 Navicat MySQL Data Transfer

 Source Server         : cfengsx
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : manage

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 10/05/2021 08:49:28
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS `social_insurance_temporary`;
DROP TABLE IF EXISTS `social_insurance`;
DROP TABLE IF EXISTS `personal_income_tax_temporary`;
DROP TABLE IF EXISTS `personal_income_tax`;
DROP TABLE IF EXISTS `labour_insurance`;
DROP TABLE IF EXISTS `tax`;
DROP TABLE IF EXISTS `cost`;
DROP TABLE IF EXISTS `salary`;
DROP TABLE IF EXISTS `commute`;
DROP TABLE IF EXISTS `work`;
DROP TABLE IF EXISTS `invoice_detail_temporary`;
DROP TABLE IF EXISTS `invoice_detail`;
DROP TABLE IF EXISTS `invoice`;
DROP TABLE IF EXISTS `ordering`;
DROP TABLE IF EXISTS `offer_detail_temporary`;
DROP TABLE IF EXISTS `offer_detail`;
DROP TABLE IF EXISTS `offer`;
DROP TABLE IF EXISTS `project`;
DROP TABLE IF EXISTS `pay_site`;
DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `staff_salary_info`;
DROP TABLE IF EXISTS `staff_project_info`;
DROP TABLE IF EXISTS `app_user`;
DROP TABLE IF EXISTS `company`;

-- ----------------------------
-- Table structure for company
-- ----------------------------

CREATE TABLE `company`  (
  `company_id` int NOT NULL AUTO_INCREMENT COMMENT '会社ID',
  `company_type` tinyint NOT NULL COMMENT '会社区分',
  `company_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会社名',
  `company_name_abbr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会社名（略称）',
  `company_adress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '住所',
  `company_post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '郵便番号',
  `company_telephone_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '電話番号',
  `company_fax_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FAX番号',
  `company_delete_flag` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `company_bank` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '銀行',
  `company_bank_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '銀行支店名',
  `company_bank_account_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '預金種類',
  `company_bank_account_number` int NULL DEFAULT NULL COMMENT '口座番号',
  `company_bank_account_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '口座名義',
  `salary_pay_time` tinyint NULL DEFAULT NULL COMMENT '給料支払日付',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '銀行店名',
  PRIMARY KEY (`company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会社' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES (1, 0, '株式会社C&J', 'C&J', '東京都中央区新川2-22-3 笹尾ビル3F', '104-0033', '03-6280-3268', '03-6280-3208', 1, '三菱UFJ銀行', '川崎支店', '普通預金', 999327, 'カ）シーアンドジエイ', 0, NULL, 10, NULL, '2021-05-12 09:33:30');

-- ----------------------------
-- Table structure for app_user
-- ----------------------------

CREATE TABLE `app_user`  (
  `user_id` int NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID',
  `company_id` int NULL DEFAULT NULL COMMENT '会社ID',
  `user_type` int NOT NULL COMMENT 'タイプ',
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名前',
  `user_sex` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性別',
  `birth_date` datetime(0) NULL DEFAULT NULL COMMENT '生年月日',
  `user_department` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部門',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'イーメールアドレス',
  `user_telephone_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '電話番号',
  `employment_date` datetime(0) NULL DEFAULT NULL COMMENT '入职日付',
  `leaving_date` datetime(0) NULL DEFAULT NULL COMMENT '退社日付',
  `employment_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '在職状態',
  `user_base_salary` int NULL DEFAULT NULL COMMENT '基本給料',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'パスワード',
  `guest_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ゲスト種類',
  `user_delete_flag` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `FK_COMPANY_ID_2`(`company_id`) USING BTREE,
  CONSTRAINT `FK_COMPANY_ID_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ユーザー' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for staff_project_info
-- ----------------------------

CREATE TABLE `staff_project_info`  (
  `staff_project_info_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int NOT NULL COMMENT '担当者ID',
  `project_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'プロジェクト名',
  `staff_in_project` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'プロジェクトにあり',
  `project_start_date` datetime(0) NOT NULL COMMENT '開始期日',
  `project_end_date` datetime(0) NULL DEFAULT NULL COMMENT '終了期日',
  `offer_id` int NULL DEFAULT NULL COMMENT '見積ID',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`staff_project_info_id`) USING BTREE,
  INDEX `FK_STAFF_ID_7`(`user_id`) USING BTREE,
  CONSTRAINT `FK_STAFF_ID_7` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'スタッフプロジェクト情報' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for staff_salary_info
-- ----------------------------

CREATE TABLE `staff_salary_info`  (
  `staff_salary_info_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int NOT NULL COMMENT '担当者ID',
  `salary_pay` int NOT NULL COMMENT '基本給',
  `user_location` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'スタッフ現在地',
  `social_insurance_flag` tinyint(1) NULL DEFAULT NULL COMMENT '社会保険入りFLAG',
  `salary_bring_up_number` int NULL DEFAULT NULL COMMENT '扶養人数',
  `salary_start_date` datetime(0) NOT NULL COMMENT '适用開始期日',
  `salary_end_date` datetime(0) NULL DEFAULT NULL COMMENT '适用終了期日',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`staff_salary_info_id`) USING BTREE,
  INDEX `FK_STAFF_ID_8`(`user_id`) USING BTREE,
  CONSTRAINT `FK_STAFF_ID_8` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'スタッフ基本給料' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for role
-- ----------------------------

CREATE TABLE `role`  (
  `role_id` int NOT NULL AUTO_INCREMENT COMMENT 'ロールID',
  `role_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ロールコード',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ロール名',
  `role_status` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ロール' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------

CREATE TABLE `user_role`  (
  `user_role_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int NOT NULL COMMENT 'ユーザーID',
  `role_id` int NOT NULL COMMENT 'ロールID',
  `user_role_status` tinyint NULL DEFAULT NULL COMMENT '削除FLAG',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`user_role_id`) USING BTREE,
  INDEX `FK_ROLE_ID`(`role_id`) USING BTREE,
  INDEX `FK_USER_ID`(`user_id`) USING BTREE,
  CONSTRAINT `FK_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ユーザーロール' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for pay_site
-- ----------------------------

CREATE TABLE `pay_site`  (
  `pay_site_id` int NOT NULL AUTO_INCREMENT COMMENT '支払いサイトID',
  `pay_site_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`pay_site_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支払いサイト' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pay_site
-- ----------------------------
INSERT INTO `pay_site` VALUES (1, '翌月末日お支払い', NULL, NULL, NULL, NULL);
INSERT INTO `pay_site` VALUES (2, '翌々月末日お支払い', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for offer
-- ----------------------------

CREATE TABLE `offer`  (
  `offer_id` int NOT NULL AUTO_INCREMENT COMMENT '見積 ID',
  `offer_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '見積番号',
  `offer_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '種類',
  `date` datetime(0) NOT NULL COMMENT '日付',
  `work_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '業務名',
  `work_time_lower_limit` int NULL DEFAULT NULL COMMENT '作業時間基準（下限）',
  `work_time_upper_limit` int NULL DEFAULT NULL COMMENT '作業時間基準（上限）',
  `work_time_calculate_standard` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作業時間精算標準',
  `work_start_date` datetime(0) NOT NULL COMMENT '作業開始期日',
  `work_end_date` datetime(0) NOT NULL COMMENT '作業終了期日',
  `send_company_id` int NOT NULL COMMENT '元会社ID',
  `receive_company_id` int NOT NULL COMMENT '先会社ID',
  `work_place` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '納入場所',
  `offer_period_of_validity` int NOT NULL COMMENT '有効期限',
  `offer_delivery_date` datetime(0) NOT NULL COMMENT '納期',
  `pay_site_id` int NOT NULL COMMENT '支払いサイトID',
  `offer_amount_without_tax` int NULL DEFAULT NULL COMMENT '税抜き合計金額',
  `offer_consumption_tax` int NULL DEFAULT NULL COMMENT '消費税',
  `offer_total_amount` int NULL DEFAULT NULL COMMENT '税込み合計金額',
  `after_payment_pattern` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '精算方法',
  `offer_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '見積ステータス',
  `offer_delete_flag` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `project_description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'プロジェクト摘要',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`offer_id`) USING BTREE,
  INDEX `FK_PAY_SITE_ID_1`(`pay_site_id`) USING BTREE,
  INDEX `FK_COMPANY_ID_3`(`receive_company_id`) USING BTREE,
  INDEX `FK_COMPANY_ID_4`(`send_company_id`) USING BTREE,
  CONSTRAINT `FK_COMPANY_ID_3` FOREIGN KEY (`receive_company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_COMPANY_ID_4` FOREIGN KEY (`send_company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_PAY_SITE_ID_1` FOREIGN KEY (`pay_site_id`) REFERENCES `pay_site` (`pay_site_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '見積' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for offer_detail
-- ----------------------------

CREATE TABLE `offer_detail`  (
  `offer_detail_id` int NOT NULL AUTO_INCREMENT COMMENT '見積詳細ID',
  `offer_id` int NOT NULL COMMENT '見積ID',
  `work_start_date` datetime(0) NOT NULL COMMENT '作業開始期日',
  `work_end_date` datetime(0) NOT NULL COMMENT '作業終了期日',
  `user_id` int NOT NULL COMMENT '作業員id',
  `work_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名',
  `amount` int NOT NULL COMMENT '単価',
  `man_hour` decimal(10, 2) NOT NULL COMMENT '工数',
  `total_amount` int NULL DEFAULT NULL COMMENT '合計金額',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`offer_detail_id`) USING BTREE,
  INDEX `FK_STAFF_ID_3`(`user_id`) USING BTREE,
  INDEX `FK_OFFER_ID_1`(`offer_id`) USING BTREE,
  CONSTRAINT `FK_OFFER_ID_1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`offer_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_STAFF_ID_3` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '見積詳細 ' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for offer_detail_temporary
-- ----------------------------

CREATE TABLE `offer_detail_temporary`  (
  `offer_detail_id` int NOT NULL AUTO_INCREMENT COMMENT '見積詳細ID',
  `offer_id` int NULL DEFAULT NULL COMMENT '見積ID',
  `work_start_date` datetime(0) NOT NULL COMMENT '作業開始期日',
  `work_end_date` datetime(0) NOT NULL COMMENT '作業終了期日',
  `user_id` int NOT NULL COMMENT '作業員id',
  `work_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名',
  `amount` int NOT NULL COMMENT '単価',
  `man_hour` decimal(10, 2) NOT NULL COMMENT '工数',
  `total_amount` int NULL DEFAULT NULL COMMENT '合計金額',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`offer_detail_id`) USING BTREE,
  INDEX `FK_STAFF_ID_3`(`user_id`) USING BTREE,
  INDEX `FK_OFFER_ID_1`(`offer_id`) USING BTREE,
  CONSTRAINT `offer_detail_temporary_ibfk_1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`offer_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `offer_detail_temporary_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '見積詳細 ' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for ordering
-- ----------------------------

CREATE TABLE `ordering`  (
  `order_id` int NOT NULL AUTO_INCREMENT COMMENT '注文ID',
  `offer_id` int NOT NULL COMMENT '見積ID',
  `order_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注文番号',
  `order_date` datetime(0) NOT NULL COMMENT '日付',
  `order_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ステータス',
  `order_delete_flag` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `FK_OFFER_ID_2`(`offer_id`) USING BTREE,
  CONSTRAINT `FK_OFFER_ID_2` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`offer_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '注文' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for invoice
-- ----------------------------

CREATE TABLE `invoice`  (
  `invoice_id` int NOT NULL AUTO_INCREMENT COMMENT '請求ID',
  `order_id` int NOT NULL COMMENT '注文ID',
  `invoice_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '請求番号',
  `invoice_date` datetime(0) NOT NULL COMMENT '日付',
  `work_start_date` datetime(0) NULL DEFAULT NULL COMMENT '開始期日',
  `work_end_date` datetime(0) NULL DEFAULT NULL COMMENT '終了期日',
  `invoice_amount_without_tax` int NULL DEFAULT NULL COMMENT '税抜き合計金額',
  `invoice_consumption_tax` int NULL DEFAULT NULL COMMENT '消費税',
  `invoice_amount_with_tax` int NULL DEFAULT NULL COMMENT '税込み合計金額',
  `invoice_delivery_date` datetime(0) NOT NULL COMMENT '支払期限日',
  `invoice_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ステータス',
  `invoice_pay_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支払状態',
  `invoice_delete_flag` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `invoice_commuting_cost` int NULL DEFAULT NULL COMMENT '交通費',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`invoice_id`) USING BTREE,
  INDEX `FK_ORDER_ID_1`(`order_id`) USING BTREE,
  CONSTRAINT `FK_ORDER_ID_1` FOREIGN KEY (`order_id`) REFERENCES `ordering` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '請求' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for invoice_detail
-- ----------------------------

CREATE TABLE `invoice_detail`  (
  `invoice_detail_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `invoice_id` int NOT NULL COMMENT '請求ID',
  `work_start_date` datetime(0) NOT NULL COMMENT '開始期日',
  `work_end_date` datetime(0) NOT NULL COMMENT '終了期日',
  `user_id` int NOT NULL COMMENT '担当者ID',
  `person_month` decimal(10, 2) NULL DEFAULT NULL COMMENT '人月',
  `salary_person_month` int NOT NULL COMMENT '人月単価',
  `amount_month` int NULL DEFAULT NULL COMMENT '月額金額',
  `over_time_work` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '残業',
  `over_time` decimal(10, 1) NULL DEFAULT NULL COMMENT '超過/控除時間',
  `salary_extra` int NULL DEFAULT NULL COMMENT '超過単価',
  `salary_deduction` int NULL DEFAULT NULL COMMENT '控除単価',
  `amount_extra` int NULL DEFAULT NULL COMMENT '超過金額',
  `commuting_cost_amount` int NULL DEFAULT NULL COMMENT '交通費',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`invoice_detail_id`) USING BTREE,
  INDEX `FK_REQUEST_ID`(`invoice_id`) USING BTREE,
  INDEX `FK_STAFF_ID_4`(`user_id`) USING BTREE,
  CONSTRAINT `FK_INVOICE_ID_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_STAFF_ID_4` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 187 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '請求詳細' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for invoice_detail_temporary
-- ----------------------------

CREATE TABLE `invoice_detail_temporary`  (
  `invoice_detail_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `invoice_id` int NULL DEFAULT NULL COMMENT '請求ID',
  `work_start_date` datetime(0) NOT NULL COMMENT '開始期日',
  `work_end_date` datetime(0) NOT NULL COMMENT '終了期日',
  `user_id` int NOT NULL COMMENT '担当者ID',
  `person_month` decimal(10, 2) NULL DEFAULT NULL COMMENT '人月',
  `salary_person_month` int NOT NULL COMMENT '人月単価',
  `amount_month` int NULL DEFAULT NULL COMMENT '月額金額',
  `over_time_work` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '残業',
  `over_time` decimal(10, 1) NULL DEFAULT NULL COMMENT '超過/控除時間',
  `salary_extra` int NULL DEFAULT NULL COMMENT '超過単価',
  `salary_deduction` int NULL DEFAULT NULL COMMENT '控除単価',
  `amount_extra` int NULL DEFAULT NULL COMMENT '超過金額',
  `commuting_cost_amount` int NULL DEFAULT NULL COMMENT '交通費',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`invoice_detail_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 187 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;


-- ----------------------------
-- Table structure for work
-- ----------------------------

CREATE TABLE `work`  (
  `work_id` int NOT NULL AUTO_INCREMENT COMMENT '作業ID',
  `user_id` int NOT NULL COMMENT 'スタッフID',
  `work_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作業種類',
  `order_id` int NULL DEFAULT NULL COMMENT '注文ID',
  `work_month` datetime(0) NOT NULL COMMENT '年月',
  `work_start_date` datetime(0) NULL DEFAULT NULL COMMENT '作業開始期日',
  `work_end_date` datetime(0) NULL DEFAULT NULL COMMENT '作業終了期日',
  `unit_price` int NULL DEFAULT NULL COMMENT '単価',
  `work_man_hour` decimal(10, 2) NULL DEFAULT NULL COMMENT '工数',
  `work_hour` decimal(10, 1) NULL DEFAULT NULL COMMENT '作業時間',
  `work_attachment_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '勤務表パース',
  `work_attachment_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '勤務表ファイル名',
  `work_admit_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '処理状態',
  `work_status` tinyint(1) NULL DEFAULT NULL COMMENT '削除FLAG',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`work_id`) USING BTREE,
  INDEX `FK_STAFF_ID_5`(`user_id`) USING BTREE,
  INDEX `FK_ORDER_ID_2`(`order_id`) USING BTREE,
  CONSTRAINT `FK_ORDER_ID_2` FOREIGN KEY (`order_id`) REFERENCES `ordering` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_STAFF_ID_5` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作業' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for commute
-- ----------------------------

CREATE TABLE `commute`  (
  `commute_id` int NOT NULL AUTO_INCREMENT COMMENT '交通費ID',
  `user_id` int NOT NULL COMMENT 'スタッフID',
  `commute_apply_month` datetime(0) NOT NULL COMMENT '申請月',
  `commute_date` datetime(0) NULL DEFAULT NULL COMMENT '乗車日',
  `commute_get_on_station` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乗車駅',
  `commute_get_off_station` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '降車駅',
  `commute_amount` int NULL DEFAULT NULL COMMENT '金額',
  `commute_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `commute_attachment_path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件パース',
  `commute_attachment_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件ファイル名',
  `commute_admit_status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '処理状態',
  `commute_status` tinyint(1) NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`commute_id`) USING BTREE,
  INDEX `FK_STAFF_ID_6`(`user_id`) USING BTREE,
  CONSTRAINT `FK_STAFF_ID_6` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '交通費' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for salary
-- ----------------------------

CREATE TABLE `salary`  (
  `salary_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int NOT NULL COMMENT 'スタッフID',
  `salary_month` datetime(0) NOT NULL COMMENT '年月',
  `base_pay` int NOT NULL DEFAULT 0 COMMENT '基本給',
  `overtime_pay` int NULL DEFAULT 0 COMMENT '残業代/営業費用/食費',
  `commuting_cost` int NULL DEFAULT 0 COMMENT '交通費',
  `social_insurance_company` int NULL DEFAULT 0 COMMENT '社会保険料(会社分)',
  `social_insurance_personal` int NULL DEFAULT 0 COMMENT '社会保険料(個人分)',
  `housing_subsidies` int NULL DEFAULT 0 COMMENT '住宅補助金、（その他）',
  `labour_insurance_company` int NOT NULL DEFAULT 0 COMMENT '労働保険料(会社分)',
  `labour_insurance_personal` int NOT NULL DEFAULT 0 COMMENT '労働保険料(個人分)',
  `bonus` int NULL DEFAULT 0 COMMENT 'ボーナス',
  `personal_income_tax` int NULL DEFAULT 0 COMMENT '個人所得税',
  `salary_amount` int NULL DEFAULT 0 COMMENT '実給',
  `bring_up_number` int NULL DEFAULT NULL COMMENT '扶養人数',
  `plan_pay_date` datetime(0) NOT NULL COMMENT '支払い予定日付',
  `pay_date` datetime(0) NULL DEFAULT NULL COMMENT '支払い日付',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '支払FLAG',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '備考',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`salary_id`) USING BTREE,
  INDEX `FK_STAFF_ID_2`(`user_id`) USING BTREE,
  CONSTRAINT `FK_STAFF_ID_2` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '給料' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for cost
-- ----------------------------

CREATE TABLE `cost`  (
  `cost_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cost_date` datetime(0) NOT NULL COMMENT '費用日付',
  `cost_summary` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '摘要',
  `income_amount` int NULL DEFAULT NULL COMMENT '収入金额',
  `expend_amount` int NULL DEFAULT NULL COMMENT '支出金额',
  `business_income_amount` int NULL DEFAULT NULL COMMENT '事務費の入金',
  `business_expend_amount` int NULL DEFAULT NULL COMMENT '事務費の支出',
  `cost_total_amount` int NULL DEFAULT NULL COMMENT '当月分費用',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注釈',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`cost_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '費用' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tax
-- ----------------------------

CREATE TABLE `tax`  (
  `tax_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tax_name` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '税名',
  `tax_rate` decimal(5, 2) NOT NULL COMMENT '税率(%)',
  `tax_upper_limit` int NULL DEFAULT NULL COMMENT '所得区間上限',
  `tax_lower_limit` int NULL DEFAULT NULL COMMENT '所得区間下限',
  `tax_start_month` datetime(0) NOT NULL COMMENT '適用年月開始時間',
  `tax_end_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月終了時間',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`tax_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '税率、保険料率' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for labour_insurance
-- ----------------------------

CREATE TABLE `labour_insurance`  (
  `labour_insurance_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `labour_insurance_rate_personal` decimal(7, 2) NOT NULL COMMENT '税率(個人分)(%)',
  `labour_insurance_rate_company` decimal(7, 2) NOT NULL COMMENT '税率(会社分)(%)',
  `labour_insurance_start_month` datetime(0) NOT NULL COMMENT '適用年月開始時間',
  `labour_insurance_end_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月終了時間',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`labour_insurance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '税率、保険料率' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for personal_income_tax
-- ----------------------------

CREATE TABLE `personal_income_tax`  (
  `personal_income_tax_id` int NOT NULL AUTO_INCREMENT COMMENT '個人所得税ID',
  `personal_income_tax_start_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月開始時間',
  `personal_income_tax_end_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月終了時間',
  `personal_income_tax_excel_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'excel保存パス',
  `personal_income_tax_json_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json保存パス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`personal_income_tax_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for personal_income_tax_temporary
-- ----------------------------

CREATE TABLE `personal_income_tax_temporary`  (
  `personal_income_tax_id` int NOT NULL AUTO_INCREMENT COMMENT '個人所得税ID',
  `min_monthly_payment` int NULL DEFAULT NULL COMMENT '報酬月額下限',
  `max_monthly_payment` int NULL DEFAULT NULL COMMENT '報酬月額上限',
  `number_of_support_0` int NULL DEFAULT NULL COMMENT '扶養人数0',
  `number_of_support_1` int NULL DEFAULT NULL COMMENT '扶養人数1',
  `number_of_support_2` int NULL DEFAULT NULL COMMENT '扶養人数2',
  `number_of_support_3` int NULL DEFAULT NULL COMMENT '扶養人数3',
  `number_of_support_4` int NULL DEFAULT NULL COMMENT '扶養人数4',
  `number_of_support_5` int NULL DEFAULT NULL COMMENT '扶養人数5',
  `number_of_support_6` int NULL DEFAULT NULL COMMENT '扶養人数6',
  `number_of_support_7` int NULL DEFAULT NULL COMMENT '扶養人数7',
  `tax` decimal(10, 5) NULL DEFAULT NULL COMMENT '料率',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`personal_income_tax_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3255 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for social_insurance
-- ----------------------------

CREATE TABLE `social_insurance`  (
  `social_insurance_id` int NOT NULL AUTO_INCREMENT COMMENT '社会保険ID',
  `social_insurance_start_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月開始年月',
  `social_insurance_end_month` datetime(0) NULL DEFAULT NULL COMMENT '適用年月終了年月',
  `social_insurance_excel_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'excel保存パス',
  `social_insurance_json_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json保存パス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`social_insurance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for social_insurance_temporary
-- ----------------------------

CREATE TABLE `social_insurance_temporary`  (
  `social_insurance_id` int NOT NULL AUTO_INCREMENT COMMENT '社会保険ID',
  `grade` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '等級',
  `monthly` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月額',
  `minMonthlyPayment` int NULL DEFAULT NULL COMMENT '報酬月額下限',
  `maxMonthlyPayment` int NULL DEFAULT NULL COMMENT '報酬月額上限',
  `healthInsuranceFull` decimal(10, 4) NOT NULL COMMENT '健康保険（全額）',
  `healthInsuranceHalf` decimal(10, 4) NOT NULL COMMENT '健康保険（折半額）',
  `healthAndNursingCareFull` decimal(10, 4) NOT NULL COMMENT '健康保険＋介護保険（全額）',
  `healthAndNursingCareHalf` decimal(10, 4) NOT NULL COMMENT '健康保険＋介護保険（折半額）',
  `welfarePensionFull` decimal(10, 4) NOT NULL COMMENT '厚生年金保険（全額）',
  `welfarePensionHalf` decimal(10, 4) NOT NULL COMMENT '厚生年金保険（折半額）',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`social_insurance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 326 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
